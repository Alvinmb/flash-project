/******************************************************************************
*
* Driver Code for the serial port
* putc defined
*
******************************************************************************/
#include <pic.h>
#include "GenericTypeDefs.h"
#include "mchp_support.h"

#include "serialport.h"



void init_serialport(void)
{

/* Initialize registers associated with serial port */

// Initialize to 9600 baud, 8 bits, 1 start and 1 stop

	//SPBRG = 51; // 9600 baud with 32MHz system clock
	SPBRG = 25; // 19200 baud with 32MHz system clock
	TX9   = 0;	// 8 bit transmission
	SYNC  = 0;	// Asynchronous transmission mode
	BRGH  = 0;	// Select low speed baud rate generator mode
	ABDEN = 0;	// Autobaud detect mode disabled
	RX9   = 0;	// 8 bit receive mode
	SPEN  = 1;	// Serial Port Enabled
}

void putch(unsigned char txchar)
{
	while (!TRMT); // What for transmit buffer to empty
	TXREG = txchar; 

}




