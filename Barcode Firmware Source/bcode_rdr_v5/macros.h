// Set up Reader 1 (CCP3) to look for every rising edge
#define RDR1_LOOK_RISING()	\
	{						\
		CCP3IE = 0;			\
		CCP3CON  = 0x05;	\
		CCP3IF = 0;			\
		CCP3IE = 1;			\
	}

// Set up Reader 1 (CCP3) to look for every falling edge
#define RDR1_LOOK_FALLING()	\
	{						\
		CCP3IE = 0;			\
		CCP3CON  = 0x04;	\
		CCP3IF = 0;			\
		CCP3IE = 1;			\
	}

#define RDR1_SWAP_EDGE()	\
	{						\
		CCP3IE = 0;			\
		CCP3M0 = !CCP3M0;	\
		CCP3IF = 0;			\
		CCP3IE = 1;			\
	}


// Set up Reader 1 (CCP1) to look for every rising edge
#define RDR2_LOOK_RISING()	\
	{						\
		CCP1IE = 0;			\
		CCP1CON  = 0x05;	\
		CCP1IF = 0;			\
		CCP1IE = 1;			\
	}

// Set up Reader 2 (CCP1) to look for every falling edge
#define RDR2_LOOK_FALLING()	\
	{						\
		CCP1IE = 0;			\
		CCP1CON  = 0x04;	\
		CCP1IF = 0;			\
		CCP1IE = 1;			\
	}

#define RDR2_SWAP_EDGE()	\
	{						\
		CCP1IE = 0;			\
		CCP1M0 = !CCP1M0;	\
		CCP1IF = 0;			\
		CCP1IE = 1;			\
	}

#define CLEAR_SCREEN()	\
	{					\
		printf("\x1B[2J\x1B[H"); \
	}