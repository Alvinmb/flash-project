/******************************************************************************
*
* Driver Code for the timers
*
******************************************************************************/
#include <pic.h>
#include "GenericTypeDefs.h"
#include "mchp_support.h"

#include "timers.h"

void init_timer1(void)
{

/* Initialize Timer1 control registers */

/* Initialize Timer1 to run off instruction clock (Fosc/4)
   1:2 Prescaler, no gating and enable timer.           */

	T1CON  = 0x39;
	T1GCON = 0x00;
    TMR1ON = 1;
    TMR1IF = 0;
    TMR1IE = 0;	//Not using the timer 1 overflow interrrupt

}