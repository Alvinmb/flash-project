/*********************** Timer 0 State Machine for Reader 2 ******************/

		switch(rdr2_state)
		{
			case WAIT_QUIET:
				if (rdr2_state_timer)
				{
					//printf("5");
					rdr2_state_timer--;

				}
				else
				{
					RA1 = 1;
					//printf("6");
					rdr2_state_timer = STATE_DELAY;	// 
					rdr2_bar_count = 1;			// Initialize bar count and edge count
					rdr2_first_edge = TRUE;
					rdr2_state = GET_FIRST_SYMBOL;
					RDR2_LOOK_RISING();
				}
				break;
			case GET_FIRST_SYMBOL:
				if (!rdr2_first_edge)
				{
					if(rdr2_state_timer)
					{	
						//printf("7");
						rdr2_state_timer--;		// state timer not expired, decrement and return
					}
					else						// state timer has expired, start over
					{
						//printf("8");
						//RA1 = !RA1;
						init_reader2();
					}
				}
				break;
			case GET_DATA:
				if(rdr2_state_timer)
				{	
					//printf("7");
					rdr2_state_timer--;		// state timer not expired, decrement and return
				}
				else						// state timer has expired, start over
				{
					//printf("8");
					init_reader2();
					
				}
				break;
			default: 
					init_reader2();
				break;
		} // Close for switch statement for Reader 1