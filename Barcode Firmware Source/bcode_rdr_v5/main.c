/*********************************************************************

*********************************************************************/

#include <pic.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "GenericTypeDefs.h"
#include "mchp_support.h"
#include "serialport.h"
#include "timers.h"
#include "ccpmodule.h"
#include "macros.h"

#define FOSC                     32000000UL
// Timer0: 1:32 prescale
// #define OPTION_REG_INIT          0b10000101
#define OPTION_REG_INIT          0b10000101	//Weak Pullups, INT on falling 
// OPTION_REG  WPUEN/='1',INTEDG='0',TMROCS='0',TMR0SE='0',PSA='0',PS2='1',PS1='0',PS0='1'
// Weak Pullups Disabled, INT on falling edge, Timer 0=Internal Clk (FOSC/4), Timer 0 increment on rising edge T0CKI
// Prescaler on Timer 0, Prescaler = 1:64
#define TIMER0_PRESCALE          64L

#define	STATE_DELAY				8	//
#define QUIET_DELAY				1	// was 4
#define BCODE_TIMEOUT			2000U //2000 * 2ms = 4s

#define	START_C					105 // START C symbol
#define STOP					106 // STOP symbol
#define	STOP_6B					107	// first 6 bars of STOP symbol
#define REV_STOP_6B				108	// first 6 bars of STOP symbol reversed
#define	REV_STOP				109	// reversed STOP symbol
#define LAST_SYMBOL				109	// last valid symbol in lookup table
#define	BAD_SYM					110 // Symbol not found in lookup

#define	REASON_NOERR			'0'
#define	REASON_BADSTART			'1'
#define REASON_BADLOOKUP		'2'
#define	REASON_UNCORSYM			'3'
#define REASON_BADSTOP			'4'
#define REASON_BADCHCKSUM		'5'

#define VERSION_NUMBER			".v05:15"

//__CONFIG(0x3FE4);	//Disables WDOG tiemr - Only one of these two lines should be uncommented 
__CONFIG(0x3FFC);  //Enables WDOG timer - Only one of these two lines should be uncommented
__CONFIG(0x1EFF);

BOOL TimeBaseManager(void);

volatile unsigned char timebase_10ms;

typedef enum  { WAIT_LOW=0, WAIT_QUIET, GET_BARS } state_t;

volatile unsigned int delay;
volatile BOOL event500ms;
BYTE		timer500ms;

BOOL		rdr1_first_edge;
BOOL		rdr1_got_bcode, rdr1_got_error, rdr1_got_start, rdr1_reverse,rdr1_rev;
volatile state_t rdr1_state;
BYTE		rdr1_bar_count;	//count of the current bar we are working on, ranges from 1 to 6 except for STOP which has 7 bars
volatile BYTE rdr1_byte_count; //count of how many bar code bytes we've received
UINT16		rdr1_ledge_counter, rdr1_tedge_counter, rdr1_bar_width;
UINT16		rdr1_black_bar_unit, rdr1_white_bar_unit;
//UINT16		rdr1_bpos_tol, rdr1_bneg_tol, rdr1_wpos_tol, rdr1_wneg_tol;
BYTE		rdr1_rxdata;
BYTE		rdr1_bcode_data[25];
volatile BYTE rdr1_state_timer;
//unsigned long int rdr1_current_code;
volatile UINT16	rdr1_bars[8];
volatile BYTE rdr1_inpntr, rdr1_outpntr;
BYTE *rdr1_print_pos;
BYTE		rdr1_symbol_bars[7];
UINT16		rdr1_white_bar_sum;
UINT16		rdr1_black_bar_sum;
volatile UINT16 rdr1_bcode_tmr;
volatile char rdr1_reason;

BOOL		rdr2_first_edge;
BOOL		rdr2_got_bcode, rdr2_got_error, rdr2_got_start, rdr2_reverse,rdr2_rev;
volatile state_t rdr2_state;
BYTE		rdr2_bar_count;	//count of the current bar we are working on, ranges from 1 to 6 except for STOP which has 7 bars
volatile BYTE rdr2_byte_count; //count of how many bar code bytes we've received
UINT16		rdr2_ledge_counter, rdr2_tedge_counter, rdr2_bar_width;
UINT16		rdr2_black_bar_unit, rdr2_white_bar_unit;
//UINT16		rdr2_bpos_tol, rdr2_bneg_tol, rdr2_wpos_tol, rdr2_wneg_tol;
BYTE		rdr2_rxdata;
BYTE rdr2_bcode_data[25];
volatile BYTE rdr2_state_timer;
unsigned long int rdr2_current_code;
volatile UINT16	rdr2_bars[8];
volatile BYTE rdr2_inpntr, rdr2_outpntr;
BYTE *rdr2_print_pos;
BYTE		rdr2_symbol_bars[7];
UINT16		rdr2_white_bar_sum;
UINT16		rdr2_black_bar_sum;
volatile UINT16 rdr2_bcode_tmr;
volatile char rdr2_reason;

void update_rdr1_black_bar (unsigned int barwidth);
void update_rdr1_white_bar (unsigned int barwidth);
void update_rdr2_black_bar (unsigned int barwidth);
void update_rdr2_white_bar (unsigned int barwidth);
void init_reader1 (void);
void init_reader2 (void);
void prn_num (BYTE *num);
void prn_string (const char *string);
//BOOL CheckParityFixErrors(BYTE *symbol_array);

BYTE lookup_sym (BYTE bar_array[]);
BYTE bar_decision(UINT16 *bar_width, UINT16 *bar_unit);
BOOL checksum(BYTE bcode[], BYTE startindex);
UINT16 get_bar_width(UINT16 *lead_edge, UINT16 *trail_edge);
//unsigned long int convert_to_code(BYTE bar_array[]);
void update_widths(UINT16 *black_bar_unit, UINT16 *white_bar_unit, UINT16 *black_bar_sum, UINT16 *white_bar_sum, BYTE *symbol_bars);

int main(void)
{
	OPTION_REG = OPTION_REG_INIT;	
	OSCCONbits.IRCF = 0b1110;		// 8 MHZ internal oscillator
	SPLLEN = 1;						// Enable 4x PLL to make 32MHZ
		    
	// peripheral configuration
	PORTC = 0;
	PORTE = 0;
	TRISC2 = 0;

	TRISA0 = 0;
	TRISA1 = 0;
	
	ANSA0 = 0;
	ANSA1 = 0;
	ANSA3 = 1;
	ANSB3 = 0;
	ANSB1 = 1;
	ANSB5 = 0;


	TRISB3 = 1;		//Port B.3 is an input
	TRISB5 = 1;
	WPUB3  = 1;		//Port B.3 has a weak pullup

	TRISC2 = 1;		//Port C.2 is an input

	timer500ms = 243; //Initialize 500ms timer

	init_timer1();

	init_serialport();
	TXEN = 1;

	APFCON = 0b01000000;

	init_ccp3();	//Initialize Capture/Compare Machine 2
	init_ccp1();	//Initialize Capture/Compate Machine 1

	LCDIE = 0;
	PEIE = 1;
	GIE = 1;

	TMR0IE = 1;

	WDTCON = 0x14; // Watchdog times out in 1 sec
	

	rdr1_state_timer = 0;
	rdr2_state_timer = 0;

	rdr1_got_bcode  = FALSE;
	rdr1_got_error	= FALSE;
	rdr1_reverse	= FALSE;
	rdr1_reason		= REASON_NOERR;

	rdr2_got_bcode  = FALSE;
	rdr2_got_error	= FALSE;
	rdr2_reverse	= FALSE;
	rdr2_reason		= 0;

	RA0 = 0;
	RA1 = 0;

	delay = 10000;		

	while (delay) delay--;		//going forward

//	prn_string ("v04.00\0");

	init_reader1();
	init_reader2();

///////////////////////////////main loop //////////////////////////////////////   

	while(1)
	{  
		if (event500ms)
		{
			event500ms = FALSE; // Clear the 500ms event flag
			CLRWDT();			// Reset the watchdog timer
		}

/******************************************************************************
*                       Put out Reader state for debug                      *
******************************************************************************/

		switch(rdr1_state)
		{
			case WAIT_LOW:
				RA0 = 0;
				RA1 = 0;
				break;
			case WAIT_QUIET:
				RA0 = 1;
				RA1 = 0;
				break;
			case GET_BARS:
				RA0 = 0;
				RA1 = 1;
				break;
			default:
				RA0 = 1;
				RA1 = 1;
				break;
		}

/******************************************************************************
*                 Reader 1 State Machine which runs in main loop              *
******************************************************************************/

		if( (rdr1_state == GET_BARS) && (rdr1_inpntr != rdr1_outpntr) )
		{
			rdr1_bar_width = rdr1_bars[rdr1_outpntr];
			if (rdr1_outpntr != 7) rdr1_outpntr++; else rdr1_outpntr = 0;

			if(!rdr1_got_start)
			{
				switch(rdr1_bar_count)
				{
					case 1:
						update_rdr1_black_bar(2U);
						rdr1_symbol_bars[0] = 2;
						rdr1_symbol_bars[6] = 0;			//Initialize last element to 0 - only non-zero on STOP symbol
						rdr1_black_bar_sum = rdr1_bar_width;
						rdr1_bar_count++;
						break;
					case 2:
						update_rdr1_white_bar(1U);
						rdr1_symbol_bars[1] = 1;
						rdr1_white_bar_sum = rdr1_bar_width;
						rdr1_bar_count++;
						break;
					case 3:
						rdr1_symbol_bars[2] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
						rdr1_black_bar_sum = rdr1_black_bar_sum + rdr1_bar_width;
						rdr1_bar_count++;
						break;
					case 4:
						rdr1_symbol_bars[3] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
						rdr1_white_bar_sum = rdr1_white_bar_sum + rdr1_bar_width;
						rdr1_bar_count++;
						break;
					case 5:
						rdr1_symbol_bars[4] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
						rdr1_black_bar_sum = rdr1_black_bar_sum + rdr1_bar_width;
						rdr1_bar_count++;
						break;
					case 6:
						rdr1_symbol_bars[5] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
						
						rdr1_white_bar_sum = rdr1_white_bar_sum + rdr1_bar_width;
						update_widths(&rdr1_black_bar_unit,&rdr1_white_bar_unit,&rdr1_black_bar_sum,&rdr1_white_bar_sum,rdr1_symbol_bars);

						//rdr1_current_code = convert_to_code(rdr1_symbol_bars);	
						//rdr1_rxdata = lookup_sym(rdr1_current_code);

						rdr1_rxdata = lookup_sym(rdr1_symbol_bars);

						//if (rdr1_rxdata == BAD_SYM)
						//{
						//	if ( CheckParityFixErrors(rdr1_symbol_bars) );
						//	{
						//		rdr1_current_code = convert_to_code(rdr1_symbol_bars);  //Try to correct error
						//		rdr1_rxdata = lookup_sym(rdr1_current_code);
						//		update_widths(&rdr1_black_bar_unit,&rdr1_white_bar_unit,&rdr1_black_bar_sum,&rdr1_white_bar_sum,rdr1_symbol_bars);
						//	}
						//}

						if (rdr1_rxdata == START_C)		//Check for START_C symbol
						{
							rdr1_bcode_data[0] = rdr1_rxdata; //Put the START_C character in buffer
							rdr1_bar_count = 1;			// Initialize bar count and edge count
							if (rdr1_byte_count < 23) rdr1_byte_count++; //increment byte count
							//rdr1_current_code = 0UL;	//zero out current code to get ready for next symbol
							rdr1_got_start = TRUE;
							rdr1_bcode_tmr = BCODE_TIMEOUT;
						}
						else if (rdr1_rxdata == REV_STOP_6B) //Check for first 6 bars of REVERSED STOP
						{
							//rdr1_current_code = rdr1_current_code * 10; // shift up one decimal place for 7th bar
							rdr1_bar_count++; 						    // increment bar count to 7
						}
						else		// if we didn't get START_C or first of reversed STOP, then start over
						{
							//rdr1_got_error = TRUE;  //Comment out so as not to generate an error on a bad symbol lookup
							//rdr1_reason = REASON_BADSTART; 
							//rdr1_bcode_data[rdr1_byte_count] = STOP; //Stick a STOP where the bad symbol occurred so we can print
							//rdr1_print_pos = 1; //Start printing at position 1 since going forward
							init_reader1();
						}
						break;
					case 7:	// Check to see if we've got a reversed STOP symbol, if so we're reading backwards
						rdr1_symbol_bars[6] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
						//rdr1_current_code = convert_to_code(rdr1_symbol_bars); //mss br549
						rdr1_rxdata = lookup_sym(rdr1_symbol_bars);

						if (rdr1_rxdata == REV_STOP)  //Check to see if we've got REVERSED STOP
						{
							rdr1_bcode_data[23] = STOP;  //Put a STOP character in the buffer
							rdr1_bar_count = 1;			// Initialize bar count and edge count
							rdr1_byte_count = 22; 		//Place characters in buffer back to front
							//rdr1_current_code = 0UL;	//zero out current code to get ready for next symbol
							rdr1_got_start = TRUE;
							rdr1_reverse = TRUE;		//Reading backwards
							rdr1_rev	 = TRUE;
							rdr1_bcode_tmr = BCODE_TIMEOUT;
						}
						else		// if we didn't get REVERSED STOP, then start over
						{
							init_reader1();
						}
						break;
					default:
						break;
				}
			}
			else
			{
				if (!rdr1_reverse)
/* Reading in the FORWARD direction */
				{
					switch(rdr1_bar_count)
					{
						case 1:
							rdr1_symbol_bars[0] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
							rdr1_symbol_bars[6] = 0;
							rdr1_black_bar_sum = rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 2:
							rdr1_symbol_bars[1] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
							rdr1_white_bar_sum = rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 3:
						case 5:
							rdr1_symbol_bars[rdr1_bar_count - 1] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
							rdr1_black_bar_sum = rdr1_black_bar_sum + rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 4:
							rdr1_symbol_bars[3] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
							rdr1_white_bar_sum = rdr1_white_bar_sum + rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 6:
							rdr1_symbol_bars[5] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
							rdr1_white_bar_sum = rdr1_white_bar_sum + rdr1_bar_width;


							//rdr1_current_code = convert_to_code(rdr1_symbol_bars);
							rdr1_rxdata = lookup_sym(rdr1_symbol_bars);

							//if (rdr1_rxdata == BAD_SYM)
							//{
							//	if ( CheckParityFixErrors(rdr1_symbol_bars) );
							//	{
							//		rdr1_current_code = convert_to_code(rdr1_symbol_bars);
							//		rdr1_rxdata = lookup_sym(rdr1_current_code);
							//		update_widths(&rdr1_black_bar_unit,&rdr1_white_bar_unit,&rdr1_black_bar_sum,&rdr1_white_bar_sum,rdr1_symbol_bars);
							//		//prn_string("!");
							//	}
							//}
							//else
								update_widths(&rdr1_black_bar_unit,&rdr1_white_bar_unit,&rdr1_black_bar_sum,&rdr1_white_bar_sum,rdr1_symbol_bars);

							if (rdr1_rxdata == STOP_6B) // check for first 6 of 7 bars of STOP code
							{
								rdr1_bar_count++; 						    // increment bar count to 7
							}
							else if (rdr1_rxdata == BAD_SYM) // Check to see if we got a bad symbol
							{
								rdr1_got_error = TRUE;  //Comment out so as not to generate an error on a bad symbol lookup
								rdr1_reason = REASON_BADLOOKUP; //MSS
								rdr1_bcode_data[rdr1_byte_count] = STOP; //Stick a STOP where the bad symbol occurred so we can print
								rdr1_print_pos = &rdr1_bcode_data[1]; //Start printing at position 1 since going forward
								//init_reader1();
							}
							else
							{		
								rdr1_bcode_data[rdr1_byte_count] = rdr1_rxdata;  //store the data we just got
								rdr1_bar_count =1;			// Initialize bar count
								if (rdr1_byte_count < 23) rdr1_byte_count++;			// icrement byte count
								//rdr1_current_code = 0UL;	// Zero current code to get ready for next symbol
							}
							break;
						case 7:
							rdr1_symbol_bars[6] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);

							//rdr1_current_code = convert_to_code(rdr1_symbol_bars);
							rdr1_rxdata = lookup_sym(rdr1_symbol_bars);

							if (rdr1_rxdata == STOP)		// Look for STOP symbol
							{		
								rdr1_bcode_data[rdr1_byte_count] = rdr1_rxdata;  //store the data we just got
								rdr1_print_pos = &rdr1_bcode_data[1] ;
								rdr1_bar_count  = 1;		// Initialize bar count
								rdr1_byte_count = 0;		// Reset byte count
							
								if (checksum(rdr1_bcode_data, (BYTE)0))
								{
									rdr1_got_bcode = TRUE;
									//init_reader1();
								}
								else
								{
									rdr1_got_error = TRUE;
									rdr1_reason = REASON_BADCHCKSUM;	// Checksum failed
									rdr1_print_pos = &rdr1_bcode_data[1];	
									//init_reader1();
								}
							}
							else	// Reset State Machine 
							{
								rdr1_got_error = TRUE;
								rdr1_reason = REASON_BADSTOP;		// Bad STOP Symbol
								rdr1_bcode_data[rdr1_byte_count] = STOP; //Stick in a STOP byte so we can print it later
								rdr1_print_pos = &rdr1_bcode_data[1];
								//init_reader1();
							}
							break;
						default:
							break;
					}
				}
				else
/* Reading in the REVERSE Direction */
				{
					switch(rdr1_bar_count)
					{
						case 1:
							rdr1_symbol_bars[5] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
							rdr1_symbol_bars[6] = 0;
							rdr1_white_bar_sum = rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 2:
							rdr1_symbol_bars[4] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
							rdr1_black_bar_sum = rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 3:
							rdr1_symbol_bars[3] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
							rdr1_white_bar_sum = rdr1_white_bar_sum + rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 4:
							rdr1_symbol_bars[2] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);							
							rdr1_black_bar_sum = rdr1_black_bar_sum + rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 5:
							rdr1_symbol_bars[1] = bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit);
							rdr1_white_bar_sum = rdr1_white_bar_sum + rdr1_bar_width;
							rdr1_bar_count++;
							break;
						case 6:
							rdr1_symbol_bars[0] = bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit);
							rdr1_black_bar_sum = rdr1_black_bar_sum + rdr1_bar_width;

							//rdr1_current_code = convert_to_code(rdr1_symbol_bars);
							rdr1_rxdata = lookup_sym(rdr1_symbol_bars);

							//if (rdr1_rxdata == BAD_SYM)
							//{
							//	if ( CheckParityFixErrors(rdr1_symbol_bars) );
							//	{
							//		rdr1_current_code = convert_to_code(rdr1_symbol_bars);
							//		rdr1_rxdata = lookup_sym(rdr1_current_code);
							//		update_widths(&rdr1_black_bar_unit,&rdr1_white_bar_unit,&rdr1_black_bar_sum,&rdr1_white_bar_sum,rdr1_symbol_bars);
							//		//prn_string("!");
							//	}
							//}
							//else
								update_widths(&rdr1_black_bar_unit,&rdr1_white_bar_unit,&rdr1_black_bar_sum,&rdr1_white_bar_sum,rdr1_symbol_bars);

							if (rdr1_rxdata == START_C) // Look for START_C symbol
							{
								rdr1_bcode_data[rdr1_byte_count] = rdr1_rxdata;  //store the data we just got
							
								if (checksum(rdr1_bcode_data, rdr1_byte_count))
								{
									rdr1_got_bcode = TRUE;
									rdr1_print_pos = &rdr1_bcode_data[rdr1_byte_count + 1];
									rdr1_bar_count  = 1;		// Initialize bar count
									rdr1_byte_count = 0;		// Reset byte count
									//init_reader1();
								}
								else
								{
									rdr1_got_error  = TRUE;
									rdr1_reason = REASON_BADCHCKSUM; //Checksum failed
									rdr1_print_pos = &rdr1_bcode_data[rdr1_byte_count + 1];
									rdr1_bar_count  = 1;
									rdr1_byte_count = 0;
									//init_reader1();
								}
							}
							else if (rdr1_rxdata == BAD_SYM) // Check to see if we got a bad symbol
							{
								rdr1_got_error = TRUE;
								rdr1_reason	= REASON_BADLOOKUP;
								rdr1_bcode_data[rdr1_byte_count] = START_C; //Stick a START where the bad symbol occurred so we can print
								rdr1_print_pos = &rdr1_bcode_data[rdr1_byte_count + 1];
								//init_reader1();
							}
							else
							{		
								rdr1_bcode_data[rdr1_byte_count] = rdr1_rxdata;  //store the data we just got
								rdr1_bar_count =1;			// Initialize bar count
								if (rdr1_byte_count != 0) rdr1_byte_count--;			// decrement byte count
								//rdr1_current_code = 0UL;	// Zero current code to get ready for next symbol
							}
							break;
						default:
							break;
					}
				}
				
			}	// Close for if != got_start
		}	// Close for rdr1_inpntr != rdr1_outpntr

/******************************************************************************
*                 Reader 2 State Machine which runs in main loop              *
******************************************************************************/

		if( (rdr2_state == GET_BARS) && (rdr2_inpntr != rdr2_outpntr) )
		{
			rdr2_bar_width = rdr2_bars[rdr2_outpntr];
			if (rdr2_outpntr != 7) rdr2_outpntr++; else rdr2_outpntr = 0;

			if(!rdr2_got_start)
			{
				switch(rdr2_bar_count)
				{
					case 1:
						update_rdr2_black_bar(2U);
						rdr2_symbol_bars[0] = 2;
						rdr2_symbol_bars[6] = 0;			//Initialize last element to 0 - only non-zero on STOP symbol
						rdr2_black_bar_sum = rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 2:
						update_rdr2_white_bar(1U);
						rdr2_symbol_bars[1] = 1;
						rdr2_white_bar_sum = rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 3:
						rdr2_symbol_bars[2] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
						rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 4:
						rdr2_symbol_bars[3] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
						rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 5:
						rdr2_symbol_bars[4] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
						rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 6:
						rdr2_symbol_bars[5] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
						
						rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
						update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,rdr2_symbol_bars);

						//rdr2_current_code = convert_to_code(rdr2_symbol_bars);	
						rdr2_rxdata = lookup_sym(rdr2_symbol_bars);

						//if (rdr2_rxdata == BAD_SYM)
						//{
						//	if ( CheckParityFixErrors(rdr2_symbol_bars) );
						//	{
						//		rdr2_current_code = convert_to_code(rdr2_symbol_bars);  //Try to correct error
						//		rdr2_rxdata = lookup_sym(rdr2_current_code);
						//		update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,rdr2_symbol_bars);
						//	}
						//}

						if (rdr2_rxdata == START_C)		//Check for START_C symbol
						{
							rdr2_bcode_data[0] = rdr2_rxdata; //Put the START_C character in buffer
							rdr2_bar_count = 1;			// Initialize bar count and edge count
							if (rdr2_byte_count < 23) rdr2_byte_count++; //increment byte count
							rdr2_current_code = 0UL;	//zero out current code to get ready for next symbol
							rdr2_got_start = TRUE;
							rdr2_bcode_tmr = BCODE_TIMEOUT;
						}
						else if (rdr2_rxdata == REV_STOP_6B) //Check for first 6 bars of REVERSED STOP
						{
							rdr2_current_code = rdr2_current_code * 10; // shift up one decimal place for 7th bar
							rdr2_bar_count++; 						    // increment bar count to 7
						}
						else		// if we didn't get START_C or first of reversed STOP, then start over
						{
							//rdr2_got_error = TRUE;  //Comment out so as not to generate an error on a bad symbol lookup
							//rdr2_reason = REASON_BADSTART; 
							//rdr2_bcode_data[rdr2_byte_count] = STOP; //Stick a STOP where the bad symbol occurred so we can print
							//rdr2_print_pos = 1; //Start printing at position 1 since going forward
							init_reader2();
						}
						break;
					case 7:	// Check to see if we've got a reversed STOP symbol, if so we're reading backwards
						rdr2_symbol_bars[6] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
						//rdr2_current_code = convert_to_code(rdr2_symbol_bars);
						rdr2_rxdata = lookup_sym(rdr2_symbol_bars);

						if (rdr2_rxdata == REV_STOP)  //Check to see if we've got REVERSED STOP
						{
							rdr2_bcode_data[23] = STOP;  //Put a STOP character in the buffer
							rdr2_bar_count = 1;			// Initialize bar count and edge count
							rdr2_byte_count = 22; 		//Place characters in buffer back to front
							rdr2_current_code = 0UL;	//zero out current code to get ready for next symbol
							rdr2_got_start = TRUE;
							rdr2_reverse = TRUE;		//Reading backwards
							rdr2_rev	 = TRUE;
							rdr2_bcode_tmr = BCODE_TIMEOUT;
						}
						else		// if we didn't get REVERSED STOP, then start over
						{
							init_reader2();
						}
						break;
					default:
						break;
				}
			}
			else
			{
				if (!rdr2_reverse)
/* Reading in the FORWARD direction */
				{
					switch(rdr2_bar_count)
					{
						case 1:
							rdr2_symbol_bars[0] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
							rdr2_symbol_bars[6] = 0;
							rdr2_black_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 2:
							rdr2_symbol_bars[1] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
							rdr2_white_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 3:
						case 5:
							rdr2_symbol_bars[rdr2_bar_count - 1] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
							rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 4:
							rdr2_symbol_bars[3] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 6:
							rdr2_symbol_bars[5] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;


							//rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							rdr2_rxdata = lookup_sym(rdr2_symbol_bars);

							//if (rdr2_rxdata == BAD_SYM)
							//{
							//	if ( CheckParityFixErrors(rdr2_symbol_bars) );
							//	{
							//		rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							//		rdr2_rxdata = lookup_sym(rdr2_current_code);
							//		update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,rdr2_symbol_bars);
							//		//prn_string("!");
							//	}
							//}
							//else
								update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,rdr2_symbol_bars);

							if (rdr2_rxdata == STOP_6B) // check for first 6 of 7 bars of STOP code
							{
								rdr2_bar_count++; 						    // increment bar count to 7
							}
							else if (rdr2_rxdata == BAD_SYM) // Check to see if we got a bad symbol
							{
								rdr2_got_error = TRUE;  //Comment out so as not to generate an error on a bad symbol lookup
								rdr2_reason = REASON_BADLOOKUP; //MSS
								rdr2_bcode_data[rdr2_byte_count] = STOP; //Stick a STOP where the bad symbol occurred so we can print
								rdr2_print_pos = &rdr2_bcode_data[1]; //Start printing at position 1 since going forward
								//init_reader2();
							}
							else
							{		
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
								rdr2_bar_count =1;			// Initialize bar count
								if (rdr2_byte_count < 23) rdr2_byte_count++;			// icrement byte count
								rdr2_current_code = 0UL;	// Zero current code to get ready for next symbol
							}
							break;
						case 7:
							rdr2_symbol_bars[6] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);

							//rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							rdr2_rxdata = lookup_sym(rdr2_symbol_bars);

							if (rdr2_rxdata == STOP)		// Look for STOP symbol
							{		
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
								rdr2_print_pos = &rdr2_bcode_data[1] ;
								rdr2_bar_count  = 1;		// Initialize bar count
								rdr2_byte_count = 0;		// Reset byte count
							
								if (checksum(rdr2_bcode_data, (BYTE)0))
								{
									rdr2_got_bcode = TRUE;
									//init_reader2();
								}
								else
								{
									rdr2_got_error = TRUE;
									rdr2_reason = REASON_BADCHCKSUM;	// Checksum failed
									rdr2_print_pos = &rdr2_bcode_data[1];	
									//init_reader2();
								}
							}
							else	// Reset State Machine 
							{
								rdr2_got_error = TRUE;
								rdr2_reason = REASON_BADSTOP;		// Bad STOP Symbol
								rdr2_bcode_data[rdr2_byte_count] = STOP; //Stick in a STOP byte so we can print it later
								rdr2_print_pos = &rdr2_bcode_data[1];
								//init_reader2();
							}
							break;
						default:
							break;
					}
				}
				else
/* Reading in the REVERSE Direction */
				{
					switch(rdr2_bar_count)
					{
						case 1:
							rdr2_symbol_bars[5] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
							rdr2_symbol_bars[6] = 0;
							rdr2_white_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 2:
							rdr2_symbol_bars[4] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
							rdr2_black_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 3:
							rdr2_symbol_bars[3] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 4:
							rdr2_symbol_bars[2] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);							
							rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 5:
							rdr2_symbol_bars[1] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 6:
							rdr2_symbol_bars[0] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit);
							rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;

							//rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							rdr2_rxdata = lookup_sym(rdr2_symbol_bars);

							//if (rdr2_rxdata == BAD_SYM)
							//{
							//	if ( CheckParityFixErrors(rdr2_symbol_bars) );
							//	{
							//		rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							//		rdr2_rxdata = lookup_sym(rdr2_current_code);
							//		update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,rdr2_symbol_bars);
							//		//prn_string("!");
							//	}
							//}
							//else
								update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,rdr2_symbol_bars);

							if (rdr2_rxdata == START_C) // Look for START_C symbol
							{
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
							
								if (checksum(rdr2_bcode_data, rdr2_byte_count))
								{
									rdr2_got_bcode = TRUE;
									rdr2_print_pos = &rdr2_bcode_data[rdr2_byte_count + 1];
									rdr2_bar_count  = 1;		// Initialize bar count
									rdr2_byte_count = 0;		// Reset byte count
									//init_reader2();
								}
								else
								{
									rdr2_got_error  = TRUE;
									rdr2_reason = REASON_BADCHCKSUM; //Checksum failed
									rdr2_print_pos = &rdr2_bcode_data[rdr2_byte_count + 1];
									rdr2_bar_count  = 1;
									rdr2_byte_count = 0;
									//init_reader2();
								}
							}
							else if (rdr2_rxdata == BAD_SYM) // Check to see if we got a bad symbol
							{
								rdr2_got_error = TRUE;
								rdr2_reason	= REASON_BADLOOKUP;
								rdr2_bcode_data[rdr2_byte_count] = START_C; //Stick a START where the bad symbol occurred so we can print
								rdr2_print_pos = &rdr2_bcode_data[rdr2_byte_count + 1];
								//init_reader2();
							}
							else
							{		
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
								rdr2_bar_count =1;			// Initialize bar count
								if (rdr2_byte_count != 0) rdr2_byte_count--;			// decrement byte count
								rdr2_current_code = 0UL;	// Zero current code to get ready for next symbol
							}
							break;
						default:
							break;
					}
				}
				
			}	// Close for if != got_start
		}	// Close for rdr2_inpntr != rdr2_outpntr

/******************************************************************************
/                          Print out barcodes if we have them                 *
/*****************************************************************************/

		if (rdr1_got_bcode)
		{
			rdr1_got_bcode = FALSE;

			while (*(rdr1_print_pos+1) != STOP)
			{
				prn_num (rdr1_print_pos);
				rdr1_print_pos++;
			}
			
			//prn_string (VERSION_NUMBER);
			putch('\r');
			init_reader1();
		}

		if (rdr1_got_error)
		{
			putch('!');		//Error alert marker
			
			if (rdr1_reason == REASON_BADSTART)
				putch('?');
			else
			{
				// Print as much barcode as we have
				while (*(rdr1_print_pos+1) != STOP)
				{
					prn_num (rdr1_print_pos);
					rdr1_print_pos++;
				}

			}

			putch('.');		//delimiter

			putch('1');		//Side 1

			putch('.');		//delimiter

			if (!rdr1_rev)
				putch('F');
			else
				putch('R');

			putch('.');		//delimiter

			putch(rdr1_reason);	//output reason code

			//prn_string (VERSION_NUMBER);
			putch('\r');		//return

			rdr1_got_error = FALSE;
			rdr1_reason	   = REASON_NOERR;
			rdr1_rev	   = FALSE;
			init_reader1();
		}

		if (rdr2_got_bcode)
		{
			rdr2_got_bcode = FALSE;

			//rdr2_print_pos = &rdr2_bcode_data[1];

			while (*(rdr2_print_pos+1) != STOP)
			{
				prn_num (rdr2_print_pos);
				rdr2_print_pos++;
			}
			
			//printf ("\r");
			//prn_string (VERSION_NUMBER);
			putch('\r');
			init_reader2();
		}

		if (rdr2_got_error)
		{
			putch('!');		//Error alert marker
			
			if (rdr2_reason == REASON_BADSTART)
				putch('?');
			else
			{
				// Print as much barcode as we have
				while (*(rdr2_print_pos+1) != STOP)
				{
					prn_num (rdr2_print_pos);
					rdr2_print_pos++;
				}

			}

			putch('.');		//delimiter

			putch('2');		//Side 1

			putch('.');		//delimiter

			if (!rdr2_rev)
				putch('F');
			else
				putch('R');

			putch('.');		//delimiter

			putch(rdr2_reason);	//output reason code

			//prn_string (VERSION_NUMBER);
			putch('\r');		//return

			rdr2_got_error = FALSE;
			rdr2_reason	   = REASON_NOERR;
			rdr2_rev	   = FALSE;
			init_reader2();
		}

	
	} // end - main loop  	    
	return 0;
} // end of main


//////////////////////// Interrupt Service Routine //////////////////////////// 

void interrupt isr(void)
{

/******** State Machine for Reader 1 that runs when an edge occurs ***********/

// Capture occurred, update state machine
	if (CCP3IF && CCP3IE)
	{
		CCP3IF = 0;					// Clear the capture flag

		switch(rdr1_state)
		{
			case WAIT_LOW:			// Signal has gone low, set state timer and wait for quiet period
				if (RB5)
				{
					//prn_string ("FAULT1");
					//putch('\r');
					init_reader1;
				}
				else
				{
					rdr1_state_timer = QUIET_DELAY;
					rdr1_state = WAIT_QUIET;
					RDR1_LOOK_RISING();
				}
				break;
			case WAIT_QUIET:
				rdr1_state = WAIT_LOW;
				RDR1_LOOK_FALLING();
				break;
			case GET_BARS:
				if (rdr1_first_edge)
				{
					rdr1_ledge_counter = CCPR3H;					// Get high byte
					rdr1_ledge_counter = rdr1_ledge_counter <<8;  	// shift into the high byte
					rdr1_ledge_counter |= CCPR3L;					// Or in the low byte
					rdr1_first_edge = FALSE;
				}
				else
				{
					rdr1_tedge_counter = CCPR3H;					// Get high byte
					rdr1_tedge_counter = rdr1_tedge_counter <<8;  	// shift into the high byte
					rdr1_tedge_counter |= CCPR3L;					// Or in the low byte

					rdr1_bars[rdr1_inpntr] = get_bar_width(&rdr1_ledge_counter, &rdr1_tedge_counter);
//					rdr1_inpntr = (rdr1_inpntr++ & 0x07); // increment pointer and zero all but lower three bits
					if (rdr1_inpntr != 7) rdr1_inpntr++; else rdr1_inpntr = 0;

					rdr1_ledge_counter = rdr1_tedge_counter;		// this trailing edge becomes the next leading edge
				}

				rdr1_state_timer = STATE_DELAY;						// Reload state timer while bars are coming in
				RDR1_SWAP_EDGE();	//Look at the opposite edge with the capture machine

				break;
		}  // Close for rdr1_state switch
	} // Close for Capture 3 Interrupt Flag

/******** State Machine for Reader 2 that runs when an edge occurs ***********/

// Capture occurred, update state machine
	if (CCP1IF && CCP1IE)
	{
		CCP1IF = 0;					// Clear the capture flag

		switch(rdr2_state)
		{
			case WAIT_LOW:			// Signal has gone low, set state timer and wait for quiet period
				if (RC2)
				{
					//prn_string ("FAULT2");
					//putch('\r');
					init_reader1;
				}
				else
				{
					rdr2_state_timer = QUIET_DELAY;
					rdr2_state = WAIT_QUIET;
					RDR2_LOOK_RISING();
				}
				break;
			case WAIT_QUIET:
				rdr2_state = WAIT_LOW;
				RDR2_LOOK_FALLING();
				break;
			case GET_BARS:
				if (rdr2_first_edge)
				{
					rdr2_ledge_counter = CCPR1H;					// Get high byte
					rdr2_ledge_counter = rdr2_ledge_counter <<8;  	// shift into the high byte
					rdr2_ledge_counter |= CCPR1L;					// Or in the low byte
					rdr2_first_edge = FALSE;
				}
				else
				{
					rdr2_tedge_counter = CCPR1H;					// Get high byte
					rdr2_tedge_counter = rdr2_tedge_counter <<8;  	// shift into the high byte
					rdr2_tedge_counter |= CCPR1L;					// Or in the low byte

					rdr2_bars[rdr2_inpntr] = get_bar_width(&rdr2_ledge_counter, &rdr2_tedge_counter);
//					rdr2_inpntr = (rdr2_inpntr++ & 0x07); // increment pointer and zero all but lower three bits
					if (rdr2_inpntr != 7) rdr2_inpntr++; else rdr2_inpntr = 0;

					rdr2_ledge_counter = rdr2_tedge_counter;		// this trailing edge becomes the next leading edge
				}

				rdr2_state_timer = STATE_DELAY;						// Reload state timer while bars are coming in
				RDR2_SWAP_EDGE();	//Look at the opposite edge with the capture machine

				break;
		}  // Close for rdr2_state switch
	} // Close for Capture 2 Interrupt Flag


/********************************* Timer 0 ***********************************/
/*                  Interrupt handler for Timer 0 Overflows                  */
/*					Time 0 Overflows at 2.048ms intervals                    */
/*****************************************************************************/

	if(T0IF && TMR0IE)
	{
		T0IF = 0;

//		if (delay) delay--;

		if (timer500ms)
		{
			timer500ms--;
		}
		else
		{
			timer500ms = 243; //reload for 500ms 
			event500ms = TRUE;
		}

/*********************** Timer 0 State Machine for Reader 1 ******************/

		switch(rdr1_state)
		{
			case WAIT_QUIET:
				if (rdr1_state_timer)
				{
					rdr1_state_timer--;

				}
				else
				{ 
					rdr1_bar_count = 1;			// Initialize bar count and edge count
					rdr1_first_edge = TRUE;
					rdr1_state_timer = STATE_DELAY;	//
					rdr1_inpntr = 0;
					rdr1_outpntr = 0;
					rdr1_got_start = FALSE;
					rdr1_state = GET_BARS;
					RDR1_LOOK_RISING();
				}
				break;
			case GET_BARS:
				if (!rdr1_first_edge)
				{
					if(rdr1_state_timer)
					{	
						rdr1_state_timer--;		// state timer not expired, decrement and return
					}
					else						// state timer has expired, start over
					{
						init_reader1();
					}
				}
				else if (rdr1_got_start)
				{
					if(!rdr1_bcode_tmr)
					{
						init_reader1();
					}
					else
					{
						rdr1_bcode_tmr--;
					}
				}
				break;
			default: 
					init_reader1();
				break;
		} // Close for switch statement for Reader 1

	}

/*********************** Timer 0 State Machine for Reader 2 ******************/

		switch(rdr2_state)
		{
			case WAIT_QUIET:
				if (rdr2_state_timer)
				{
					rdr2_state_timer--;

				}
				else
				{
					rdr2_bar_count = 1;			// Initialize bar count and edge count
					rdr2_first_edge = TRUE;
					rdr2_state_timer = STATE_DELAY;	//
					rdr2_inpntr = 0;
					rdr2_outpntr = 0;
					rdr2_got_start = FALSE;
					rdr2_state = GET_BARS;
					RDR2_LOOK_RISING();
				}
				break;
			case GET_BARS:
				if (!rdr2_first_edge)
				{
					if(rdr2_state_timer)
					{	
						rdr2_state_timer--;		// state timer not expired, decrement and return
					}
					else						// state timer has expired, start over
					{
						init_reader2();
					}
				}
				else if (rdr2_got_start)
				{
					if(!rdr2_bcode_tmr)
					{
						init_reader2();
					}
					else
					{
						rdr2_bcode_tmr--;
					}
				}		
				break;
			default: 
					init_reader2();
				break;
		} // Close for switch statement for Reader 2

} //Close for ISR function

BYTE lookup_sym (BYTE *bar_array)
{                
  
const unsigned long int sym_table[LAST_SYMBOL + 1] = {
	2122220UL,
	2221220UL,
	2222210UL,
	1212230UL,
	1213220UL,
	1312220UL,
	1222130UL,
	1223120UL,
	1322120UL,
	2212130UL,
	2213120UL,
	2312120UL,
	1122320UL,
	1221320UL,
	1222310UL,
	1132220UL,
	1231220UL,
	1232210UL,
	2232110UL,
	2211320UL,
	2212310UL,
	2132120UL,
	2231120UL,
	3121310UL,
	3112220UL,
	3211220UL,
	3212210UL,
	3122120UL,
	3221120UL,
	3222110UL,
	2121230UL,
	2123210UL,
	2321210UL,
	1113230UL,
	1311230UL,
	1313210UL,
	1123130UL,
	1321130UL,
	1323110UL,
	2113130UL,
	2311130UL,
	2313110UL,
	1121330UL,
	1123310UL,
	1321310UL,
	1131230UL,
	1133210UL,
	1331210UL,
	3131210UL,
	2113310UL,
	2311310UL,
	2131130UL,
	2133110UL,
	2131310UL,
	3111230UL,
	3113210UL,
	3311210UL,
	3121130UL,
	3123110UL,
	3321110UL,
	3141110UL,
	2214110UL,
	4311110UL,
	1112240UL,
	1114220UL,
	1211240UL,
	1214210UL,
	1411220UL,
	1412210UL,
	1122140UL,
	1124120UL,
	1221140UL,
	1224110UL,
	1421120UL,
	1422110UL,
	2412110UL,
	2211140UL,
	4131110UL,
	2411120UL,
	1341110UL,
	1112420UL,
	1211420UL,
	1212410UL,
	1142120UL,
	1241120UL,
	1242110UL,
	4112120UL,
	4211120UL,
	4212110UL,
	2121410UL,
	2141210UL,
	4121210UL,
	1111430UL,
	1113410UL,
	1311410UL,
	1141130UL,
	1143110UL,
	4111130UL,
	4113110UL,
	1131410UL,
	1141310UL,
	3111410UL,
	4111310UL,
	2114120UL,		//START A
	2112140UL,		//START B
	2112320UL,		//START C
	2331112UL,		//STOP
	2331110UL,		//1st 6 bars of STOP
	2111330UL,		//1st 6 bars of REVERSE STOP
	2111332UL };	//REVERSE STOP	

BYTE i;
BYTE sym_array[7];
BYTE sym_dist, min_sym_dist = 99, best_sym = 0;


	for (i = 0; i < (LAST_SYMBOL + 1); i++)
	{

		// Expand compressed table entry into bar data
		sym_array[0] = (sym_table[i]/1000000);
		sym_array[1] = (sym_table[i]%1000000)/100000;
		sym_array[2] = (sym_table[i]%100000 )/10000;
		sym_array[3] = (sym_table[i]%10000  )/1000;
		sym_array[4] = (sym_table[i]%1000   )/100;
		sym_array[5] = (sym_table[i]%100    )/10;
		sym_array[6] = (sym_table[i]%10     );

		sym_dist = abs(bar_array[6] - sym_array[6]) + abs(bar_array[5] - sym_array[5]) + abs(bar_array[4] - sym_array[4]) +
				   abs(bar_array[3] - sym_array[3]) + abs(bar_array[2] - sym_array[2]) + abs(bar_array[1] - sym_array[1]) +
				   abs(bar_array[0] - sym_array[0]);


		if (sym_dist == 0)
		{
			min_sym_dist = 0;
			best_sym = i; 
			break;
		}
		else
		{
			if (sym_dist <= min_sym_dist)
			{
				min_sym_dist = sym_dist;
				best_sym = i;
			}
		}
	}

	return best_sym; //returns symbol with the lowest error distance (0 distance for perfect match)                         
}

void init_reader1 (void)
{

BYTE i;

	for (i=0; i<25; i++)
		rdr1_bcode_data[i] = STOP;

	rdr1_got_bcode = FALSE;
	rdr1_got_error = FALSE;
	rdr1_first_edge = FALSE;
	rdr1_got_start = FALSE;
	rdr1_reverse = FALSE;
	rdr1_rev = FALSE;
	//rdr1_current_code = 0UL;
	rdr1_black_bar_sum = 0;
	rdr1_white_bar_sum = 0;
	rdr1_bar_count = 1;
	rdr1_byte_count = 0;
	rdr1_inpntr = 0;
	rdr1_outpntr = 0;
	rdr1_print_pos = &rdr1_bcode_data[1];
	rdr1_bcode_data[23] = STOP;

	rdr1_ledge_counter = 0;
	rdr1_tedge_counter = 0;
	rdr1_bar_width = 0;
//	rdr1_black_bar_unit = 0;
//	rdr1_white_bar_unit = 0;
	rdr1_rxdata = 0;
	rdr1_state_timer = 1;
//	rdr1_current_code = 0;

	for (i=0; i<8; i++)
		rdr1_bars[i] = 0;

	for (i=0; i<7; i++)
		rdr1_symbol_bars[i] = 0;

	rdr1_white_bar_sum = 0;
	rdr1_black_bar_sum = 0;

	rdr1_bcode_tmr	= 0;
	rdr1_reason = 0;



	if (RB5)	// Check state of the reader 1 data line to decide which state to initialize to
	{ 
 		rdr1_state = WAIT_LOW; 
		RDR1_LOOK_FALLING();
	}
	else 
	{	
		rdr1_state_timer = QUIET_DELAY;
		rdr1_state = WAIT_QUIET;  //Initialize reader 1 state
		RDR1_LOOK_RISING();
	}
	return;
}

void init_reader2 (void)
{

BYTE i;

	for (i=0; i<25; i++)
		rdr2_bcode_data[i] = STOP;

	rdr2_got_bcode = FALSE;
	rdr2_got_error = FALSE;
	rdr2_first_edge = FALSE;
	rdr2_got_start = FALSE;
	rdr2_reverse = FALSE;
	rdr2_rev = FALSE;
	rdr2_current_code = 0UL;
	rdr2_black_bar_sum = 0;
	rdr2_white_bar_sum = 0;
	rdr2_bar_count = 1;
	rdr2_byte_count = 0;
	rdr2_inpntr = 0;
	rdr2_outpntr = 0;
	rdr2_print_pos = &rdr2_bcode_data[1];
	rdr2_bcode_data[23] = STOP;

	rdr2_ledge_counter = 0;
	rdr2_tedge_counter = 0;
	rdr2_bar_width = 0;
//	rdr2_black_bar_unit = 0;
//	rdr2_white_bar_unit = 0;
	rdr2_rxdata = 0;
	rdr2_state_timer = 1;
//	rdr2_current_code = 0;

	for (i=0; i<8; i++)
		rdr2_bars[i] = 0;

	for (i=0; i<7; i++)
		rdr2_symbol_bars[i] = 0;

	rdr2_white_bar_sum = 0;
	rdr2_black_bar_sum = 0;

	rdr2_bcode_tmr	= 0;
	rdr2_reason = 0;

	if (RC2)	// Check state of the reader 2 data line to decide which state to initialize to
	{ 
 		rdr2_state = WAIT_LOW; 
		RDR2_LOOK_FALLING();
	}
	else 
	{	
		rdr2_state_timer = QUIET_DELAY;
		rdr2_state = WAIT_QUIET;  //Initialize reader 2 state
		RDR2_LOOK_RISING();
	}
	return;
}

BOOL checksum(BYTE *bcode, BYTE startindex)
{
UINT16 sum;
BYTE index;
BYTE position;
BYTE modulo;

	index = startindex;	//points to the START code

	sum = bcode[index];
	
	index++;
	position = 1;

	while (bcode[index+1] != STOP)
	{
		sum = sum + bcode[index] * position;
		index++;
		position++;
	}

	modulo = sum % 103;

	if (modulo == bcode[index])
		return TRUE;
	else
		return FALSE;
}

void update_rdr1_black_bar (unsigned int barwidth)
{
	rdr1_black_bar_unit = rdr1_bar_width/barwidth;
	//rdr1_bpos_tol = rdr1_black_bar_unit/2;
	//rdr1_bneg_tol = rdr1_black_bar_unit/2;
	return;
}

void update_rdr1_white_bar (unsigned int barwidth)
{
	rdr1_white_bar_unit = rdr1_bar_width/barwidth;
	//rdr1_wpos_tol = rdr1_white_bar_unit/2;
	//rdr1_wneg_tol = rdr1_white_bar_unit/2;
	return;
}

void update_rdr2_black_bar (unsigned int barwidth)
{
	rdr2_black_bar_unit = rdr2_bar_width/barwidth;
	//rdr2_bpos_tol = rdr2_black_bar_unit/2;
	//rdr2_bneg_tol = rdr2_black_bar_unit/2;
	return;
}

void update_rdr2_white_bar (unsigned int barwidth)
{
	rdr2_white_bar_unit = rdr2_bar_width/barwidth;
	//rdr2_wpos_tol = rdr2_white_bar_unit/2 ;
	//rdr2_wneg_tol = rdr2_white_bar_unit/2;
	return;
}


BYTE bar_decision(UINT16 *bar_width, UINT16 *bar_unit)
{

	if ( (*bar_unit + (*bar_unit/2) > *bar_width) )
	{
		return 1U;
	}
	else if ( (2 * *bar_unit + (*bar_unit/2) > *bar_width) && (*bar_width > 2 * *bar_unit - (*bar_unit/2) ) )
	{
		return 2U;
	}
	else if ( (3 * *bar_unit + (*bar_unit/2) > *bar_width) && (*bar_width > 3 * *bar_unit - (*bar_unit/2) ) )
	{
		return 3U;
	}
	else
	{
		return 4U;
	}
}


void update_widths(UINT16 *black_bar_unit, UINT16 *white_bar_unit, UINT16 *black_bar_sum, UINT16 *white_bar_sum, BYTE *symbol_bars)
{

	*black_bar_unit = *black_bar_sum/(symbol_bars[0] + symbol_bars[2] + symbol_bars[4]);
	//*bpos_tol = *black_bar_unit/2;
	//*bneg_tol = *black_bar_unit/2;

	*white_bar_unit = *white_bar_sum/(symbol_bars[1] + symbol_bars[3] + symbol_bars[5]);
	//*bpos_tol = *white_bar_unit/2;
	//*bneg_tol = *white_bar_unit/2;
	
	return;
}

//BOOL CheckParityFixErrors(unsigned char* digitizedarray)
//{
//	BYTE bparity;
//	BYTE wparity;
//	BYTE total;
//	BYTE diff;
//	BYTE i;
//	char eliminate;
//
//	total = digitizedarray[0]+digitizedarray[1]+digitizedarray[2]+digitizedarray[3]+digitizedarray[4]+digitizedarray[5];
//	wparity = digitizedarray[1]+digitizedarray[3]+digitizedarray[5];
//	bparity = digitizedarray[0]+digitizedarray[2]+digitizedarray[4];
//	if (total > 11) {
//		diff = total-11;
//		if(diff > 4)
//			return FALSE;
//		eliminate = 4;
//		while(diff){
//			if( (wparity & 0x01) && !(bparity & 0x01) && !(diff&0x01)  ) {	// if both parities are correct and the diff is even.
//				for(i = 1; (i <= 5)&&(diff); i+=2)
//					if(digitizedarray[i] >= eliminate) {
//						digitizedarray[i]--;
//						diff--;
//						wparity--;
//					}
//				for(i = 0; (i <= 4)&&(diff); i+=2)
//					if(digitizedarray[i] >= eliminate){
//						digitizedarray[i]--;
//						diff--;
//						bparity--;
//					}
//				if(diff){
//					eliminate--;
//					if(eliminate == 1)	// Nothing left to eliminate.
//						return FALSE;
//				}
//			}
//			else if(bparity & 0x01){ // Black parity is wrong.
//				for(i = 0; i < 6; i+=2)
//					if(digitizedarray[i] >= eliminate){
//						digitizedarray[i]--;
//						diff--;
//						bparity--;
//						break;
//					}
//				if(diff){
//					eliminate--;
//					if(eliminate == 1)	// Nothing left to eliminate.
//						return FALSE;
//				}
//			} else if(!(wparity & 0x01)) { // White parity is wrong.
//				for(i = 1; i <= 5; i+=2)
//					if(digitizedarray[i] >= eliminate) {
//						digitizedarray[i]--;
//						diff--;
//						wparity--;
//						break;
//					}
//				if(diff){
//					eliminate--;
//					if(eliminate == 1)	// Nothing left to eliminate.
//						return FALSE;
//				}
//			} else	// Both parities are correct, but the diff is odd. Can't do anything.
//				return FALSE;
//		}
//	} else if (total < 11){
//		diff = 11-total;
//		if(diff > 4)
//			return FALSE;
//		eliminate = 1;
//		while(diff){
//			if( (wparity & 0x01) && !(bparity & 0x01) && !(diff&0x01)  ) {	// if both parities are correct and the diff is even.
//				for(i = 1; (i <= 5)&&(diff); i+=2)
//					if(digitizedarray[i] <= eliminate) {
//						digitizedarray[i]++;
//						diff--;
//						wparity++;
//					}
//				for(i = 0; (i <= 4)&&(diff); i+=2)
//					if(digitizedarray[i] <= eliminate){
//						digitizedarray[i]++;
//						diff--;
//						bparity++;
//					}
//				if(diff){
//					eliminate++;
//					if(eliminate == 4)	// Nothing left to eliminate.
//						return FALSE;
//				}
//			}
//			else if(bparity & 0x01){ // Black parity is wrong.
//				for(i = 0; i < 6; i+=2)
//					if(digitizedarray[i] <= eliminate){
//						digitizedarray[i]++;
//						diff--;
//						bparity++;
//						break;
//					}
//				if(diff){
//					eliminate++;
//					if(eliminate == 4)	// Nothing left to eliminate.
//						return FALSE;
//				}
//			} else if(!(wparity & 0x01)) { // White parity is wrong.
//				for(i = 1; i <= 5; i+=2)
//					if(digitizedarray[i] <= eliminate) {
//						digitizedarray[i]++;
//						diff--;
//						wparity++;
//						break;
//					}
//				if(diff){
//					eliminate++;
//					if(eliminate == 4)	// Nothing left to eliminate.
//						return FALSE;
//				}
//			} else	// Both parities are correct, but the diff is odd. Can't do anything.
//				return FALSE;
//		}
//	}
//	return TRUE;
//}




UINT16 get_bar_width(UINT16 *lead_edge, UINT16 *trail_edge)
{

	if (*trail_edge > *lead_edge)
	{
		return (*trail_edge - *lead_edge);
	}
	else
	{
		return (((0xFFFFU - *lead_edge) + *trail_edge) + 1U);
	}

}

//unsigned long int convert_to_code(BYTE *bar_array)
//{
//	if (!(bar_array[6])) 
//		return (bar_array[0] * 100000UL + bar_array[1] * 10000UL + bar_array[2] * 1000UL + bar_array[3] * 100UL + bar_array[4] * 10UL + bar_array[5];
//	else
//		return (bar_array[0] * 1000000UL + bar_array[1] * 100000UL + bar_array[2] * 10000UL + bar_array[3] * 1000UL + bar_array[4] * 100UL + bar_array[5] * 10UL + bar_array[6];
//}

void prn_num ( BYTE *num)
{

	putch( ((*num/10) + '0'));

	putch( ((*num%10) + '0'));

	return;
}

void prn_string (const char *string)
{
	for( ; *string; ++string) putch(*string);
	return;
}

