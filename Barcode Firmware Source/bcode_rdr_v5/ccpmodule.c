/******************************************************************************
*
* Driver Code for the timers
*
******************************************************************************/
#include <pic.h>
#include "GenericTypeDefs.h"
#include "mchp_support.h"

#include "ccpmodule.h"

void init_ccp3(void)
{

/* Initialize Capture Compare Machine 3 */

/* Initialize to capture on every falling edge, state machine will change as needed   */

	CCP3IE = 0;			//Ensure interrupt is disabled
	CCP3SEL = 1;		//CCP2 on RB3 (pin 11)
	CCP3CON  = 0x04;	//Capture mode on every falling edge
	CCP3IF = 0;			//Clear any potential spurious interrupt
	CCP3IE = 1;			//Re-enable interrupt

}

void init_ccp1(void)
{

/* Initialize Capture Compare Machine 1 */

/* Initialize to capture on every falling edge, state machine will change as needed   */

	CCP1IE = 0;			//Ensure interrupt is disabled
	CCP1CON  = 0x04;	//Capture mode on every falling edge
	CCP1IF = 0;			//Clear any potential spurious interrupt
	CCP1IE = 1;			//Re-enable interrupt

}