/******** State Machine for Reader 2 that runs when an edge occurs ***********/

// Capture occurred, update state machine
	if (CCP1IF && CCP1IE)
	{
		CCP1IF = 0;					// Clear the capture flag

		switch(rdr2_state)
		{
			case WAIT_LOW:			// Signal has gone low, set state timer and wait for quiet period
				//printf("3");
				rdr2_state_timer = STATE_DELAY;
				rdr2_state = WAIT_QUIET;
				RDR2_LOOK_RISING();
				break;
			case WAIT_QUIET:
				//printf("4");
				rdr2_state = WAIT_LOW;
				RDR2_LOOK_FALLING();
				break;
			case GET_FIRST_SYMBOL:
				//printf("9");

				rdr2_state_timer = STATE_DELAY;						// Reload state timer

				if (rdr2_first_edge)
				{
					rdr2_ledge_counter = CCPR1H;					// Get high byte
					rdr2_ledge_counter = rdr2_ledge_counter <<8;  	// shift into the high byte
					rdr2_ledge_counter |= CCPR1L;					// Or in the low byte
					rdr2_first_edge = FALSE;
					rdr2_byte_count = 0;
					rdr2_state_timer = STATE_DELAY;
					RDR2_SWAP_EDGE();	//Look at the opposite edge with the capture machine
				}
				else
				{
					rdr2_tedge_counter = CCPR1H;					// Get high byte
					rdr2_tedge_counter = rdr2_tedge_counter <<8;  	// shift into the high byte
					rdr2_tedge_counter |= CCPR1L;					// Or in the low byte

					if (rdr2_tedge_counter > rdr2_ledge_counter)
					{
						rdr2_bar_width = rdr2_tedge_counter - rdr2_ledge_counter;
						//printf (" !=%u ", rdr2_bar_width);
					}
					else
					{
						rdr2_bar_width = ((0xFFFFU - rdr2_ledge_counter) + rdr2_tedge_counter) + 1U;
						//printf (" ?=%u ", rdr2_bar_width);
					}

					switch(rdr2_bar_count)
					{
						case 1:
							update_black_bar(2U);
							rdr2_current_code = 200000UL;		// we define first bar to be width 2
							rdr2_bar_count++;
							break;
						case 2:
							update_white_bar(1U);
							rdr2_current_code = rdr2_current_code + 10000UL;	// we define the second bar to be width 1
							rdr2_bar_count++;
							break;
						case 3:
							rdr2_current_code = rdr2_current_code + 1000 * bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
							rdr2_bar_count++;
							break;
						case 4:
							rdr2_current_code = rdr2_current_code + 100 * bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_bar_count++;
							break;
						case 5:
							rdr2_current_code = rdr2_current_code + 10 * bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
							rdr2_bar_count++;
							break;
						case 6:
							rdr2_current_code = rdr2_current_code + bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							
							rdr2_rxdata = lookup_sym(rdr2_current_code);

							if (rdr2_rxdata == 105)		//Check for START_C symbol
							{
								rdr2_bcode_data[0] = rdr2_rxdata; //Put the START_C character in buffer
								rdr2_bar_count = 1;			// Initialize bar count and edge count
								if (rdr2_byte_count < 31) rdr2_byte_count++; //increment byte count
								rdr2_current_code = 0UL;	//zero out current code to get ready for next symbol
								rdr2_state_timer = STATE_DELAY;
								rdr2_state = GET_DATA;
							}
							else		// if we didn't get START_C, then start over
							{
								init_reader2();
							}
							break;
						case 7:	// future use
							break;
						default: // future use
							break;
					}
					RDR2_SWAP_EDGE();	//Look at the opposite edge with the capture machine
					rdr2_ledge_counter = rdr2_tedge_counter; // this trailing edge is the next leading edge
					rdr2_state_timer = STATE_DELAY;
				}
				break;
			case GET_DATA:
				rdr2_tedge_counter = CCPR1H;					// Get high byte
				rdr2_tedge_counter = rdr2_tedge_counter <<8;  	// shift into the high byte
				rdr2_tedge_counter |= CCPR1L;					// Or in the low byte

				rdr2_state_timer = STATE_DELAY;					// Reload state timer

				if (rdr2_tedge_counter > rdr2_ledge_counter)
				{
					rdr2_bar_width = rdr2_tedge_counter - rdr2_ledge_counter;
					//printf (" !=%u ", rdr2_bar_width);
				}
				else
				{
					rdr2_bar_width = (0xFFFF - rdr2_ledge_counter) + rdr2_tedge_counter;
					//printf (" ?=%u ", rdr2_bar_width);
				}

				switch(rdr2_bar_count)
				{
					case 1:
						rdr2_current_code = rdr2_current_code + 100000 * bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
						rdr2_bar_count++;
						break;
					case 2:
						rdr2_current_code = rdr2_current_code + 10000 * bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
						rdr2_bar_count++;
						break;
					case 3:
						rdr2_current_code = rdr2_current_code + 1000 * bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
						rdr2_bar_count++;
						break;
					case 4:
						rdr2_current_code = rdr2_current_code + 100 * bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
						rdr2_bar_count++;
						break;
					case 5:
						rdr2_current_code = rdr2_current_code + 10 * bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
						rdr2_bar_count++;
						break;
					case 6:
						rdr2_current_code = rdr2_current_code + bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);

						rdr2_rxdata = lookup_sym(rdr2_current_code);

						if (rdr2_rxdata != 155) // If lookup_sym on current_code returns 155 we have 6 of 7 bars of STOP code
						{		
							rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
							rdr2_bar_count =1;			// Initialize bar count
							if (rdr2_byte_count < 31) rdr2_byte_count++;			// icrement byte count
							rdr2_current_code = 0UL;	// Zero current code to get ready for next symbol
						}
						else if (rdr2_rxdata == 255) // Check to see if we got a bad symbol
						{
							rdr2_got_error = TRUE;
							init_reader2();
						}
						else
						{
							rdr2_current_code = rdr2_current_code * 10; // shift up one decimal place for 7th bar
							rdr2_bar_count++; 						    // increment bar count to 7
						}
						break;
					case 7:
						rdr2_current_code = rdr2_current_code + bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);

						rdr2_rxdata = lookup_sym(rdr2_current_code);

						if (rdr2_rxdata == 106)		// Look for STOP symbol
						{		
							rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
							rdr2_bar_count  = 1;		// Initialize bar count
							rdr2_byte_count = 0;		// Reset byte count
							
							if (checksum(rdr2_bcode_data)) rdr2_got_bcode = TRUE;
							init_reader2();
						}
						else	// Reset State Machine 
						{
							init_reader2();
						}
						break;
					default:
						break;
					}
					RDR2_SWAP_EDGE();	//Look at the opposite edge with the capture machine
					rdr2_ledge_counter = rdr2_tedge_counter; // this trailing edge is the next leading edge
					rdr2_state_timer = STATE_DELAY;
			default:
				break;
		} // Close from first SWITCH statement

	} // Close for Capture 2 Interrupt Flag