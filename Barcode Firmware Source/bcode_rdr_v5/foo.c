/******************************************************************************
*                 Reader 2 State Machine which runs in main loop              *
******************************************************************************/

		if( (rdr2_state == GET_BARS) && (rdr2_inpntr != rdr2_outpntr) )
		{
			rdr2_bar_width = rdr2_bars[rdr2_outpntr];
			if (rdr2_outpntr != 7) rdr2_outpntr++; else rdr2_outpntr = 0;

			if(!rdr2_got_start)
			{
				switch(rdr2_bar_count)
				{
					case 1:
						update_rdr2_black_bar(2U);
						rdr2_symbol_bars[0] = 2;
						rdr2_symbol_bars[6] = 0;			//Initialize last element to 0 - only non-zero on STOP symbol
						rdr2_black_bar_sum = rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 2:
						update_rdr2_white_bar(1U);
						rdr2_symbol_bars[1] = 1;
						rdr2_white_bar_sum = rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 3:
						rdr2_symbol_bars[2] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
						rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 4:
						rdr2_symbol_bars[3] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
						rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 5:
						rdr2_symbol_bars[4] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
						rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
						rdr2_bar_count++;
						break;
					case 6:
						rdr2_symbol_bars[5] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
						
						rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
						update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,&rdr2_bpos_tol,&rdr2_bneg_tol,&rdr2_wpos_tol,&rdr2_wneg_tol,rdr2_symbol_bars);

						rdr2_current_code = convert_to_code(rdr2_symbol_bars);	
						rdr2_rxdata = lookup_sym(rdr2_current_code);

						if (rdr2_rxdata == BAD_SYM)
						{
							if ( CheckParityFixErrors(rdr2_symbol_bars) );
							{
								rdr2_current_code = convert_to_code(rdr2_symbol_bars);  //Try to correct error
								rdr2_rxdata = lookup_sym(rdr2_current_code);
								update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,&rdr2_bpos_tol,&rdr2_bneg_tol,&rdr2_wpos_tol,&rdr2_wneg_tol,rdr2_symbol_bars);
							}
						}

						if (rdr2_rxdata == START_C)		//Check for START_C symbol
						{
							rdr2_bcode_data[0] = rdr2_rxdata; //Put the START_C character in buffer
							rdr2_bar_count = 1;			// Initialize bar count and edge count
							if (rdr2_byte_count < 23) rdr2_byte_count++; //increment byte count
							rdr2_current_code = 0UL;	//zero out current code to get ready for next symbol
							rdr2_got_start = TRUE;
							rdr2_bcode_tmr = BCODE_TIMEOUT;
						}
						else if (rdr2_rxdata == REV_STOP_6B) //Check for first 6 bars of REVERSED STOP
						{
							rdr2_current_code = rdr2_current_code * 10; // shift up one decimal place for 7th bar
							rdr2_bar_count++; 						    // increment bar count to 7
						}
						else		// if we didn't get START_C or first of reversed STOP, then start over
						{
							//rdr2_got_error = TRUE;  //Comment out so as not to generate an error on a bad symbol lookup
							//rdr2_reason = REASON_BADSTART; 
							//rdr2_bcode_data[rdr2_byte_count] = STOP; //Stick a STOP where the bad symbol occurred so we can print
							//rdr2_print_pos = 1; //Start printing at position 1 since going forward
							init_reader2();
						}
						break;
					case 7:	// Check to see if we've got a reversed STOP symbol, if so we're reading backwards
						rdr2_symbol_bars[6] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
						rdr2_current_code = convert_to_code(rdr2_symbol_bars);
						rdr2_rxdata = lookup_sym(rdr2_current_code);

						if (rdr2_rxdata == REV_STOP)  //Check to see if we've got REVERSED STOP
						{
							rdr2_bcode_data[23] = STOP;  //Put a STOP character in the buffer
							rdr2_bar_count = 1;			// Initialize bar count and edge count
							rdr2_byte_count = 22; 		//Place characters in buffer back to front
							rdr2_current_code = 0UL;	//zero out current code to get ready for next symbol
							rdr2_got_start = TRUE;
							rdr2_reverse = TRUE;		//Reading backwards
							rdr2_rev	 = TRUE;
							rdr2_bcode_tmr = BCODE_TIMEOUT;
						}
						else		// if we didn't get REVERSED STOP, then start over
						{
							init_reader2();
						}
						break;
					default:
						break;
				}
			}
			else
			{
				if (!rdr2_reverse)
/* Reading in the FORWARD direction */
				{
					switch(rdr2_bar_count)
					{
						case 1:
							rdr2_symbol_bars[0] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
							rdr2_symbol_bars[6] = 0;
							rdr2_black_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 2:
							rdr2_symbol_bars[1] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_white_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 3:
						case 5:
							rdr2_symbol_bars[rdr2_bar_count - 1] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
							rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 4:
							rdr2_symbol_bars[3] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 6:
							rdr2_symbol_bars[5] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;


							rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							rdr2_rxdata = lookup_sym(rdr2_current_code);

							if (rdr2_rxdata == BAD_SYM)
							{
								if ( CheckParityFixErrors(rdr2_symbol_bars) );
								{
									rdr2_current_code = convert_to_code(rdr2_symbol_bars);
									rdr2_rxdata = lookup_sym(rdr2_current_code);
									update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,&rdr2_bpos_tol,&rdr2_bneg_tol,&rdr2_wpos_tol,&rdr2_wneg_tol,rdr2_symbol_bars);
									//prn_string("!");
								}
							}
							else
								update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,&rdr2_bpos_tol,&rdr2_bneg_tol,&rdr2_wpos_tol,&rdr2_wneg_tol,rdr2_symbol_bars);

							if (rdr2_rxdata == STOP_6B) // check for first 6 of 7 bars of STOP code
							{
								rdr2_bar_count++; 						    // increment bar count to 7
							}
							else if (rdr2_rxdata == BAD_SYM) // Check to see if we got a bad symbol
							{
								rdr2_got_error = TRUE;  //Comment out so as not to generate an error on a bad symbol lookup
								rdr2_reason = REASON_BADLOOKUP; //MSS
								rdr2_bcode_data[rdr2_byte_count] = STOP; //Stick a STOP where the bad symbol occurred so we can print
								rdr2_print_pos = 1; //Start printing at position 1 since going forward
								init_reader2();
							}
							else
							{		
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
								rdr2_bar_count =1;			// Initialize bar count
								if (rdr2_byte_count < 23) rdr2_byte_count++;			// icrement byte count
								rdr2_current_code = 0UL;	// Zero current code to get ready for next symbol
							}
							break;
						case 7:
							rdr2_symbol_bars[6] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);

							rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							rdr2_rxdata = lookup_sym(rdr2_current_code);

							if (rdr2_rxdata == STOP)		// Look for STOP symbol
							{		
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
								rdr2_print_pos = 1 ;
								rdr2_bar_count  = 1;		// Initialize bar count
								rdr2_byte_count = 0;		// Reset byte count
							
								if (checksum(rdr2_bcode_data, (BYTE)0))
								{
									rdr2_got_bcode = TRUE;
									init_reader2();
								}
								else
								{
									rdr2_got_error = TRUE;
									rdr2_reason = REASON_BADCHCKSUM;	// Checksum failed
									rdr2_print_pos = 1;	
									init_reader2();
								}
							}
							else	// Reset State Machine 
							{
								rdr2_got_error = TRUE;
								rdr2_reason = REASON_BADSTOP;		// Bad STOP Symbol
								rdr2_bcode_data[rdr2_byte_count] = STOP; //Stick in a STOP byte so we can print it later
								rdr2_print_pos = 1;
								init_reader2();
							}
							break;
						default:
							break;
					}
				}
				else
/* Reading in the REVERSE Direction */
				{
					switch(rdr2_bar_count)
					{
						case 1:
							rdr2_symbol_bars[5] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_symbol_bars[6] = 0;
							rdr2_white_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 2:
							rdr2_symbol_bars[4] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
							rdr2_black_bar_sum = rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 3:
							rdr2_symbol_bars[3] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 4:
							rdr2_symbol_bars[2] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);							
							rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 5:
							rdr2_symbol_bars[1] = bar_decision(&rdr2_bar_width, &rdr2_white_bar_unit, &rdr2_wpos_tol, &rdr2_wneg_tol);
							rdr2_white_bar_sum = rdr2_white_bar_sum + rdr2_bar_width;
							rdr2_bar_count++;
							break;
						case 6:
							rdr2_symbol_bars[0] = bar_decision(&rdr2_bar_width, &rdr2_black_bar_unit, &rdr2_bpos_tol, &rdr2_bneg_tol);
							rdr2_black_bar_sum = rdr2_black_bar_sum + rdr2_bar_width;

							rdr2_current_code = convert_to_code(rdr2_symbol_bars);
							rdr2_rxdata = lookup_sym(rdr2_current_code);

							if (rdr2_rxdata == BAD_SYM)
							{
								if ( CheckParityFixErrors(rdr2_symbol_bars) );
								{
									rdr2_current_code = convert_to_code(rdr2_symbol_bars);
									rdr2_rxdata = lookup_sym(rdr2_current_code);
									update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,&rdr2_bpos_tol,&rdr2_bneg_tol,&rdr2_wpos_tol,&rdr2_wneg_tol,rdr2_symbol_bars);
									//prn_string("!");
								}
							}
							else
								update_widths(&rdr2_black_bar_unit,&rdr2_white_bar_unit,&rdr2_black_bar_sum,&rdr2_white_bar_sum,&rdr2_bpos_tol,&rdr2_bneg_tol,&rdr2_wpos_tol,&rdr2_wneg_tol,rdr2_symbol_bars);

							if (rdr2_rxdata == START_C) // Look for START_C symbol
							{
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
							
								if (checksum(rdr2_bcode_data, rdr2_byte_count))
								{
									rdr2_got_bcode = TRUE;
									rdr2_print_pos = rdr2_byte_count + 1;
									rdr2_bar_count  = 1;		// Initialize bar count
									rdr2_byte_count = 0;		// Reset byte count
									init_reader2();
								}
								else
								{
									rdr2_got_error  = TRUE;
									rdr2_reason = REASON_BADCHCKSUM; //Checksum failed
									rdr2_print_pos = rdr2_byte_count + 1;
									rdr2_bar_count  = 1;
									rdr2_byte_count = 0;
									init_reader2();
								}
							}
							else if (rdr2_rxdata == BAD_SYM) // Check to see if we got a bad symbol
							{
								rdr2_got_error = TRUE;
								rdr2_reason	= REASON_BADLOOKUP;
								rdr2_bcode_data[rdr2_byte_count] = START_C; //Stick a START where the bad symbol occurred so we can print
								rdr2_print_pos = rdr2_byte_count + 1;
								init_reader2();
							}
							else
							{		
								rdr2_bcode_data[rdr2_byte_count] = rdr2_rxdata;  //store the data we just got
								rdr2_bar_count =1;			// Initialize bar count
								if (rdr2_byte_count != 0) rdr2_byte_count--;			// decrement byte count
								rdr2_current_code = 0UL;	// Zero current code to get ready for next symbol
							}
							break;
						default:
							break;
					}
				}
				
			}	// Close for if != got_start
		}	// Close for rdr2_inpntr != rdr2_outpntr