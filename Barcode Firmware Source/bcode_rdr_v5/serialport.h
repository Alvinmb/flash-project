/******************************************************************************
*
* Include file for serial port driver 
*
******************************************************************************/

void init_serialport(void);
void putch(unsigned char txchar);