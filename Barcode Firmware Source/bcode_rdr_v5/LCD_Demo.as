opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 10920"

opt pagewidth 120

	opt pm

	processor	16LF1937
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
indf1	equ	1
pc	equ	2
pcl	equ	2
status	equ	3
fsr0l	equ	4
fsr0h	equ	5
fsr1l	equ	6
fsr1h	equ	7
bsr	equ	8
wreg	equ	9
intcon	equ	11
c	equ	1
z	equ	0
pclath	equ	10
# 46 "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 46 "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	dw 0x3FE4 ;#
# 47 "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 47 "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	dw 0x1EFF ;#
	FNCALL	_main,_init_timer1
	FNCALL	_main,_init_serialport
	FNCALL	_main,_init_ccp2
	FNCALL	_main,_init_ccp1
	FNCALL	_main,_printf
	FNCALL	_main,_init_reader1
	FNCALL	_main,_init_reader2
	FNCALL	_printf,_putch
	FNCALL	_printf,_isdigit
	FNCALL	_printf,___wmul
	FNCALL	_printf,___bmul
	FNCALL	_printf,___lwdiv
	FNCALL	_printf,___lwmod
	FNROOT	_main
	FNCALL	_isr,_update_black_bar
	FNCALL	_isr,_update_white_bar
	FNCALL	_isr,_bar_decision
	FNCALL	_isr,i1___wmul
	FNCALL	_isr,_lookup_sym
	FNCALL	_isr,i1_init_reader1
	FNCALL	_isr,___lmul
	FNCALL	_isr,_checksum
	FNCALL	_checksum,i1___wmul
	FNCALL	_checksum,i1___lwmod
	FNCALL	_bar_decision,i1___wmul
	FNCALL	_bar_decision,i1___lwdiv
	FNCALL	_update_white_bar,i1___lwdiv
	FNCALL	_update_white_bar,i1___wmul
	FNCALL	_update_black_bar,i1___lwdiv
	FNCALL	_update_black_bar,i1___wmul
	FNCALL	intlevel1,_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_dpowers
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
	global    __stringtab
__stringtab:
	retlw	0
psect	strings
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\lib\doprnt.c"
	line	350
_dpowers:
	retlw	01h
	retlw	0

	retlw	0Ah
	retlw	0

	retlw	064h
	retlw	0

	retlw	0E8h
	retlw	03h

	retlw	010h
	retlw	027h

	global	_dpowers
	global	_rdr1_ledge_counter
	global	_rdr1_tedge_counter
	global	_i
	global	_rdr1_bar_count
	global	_rdr1_byte_count
	global	_rdr1_first_edge
	global	_rdr1_got_bcode
	global	_rdr1_got_error
	global	_rdr1_rxdata
	global	_rdr1_state
	global	_rdr1_state_timer
	global	_rdr2_got_bcode
	global	_rdr2_state_timer
	global	_timebase_10ms
	global	_rdr2_bcode_data
	global	_rdr1_current_code
	global	_rdr1_black_bar_unit
	global	_rdr1_bneg_tol
	global	_rdr1_bpos_tol
	global	_rdr1_white_bar_unit
	global	_rdr1_wneg_tol
	global	_rdr1_wpos_tol
	global	_rdr1_bar_width
	global	_rdr1_bcode_data
	global	_PORTC
_PORTC	set	14
	global	_PORTD
_PORTD	set	15
	global	_PORTE
_PORTE	set	16
	global	_T1CON
_T1CON	set	24
	global	_T1GCON
_T1GCON	set	25
	global	_CCP1IF
_CCP1IF	set	138
	global	_CCP2IF
_CCP2IF	set	144
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_RA1
_RA1	set	97
	global	_RB3
_RB3	set	107
	global	_RC2
_RC2	set	114
	global	_T0IF
_T0IF	set	90
	global	_TMR0IE
_TMR0IE	set	93
	global	_TMR1IF
_TMR1IF	set	136
	global	_TMR1ON
_TMR1ON	set	192
	global	_OPTION_REG
_OPTION_REG	set	149
	global	_OSCCONbits
_OSCCONbits	set	153
	global	_CCP1IE
_CCP1IE	set	1162
	global	_CCP2IE
_CCP2IE	set	1168
	global	_LCDIE
_LCDIE	set	1170
	global	_SPLLEN
_SPLLEN	set	1231
	global	_TMR1IE
_TMR1IE	set	1160
	global	_TRISA1
_TRISA1	set	1121
	global	_TRISB3
_TRISB3	set	1131
	global	_TRISC2
_TRISC2	set	1138
	global	_TRISD1
_TRISD1	set	1145
	global	_TRISD5
_TRISD5	set	1149
	global	_TRISD6
_TRISD6	set	1150
	global	_TRISE0
_TRISE0	set	1152
	global	_TRISE1
_TRISE1	set	1153
	global	_TRISE2
_TRISE2	set	1154
	global	_CCP2SEL
_CCP2SEL	set	2280
	global	_SPBRG
_SPBRG	set	411
	global	_TXREG
_TXREG	set	410
	global	_ABDEN
_ABDEN	set	3320
	global	_ANSA1
_ANSA1	set	3169
	global	_ANSA3
_ANSA3	set	3171
	global	_ANSB1
_ANSB1	set	3177
	global	_ANSB3
_ANSB3	set	3179
	global	_BRGH
_BRGH	set	3314
	global	_RX9
_RX9	set	3310
	global	_SPEN
_SPEN	set	3311
	global	_SYNC
_SYNC	set	3316
	global	_TRMT
_TRMT	set	3313
	global	_TX9
_TX9	set	3318
	global	_TXEN
_TXEN	set	3317
	global	_WPUB3
_WPUB3	set	4203
	global	_CCP1CON
_CCP1CON	set	659
	global	_CCP2CON
_CCP2CON	set	666
	global	_CCPR2H
_CCPR2H	set	665
	global	_CCPR2L
_CCPR2L	set	664
	global	_CCP2M0
_CCP2M0	set	5328
	global	_delay
psect	nvBANK1,class=BANK1,space=1
global __pnvBANK1
__pnvBANK1:
_delay:
       ds      2

psect	strings
	
STR_5:	
	retlw	82	;'R'
	retlw	68	;'D'
	retlw	82	;'R'
	retlw	49	;'1'
	retlw	33	;'!'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	114	;'r'
	retlw	111	;'o'
	retlw	114	;'r'
	retlw	13
	retlw	10
	retlw	0
psect	strings
	
STR_2:	
	retlw	82	;'R'
	retlw	68	;'D'
	retlw	82	;'R'
	retlw	49	;'1'
	retlw	33	;'!'
	retlw	0
psect	strings
	
STR_6:	
	retlw	82	;'R'
	retlw	68	;'D'
	retlw	82	;'R'
	retlw	50	;'2'
	retlw	33	;'!'
	retlw	0
psect	strings
	
STR_3:	
	retlw	37	;'%'
	retlw	50	;'2'
	retlw	46	;'.'
	retlw	50	;'2'
	retlw	117	;'u'
	retlw	0
psect	strings
	
STR_1:	
	retlw	79	;'O'
	retlw	75	;'K'
	retlw	13
	retlw	10
	retlw	0
psect	strings
STR_7	equ	STR_3+0
STR_4	equ	STR_1+0
STR_8	equ	STR_1+0
	file	"LCD_Demo.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_rdr1_bar_width:
       ds      2

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_rdr1_ledge_counter:
       ds      2

_rdr1_tedge_counter:
       ds      2

_i:
       ds      1

_rdr1_bar_count:
       ds      1

_rdr1_byte_count:
       ds      1

_rdr1_first_edge:
       ds      1

_rdr1_got_bcode:
       ds      1

_rdr1_got_error:
       ds      1

_rdr1_rxdata:
       ds      1

_rdr1_state:
       ds      1

_rdr1_state_timer:
       ds      1

_rdr2_got_bcode:
       ds      1

_rdr2_state_timer:
       ds      1

_timebase_10ms:
       ds      1

_rdr1_black_bar_unit:
       ds      2

_rdr1_bneg_tol:
       ds      2

_rdr1_bpos_tol:
       ds      2

_rdr1_white_bar_unit:
       ds      2

_rdr1_wneg_tol:
       ds      2

_rdr1_wpos_tol:
       ds      2

psect	bssBANK1,class=BANK1,space=1
global __pbssBANK1
__pbssBANK1:
_rdr2_bcode_data:
       ds      32

_rdr1_current_code:
       ds      4

_rdr1_bcode_data:
       ds      32

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR0 containing the base address, and
;	WREG with the size to clear
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf0		;clear RAM location pointed to by FSR
	addfsr	0,1
	decfsz wreg		;Have we reached the end of clearing yet?
	goto clrloop	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	global __pbssCOMMON
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	global __pbssBANK0
	movlw	low(__pbssBANK0)
	movwf	fsr0l
	movlw	high(__pbssBANK0)
	movwf	fsr0h
	movlw	01Ch
	fcall	clear_ram
; Clear objects allocated to BANK1
psect cinit,class=CODE,delta=2
	global __pbssBANK1
	movlw	low(__pbssBANK1)
	movwf	fsr0l
	movlw	high(__pbssBANK1)
	movwf	fsr0h
	movlw	044h
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
movlb 0
ljmp _main	;jump to C main() function
psect	cstackBANK2,class=BANK2,space=1
global __pcstackBANK2
__pcstackBANK2:
	global	printf@ap
printf@ap:	; 1 bytes @ 0x0
	ds	1
	global	printf@_val
printf@_val:	; 4 bytes @ 0x1
	ds	4
	global	printf@width
printf@width:	; 2 bytes @ 0x5
	ds	2
	global	printf@prec
printf@prec:	; 1 bytes @ 0x7
	ds	1
	global	printf@f
printf@f:	; 1 bytes @ 0x8
	ds	1
	global	printf@c
printf@c:	; 1 bytes @ 0x9
	ds	1
psect	cstackBANK1,class=BANK1,space=1
global __pcstackBANK1
__pcstackBANK1:
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x0
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x0
	ds	2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x2
	ds	2
	global	?_printf
?_printf:	; 2 bytes @ 0x4
	ds	2
	global	??_printf
??_printf:	; 0 bytes @ 0x6
	ds	4
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init_timer1
?_init_timer1:	; 0 bytes @ 0x0
	global	?_init_serialport
?_init_serialport:	; 0 bytes @ 0x0
	global	?_init_ccp2
?_init_ccp2:	; 0 bytes @ 0x0
	global	?_init_ccp1
?_init_ccp1:	; 0 bytes @ 0x0
	global	?_init_reader1
?_init_reader1:	; 0 bytes @ 0x0
	global	?_init_reader2
?_init_reader2:	; 0 bytes @ 0x0
	global	?_isdigit
?_isdigit:	; 1 bit 
	global	?_isr
?_isr:	; 0 bytes @ 0x0
	global	?_putch
?_putch:	; 0 bytes @ 0x0
	global	?i1_init_reader1
?i1_init_reader1:	; 0 bytes @ 0x0
	global	??i1_init_reader1
??i1_init_reader1:	; 0 bytes @ 0x0
	global	?_lookup_sym
?_lookup_sym:	; 1 bytes @ 0x0
	global	?_checksum
?_checksum:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	?i1___wmul
?i1___wmul:	; 2 bytes @ 0x0
	global	?i1___lwdiv
?i1___lwdiv:	; 2 bytes @ 0x0
	global	?i1___lwmod
?i1___lwmod:	; 2 bytes @ 0x0
	global	i1___wmul@multiplier
i1___wmul@multiplier:	; 2 bytes @ 0x0
	global	i1___lwdiv@divisor
i1___lwdiv@divisor:	; 2 bytes @ 0x0
	global	i1___lwmod@divisor
i1___lwmod@divisor:	; 2 bytes @ 0x0
	global	lookup_sym@symbol
lookup_sym@symbol:	; 4 bytes @ 0x0
	ds	2
	global	i1___wmul@multiplicand
i1___wmul@multiplicand:	; 2 bytes @ 0x2
	global	i1___lwdiv@dividend
i1___lwdiv@dividend:	; 2 bytes @ 0x2
	global	i1___lwmod@dividend
i1___lwmod@dividend:	; 2 bytes @ 0x2
	ds	2
	global	??_lookup_sym
??_lookup_sym:	; 0 bytes @ 0x4
	global	??i1___wmul
??i1___wmul:	; 0 bytes @ 0x4
	global	??i1___lwdiv
??i1___lwdiv:	; 0 bytes @ 0x4
	global	??i1___lwmod
??i1___lwmod:	; 0 bytes @ 0x4
	global	lookup_sym@retvalue
lookup_sym@retvalue:	; 1 bytes @ 0x4
	global	i1___lwdiv@counter
i1___lwdiv@counter:	; 1 bytes @ 0x4
	global	i1___lwmod@counter
i1___lwmod@counter:	; 1 bytes @ 0x4
	global	i1___wmul@product
i1___wmul@product:	; 2 bytes @ 0x4
	ds	1
	global	i1___lwdiv@quotient
i1___lwdiv@quotient:	; 2 bytes @ 0x5
	ds	1
	global	??_checksum
??_checksum:	; 0 bytes @ 0x6
	ds	1
	global	?_update_black_bar
?_update_black_bar:	; 0 bytes @ 0x7
	global	?_update_white_bar
?_update_white_bar:	; 0 bytes @ 0x7
	global	??___lmul
??___lmul:	; 0 bytes @ 0x7
	global	update_black_bar@barwidth
update_black_bar@barwidth:	; 2 bytes @ 0x7
	global	update_white_bar@barwidth
update_white_bar@barwidth:	; 2 bytes @ 0x7
	global	checksum@sum
checksum@sum:	; 2 bytes @ 0x7
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x7
	ds	2
	global	??_update_black_bar
??_update_black_bar:	; 0 bytes @ 0x9
	global	??_update_white_bar
??_update_white_bar:	; 0 bytes @ 0x9
	global	checksum@bcode
checksum@bcode:	; 1 bytes @ 0x9
	ds	1
	global	checksum@i
checksum@i:	; 1 bytes @ 0xA
	ds	1
	global	bar_decision@retvalue
bar_decision@retvalue:	; 1 bytes @ 0xB
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	ds	4
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	ds	4
	global	?_bar_decision
?_bar_decision:	; 1 bytes @ 0x8
	global	bar_decision@bar_width
bar_decision@bar_width:	; 1 bytes @ 0x8
	ds	1
	global	bar_decision@bar_unit
bar_decision@bar_unit:	; 1 bytes @ 0x9
	ds	1
	global	bar_decision@pos_tol
bar_decision@pos_tol:	; 1 bytes @ 0xA
	ds	1
	global	bar_decision@neg_tol
bar_decision@neg_tol:	; 1 bytes @ 0xB
	ds	1
	global	??_bar_decision
??_bar_decision:	; 0 bytes @ 0xC
	ds	6
	global	??_isr
??_isr:	; 0 bytes @ 0x12
	ds	4
	global	??_init_timer1
??_init_timer1:	; 0 bytes @ 0x16
	global	??_init_serialport
??_init_serialport:	; 0 bytes @ 0x16
	global	??_init_ccp2
??_init_ccp2:	; 0 bytes @ 0x16
	global	??_init_ccp1
??_init_ccp1:	; 0 bytes @ 0x16
	global	??_init_reader1
??_init_reader1:	; 0 bytes @ 0x16
	global	??_init_reader2
??_init_reader2:	; 0 bytes @ 0x16
	global	??_isdigit
??_isdigit:	; 0 bytes @ 0x16
	global	??_putch
??_putch:	; 0 bytes @ 0x16
	global	?___bmul
?___bmul:	; 1 bytes @ 0x16
	global	?___wmul
?___wmul:	; 2 bytes @ 0x16
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x16
	global	putch@txchar
putch@txchar:	; 1 bytes @ 0x16
	global	_isdigit$11171
_isdigit$11171:	; 1 bytes @ 0x16
	global	___bmul@multiplicand
___bmul@multiplicand:	; 1 bytes @ 0x16
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x16
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x16
	ds	1
	global	??___bmul
??___bmul:	; 0 bytes @ 0x17
	global	isdigit@c
isdigit@c:	; 1 bytes @ 0x17
	global	___bmul@product
___bmul@product:	; 1 bytes @ 0x17
	ds	1
	global	___bmul@multiplier
___bmul@multiplier:	; 1 bytes @ 0x18
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x18
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x18
	ds	2
	global	??___wmul
??___wmul:	; 0 bytes @ 0x1A
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x1A
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x1A
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x1A
	ds	1
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x1B
	ds	2
	global	??___lwmod
??___lwmod:	; 0 bytes @ 0x1D
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0x1D
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x1E
;;Data sizes: Strings 36, constant 10, data 0, bss 98, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     12      14
;; BANK0           80     30      58
;; BANK1           80     10      80
;; BANK2           80     10      10
;; BANK3           80      0       0
;; BANK4           80      0       0
;; BANK5           80      0       0
;; BANK6            5      0       0

;;
;; Pointer list with targets:

;; ?i1___lwmod	unsigned int  size(1) Largest target is 0
;;
;; ?i1___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?i1___wmul	unsigned int  size(1) Largest target is 0
;;
;; ?___lwmod	unsigned int  size(1) Largest target is 0
;;
;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?___wmul	unsigned int  size(1) Largest target is 0
;;
;; printf@f	PTR const unsigned char  size(1) Largest target is 13
;;		 -> STR_8(CODE[5]), STR_7(CODE[6]), STR_6(CODE[6]), STR_5(CODE[13]), 
;;		 -> STR_4(CODE[5]), STR_3(CODE[6]), STR_2(CODE[6]), STR_1(CODE[5]), 
;;
;; ?_printf	int  size(1) Largest target is 0
;;
;; printf@ap	PTR void [1] size(1) Largest target is 2
;;		 -> ?_printf(BANK1[2]), 
;;
;; S10216$_cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; _val._str._cp	PTR const unsigned char  size(1) Largest target is 0
;;
;; bar_decision@neg_tol	PTR unsigned short  size(1) Largest target is 2
;;		 -> rdr1_wneg_tol(BANK0[2]), rdr1_bneg_tol(BANK0[2]), 
;;
;; bar_decision@bar_width	PTR unsigned short  size(1) Largest target is 2
;;		 -> rdr1_bar_width(COMMON[2]), 
;;
;; bar_decision@pos_tol	PTR unsigned short  size(1) Largest target is 2
;;		 -> rdr1_wpos_tol(BANK0[2]), rdr1_bpos_tol(BANK0[2]), 
;;
;; bar_decision@bar_unit	PTR unsigned short  size(1) Largest target is 2
;;		 -> rdr1_white_bar_unit(BANK0[2]), rdr1_black_bar_unit(BANK0[2]), 
;;
;; checksum@bcode	PTR unsigned char  size(1) Largest target is 32
;;		 -> rdr1_bcode_data(BANK1[32]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _isr in COMMON
;;
;;   _isr->_bar_decision
;;   _checksum->i1___wmul
;;   _bar_decision->___lmul
;;   _update_white_bar->i1___lwdiv
;;   _update_black_bar->i1___lwdiv
;;   ___lmul->i1___lwdiv
;;
;; Critical Paths under _main in BANK0
;;
;;   _printf->___lwmod
;;   ___lwmod->___lwdiv
;;
;; Critical Paths under _isr in BANK0
;;
;;   _isr->_bar_decision
;;   _bar_decision->___lmul
;;
;; Critical Paths under _main in BANK1
;;
;;   _main->_printf
;;   _printf->___lwmod
;;
;; Critical Paths under _isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   _main->_printf
;;
;; Critical Paths under _isr in BANK2
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _isr in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK4
;;
;;   None.
;;
;; Critical Paths under _isr in BANK4
;;
;;   None.
;;
;; Critical Paths under _main in BANK5
;;
;;   None.
;;
;; Critical Paths under _isr in BANK5
;;
;;   None.
;;
;; Critical Paths under _main in BANK6
;;
;;   None.
;;
;; Critical Paths under _isr in BANK6
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 0     0      0    1995
;;                        _init_timer1
;;                    _init_serialport
;;                          _init_ccp2
;;                          _init_ccp1
;;                             _printf
;;                       _init_reader1
;;                       _init_reader2
;; ---------------------------------------------------------------------------------
;; (1) _printf                                              17    15      2    1995
;;                                              4 BANK1      6     4      2
;;                                              0 BANK2     10    10      0
;;                              _putch
;;                            _isdigit
;;                             ___wmul
;;                             ___bmul
;;                            ___lwdiv
;;                            ___lwmod
;; ---------------------------------------------------------------------------------
;; (2) ___lwmod                                              5     1      4     232
;;                                             29 BANK0      1     1      0
;;                                              0 BANK1      4     0      4
;;                            ___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (2) ___lwdiv                                              7     3      4     241
;;                                             22 BANK0      7     3      4
;; ---------------------------------------------------------------------------------
;; (2) ___wmul                                               6     2      4     136
;;                                             22 BANK0      6     2      4
;; ---------------------------------------------------------------------------------
;; (2) ___bmul                                               3     2      1     136
;;                                             22 BANK0      3     2      1
;; ---------------------------------------------------------------------------------
;; (2) _isdigit                                              2     2      0      99
;;                                             22 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (2) _putch                                                1     1      0      31
;;                                             22 BANK0      1     1      0
;; ---------------------------------------------------------------------------------
;; (1) _init_reader2                                         0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init_reader1                                         0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init_ccp1                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init_ccp2                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init_serialport                                      0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init_timer1                                          0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _isr                                                  4     4      0    4955
;;                                             18 BANK0      4     4      0
;;                   _update_black_bar
;;                   _update_white_bar
;;                       _bar_decision
;;                           i1___wmul
;;                         _lookup_sym
;;                     i1_init_reader1
;;                             ___lmul
;;                           _checksum
;; ---------------------------------------------------------------------------------
;; (4) _checksum                                             6     6      0    1023
;;                                              6 COMMON     5     5      0
;;                           i1___wmul
;;                          i1___lwmod
;; ---------------------------------------------------------------------------------
;; (4) _bar_decision                                        11     7      4    1689
;;                                             11 COMMON     1     1      0
;;                                              8 BANK0     10     6      4
;;                           i1___wmul
;;                          i1___lwdiv
;;                             ___lmul (ARG)
;; ---------------------------------------------------------------------------------
;; (4) _update_white_bar                                     2     0      2     849
;;                                              7 COMMON     2     0      2
;;                          i1___lwdiv
;;                           i1___wmul
;; ---------------------------------------------------------------------------------
;; (4) _update_black_bar                                     2     0      2     849
;;                                              7 COMMON     2     0      2
;;                          i1___lwdiv
;;                           i1___wmul
;; ---------------------------------------------------------------------------------
;; (5) i1___lwmod                                            5     1      4     521
;;                                              0 COMMON     5     1      4
;; ---------------------------------------------------------------------------------
;; (5) i1___lwdiv                                            7     3      4     527
;;                                              0 COMMON     7     3      4
;; ---------------------------------------------------------------------------------
;; (5) i1___wmul                                             6     2      4     300
;;                                              0 COMMON     6     2      4
;; ---------------------------------------------------------------------------------
;; (4) i1_init_reader1                                       0     0      0       0
;; ---------------------------------------------------------------------------------
;; (4) ___lmul                                              12     4      8      92
;;                                              7 COMMON     4     4      0
;;                                              0 BANK0      8     0      8
;;                           i1___wmul (ARG)
;;                          i1___lwdiv (ARG)
;; ---------------------------------------------------------------------------------
;; (4) _lookup_sym                                           5     1      4     153
;;                                              0 COMMON     5     1      4
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 5
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init_timer1
;;   _init_serialport
;;   _init_ccp2
;;   _init_ccp1
;;   _printf
;;     _putch
;;     _isdigit
;;     ___wmul
;;     ___bmul
;;     ___lwdiv
;;     ___lwmod
;;       ___lwdiv (ARG)
;;   _init_reader1
;;   _init_reader2
;;
;; _isr (ROOT)
;;   _update_black_bar
;;     i1___lwdiv
;;     i1___wmul
;;   _update_white_bar
;;     i1___lwdiv
;;     i1___wmul
;;   _bar_decision
;;     i1___wmul
;;     i1___lwdiv
;;     ___lmul (ARG)
;;       i1___wmul (ARG)
;;       i1___lwdiv (ARG)
;;   i1___wmul
;;   _lookup_sym
;;   i1_init_reader1
;;   ___lmul
;;     i1___wmul (ARG)
;;     i1___lwdiv (ARG)
;;   _checksum
;;     i1___wmul
;;     i1___lwmod
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BIGRAM             1F0      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;BITCOMMON            E      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;COMMON               E      C       E       2      100.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR2              0      0       0       3        0.0%
;;SFR2                 0      0       0       3        0.0%
;;STACK                0      0       8       3        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0      A2       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR4              0      0       0       5        0.0%
;;SFR4                 0      0       0       5        0.0%
;;BANK0               50     1E      3A       6       72.5%
;;BITSFR5              0      0       0       6        0.0%
;;SFR5                 0      0       0       6        0.0%
;;BITBANK1            50      0       0       7        0.0%
;;BITSFR6              0      0       0       7        0.0%
;;SFR6                 0      0       0       7        0.0%
;;BANK1               50      A      50       8      100.0%
;;BITSFR7              0      0       0       8        0.0%
;;SFR7                 0      0       0       8        0.0%
;;BITBANK2            50      0       0       9        0.0%
;;BITSFR8              0      0       0       9        0.0%
;;SFR8                 0      0       0       9        0.0%
;;BANK2               50      A       A      10       12.5%
;;BITSFR9              0      0       0      10        0.0%
;;SFR9                 0      0       0      10        0.0%
;;BITBANK3            50      0       0      11        0.0%
;;BITSFR10             0      0       0      11        0.0%
;;SFR10                0      0       0      11        0.0%
;;BANK3               50      0       0      12        0.0%
;;BITSFR11             0      0       0      12        0.0%
;;SFR11                0      0       0      12        0.0%
;;BITBANK4            50      0       0      13        0.0%
;;BITSFR12             0      0       0      13        0.0%
;;SFR12                0      0       0      13        0.0%
;;BANK4               50      0       0      14        0.0%
;;BITSFR13             0      0       0      14        0.0%
;;SFR13                0      0       0      14        0.0%
;;BITBANK5            50      0       0      15        0.0%
;;BITSFR14             0      0       0      15        0.0%
;;SFR14                0      0       0      15        0.0%
;;BANK5               50      0       0      16        0.0%
;;BITSFR15             0      0       0      16        0.0%
;;SFR15                0      0       0      16        0.0%
;;BITBANK6             5      0       0      17        0.0%
;;BITSFR16             0      0       0      17        0.0%
;;SFR16                0      0       0      17        0.0%
;;BANK6                5      0       0      18        0.0%
;;BITSFR17             0      0       0      18        0.0%
;;SFR17                0      0       0      18        0.0%
;;BITSFR18             0      0       0      19        0.0%
;;SFR18                0      0       0      19        0.0%
;;DATA                 0      0      AA      19        0.0%
;;BITSFR19             0      0       0      20        0.0%
;;SFR19                0      0       0      20        0.0%
;;BITSFR20             0      0       0      21        0.0%
;;SFR20                0      0       0      21        0.0%
;;BITSFR21             0      0       0      22        0.0%
;;SFR21                0      0       0      22        0.0%
;;BITSFR22             0      0       0      23        0.0%
;;SFR22                0      0       0      23        0.0%
;;BITSFR23             0      0       0      24        0.0%
;;SFR23                0      0       0      24        0.0%
;;BITSFR24             0      0       0      25        0.0%
;;SFR24                0      0       0      25        0.0%
;;BITSFR25             0      0       0      26        0.0%
;;SFR25                0      0       0      26        0.0%
;;BITSFR26             0      0       0      27        0.0%
;;SFR26                0      0       0      27        0.0%
;;BITSFR27             0      0       0      28        0.0%
;;SFR27                0      0       0      28        0.0%
;;BITSFR28             0      0       0      29        0.0%
;;SFR28                0      0       0      29        0.0%
;;BITSFR29             0      0       0      30        0.0%
;;SFR29                0      0       0      30        0.0%
;;BITSFR30             0      0       0      31        0.0%
;;SFR30                0      0       0      31        0.0%
;;BITSFR31             0      0       0      32        0.0%
;;SFR31                0      0       0      32        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 101 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2  2126[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 1F/2
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_init_timer1
;;		_init_serialport
;;		_init_ccp2
;;		_init_ccp1
;;		_printf
;;		_init_reader1
;;		_init_reader2
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	101
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 11
; Regs used in _main: [wreg-status,0+pclath+cstack]
	line	102
	
l15068:	
;main.c: 102: OPTION_REG = 0b10000101;
	movlw	(085h)
	movlb 1	; select bank1
	movwf	(149)^080h	;volatile
	line	103
	
l15070:	
;main.c: 103: OSCCONbits.IRCF = 0b1110;
	movf	(153)^080h,w	;volatile
	andlw	not (((1<<4)-1)<<3)
	iorlw	(0Eh & ((1<<4)-1))<<3
	movwf	(153)^080h	;volatile
	line	104
	
l15072:	
;main.c: 104: SPLLEN = 1;
	bsf	(1231/8)^080h,(1231)&7
	line	107
	
l15074:	
;main.c: 107: PORTC = 0;
	movlb 0	; select bank0
	clrf	(14)	;volatile
	line	108
	
l15076:	
;main.c: 108: PORTD = 0;
	clrf	(15)	;volatile
	line	109
	
l15078:	
;main.c: 109: PORTE = 0;
	clrf	(16)	;volatile
	line	110
	
l15080:	
;main.c: 110: TRISC2 = 0;
	movlb 1	; select bank1
	bcf	(1138/8)^080h,(1138)&7
	line	111
	
l15082:	
;main.c: 111: TRISE0 = 0;
	bcf	(1152/8)^080h,(1152)&7
	line	112
	
l15084:	
;main.c: 112: TRISE1 = 0;
	bcf	(1153/8)^080h,(1153)&7
	line	113
	
l15086:	
;main.c: 113: TRISE2 = 0;
	bcf	(1154/8)^080h,(1154)&7
	line	114
	
l15088:	
;main.c: 114: TRISD1 = 0;
	bcf	(1145/8)^080h,(1145)&7
	line	115
	
l15090:	
;main.c: 115: TRISD5 = 0;
	bcf	(1149/8)^080h,(1149)&7
	line	116
	
l15092:	
;main.c: 116: TRISD6 = 0;
	bcf	(1150/8)^080h,(1150)&7
	line	118
	
l15094:	
;main.c: 118: TRISA1 = 0;
	bcf	(1121/8)^080h,(1121)&7
	line	121
	
l15096:	
;main.c: 121: ANSA1 = 0;
	movlb 3	; select bank3
	bcf	(3169/8)^0180h,(3169)&7
	line	122
	
l15098:	
;main.c: 122: ANSA3 = 1;
	bsf	(3171/8)^0180h,(3171)&7
	line	123
	
l15100:	
;main.c: 123: ANSB3 = 0;
	bcf	(3179/8)^0180h,(3179)&7
	line	124
	
l15102:	
;main.c: 124: ANSB1 = 1;
	bsf	(3177/8)^0180h,(3177)&7
	line	127
	
l15104:	
;main.c: 127: TRISB3 = 1;
	movlb 1	; select bank1
	bsf	(1131/8)^080h,(1131)&7
	line	128
	
l15106:	
;main.c: 128: WPUB3 = 1;
	movlb 4	; select bank4
	bsf	(4203/8)^0200h,(4203)&7
	line	130
	
l15108:	
;main.c: 130: TRISC2 = 1;
	movlb 1	; select bank1
	bsf	(1138/8)^080h,(1138)&7
	line	133
	
l15110:	
;main.c: 133: init_timer1();
	fcall	_init_timer1
	line	135
	
l15112:	
;main.c: 135: init_serialport();
	fcall	_init_serialport
	line	136
	
l15114:	
;main.c: 136: TXEN = 1;
	bsf	(3317/8)^0180h,(3317)&7
	line	138
	
l15116:	
;main.c: 138: init_ccp2();
	fcall	_init_ccp2
	line	139
	
l15118:	
;main.c: 139: init_ccp1();
	fcall	_init_ccp1
	line	141
	
l15120:	
;main.c: 141: LCDIE = 0;
	bcf	(1170/8)^080h,(1170)&7
	line	142
	
l15122:	
;main.c: 142: PEIE = 1;
	bsf	(94/8),(94)&7
	line	143
	
l15124:	
;main.c: 143: GIE = 1;
	bsf	(95/8),(95)&7
	line	145
	
l15126:	
;main.c: 145: TMR0IE = 1;
	bsf	(93/8),(93)&7
	line	147
	
l15128:	
;main.c: 147: rdr1_state_timer = 0;
	movlb 0	; select bank0
	clrf	(_rdr1_state_timer)	;volatile
	line	148
	
l15130:	
;main.c: 148: rdr2_state_timer = 0;
	clrf	(_rdr2_state_timer)	;volatile
	line	150
	
l15132:	
;main.c: 150: RA1 = 0;
	bcf	(97/8),(97)&7
	line	152
	
l15134:	
;main.c: 152: rdr1_got_bcode = FALSE;
	clrf	(_rdr1_got_bcode)
	line	153
	
l15136:	
;main.c: 153: rdr1_got_error = FALSE;
	clrf	(_rdr1_got_error)
	line	155
	
l15138:	
;main.c: 156: rdr2_got_error = FALSE;
	clrf	(_rdr2_got_bcode)
	line	158
	
l15140:	
;main.c: 158: delay = 32767;
	movlw	low(07FFFh)
	movlb 1	; select bank1
	movwf	(_delay)^080h
	movlw	high(07FFFh)
	movwf	((_delay)^080h)+1
	line	159
;main.c: 159: while (delay) delay--;
	goto	l15144
	
l15142:	
	movlw	low(01h)
	subwf	(_delay)^080h,f
	movlw	high(01h)
	subwfb	(_delay+1)^080h,f
	
l15144:	
	movf	((_delay+1)^080h),w
	iorwf	((_delay)^080h),w
	skipz
	goto	u2031
	goto	u2030
u2031:
	goto	l15142
u2030:
	line	163
	
l15146:	
;main.c: 163: printf("OK\r\n");
	movlw	(STR_1|8000h)&0ffh
	fcall	_printf
	line	165
	
l15148:	
;main.c: 165: init_reader1();
	fcall	_init_reader1
	line	166
	
l15150:	
;main.c: 166: init_reader2();
	fcall	_init_reader2
	line	175
	
l15152:	
;main.c: 171: {
;main.c: 175: if (rdr1_got_bcode)
	movlb 0	; select bank0
	movf	(_rdr1_got_bcode),w
	skipz
	goto	u2040
	goto	l15168
u2040:
	line	177
	
l15154:	
;main.c: 176: {
;main.c: 177: rdr1_got_bcode = FALSE;
	clrf	(_rdr1_got_bcode)
	line	178
	
l15156:	
;main.c: 178: i = 1;
	clrf	(_i)
	incf	(_i),f
	line	180
	
l15158:	
;main.c: 180: printf ("RDR1!");
	movlw	(STR_2|8000h)&0ffh
	fcall	_printf
	line	182
;main.c: 182: while (rdr1_bcode_data[i+1] != 106)
	goto	l15164
	line	184
	
l15160:	
;main.c: 183: {
;main.c: 184: printf ("%2.2u", rdr1_bcode_data[i]);
	movf	(_i),w
	addlw	_rdr1_bcode_data&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	movlb 1	; select bank1
	movwf	(?_printf)^080h
	clrf	(?_printf+1)^080h
	movlw	(STR_3|8000h)&0ffh
	fcall	_printf
	line	185
	
l15162:	
;main.c: 185: i++;
	movlb 0	; select bank0
	incf	(_i),f
	line	182
	
l15164:	
	movlb 0	; select bank0
	movf	(_i),w
	addlw	_rdr1_bcode_data+01h&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	xorlw	06Ah&0ffh
	skipz
	goto	u2051
	goto	u2050
u2051:
	goto	l15160
u2050:
	line	188
	
l15166:	
;main.c: 186: }
;main.c: 188: printf ("OK\r\n");
	movlw	(STR_4|8000h)&0ffh
	fcall	_printf
	line	191
	
l15168:	
;main.c: 189: }
;main.c: 191: if (rdr1_got_error)
	movlb 0	; select bank0
	movf	(_rdr1_got_error),w
	skipz
	goto	u2060
	goto	l15174
u2060:
	line	193
	
l15170:	
;main.c: 192: {
;main.c: 193: rdr1_got_error = FALSE;
	clrf	(_rdr1_got_error)
	line	194
	
l15172:	
;main.c: 194: printf ("RDR1!error\r\n");
	movlw	(STR_5|8000h)&0ffh
	fcall	_printf
	line	197
	
l15174:	
;main.c: 195: }
;main.c: 197: if (rdr2_got_bcode)
	movlb 0	; select bank0
	movf	(_rdr2_got_bcode),w
	skipz
	goto	u2070
	goto	l15152
u2070:
	line	199
	
l15176:	
;main.c: 198: {
;main.c: 199: rdr2_got_bcode = FALSE;
	clrf	(_rdr2_got_bcode)
	line	200
	
l15178:	
;main.c: 200: i = 1;
	clrf	(_i)
	incf	(_i),f
	line	202
	
l15180:	
;main.c: 202: printf ("RDR2!");
	movlw	(STR_6|8000h)&0ffh
	fcall	_printf
	line	204
;main.c: 204: while (rdr2_bcode_data[i+1] != 106)
	goto	l15186
	line	206
	
l15182:	
;main.c: 205: {
;main.c: 206: printf ("%2.2u", rdr1_bcode_data[i]);
	movf	(_i),w
	addlw	_rdr1_bcode_data&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	movlb 1	; select bank1
	movwf	(?_printf)^080h
	clrf	(?_printf+1)^080h
	movlw	(STR_7|8000h)&0ffh
	fcall	_printf
	line	207
	
l15184:	
;main.c: 207: i++;
	movlb 0	; select bank0
	incf	(_i),f
	line	204
	
l15186:	
	movlb 0	; select bank0
	movf	(_i),w
	addlw	_rdr2_bcode_data+01h&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	xorlw	06Ah&0ffh
	skipz
	goto	u2081
	goto	u2080
u2081:
	goto	l15182
u2080:
	line	210
	
l15188:	
;main.c: 208: }
;main.c: 210: printf ("OK\r\n");
	movlw	(STR_8|8000h)&0ffh
	fcall	_printf
	goto	l15152
	global	start
	ljmp	start
	opt stack 0
psect	maintext
	line	216
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_printf
psect	text646,local,class=CODE,delta=2
global __ptext646
__ptext646:

;; *************** function _printf *****************
;; Defined at:
;;		line 461 in file "C:\Program Files\HI-TECH Software\PICC\9.83\lib\doprnt.c"
;; Parameters:    Size  Location     Type
;;  f               1    wreg     PTR const unsigned char 
;;		 -> STR_8(5), STR_7(6), STR_6(6), STR_5(13), 
;;		 -> STR_4(5), STR_3(6), STR_2(6), STR_1(5), 
;; Auto vars:     Size  Location     Type
;;  f               1    8[BANK2 ] PTR const unsigned char 
;;		 -> STR_8(5), STR_7(6), STR_6(6), STR_5(13), 
;;		 -> STR_4(5), STR_3(6), STR_2(6), STR_1(5), 
;;  _val            4    1[BANK2 ] struct .
;;  width           2    5[BANK2 ] int 
;;  c               1    9[BANK2 ] char 
;;  prec            1    7[BANK2 ] char 
;;  ap              1    0[BANK2 ] PTR void [1]
;;		 -> ?_printf(2), 
;;  flag            1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  2    4[BANK1 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1E/1
;;		On exit  : 1F/2
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       2       0       0       0       0       0
;;      Locals:         0       0       0      10       0       0       0       0
;;      Temps:          0       0       4       0       0       0       0       0
;;      Totals:         0       0       6      10       0       0       0       0
;;Total ram usage:       16 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_putch
;;		_isdigit
;;		___wmul
;;		___bmul
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text646
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\doprnt.c"
	line	461
	global	__size_of_printf
	__size_of_printf	equ	__end_of_printf-_printf
	
_printf:	
	opt	stack 11
; Regs used in _printf: [wreg-status,0+pclath+cstack]
;printf@f stored from wreg
	line	537
	movlb 2	; select bank2
	movwf	(printf@f)^0100h
	
l14982:	
	movlw	(?_printf)&0ffh
	movwf	(printf@ap)^0100h
	line	540
	goto	l15066
	line	542
	
l14984:	
	movf	(printf@c)^0100h,w
	xorlw	025h&0ffh
	skipnz
	goto	u1871
	goto	u1870
u1871:
	goto	l14988
u1870:
	line	545
	
l14986:	
	movf	(printf@c)^0100h,w
	fcall	_putch
	line	546
	goto	l15066
	line	550
	
l14988:	
	clrf	(printf@width)^0100h
	clrf	(printf@width+1)^0100h
	line	601
	
l14990:	
	movf	(printf@f)^0100h,w
	movlp	high __stringtab
	callw
	pagesel	$
	fcall	_isdigit
	btfss	status,0
	goto	u1881
	goto	u1880
u1881:
	goto	l15000
u1880:
	line	602
	
l14992:	
	movlb 2	; select bank2
	clrf	(printf@width)^0100h
	clrf	(printf@width+1)^0100h
	line	604
	
l14994:	
	movlb 2	; select bank2
	movf	(printf@f)^0100h,w
	movlp	high __stringtab
	callw
	pagesel	$
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	clrf	(??_printf+0)^080h+0+1
	movlb 2	; select bank2
	movf	(printf@width+1)^0100h,w
	movlb 0	; select bank0
	movwf	(?___wmul+1)
	movlb 2	; select bank2
	movf	(printf@width)^0100h,w
	movlb 0	; select bank0
	movwf	(?___wmul)
	movlw	0Ah
	movwf	0+(?___wmul)+02h
	clrf	1+(?___wmul)+02h
	fcall	___wmul
	movlb 1	; select bank1
	movf	0+(??_printf+0)^080h+0,w
	movlb 0	; select bank0
	addwf	(0+(?___wmul)),w
	movlb 1	; select bank1
	movwf	(??_printf+2)^080h+0
	movf	1+(??_printf+0)^080h+0,w
	movlb 0	; select bank0
	addwfc	(1+(?___wmul)),w
	movlb 1	; select bank1
	movwf	1+(??_printf+2)^080h+0
	movf	0+(??_printf+2)^080h+0,w
	addlw	low(0FFD0h)
	movlb 2	; select bank2
	movwf	(printf@width)^0100h
	movlw	high(0FFD0h)
	movlb 1	; select bank1
	addwfc	1+(??_printf+2)^080h+0,w
	movlb 2	; select bank2
	movwf	1+(printf@width)^0100h
	
l14996:	
	incf	(printf@f)^0100h,f
	line	605
	
l14998:	
	movf	(printf@f)^0100h,w
	movlp	high __stringtab
	callw
	pagesel	$
	fcall	_isdigit
	btfsc	status,0
	goto	u1891
	goto	u1890
u1891:
	goto	l14994
u1890:
	line	614
	
l15000:	
	movlb 2	; select bank2
	movf	(printf@f)^0100h,w
	movlp	high __stringtab
	callw
	pagesel	$
	xorlw	02Eh
	skipz
	goto	u1901
	goto	u1900
u1901:
	goto	l15012
u1900:
	line	616
	
l15002:	
	incf	(printf@f)^0100h,f
	line	624
	
l15004:	
	clrf	(printf@prec)^0100h
	line	625
	goto	l15010
	line	626
	
l15006:	
	movlw	(0Ah)
	movwf	(?___bmul)
	movlb 2	; select bank2
	movf	(printf@prec)^0100h,w
	fcall	___bmul
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	movlb 2	; select bank2
	movf	(printf@f)^0100h,w
	movlp	high __stringtab
	callw
	pagesel	$
	movlb 1	; select bank1
	addwf	0+(??_printf+0)^080h+0,w
	addlw	-48
	movlb 2	; select bank2
	movwf	(printf@prec)^0100h
	
l15008:	
	incf	(printf@f)^0100h,f
	line	625
	
l15010:	
	movf	(printf@f)^0100h,w
	movlp	high __stringtab
	callw
	pagesel	$
	fcall	_isdigit
	btfsc	status,0
	goto	u1911
	goto	u1910
u1911:
	goto	l15006
u1910:
	goto	l15016
	line	629
	
l15012:	
	clrf	(printf@prec)^0100h
	line	638
	
l15016:	
	movlb 2	; select bank2
	movf	(printf@f)^0100h,w
	incf	(printf@f)^0100h,f
	movlp	high __stringtab
	callw
	pagesel	$
	movwf	(printf@c)^0100h
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 0 to 117
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          242     6 (fixed)
; jumptable            260     6 (fixed)
; rangetable           122     4 (fixed)
; spacedrange          241     6 (fixed)
; locatedrange         118     3 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	l8464
	xorlw	117^0	; case 117
	skipnz
	goto	l15018
	goto	l15066
	opt asmopt_on

	line	1281
	
l15018:	
	movf	(printf@ap)^0100h,w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(printf@_val)^0100h
	moviw	[1]fsr1
	movwf	(printf@_val+1)^0100h
	
l15020:	
	incf	(printf@ap)^0100h,f
	incf	(printf@ap)^0100h,f
	line	1285
	
l15022:	
	movf	(printf@prec)^0100h,f
	skipz
	goto	u1921
	goto	u1920
u1921:
	goto	l15028
u1920:
	
l15024:	
	movf	((printf@_val+1)^0100h),w
	iorwf	((printf@_val)^0100h),w
	skipz
	goto	u1931
	goto	u1930
u1931:
	goto	l15028
u1930:
	line	1286
	
l15026:	
	incf	(printf@prec)^0100h,f
	line	1300
	
l15028:	
	clrf	(printf@c)^0100h
	incf	(printf@c)^0100h,f
	line	1301
	
l15034:	
	lslf	(printf@c)^0100h,w
	addlw	low(_dpowers|8000h)
	movwf	fsr0l
	movlw	high(_dpowers|8000h)
	movwf	fsr0h
	moviw	[0]fsr0
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	moviw	[1]fsr0
	movwf	(??_printf+0)^080h+0+1
	movf	1+(??_printf+0)^080h+0,w
	movlb 2	; select bank2
	subwf	(printf@_val+1)^0100h,w
	skipz
	goto	u1945
	movlb 1	; select bank1
	movf	0+(??_printf+0)^080h+0,w
	movlb 2	; select bank2
	subwf	(printf@_val)^0100h,w
u1945:
	skipnc
	goto	u1941
	goto	u1940
u1941:
	goto	l15038
u1940:
	goto	l15042
	line	1300
	
l15038:	
	incf	(printf@c)^0100h,f
	
l15040:	
	movf	(printf@c)^0100h,w
	xorlw	05h&0ffh
	skipz
	goto	u1951
	goto	u1950
u1951:
	goto	l15034
u1950:
	line	1334
	
l15042:	
	movf	(printf@c)^0100h,w
	xorlw	80h
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	movlb 2	; select bank2
	movf	(printf@prec)^0100h,w
	xorlw	80h
	movlb 1	; select bank1
	subwf	(??_printf+0)^080h+0
	skipnc
	goto	u1961
	goto	u1960
u1961:
	goto	l15046
u1960:
	line	1335
	
l15044:	
	movlb 2	; select bank2
	movf	(printf@prec)^0100h,w
	movwf	(printf@c)^0100h
	goto	l8453
	line	1336
	
l15046:	
	movlb 2	; select bank2
	movf	(printf@prec)^0100h,w
	xorlw	80h
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	movlb 2	; select bank2
	movf	(printf@c)^0100h,w
	xorlw	80h
	movlb 1	; select bank1
	subwf	(??_printf+0)^080h+0
	skipnc
	goto	u1971
	goto	u1970
u1971:
	goto	l8453
u1970:
	line	1337
	
l15048:	
	movlb 2	; select bank2
	movf	(printf@c)^0100h,w
	movwf	(printf@prec)^0100h
	line	1376
	
l8453:	
	movlb 2	; select bank2
	movf	(printf@c)^0100h,w
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	clrf	(??_printf+0)^080h+0+1
	btfsc	(??_printf+0)^080h+0,7
	decf	(??_printf+0)^080h+0+1,f
	movf	1+(??_printf+0)^080h+0,w
	xorlw	80h
	movwf	(??_printf+2)^080h+0
	movlb 2	; select bank2
	movf	(printf@width+1)^0100h,w
	xorlw	80h
	movlb 1	; select bank1
	subwf	(??_printf+2)^080h+0,w
	skipz
	goto	u1985
	movlb 2	; select bank2
	movf	(printf@width)^0100h,w
	movlb 1	; select bank1
	subwf	0+(??_printf+0)^080h+0,w
u1985:

	skipnc
	goto	u1981
	goto	u1980
u1981:
	goto	l15052
u1980:
	line	1377
	
l15050:	
	movlb 2	; select bank2
	movf	(printf@c)^0100h,w
	movlb 1	; select bank1
	movwf	(??_printf+0)^080h+0
	clrf	(??_printf+0)^080h+0+1
	btfsc	(??_printf+0)^080h+0,7
	decf	(??_printf+0)^080h+0+1,f
	movf	0+(??_printf+0)^080h+0,w
	movlb 2	; select bank2
	subwf	(printf@width)^0100h,f
	movlb 1	; select bank1
	movf	1+(??_printf+0)^080h+0,w
	movlb 2	; select bank2
	subwfb	(printf@width+1)^0100h,f
	goto	l15054
	line	1379
	
l15052:	
	movlb 2	; select bank2
	clrf	(printf@width)^0100h
	clrf	(printf@width+1)^0100h
	line	1423
	
l15054:	
	movf	(printf@width+1)^0100h,w
	iorwf	(printf@width)^0100h,w
	skipnz
	goto	u1991
	goto	u1990
u1991:
	goto	l15064
u1990:
	line	1425
	
l15056:	
	movlw	(020h)
	fcall	_putch
	line	1426
	
l15058:	
	movlw	-1
	movlb 2	; select bank2
	addwf	(printf@width)^0100h,f
	skipc
	decf	(printf@width+1)^0100h,f
	movf	(((printf@width+1)^0100h)),w
	iorwf	(((printf@width)^0100h)),w
	skipz
	goto	u2001
	goto	u2000
u2001:
	goto	l15056
u2000:
	goto	l15064
	line	1484
	
l15060:	
	movlw	0Ah
	movlb 1	; select bank1
	movwf	(?___lwmod)^080h
	clrf	(?___lwmod+1)^080h
	movlb 2	; select bank2
	lslf	(printf@prec)^0100h,w
	addlw	low(_dpowers|8000h)
	movwf	fsr0l
	movlw	high(_dpowers|8000h)
	movwf	fsr0h
	moviw	[0]fsr0
	movlb 0	; select bank0
	movwf	(?___lwdiv)
	moviw	[1]fsr0
	movwf	(?___lwdiv+1)
	movlb 2	; select bank2
	movf	(printf@_val+1)^0100h,w
	movlb 0	; select bank0
	movwf	1+(?___lwdiv)+02h
	movlb 2	; select bank2
	movf	(printf@_val)^0100h,w
	movlb 0	; select bank0
	movwf	0+(?___lwdiv)+02h
	fcall	___lwdiv
	movf	(1+(?___lwdiv)),w
	movlb 1	; select bank1
	movwf	1+(?___lwmod)^080h+02h
	movlb 0	; select bank0
	movf	(0+(?___lwdiv)),w
	movlb 1	; select bank1
	movwf	0+(?___lwmod)^080h+02h
	fcall	___lwmod
	movf	(0+(?___lwmod))^080h,w
	addlw	030h
	movlb 2	; select bank2
	movwf	(printf@c)^0100h
	line	1516
	
l15062:	
	movf	(printf@c)^0100h,w
	fcall	_putch
	line	1469
	
l15064:	
	movlb 2	; select bank2
	decf	(printf@prec)^0100h,f
	incf	((printf@prec)^0100h),w
	skipz
	goto	u2011
	goto	u2010
u2011:
	goto	l15060
u2010:
	line	540
	
l15066:	
	movlb 2	; select bank2
	movf	(printf@f)^0100h,w
	incf	(printf@f)^0100h,f
	movlp	high __stringtab
	callw
	pagesel	$
	movwf	(printf@c)^0100h
	movf	((printf@c)^0100h),f
	skipz
	goto	u2021
	goto	u2020
u2021:
	goto	l14984
u2020:
	line	1533
	
l8464:	
	return
	opt stack 0
GLOBAL	__end_of_printf
	__end_of_printf:
;; =============== function _printf ends ============

	signat	_printf,602
	global	___lwmod
psect	text647,local,class=CODE,delta=2
global __ptext647
__ptext647:

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK1 ] unsigned int 
;;  dividend        2    2[BANK1 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1   29[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK1 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/1
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       4       0       0       0       0       0
;;      Locals:         0       1       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       1       4       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text647
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
	opt	stack 11
; Regs used in ___lwmod: [wreg+status,2+status,0]
	line	8
	
l14962:	
	movf	(___lwmod@divisor+1)^080h,w
	iorwf	(___lwmod@divisor)^080h,w
	skipnz
	goto	u1831
	goto	u1830
u1831:
	goto	l14978
u1830:
	line	9
	
l14964:	
	movlb 0	; select bank0
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	10
	goto	l14968
	line	11
	
l14966:	
	lslf	(___lwmod@divisor)^080h,f
	rlf	(___lwmod@divisor+1)^080h,f
	line	12
	movlb 0	; select bank0
	incf	(___lwmod@counter),f
	line	10
	
l14968:	
	movlb 1	; select bank1
	btfss	(___lwmod@divisor+1)^080h,(15)&7
	goto	u1841
	goto	u1840
u1841:
	goto	l14966
u1840:
	line	15
	
l14970:	
	movlb 1	; select bank1
	movf	(___lwmod@divisor+1)^080h,w
	subwf	(___lwmod@dividend+1)^080h,w
	skipz
	goto	u1855
	movf	(___lwmod@divisor)^080h,w
	subwf	(___lwmod@dividend)^080h,w
u1855:
	skipc
	goto	u1851
	goto	u1850
u1851:
	goto	l14974
u1850:
	line	16
	
l14972:	
	movf	(___lwmod@divisor)^080h,w
	subwf	(___lwmod@dividend)^080h,f
	movf	(___lwmod@divisor+1)^080h,w
	subwfb	(___lwmod@dividend+1)^080h,f
	line	17
	
l14974:	
	lsrf	(___lwmod@divisor+1)^080h,f
	rrf	(___lwmod@divisor)^080h,f
	line	18
	
l14976:	
	movlb 0	; select bank0
	decfsz	(___lwmod@counter),f
	goto	u1861
	goto	u1860
u1861:
	goto	l14970
u1860:
	line	20
	
l14978:	
	movlb 1	; select bank1
	movf	(___lwmod@dividend+1)^080h,w
	movwf	(?___lwmod+1)^080h
	movf	(___lwmod@dividend)^080h,w
	movwf	(?___lwmod)^080h
	line	21
	
l10355:	
	return
	opt stack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
;; =============== function ___lwmod ends ============

	signat	___lwmod,8314
	global	___lwdiv
psect	text648,local,class=CODE,delta=2
global __ptext648
__ptext648:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2   22[BANK0 ] unsigned int 
;;  dividend        2   24[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2   27[BANK0 ] unsigned int 
;;  counter         1   26[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2   22[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       4       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       7       0       0       0       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text648
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 11
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l14936:	
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	10
	
l14938:	
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u1791
	goto	u1790
u1791:
	goto	l14958
u1790:
	line	11
	
l14940:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	12
	goto	l14944
	line	13
	
l14942:	
	lslf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	line	14
	incf	(___lwdiv@counter),f
	line	12
	
l14944:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u1801
	goto	u1800
u1801:
	goto	l14942
u1800:
	line	17
	
l14946:	
	lslf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	line	18
	
l14948:	
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u1815
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u1815:
	skipc
	goto	u1811
	goto	u1810
u1811:
	goto	l14954
u1810:
	line	19
	
l14950:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	subwfb	(___lwdiv@dividend+1),f
	line	20
	
l14952:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
l14954:	
	lsrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	line	23
	
l14956:	
	decfsz	(___lwdiv@counter),f
	goto	u1821
	goto	u1820
u1821:
	goto	l14946
u1820:
	line	25
	
l14958:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	26
	
l10345:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	___wmul
psect	text649,local,class=CODE,delta=2
global __ptext649
__ptext649:

;; *************** function ___wmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\wmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      2   22[BANK0 ] unsigned int 
;;  multiplicand    2   24[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  product         2   26[BANK0 ] unsigned int 
;; Return value:  Size  Location     Type
;;                  2   22[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       4       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       6       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text649
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\wmul.c"
	line	3
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
	opt	stack 11
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	4
	
l14920:	
	clrf	(___wmul@product)
	clrf	(___wmul@product+1)
	line	7
	
l14922:	
	btfss	(___wmul@multiplier),(0)&7
	goto	u1771
	goto	u1770
u1771:
	goto	l14926
u1770:
	line	8
	
l14924:	
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	movf	(___wmul@multiplicand+1),w
	addwfc	(___wmul@product+1),f
	line	9
	
l14926:	
	lslf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	line	10
	
l14928:	
	lsrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	line	11
	
l14930:	
	movf	((___wmul@multiplier+1)),w
	iorwf	((___wmul@multiplier)),w
	skipz
	goto	u1781
	goto	u1780
u1781:
	goto	l14922
u1780:
	line	12
	
l14932:	
	movf	(___wmul@product+1),w
	movwf	(?___wmul+1)
	movf	(___wmul@product),w
	movwf	(?___wmul)
	line	13
	
l10335:	
	return
	opt stack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
;; =============== function ___wmul ends ============

	signat	___wmul,8314
	global	___bmul
psect	text650,local,class=CODE,delta=2
global __ptext650
__ptext650:

;; *************** function ___bmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\bmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      1    wreg     unsigned char 
;;  multiplicand    1   22[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  multiplier      1   24[BANK0 ] unsigned char 
;;  product         1   23[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       1       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       3       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text650
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\bmul.c"
	line	3
	global	__size_of___bmul
	__size_of___bmul	equ	__end_of___bmul-___bmul
	
___bmul:	
	opt	stack 11
; Regs used in ___bmul: [wreg+status,2+status,0]
;___bmul@multiplier stored from wreg
	movlb 0	; select bank0
	movwf	(___bmul@multiplier)
	line	4
	
l14904:	
	clrf	(___bmul@product)
	line	7
	
l14906:	
	btfss	(___bmul@multiplier),(0)&7
	goto	u1751
	goto	u1750
u1751:
	goto	l14910
u1750:
	line	8
	
l14908:	
	movf	(___bmul@multiplicand),w
	addwf	(___bmul@product),f
	line	9
	
l14910:	
	lslf	(___bmul@multiplicand),f
	line	10
	
l14912:	
	lsrf	(___bmul@multiplier),f
	line	11
	
l14914:	
	movf	(___bmul@multiplier),f
	skipz
	goto	u1761
	goto	u1760
u1761:
	goto	l14906
u1760:
	line	12
	
l14916:	
	movf	(___bmul@product),w
	line	13
	
l10329:	
	return
	opt stack 0
GLOBAL	__end_of___bmul
	__end_of___bmul:
;; =============== function ___bmul ends ============

	signat	___bmul,8313
	global	_isdigit
psect	text651,local,class=CODE,delta=2
global __ptext651
__ptext651:

;; *************** function _isdigit *****************
;; Defined at:
;;		line 13 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\isdigit.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1   23[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       2       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text651
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\isdigit.c"
	global	__size_of_isdigit
	__size_of_isdigit	equ	__end_of_isdigit-_isdigit
	
_isdigit:	
	opt	stack 11
; Regs used in _isdigit: [wreg+status,2+status,0]
;isdigit@c stored from wreg
	movlb 0	; select bank0
	movwf	(isdigit@c)
	line	14
	
l14892:	
	clrf	(_isdigit$11171)
	
l14894:	
	movlw	(03Ah)
	subwf	(isdigit@c),w
	skipnc
	goto	u1731
	goto	u1730
u1731:
	goto	l14900
u1730:
	
l14896:	
	movlw	(030h)
	subwf	(isdigit@c),w
	skipc
	goto	u1741
	goto	u1740
u1741:
	goto	l14900
u1740:
	
l14898:	
	clrf	(_isdigit$11171)
	incf	(_isdigit$11171),f
	
l14900:	
	rrf	(_isdigit$11171),w
	
	line	15
	
l10323:	
	return
	opt stack 0
GLOBAL	__end_of_isdigit
	__end_of_isdigit:
;; =============== function _isdigit ends ============

	signat	_isdigit,4216
	global	_putch
psect	text652,local,class=CODE,delta=2
global __ptext652
__ptext652:

;; *************** function _putch *****************
;; Defined at:
;;		line 33 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\serialport.c"
;; Parameters:    Size  Location     Type
;;  txchar          1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  txchar          1   22[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/3
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       1       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       1       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_printf
;; This function uses a non-reentrant model
;;
psect	text652
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\serialport.c"
	line	33
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
	opt	stack 11
; Regs used in _putch: [wreg]
;putch@txchar stored from wreg
	movlb 0	; select bank0
	movwf	(putch@txchar)
	line	34
	
l14888:	
;serialport.c: 34: while (!TRMT);
	
l4374:	
	movlb 3	; select bank3
	btfss	(3313/8)^0180h,(3313)&7
	goto	u1721
	goto	u1720
u1721:
	goto	l4374
u1720:
	line	35
	
l14890:	
;serialport.c: 35: TXREG = txchar;
	movlb 0	; select bank0
	movf	(putch@txchar),w
	movlb 3	; select bank3
	movwf	(410)^0180h	;volatile
	line	37
	
l4377:	
	return
	opt stack 0
GLOBAL	__end_of_putch
	__end_of_putch:
;; =============== function _putch ends ============

	signat	_putch,4216
	global	_init_reader2
psect	text653,local,class=CODE,delta=2
global __ptext653
__ptext653:

;; *************** function _init_reader2 *****************
;; Defined at:
;;		line 902 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 1F/1
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text653
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	902
	global	__size_of_init_reader2
	__size_of_init_reader2	equ	__end_of_init_reader2-_init_reader2
	
_init_reader2:	
	opt	stack 12
; Regs used in _init_reader2: [wreg]
	line	903
	
l13148:	
;main.c: 904: {
;main.c: 905: rdr2_state = WAIT_LOW;
	movlb 0	; select bank0
	btfss	(114/8),(114)&7
	goto	u931
	goto	u930
u931:
	goto	l13158
u930:
	line	906
	
l13150:	
;main.c: 906: { CCP1IE = 0; CCP1CON = 0x04; CCP1IF = 0; CCP1IE = 1; };
	movlb 1	; select bank1
	bcf	(1162/8)^080h,(1162)&7
	
l13152:	
	movlw	(04h)
	movlb 5	; select bank5
	movwf	(659)^0280h	;volatile
	
l13154:	
	movlb 0	; select bank0
	bcf	(138/8),(138)&7
	
l13156:	
	movlb 1	; select bank1
	bsf	(1162/8)^080h,(1162)&7
	line	907
;main.c: 907: }
	goto	l2335
	line	910
	
l13158:	
;main.c: 911: rdr2_state = WAIT_QUIET;
	movlw	(014h)
	movwf	(_rdr2_state_timer)	;volatile
	line	912
	
l13160:	
;main.c: 912: { CCP1IE = 0; CCP1CON = 0x05; CCP1IF = 0; CCP1IE = 1; };
	movlb 1	; select bank1
	bcf	(1162/8)^080h,(1162)&7
	movlw	(05h)
	movlb 5	; select bank5
	movwf	(659)^0280h	;volatile
	goto	l13154
	line	914
	
l2335:	
	return
	opt stack 0
GLOBAL	__end_of_init_reader2
	__end_of_init_reader2:
;; =============== function _init_reader2 ends ============

	signat	_init_reader2,88
	global	_init_reader1
psect	text654,local,class=CODE,delta=2
global __ptext654
__ptext654:

;; *************** function _init_reader1 *****************
;; Defined at:
;;		line 882 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 1F/2
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text654
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	882
	global	__size_of_init_reader1
	__size_of_init_reader1	equ	__end_of_init_reader1-_init_reader1
	
_init_reader1:	
	opt	stack 12
; Regs used in _init_reader1: [wreg+status,2]
	line	885
	
l13124:	
;main.c: 885: rdr1_first_edge = FALSE;
	movlb 0	; select bank0
	clrf	(_rdr1_first_edge)
	line	886
;main.c: 886: rdr1_bar_count = 0;
	clrf	(_rdr1_bar_count)
	line	887
;main.c: 887: rdr1_byte_count = 0;
	clrf	(_rdr1_byte_count)
	line	888
	
l13126:	
;main.c: 888: if (RB3)
	btfss	(107/8),(107)&7
	goto	u921
	goto	u920
u921:
	goto	l13138
u920:
	line	890
	
l13128:	
;main.c: 889: {
;main.c: 890: rdr1_state = WAIT_LOW;
	clrf	(_rdr1_state)
	line	891
	
l13130:	
;main.c: 891: { CCP2IE = 0; CCP2CON = 0x04; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	
l13132:	
	movlw	(04h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	
l13134:	
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	
l13136:	
	movlb 1	; select bank1
	bsf	(1168/8)^080h,(1168)&7
	line	892
;main.c: 892: }
	goto	l2330
	line	895
	
l13138:	
;main.c: 893: else
;main.c: 894: {
;main.c: 895: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	896
	
l13140:	
;main.c: 896: rdr1_state = WAIT_QUIET;
	clrf	(_rdr1_state)
	incf	(_rdr1_state),f
	line	897
	
l13142:	
;main.c: 897: { CCP2IE = 0; CCP2CON = 0x05; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	movlw	(05h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	goto	l13134
	line	899
	
l2330:	
	return
	opt stack 0
GLOBAL	__end_of_init_reader1
	__end_of_init_reader1:
;; =============== function _init_reader1 ends ============

	signat	_init_reader1,88
	global	_init_ccp1
psect	text655,local,class=CODE,delta=2
global __ptext655
__ptext655:

;; *************** function _init_ccp1 *****************
;; Defined at:
;;		line 28 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\ccpmodule.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 17F/1
;;		On exit  : 17F/1
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text655
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\ccpmodule.c"
	line	28
	global	__size_of_init_ccp1
	__size_of_init_ccp1	equ	__end_of_init_ccp1-_init_ccp1
	
_init_ccp1:	
	opt	stack 12
; Regs used in _init_ccp1: [wreg]
	line	34
	
l13116:	
;ccpmodule.c: 34: CCP1IE = 0;
	bcf	(1162/8)^080h,(1162)&7
	line	35
	
l13118:	
;ccpmodule.c: 35: CCP1CON = 0x04;
	movlw	(04h)
	movlb 5	; select bank5
	movwf	(659)^0280h	;volatile
	line	36
	
l13120:	
;ccpmodule.c: 36: CCP1IF = 0;
	movlb 0	; select bank0
	bcf	(138/8),(138)&7
	line	37
	
l13122:	
;ccpmodule.c: 37: CCP1IE = 1;
	movlb 1	; select bank1
	bsf	(1162/8)^080h,(1162)&7
	line	39
	
l8418:	
	return
	opt stack 0
GLOBAL	__end_of_init_ccp1
	__end_of_init_ccp1:
;; =============== function _init_ccp1 ends ============

	signat	_init_ccp1,88
	global	_init_ccp2
psect	text656,local,class=CODE,delta=2
global __ptext656
__ptext656:

;; *************** function _init_ccp2 *****************
;; Defined at:
;;		line 13 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\ccpmodule.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 17F/3
;;		On exit  : 17F/1
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text656
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\ccpmodule.c"
	line	13
	global	__size_of_init_ccp2
	__size_of_init_ccp2	equ	__end_of_init_ccp2-_init_ccp2
	
_init_ccp2:	
	opt	stack 12
; Regs used in _init_ccp2: [wreg]
	line	19
	
l13108:	
;ccpmodule.c: 19: CCP2IE = 0;
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	line	20
;ccpmodule.c: 20: CCP2SEL = 1;
	movlb 2	; select bank2
	bsf	(2280/8)^0100h,(2280)&7
	line	21
	
l13110:	
;ccpmodule.c: 21: CCP2CON = 0x04;
	movlw	(04h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	line	22
	
l13112:	
;ccpmodule.c: 22: CCP2IF = 0;
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	line	23
	
l13114:	
;ccpmodule.c: 23: CCP2IE = 1;
	movlb 1	; select bank1
	bsf	(1168/8)^080h,(1168)&7
	line	25
	
l8415:	
	return
	opt stack 0
GLOBAL	__end_of_init_ccp2
	__end_of_init_ccp2:
;; =============== function _init_ccp2 ends ============

	signat	_init_ccp2,88
	global	_init_serialport
psect	text657,local,class=CODE,delta=2
global __ptext657
__ptext657:

;; *************** function _init_serialport *****************
;; Defined at:
;;		line 16 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\serialport.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 17F/1
;;		On exit  : 17F/3
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text657
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\serialport.c"
	line	16
	global	__size_of_init_serialport
	__size_of_init_serialport	equ	__end_of_init_serialport-_init_serialport
	
_init_serialport:	
	opt	stack 12
; Regs used in _init_serialport: [wreg]
	line	23
	
l13094:	
;serialport.c: 23: SPBRG = 25;
	movlw	(019h)
	movlb 3	; select bank3
	movwf	(411)^0180h	;volatile
	line	24
	
l13096:	
;serialport.c: 24: TX9 = 0;
	bcf	(3318/8)^0180h,(3318)&7
	line	25
	
l13098:	
;serialport.c: 25: SYNC = 0;
	bcf	(3316/8)^0180h,(3316)&7
	line	26
	
l13100:	
;serialport.c: 26: BRGH = 0;
	bcf	(3314/8)^0180h,(3314)&7
	line	27
	
l13102:	
;serialport.c: 27: ABDEN = 0;
	bcf	(3320/8)^0180h,(3320)&7
	line	28
	
l13104:	
;serialport.c: 28: RX9 = 0;
	bcf	(3310/8)^0180h,(3310)&7
	line	29
	
l13106:	
;serialport.c: 29: SPEN = 1;
	bsf	(3311/8)^0180h,(3311)&7
	line	30
	
l4371:	
	return
	opt stack 0
GLOBAL	__end_of_init_serialport
	__end_of_init_serialport:
;; =============== function _init_serialport ends ============

	signat	_init_serialport,88
	global	_init_timer1
psect	text658,local,class=CODE,delta=2
global __ptext658
__ptext658:

;; *************** function _init_timer1 *****************
;; Defined at:
;;		line 13 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\timers.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 17F/1
;;		On exit  : 17F/1
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text658
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\timers.c"
	line	13
	global	__size_of_init_timer1
	__size_of_init_timer1	equ	__end_of_init_timer1-_init_timer1
	
_init_timer1:	
	opt	stack 12
; Regs used in _init_timer1: [wreg+status,2]
	line	20
	
l13084:	
;timers.c: 20: T1CON = 0x39;
	movlw	(039h)
	movlb 0	; select bank0
	movwf	(24)	;volatile
	line	21
	
l13086:	
;timers.c: 21: T1GCON = 0x00;
	clrf	(25)	;volatile
	line	22
	
l13088:	
;timers.c: 22: TMR1ON = 1;
	bsf	(192/8),(192)&7
	line	23
	
l13090:	
;timers.c: 23: TMR1IF = 0;
	bcf	(136/8),(136)&7
	line	24
	
l13092:	
;timers.c: 24: TMR1IE = 0;
	movlb 1	; select bank1
	bcf	(1160/8)^080h,(1160)&7
	line	26
	
l6396:	
	return
	opt stack 0
GLOBAL	__end_of_init_timer1
	__end_of_init_timer1:
;; =============== function _init_timer1 ends ============

	signat	_init_timer1,88
	global	_isr
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:

;; *************** function _isr *****************
;; Defined at:
;;		line 248 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 1E/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       4       0       0       0       0       0       0
;;      Totals:         0       4       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_update_black_bar
;;		_update_white_bar
;;		_bar_decision
;;		i1___wmul
;;		_lookup_sym
;;		i1_init_reader1
;;		___lmul
;;		_checksum
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	intentry
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	248
	global	__size_of_isr
	__size_of_isr	equ	__end_of_isr-_isr
	
_isr:	
	opt	stack 11
; Regs used in _isr: [wreg-status,0+pclath+cstack]
psect	intentry
	pagesel	$
	line	253
	
i1l13166:	
;main.c: 253: if (CCP2IF && CCP2IE)
	movlb 0	; select bank0
	btfss	(144/8),(144)&7
	goto	u94_21
	goto	u94_20
u94_21:
	goto	i1l2145
u94_20:
	
i1l13168:	
	movlb 1	; select bank1
	btfss	(1168/8)^080h,(1168)&7
	goto	u95_21
	goto	u95_20
u95_21:
	goto	i1l2145
u95_20:
	line	255
	
i1l13170:	
;main.c: 254: {
;main.c: 255: CCP2IF = 0;
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	line	257
;main.c: 257: switch(rdr1_state)
	goto	i1l13352
	line	261
	
i1l13172:	
;main.c: 261: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	262
	
i1l13174:	
;main.c: 262: rdr1_state = WAIT_QUIET;
	clrf	(_rdr1_state)
	incf	(_rdr1_state),f
	line	263
	
i1l13176:	
;main.c: 263: { CCP2IE = 0; CCP2CON = 0x05; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	movlw	(05h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	
i1l13178:	
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	
i1l13180:	
	movlb 1	; select bank1
	bsf	(1168/8)^080h,(1168)&7
	line	264
;main.c: 264: break;
	goto	i1l2145
	line	267
	
i1l13182:	
;main.c: 267: rdr1_state = WAIT_LOW;
	clrf	(_rdr1_state)
	line	268
	
i1l13184:	
;main.c: 268: { CCP2IE = 0; CCP2CON = 0x04; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	
i1l13186:	
	movlw	(04h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	goto	i1l13178
	line	273
	
i1l13192:	
;main.c: 273: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	275
;main.c: 275: if (rdr1_first_edge)
	movf	(_rdr1_first_edge),w
	skipz
	goto	u96_20
	goto	i1l13206
u96_20:
	line	277
	
i1l13194:	
;main.c: 276: {
;main.c: 277: rdr1_ledge_counter = CCPR2H;
	movlb 5	; select bank5
	movf	(665)^0280h,w	;volatile
	movlb 0	; select bank0
	movwf	(_rdr1_ledge_counter)
	clrf	(_rdr1_ledge_counter+1)
	line	278
;main.c: 278: rdr1_ledge_counter = rdr1_ledge_counter <<8;
	movf	(_rdr1_ledge_counter),w
	movwf	(_rdr1_ledge_counter+1)
	clrf	(_rdr1_ledge_counter)
	line	279
;main.c: 279: rdr1_ledge_counter |= CCPR2L;
	movlb 5	; select bank5
	movf	(664)^0280h,w	;volatile
	movlb 0	; select bank0
	iorwf	(_rdr1_ledge_counter),f
	line	280
	
i1l13196:	
;main.c: 280: rdr1_first_edge = FALSE;
	clrf	(_rdr1_first_edge)
	line	281
	
i1l13198:	
;main.c: 281: rdr1_byte_count = 0;
	clrf	(_rdr1_byte_count)
	line	282
;main.c: 282: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	283
	
i1l13200:	
;main.c: 283: { CCP2IE = 0; CCP2M0 = !CCP2M0; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	movlw	1<<((5328)&7)
	movlb 5	; select bank5
	xorwf	((5328)/8)^0280h,f
	goto	i1l13178
	line	287
	
i1l13206:	
;main.c: 285: else
;main.c: 286: {
;main.c: 287: rdr1_tedge_counter = CCPR2H;
	movlb 5	; select bank5
	movf	(665)^0280h,w	;volatile
	movlb 0	; select bank0
	movwf	(_rdr1_tedge_counter)
	clrf	(_rdr1_tedge_counter+1)
	line	288
;main.c: 288: rdr1_tedge_counter = rdr1_tedge_counter <<8;
	movf	(_rdr1_tedge_counter),w
	movwf	(_rdr1_tedge_counter+1)
	clrf	(_rdr1_tedge_counter)
	line	289
;main.c: 289: rdr1_tedge_counter |= CCPR2L;
	movlb 5	; select bank5
	movf	(664)^0280h,w	;volatile
	movlb 0	; select bank0
	iorwf	(_rdr1_tedge_counter),f
	line	291
;main.c: 291: if (rdr1_tedge_counter > rdr1_ledge_counter)
	movf	(_rdr1_tedge_counter+1),w
	subwf	(_rdr1_ledge_counter+1),w
	skipz
	goto	u97_25
	movf	(_rdr1_tedge_counter),w
	subwf	(_rdr1_ledge_counter),w
u97_25:
	skipnc
	goto	u97_21
	goto	u97_20
u97_21:
	goto	i1l13210
u97_20:
	line	293
	
i1l13208:	
;main.c: 292: {
;main.c: 293: rdr1_bar_width = rdr1_tedge_counter - rdr1_ledge_counter;
	movf	(_rdr1_tedge_counter+1),w
	movwf	(_rdr1_bar_width+1)
	movf	(_rdr1_tedge_counter),w
	movwf	(_rdr1_bar_width)
	movf	(_rdr1_ledge_counter),w
	subwf	(_rdr1_bar_width),f
	movf	(_rdr1_ledge_counter+1),w
	subwfb	(_rdr1_bar_width+1),f
	line	295
;main.c: 295: }
	goto	i1l13260
	line	298
	
i1l13210:	
;main.c: 296: else
;main.c: 297: {
;main.c: 298: rdr1_bar_width = ((0xFFFFU - rdr1_ledge_counter) + rdr1_tedge_counter) + 1U;
	movf	(_rdr1_ledge_counter+1),w
	movwf	(??_isr+0)+0+1
	movf	(_rdr1_ledge_counter),w
	movwf	(??_isr+0)+0
	comf	(??_isr+0)+0,f
	comf	(??_isr+0)+1,f
	movf	0+(??_isr+0)+0,w
	movwf	(_rdr1_bar_width)
	movf	1+(??_isr+0)+0,w
	movwf	(_rdr1_bar_width+1)
	movf	(_rdr1_tedge_counter),w
	addwf	(_rdr1_bar_width),f
	movf	(_rdr1_tedge_counter+1),w
	addwfc	(_rdr1_bar_width+1),f
	
i1l13212:	
	incf	(_rdr1_bar_width),f
	skipnz
	incf	(_rdr1_bar_width+1),f
	goto	i1l13260
	line	305
	
i1l13214:	
;main.c: 305: update_black_bar(2U);
	movlw	02h
	movwf	(?_update_black_bar)
	clrf	(?_update_black_bar+1)
	fcall	_update_black_bar
	line	306
	
i1l13216:	
;main.c: 306: rdr1_current_code = 200000UL;
	movlw	0
	movlb 1	; select bank1
	movwf	(_rdr1_current_code+3)^080h
	movlw	03h
	movwf	(_rdr1_current_code+2)^080h
	movlw	0Dh
	movwf	(_rdr1_current_code+1)^080h
	movlw	040h
	movwf	(_rdr1_current_code)^080h

	line	307
	
i1l13218:	
;main.c: 307: rdr1_bar_count++;
	movlb 0	; select bank0
	incf	(_rdr1_bar_count),f
	line	308
;main.c: 308: break;
	goto	i1l2157
	line	310
	
i1l13220:	
;main.c: 310: update_white_bar(1U);
	clrf	(?_update_white_bar)
	incf	(?_update_white_bar),f
	clrf	(?_update_white_bar+1)
	fcall	_update_white_bar
	line	311
	
i1l13222:	
;main.c: 311: rdr1_current_code = rdr1_current_code + 10000UL;
	movlw	010h
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlw	027h
	addwfc	(_rdr1_current_code+1)^080h,f
	movlw	0
	addwfc	(_rdr1_current_code+2)^080h,f
	movlw	0
	addwfc	(_rdr1_current_code+3)^080h,f
	goto	i1l13218
	line	315
	
i1l13226:	
;main.c: 315: rdr1_current_code = rdr1_current_code + 1000 * bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit, &rdr1_bpos_tol, &rdr1_bneg_tol);
	movlw	(_rdr1_bneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	movlw	(_rdr1_bpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_black_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	fcall	_bar_decision
	movwf	(?i1___wmul)
	clrf	(?i1___wmul+1)
	movlw	low(03E8h)
	movwf	0+(?i1___wmul)+02h
	movlw	high(03E8h)
	movwf	(0+(?i1___wmul)+02h)+1
	fcall	i1___wmul
	movf	(0+(?i1___wmul)),w
	movwf	((??_isr+0)+0)
	movf	(1+(?i1___wmul)),w
	movwf	((??_isr+0)+0+1)
	movlw	0
	btfsc	((??_isr+0)+0+1),7
	movlw	255
	movwf	((??_isr+0)+0+2)
	movwf	((??_isr+0)+0+3)
	movf	0+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlb 0	; select bank0
	movf	1+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+1)^080h,f
	movlb 0	; select bank0
	movf	2+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+2)^080h,f
	movlb 0	; select bank0
	movf	3+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+3)^080h,f
	goto	i1l13218
	line	319
	
i1l13230:	
;main.c: 319: rdr1_current_code = rdr1_current_code + 100 * bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit, &rdr1_wpos_tol, &rdr1_wneg_tol);
	movlw	(_rdr1_wneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	movlw	(_rdr1_wpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_white_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	fcall	_bar_decision
	movwf	(?i1___wmul)
	clrf	(?i1___wmul+1)
	movlw	064h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+(?i1___wmul)),w
	movwf	((??_isr+0)+0)
	movf	(1+(?i1___wmul)),w
	movwf	((??_isr+0)+0+1)
	movlw	0
	btfsc	((??_isr+0)+0+1),7
	movlw	255
	movwf	((??_isr+0)+0+2)
	movwf	((??_isr+0)+0+3)
	movf	0+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlb 0	; select bank0
	movf	1+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+1)^080h,f
	movlb 0	; select bank0
	movf	2+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+2)^080h,f
	movlb 0	; select bank0
	movf	3+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+3)^080h,f
	goto	i1l13218
	line	323
	
i1l13234:	
;main.c: 323: rdr1_current_code = rdr1_current_code + 10 * bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit, &rdr1_bpos_tol, &rdr1_bneg_tol);
	movlw	(_rdr1_bneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	movlw	(_rdr1_bpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_black_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	fcall	_bar_decision
	movwf	(?i1___wmul)
	clrf	(?i1___wmul+1)
	movlw	0Ah
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+(?i1___wmul)),w
	movwf	((??_isr+0)+0)
	movf	(1+(?i1___wmul)),w
	movwf	((??_isr+0)+0+1)
	movlw	0
	btfsc	((??_isr+0)+0+1),7
	movlw	255
	movwf	((??_isr+0)+0+2)
	movwf	((??_isr+0)+0+3)
	movf	0+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlb 0	; select bank0
	movf	1+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+1)^080h,f
	movlb 0	; select bank0
	movf	2+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+2)^080h,f
	movlb 0	; select bank0
	movf	3+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+3)^080h,f
	goto	i1l13218
	line	327
	
i1l13238:	
;main.c: 327: rdr1_current_code = rdr1_current_code + bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit, &rdr1_wpos_tol, &rdr1_wneg_tol);
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	movlw	(_rdr1_white_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_wpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_wneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	fcall	_bar_decision
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlw	1
	skipnc
	addwf	(_rdr1_current_code+1)^080h,f
	skipnc
	addwf	(_rdr1_current_code+2)^080h,f
	skipnc
	addwf	(_rdr1_current_code+3)^080h,f
	line	329
	
i1l13240:	
;main.c: 329: rdr1_rxdata = lookup_sym(rdr1_current_code);
	movf	(_rdr1_current_code+3)^080h,w
	movwf	(?_lookup_sym+3)
	movf	(_rdr1_current_code+2)^080h,w
	movwf	(?_lookup_sym+2)
	movf	(_rdr1_current_code+1)^080h,w
	movwf	(?_lookup_sym+1)
	movf	(_rdr1_current_code)^080h,w
	movwf	(?_lookup_sym)

	fcall	_lookup_sym
	movlb 0	; select bank0
	movwf	(_rdr1_rxdata)
	line	331
	
i1l13242:	
;main.c: 331: if (rdr1_rxdata == 105)
	movf	(_rdr1_rxdata),w
	xorlw	069h&0ffh
	skipz
	goto	u98_21
	goto	u98_20
u98_21:
	goto	i1l13256
u98_20:
	line	333
	
i1l13244:	
;main.c: 332: {
;main.c: 333: rdr1_bcode_data[0] = rdr1_rxdata;
	movf	(_rdr1_rxdata),w
	movlb 1	; select bank1
	movwf	(_rdr1_bcode_data)^080h
	line	334
	
i1l13246:	
;main.c: 334: rdr1_bar_count = 1;
	movlb 0	; select bank0
	clrf	(_rdr1_bar_count)
	incf	(_rdr1_bar_count),f
	line	335
;main.c: 335: if (rdr1_byte_count < 31) rdr1_byte_count++;
	movlw	(01Fh)
	subwf	(_rdr1_byte_count),w
	skipnc
	goto	u99_21
	goto	u99_20
u99_21:
	goto	i1l13250
u99_20:
	
i1l13248:	
	incf	(_rdr1_byte_count),f
	line	336
	
i1l13250:	
;main.c: 336: rdr1_current_code = 0UL;
	movlb 1	; select bank1
	clrf	(_rdr1_current_code)^080h
	clrf	(_rdr1_current_code+1)^080h
	clrf	(_rdr1_current_code+2)^080h
	clrf	(_rdr1_current_code+3)^080h
	line	337
	
i1l13252:	
;main.c: 337: rdr1_state_timer = 20;
	movlw	(014h)
	movlb 0	; select bank0
	movwf	(_rdr1_state_timer)	;volatile
	line	338
	
i1l13254:	
;main.c: 338: rdr1_state = GET_DATA;
	movlw	(03h)
	movwf	(_rdr1_state)
	line	339
;main.c: 339: }
	goto	i1l2157
	line	342
	
i1l13256:	
;main.c: 340: else
;main.c: 341: {
;main.c: 342: init_reader1();
	fcall	i1_init_reader1
	goto	i1l2157
	line	302
	
i1l13260:	
	movf	(_rdr1_bar_count),w
	; Switch size 1, requested type "space"
; Number of cases is 7, Range of values is 1 to 7
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           22    12 (average)
; direct_byte           23     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	i1l13214
	xorlw	2^1	; case 2
	skipnz
	goto	i1l13220
	xorlw	3^2	; case 3
	skipnz
	goto	i1l13226
	xorlw	4^3	; case 4
	skipnz
	goto	i1l13230
	xorlw	5^4	; case 5
	skipnz
	goto	i1l13234
	xorlw	6^5	; case 6
	skipnz
	goto	i1l13238
	xorlw	7^6	; case 7
	skipnz
	goto	i1l2157
	goto	i1l2157
	opt asmopt_on

	line	349
	
i1l2157:	
	line	350
;main.c: 350: { CCP2IE = 0; CCP2M0 = !CCP2M0; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	
i1l13262:	
	movlw	1<<((5328)&7)
	movlb 5	; select bank5
	xorwf	((5328)/8)^0280h,f
	
i1l13264:	
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	
i1l13266:	
	movlb 1	; select bank1
	bsf	(1168/8)^080h,(1168)&7
	line	351
;main.c: 351: rdr1_ledge_counter = rdr1_tedge_counter;
	movlb 0	; select bank0
	movf	(_rdr1_tedge_counter+1),w
	movwf	(_rdr1_ledge_counter+1)
	movf	(_rdr1_tedge_counter),w
	movwf	(_rdr1_ledge_counter)
	line	352
;main.c: 352: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	goto	i1l2145
	line	356
	
i1l13268:	
;main.c: 356: rdr1_tedge_counter = CCPR2H;
	movlb 5	; select bank5
	movf	(665)^0280h,w	;volatile
	movlb 0	; select bank0
	movwf	(_rdr1_tedge_counter)
	clrf	(_rdr1_tedge_counter+1)
	line	357
;main.c: 357: rdr1_tedge_counter = rdr1_tedge_counter <<8;
	movf	(_rdr1_tedge_counter),w
	movwf	(_rdr1_tedge_counter+1)
	clrf	(_rdr1_tedge_counter)
	line	358
;main.c: 358: rdr1_tedge_counter |= CCPR2L;
	movlb 5	; select bank5
	movf	(664)^0280h,w	;volatile
	movlb 0	; select bank0
	iorwf	(_rdr1_tedge_counter),f
	line	360
;main.c: 360: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	362
;main.c: 362: if (rdr1_tedge_counter > rdr1_ledge_counter)
	movf	(_rdr1_tedge_counter+1),w
	subwf	(_rdr1_ledge_counter+1),w
	skipz
	goto	u100_25
	movf	(_rdr1_tedge_counter),w
	subwf	(_rdr1_ledge_counter),w
u100_25:
	skipnc
	goto	u100_21
	goto	u100_20
u100_21:
	goto	i1l13272
u100_20:
	line	364
	
i1l13270:	
;main.c: 363: {
;main.c: 364: rdr1_bar_width = rdr1_tedge_counter - rdr1_ledge_counter;
	movf	(_rdr1_tedge_counter+1),w
	movwf	(_rdr1_bar_width+1)
	movf	(_rdr1_tedge_counter),w
	movwf	(_rdr1_bar_width)
	movf	(_rdr1_ledge_counter),w
	subwf	(_rdr1_bar_width),f
	movf	(_rdr1_ledge_counter+1),w
	subwfb	(_rdr1_bar_width+1),f
	line	366
;main.c: 366: }
	goto	i1l13342
	line	369
	
i1l13272:	
;main.c: 367: else
;main.c: 368: {
;main.c: 369: rdr1_bar_width = (0xFFFF - rdr1_ledge_counter) + rdr1_tedge_counter;
	movf	(_rdr1_ledge_counter+1),w
	movwf	(??_isr+0)+0+1
	movf	(_rdr1_ledge_counter),w
	movwf	(??_isr+0)+0
	comf	(??_isr+0)+0,f
	comf	(??_isr+0)+1,f
	movf	0+(??_isr+0)+0,w
	movwf	(_rdr1_bar_width)
	movf	1+(??_isr+0)+0,w
	movwf	(_rdr1_bar_width+1)
	movf	(_rdr1_tedge_counter),w
	addwf	(_rdr1_bar_width),f
	movf	(_rdr1_tedge_counter+1),w
	addwfc	(_rdr1_bar_width+1),f
	goto	i1l13342
	line	376
	
i1l13274:	
;main.c: 376: rdr1_current_code = rdr1_current_code + 100000 * bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit, &rdr1_bpos_tol, &rdr1_bneg_tol);
	movlw	(_rdr1_bneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	movlw	(_rdr1_bpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_black_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	fcall	_bar_decision
	movwf	(?___lmul)
	clrf	(?___lmul+1)
	clrf	(?___lmul+2)
	clrf	(?___lmul+3)

	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	01h
	movwf	2+(?___lmul)+04h
	movlw	086h
	movwf	1+(?___lmul)+04h
	movlw	0A0h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(0+(?___lmul)),w
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlb 0	; select bank0
	movf	(1+(?___lmul)),w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+1)^080h,f
	movlb 0	; select bank0
	movf	(2+(?___lmul)),w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+2)^080h,f
	movlb 0	; select bank0
	movf	(3+(?___lmul)),w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+3)^080h,f
	goto	i1l13218
	line	380
	
i1l13278:	
;main.c: 380: rdr1_current_code = rdr1_current_code + 10000 * bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit, &rdr1_wpos_tol, &rdr1_wneg_tol);
	movlw	(_rdr1_wneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	movlw	(_rdr1_wpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_white_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	fcall	_bar_decision
	movwf	(?i1___wmul)
	clrf	(?i1___wmul+1)
	movlw	low(02710h)
	movwf	0+(?i1___wmul)+02h
	movlw	high(02710h)
	movwf	(0+(?i1___wmul)+02h)+1
	fcall	i1___wmul
	movf	(0+(?i1___wmul)),w
	movwf	((??_isr+0)+0)
	movf	(1+(?i1___wmul)),w
	movwf	((??_isr+0)+0+1)
	movlw	0
	btfsc	((??_isr+0)+0+1),7
	movlw	255
	movwf	((??_isr+0)+0+2)
	movwf	((??_isr+0)+0+3)
	movf	0+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlb 0	; select bank0
	movf	1+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+1)^080h,f
	movlb 0	; select bank0
	movf	2+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+2)^080h,f
	movlb 0	; select bank0
	movf	3+(??_isr+0)+0,w
	movlb 1	; select bank1
	addwfc	(_rdr1_current_code+3)^080h,f
	goto	i1l13218
	line	396
	
i1l13294:	
;main.c: 396: rdr1_current_code = rdr1_current_code + bar_decision(&rdr1_bar_width, &rdr1_white_bar_unit, &rdr1_wpos_tol, &rdr1_wneg_tol);
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	movlw	(_rdr1_white_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_wpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_wneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	fcall	_bar_decision
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlw	1
	skipnc
	addwf	(_rdr1_current_code+1)^080h,f
	skipnc
	addwf	(_rdr1_current_code+2)^080h,f
	skipnc
	addwf	(_rdr1_current_code+3)^080h,f
	line	398
	
i1l13296:	
;main.c: 398: rdr1_rxdata = lookup_sym(rdr1_current_code);
	movf	(_rdr1_current_code+3)^080h,w
	movwf	(?_lookup_sym+3)
	movf	(_rdr1_current_code+2)^080h,w
	movwf	(?_lookup_sym+2)
	movf	(_rdr1_current_code+1)^080h,w
	movwf	(?_lookup_sym+1)
	movf	(_rdr1_current_code)^080h,w
	movwf	(?_lookup_sym)

	fcall	_lookup_sym
	movlb 0	; select bank0
	movwf	(_rdr1_rxdata)
	line	400
	
i1l13298:	
;main.c: 400: if (rdr1_rxdata != 155)
	movf	(_rdr1_rxdata),w
	xorlw	09Bh&0ffh
	skipnz
	goto	u101_21
	goto	u101_20
u101_21:
	goto	i1l13310
u101_20:
	line	402
	
i1l13300:	
;main.c: 401: {
;main.c: 402: rdr1_bcode_data[rdr1_byte_count] = rdr1_rxdata;
	movf	(_rdr1_byte_count),w
	addlw	_rdr1_bcode_data&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(_rdr1_rxdata),w
	movwf	indf1
	line	403
	
i1l13302:	
;main.c: 403: rdr1_bar_count =1;
	clrf	(_rdr1_bar_count)
	incf	(_rdr1_bar_count),f
	line	404
	
i1l13304:	
;main.c: 404: if (rdr1_byte_count < 31) rdr1_byte_count++;
	movlw	(01Fh)
	subwf	(_rdr1_byte_count),w
	skipnc
	goto	u102_21
	goto	u102_20
u102_21:
	goto	i1l13308
u102_20:
	
i1l13306:	
	incf	(_rdr1_byte_count),f
	line	405
	
i1l13308:	
;main.c: 405: rdr1_current_code = 0UL;
	movlb 1	; select bank1
	clrf	(_rdr1_current_code)^080h
	clrf	(_rdr1_current_code+1)^080h
	clrf	(_rdr1_current_code+2)^080h
	clrf	(_rdr1_current_code+3)^080h
	line	406
;main.c: 406: }
	goto	i1l2157
	line	407
	
i1l13310:	
;main.c: 407: else if (rdr1_rxdata == 255)
	movf	(_rdr1_rxdata),w
	xorlw	0FFh&0ffh
	skipz
	goto	u103_21
	goto	u103_20
u103_21:
	goto	i1l13316
u103_20:
	line	409
	
i1l13312:	
;main.c: 408: {
;main.c: 409: rdr1_got_error = TRUE;
	clrf	(_rdr1_got_error)
	incf	(_rdr1_got_error),f
	goto	i1l13256
	line	414
	
i1l13316:	
;main.c: 412: else
;main.c: 413: {
;main.c: 414: rdr1_current_code = rdr1_current_code * 10;
	movlw	0Ah
	movwf	(?___lmul)
	clrf	(?___lmul+1)
	clrf	(?___lmul+2)
	clrf	(?___lmul+3)

	movlb 1	; select bank1
	movf	(_rdr1_current_code+3)^080h,w
	movlb 0	; select bank0
	movwf	3+(?___lmul)+04h
	movlb 1	; select bank1
	movf	(_rdr1_current_code+2)^080h,w
	movlb 0	; select bank0
	movwf	2+(?___lmul)+04h
	movlb 1	; select bank1
	movf	(_rdr1_current_code+1)^080h,w
	movlb 0	; select bank0
	movwf	1+(?___lmul)+04h
	movlb 1	; select bank1
	movf	(_rdr1_current_code)^080h,w
	movlb 0	; select bank0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(3+(?___lmul)),w
	movlb 1	; select bank1
	movwf	(_rdr1_current_code+3)^080h
	movlb 0	; select bank0
	movf	(2+(?___lmul)),w
	movlb 1	; select bank1
	movwf	(_rdr1_current_code+2)^080h
	movlb 0	; select bank0
	movf	(1+(?___lmul)),w
	movlb 1	; select bank1
	movwf	(_rdr1_current_code+1)^080h
	movlb 0	; select bank0
	movf	(0+(?___lmul)),w
	movlb 1	; select bank1
	movwf	(_rdr1_current_code)^080h

	goto	i1l13218
	line	419
	
i1l13320:	
;main.c: 419: rdr1_current_code = rdr1_current_code + bar_decision(&rdr1_bar_width, &rdr1_black_bar_unit, &rdr1_bpos_tol, &rdr1_bneg_tol);
	movlw	(_rdr1_bar_width)&0ffh
	movwf	(?_bar_decision)
	movlw	(_rdr1_black_bar_unit)&0ffh
	movwf	(0+?_bar_decision+01h)
	movlw	(_rdr1_bpos_tol)&0ffh
	movwf	(0+?_bar_decision+02h)
	movlw	(_rdr1_bneg_tol)&0ffh
	movwf	(0+?_bar_decision+03h)
	fcall	_bar_decision
	movlb 1	; select bank1
	addwf	(_rdr1_current_code)^080h,f
	movlw	1
	skipnc
	addwf	(_rdr1_current_code+1)^080h,f
	skipnc
	addwf	(_rdr1_current_code+2)^080h,f
	skipnc
	addwf	(_rdr1_current_code+3)^080h,f
	line	421
	
i1l13322:	
;main.c: 421: rdr1_rxdata = lookup_sym(rdr1_current_code);
	movf	(_rdr1_current_code+3)^080h,w
	movwf	(?_lookup_sym+3)
	movf	(_rdr1_current_code+2)^080h,w
	movwf	(?_lookup_sym+2)
	movf	(_rdr1_current_code+1)^080h,w
	movwf	(?_lookup_sym+1)
	movf	(_rdr1_current_code)^080h,w
	movwf	(?_lookup_sym)

	fcall	_lookup_sym
	movlb 0	; select bank0
	movwf	(_rdr1_rxdata)
	line	423
	
i1l13324:	
;main.c: 423: if (rdr1_rxdata == 106)
	movf	(_rdr1_rxdata),w
	xorlw	06Ah&0ffh
	skipz
	goto	u104_21
	goto	u104_20
u104_21:
	goto	i1l13256
u104_20:
	line	425
	
i1l13326:	
;main.c: 424: {
;main.c: 425: rdr1_bcode_data[rdr1_byte_count] = rdr1_rxdata;
	movf	(_rdr1_byte_count),w
	addlw	_rdr1_bcode_data&0ffh
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(_rdr1_rxdata),w
	movwf	indf1
	line	426
	
i1l13328:	
;main.c: 426: rdr1_bar_count = 1;
	clrf	(_rdr1_bar_count)
	incf	(_rdr1_bar_count),f
	line	427
	
i1l13330:	
;main.c: 427: rdr1_byte_count = 0;
	clrf	(_rdr1_byte_count)
	line	429
	
i1l13332:	
;main.c: 429: if (checksum(rdr1_bcode_data)) rdr1_got_bcode = TRUE;
	movlw	(_rdr1_bcode_data)&0ffh
	fcall	_checksum
	xorlw	0&0ffh
	skipnz
	goto	u105_21
	goto	u105_20
u105_21:
	goto	i1l13256
u105_20:
	
i1l13334:	
	clrf	(_rdr1_got_bcode)
	incf	(_rdr1_got_bcode),f
	goto	i1l13256
	line	373
	
i1l13342:	
	movf	(_rdr1_bar_count),w
	; Switch size 1, requested type "space"
; Number of cases is 7, Range of values is 1 to 7
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           22    12 (average)
; direct_byte           23     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	i1l13274
	xorlw	2^1	; case 2
	skipnz
	goto	i1l13278
	xorlw	3^2	; case 3
	skipnz
	goto	i1l13226
	xorlw	4^3	; case 4
	skipnz
	goto	i1l13230
	xorlw	5^4	; case 5
	skipnz
	goto	i1l13234
	xorlw	6^5	; case 6
	skipnz
	goto	i1l13294
	xorlw	7^6	; case 7
	skipnz
	goto	i1l13320
	goto	i1l2157
	opt asmopt_on

	line	257
	
i1l13352:	
	movf	(_rdr1_state),w
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 0 to 3
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; direct_byte           14     6 (fixed)
; jumptable            260     6 (fixed)
; rangetable             8     4 (fixed)
; spacedrange           13     6 (fixed)
; locatedrange           4     3 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	i1l13172
	xorlw	1^0	; case 1
	skipnz
	goto	i1l13182
	xorlw	2^1	; case 2
	skipnz
	goto	i1l13192
	xorlw	3^2	; case 3
	skipnz
	goto	i1l13268
	goto	i1l2145
	opt asmopt_on

	line	447
	
i1l2145:	
	line	454
;main.c: 447: }
;main.c: 454: if (CCP1IF && CCP1IE)
	movlb 0	; select bank0
	btfss	(138/8),(138)&7
	goto	u106_21
	goto	u106_20
u106_21:
	goto	i1l2190
u106_20:
	
i1l13354:	
	movlb 1	; select bank1
	btfss	(1162/8)^080h,(1162)&7
	goto	u107_21
	goto	u107_20
u107_21:
	goto	i1l2190
u107_20:
	line	456
	
i1l13356:	
;main.c: 455: {
;main.c: 456: CCP1IF = 0;
	movlb 0	; select bank0
	bcf	(138/8),(138)&7
	line	459
	
i1l2190:	
	line	466
;main.c: 459: }
;main.c: 466: if(T0IF && TMR0IE)
	btfss	(90/8),(90)&7
	goto	u108_21
	goto	u108_20
u108_21:
	goto	i1l2205
u108_20:
	
i1l13358:	
	btfss	(93/8),(93)&7
	goto	u109_21
	goto	u109_20
u109_21:
	goto	i1l2205
u109_20:
	line	468
	
i1l13360:	
;main.c: 467: {
;main.c: 468: T0IF = 0;
	bcf	(90/8),(90)&7
	line	469
	
i1l13362:	
;main.c: 469: RA1 = !RA1;
	movlw	1<<((97)&7)
	movlb 0	; select bank0
	xorwf	((97)/8),f
	line	472
;main.c: 472: switch(rdr1_state)
	goto	i1l13396
	line	475
	
i1l13364:	
;main.c: 475: if (rdr1_state_timer)
	movf	(_rdr1_state_timer),w	;volatile
	skipz
	goto	u110_20
	goto	i1l2194
u110_20:
	line	478
	
i1l13366:	
;main.c: 476: {
;main.c: 478: rdr1_state_timer--;
	decf	(_rdr1_state_timer),f	;volatile
	line	480
;main.c: 480: }
	goto	i1l2205
	line	481
	
i1l2194:	
	line	483
;main.c: 481: else
;main.c: 482: {
;main.c: 483: RA1 = 1;
	bsf	(97/8),(97)&7
	line	485
	
i1l13368:	
;main.c: 485: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	486
	
i1l13370:	
;main.c: 486: rdr1_bar_count = 1;
	clrf	(_rdr1_bar_count)
	incf	(_rdr1_bar_count),f
	line	487
	
i1l13372:	
;main.c: 487: rdr1_first_edge = TRUE;
	clrf	(_rdr1_first_edge)
	incf	(_rdr1_first_edge),f
	line	488
;main.c: 488: rdr1_state = GET_FIRST_SYMBOL;
	movlw	(02h)
	movwf	(_rdr1_state)
	line	489
	
i1l13374:	
;main.c: 489: { CCP2IE = 0; CCP2CON = 0x05; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	movlw	(05h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	
i1l13376:	
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	
i1l13378:	
	movlb 1	; select bank1
	bsf	(1168/8)^080h,(1168)&7
	goto	i1l2205
	line	493
	
i1l13380:	
;main.c: 493: if (!rdr1_first_edge)
	movf	(_rdr1_first_edge),f
	skipz
	goto	u111_21
	goto	u111_20
u111_21:
	goto	i1l2205
u111_20:
	line	495
	
i1l13382:	
;main.c: 494: {
;main.c: 495: if(rdr1_state_timer)
	movf	(_rdr1_state_timer),w	;volatile
	skipz
	goto	u112_20
	goto	i1l13386
u112_20:
	goto	i1l13366
	line	503
	
i1l13386:	
;main.c: 500: else
;main.c: 501: {
;main.c: 503: init_reader1();
	fcall	i1_init_reader1
	goto	i1l2205
	line	509
	
i1l13388:	
;main.c: 509: if(rdr1_state_timer)
	movf	(_rdr1_state_timer),w	;volatile
	skipz
	goto	u113_20
	goto	i1l13392
u113_20:
	line	512
	
i1l13390:	
;main.c: 510: {
;main.c: 512: rdr1_state_timer--;
	decf	(_rdr1_state_timer),f	;volatile
	line	513
;main.c: 513: }
	goto	i1l13386
	line	517
	
i1l13392:	
;main.c: 514: else
;main.c: 515: {
;main.c: 517: init_reader1();
	fcall	i1_init_reader1
	goto	i1l13386
	line	472
	
i1l13396:	
	movf	(_rdr1_state),w
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 1 to 3
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
; direct_byte           15     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	i1l13364
	xorlw	2^1	; case 2
	skipnz
	goto	i1l13380
	xorlw	3^2	; case 3
	skipnz
	goto	i1l13388
	goto	i1l13386
	opt asmopt_on

	line	527
	
i1l2205:	
	retfie
	opt stack 0
GLOBAL	__end_of_isr
	__end_of_isr:
;; =============== function _isr ends ============

	signat	_isr,88
	global	_checksum
psect	text660,local,class=CODE,delta=2
global __ptext660
__ptext660:

;; *************** function _checksum *****************
;; Defined at:
;;		line 917 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;  bcode           1    wreg     PTR unsigned char 
;;		 -> rdr1_bcode_data(32), 
;; Auto vars:     Size  Location     Type
;;  bcode           1    9[COMMON] PTR unsigned char 
;;		 -> rdr1_bcode_data(32), 
;;  sum             2    7[COMMON] unsigned short 
;;  i               1   10[COMMON] unsigned char 
;;  modulo          1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      enum E3219
;; Registers used:
;;		wreg, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         4       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0
;;      Totals:         5       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1___wmul
;;		i1___lwmod
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text660
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	917
	global	__size_of_checksum
	__size_of_checksum	equ	__end_of_checksum-_checksum
	
_checksum:	
	opt	stack 11
; Regs used in _checksum: [wreg+fsr1l-status,0+pclath+cstack]
;checksum@bcode stored from wreg
	line	922
	movwf	(checksum@bcode)
	
i1l13816:	
;main.c: 918: UINT16 sum;
;main.c: 919: BYTE i;
;main.c: 920: BYTE modulo;
;main.c: 922: sum = bcode[0];
	movf	(checksum@bcode),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	movwf	(checksum@sum)
	clrf	(checksum@sum+1)
	line	923
	
i1l13818:	
;main.c: 923: i = 1;
	clrf	(checksum@i)
	incf	(checksum@i),f
	line	925
;main.c: 925: while (bcode[i+1] != 106)
	goto	i1l13824
	line	927
	
i1l13820:	
;main.c: 926: {
;main.c: 927: sum = sum + bcode[i] * i;
	movf	(checksum@i),w
	addwf	(checksum@bcode),w
	movwf	(??_checksum+0)+0
	movf	0+(??_checksum+0)+0,w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	movwf	(?i1___wmul)
	clrf	(?i1___wmul+1)
	movf	(checksum@i),w
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+(?i1___wmul)),w
	addwf	(checksum@sum),f
	movf	(1+(?i1___wmul)),w
	addwfc	(checksum@sum+1),f
	line	928
	
i1l13822:	
;main.c: 928: i++;
	incf	(checksum@i),f
	line	925
	
i1l13824:	
	movf	(checksum@i),w
	addwf	(checksum@bcode),w
	movwf	(??_checksum+0)+0
	movf	0+(??_checksum+0)+0,w
	addlw	01h
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	xorlw	06Ah&0ffh
	skipz
	goto	u120_21
	goto	u120_20
u120_21:
	goto	i1l13820
u120_20:
	line	931
	
i1l13826:	
;main.c: 929: }
;main.c: 931: modulo = sum % 103;
	movlw	067h
	movwf	(?i1___lwmod)
	clrf	(?i1___lwmod+1)
	movf	(checksum@sum+1),w
	movwf	1+(?i1___lwmod)+02h
	movf	(checksum@sum),w
	movwf	0+(?i1___lwmod)+02h
	fcall	i1___lwmod
	line	933
	
i1l13828:	
;main.c: 933: if (modulo = bcode[i])
	movf	(checksum@i),w
	addwf	(checksum@bcode),w
	movwf	(??_checksum+0)+0
	movf	0+(??_checksum+0)+0,w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	indf1,w
	xorlw	0&0ffh
	skipnz
	goto	u121_21
	goto	u121_20
u121_21:
	goto	i1l13836
u121_20:
	line	934
	
i1l13830:	
;main.c: 934: return TRUE;
	movlw	(01h)
	goto	i1l2342
	line	936
	
i1l13836:	
;main.c: 935: else
;main.c: 936: return FALSE;
	movlw	(0)
	line	937
	
i1l2342:	
	return
	opt stack 0
GLOBAL	__end_of_checksum
	__end_of_checksum:
;; =============== function _checksum ends ============

	signat	_checksum,4217
	global	_bar_decision
psect	text661,local,class=CODE,delta=2
global __ptext661
__ptext661:

;; *************** function _bar_decision *****************
;; Defined at:
;;		line 940 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;  bar_width       1    8[BANK0 ] PTR unsigned short 
;;		 -> rdr1_bar_width(2), 
;;  bar_unit        1    9[BANK0 ] PTR unsigned short 
;;		 -> rdr1_white_bar_unit(2), rdr1_black_bar_unit(2), 
;;  pos_tol         1   10[BANK0 ] PTR unsigned short 
;;		 -> rdr1_wpos_tol(2), rdr1_bpos_tol(2), 
;;  neg_tol         1   11[BANK0 ] PTR unsigned short 
;;		 -> rdr1_wneg_tol(2), rdr1_bneg_tol(2), 
;; Auto vars:     Size  Location     Type
;;  retvalue        1   11[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       4       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0
;;      Temps:          0       6       0       0       0       0       0       0
;;      Totals:         1      10       0       0       0       0       0       0
;;Total ram usage:       11 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1___wmul
;;		i1___lwdiv
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text661
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	940
	global	__size_of_bar_decision
	__size_of_bar_decision	equ	__end_of_bar_decision-_bar_decision
	
_bar_decision:	
	opt	stack 11
; Regs used in _bar_decision: [wreg-status,0+pclath+cstack]
	line	943
	
i1l13410:	
;main.c: 941: BYTE retvalue;
;main.c: 943: if ( (*bar_unit + *pos_tol > *bar_width) && ( *bar_width > *bar_unit - *neg_tol) )
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(bar_decision@bar_unit),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr1
	addwf	indf0,w
	movwf	(??_bar_decision+0)+0
	addfsr	fsr0,1
	moviw	[1]fsr1
	addwfc	indf0,w
	movwf	(??_bar_decision+0)+0+1
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+2)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+2)+0+1
	movf	1+(??_bar_decision+0)+0,w
	subwf	1+(??_bar_decision+2)+0,w
	skipz
	goto	u114_25
	movf	0+(??_bar_decision+0)+0,w
	subwf	0+(??_bar_decision+2)+0,w
u114_25:
	skipnc
	goto	u114_21
	goto	u114_20
u114_21:
	goto	i1l13424
u114_20:
	
i1l13412:	
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+0)+0+1
	movf	(bar_decision@neg_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(bar_decision@bar_unit),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr1
	subwf	indf0,w
	movwf	(??_bar_decision+2)+0
	addfsr	fsr0,1
	moviw	[1]fsr1
	subwfb	indf0,w
	movwf	(??_bar_decision+2)+0+1
	movf	1+(??_bar_decision+0)+0,w
	subwf	1+(??_bar_decision+2)+0,w
	skipz
	goto	u115_25
	movf	0+(??_bar_decision+0)+0,w
	subwf	0+(??_bar_decision+2)+0,w
u115_25:
	skipnc
	goto	u115_21
	goto	u115_20
u115_21:
	goto	i1l13424
u115_20:
	line	945
	
i1l13414:	
;main.c: 944: {
;main.c: 945: *bar_unit = *bar_width;
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+0)+0+1
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	946
	
i1l13416:	
;main.c: 946: *pos_tol = (*bar_unit * 3)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+?i1___wmul),w
	movwf	(??_bar_decision+0)+0
	movf	(1+?i1___wmul),w
	movwf	((??_bar_decision+0)+0+1)
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	947
	
i1l13418:	
;main.c: 947: rdr1_bneg_tol = (*bar_unit * 5)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	05h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol)
	
i1l13420:	
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	line	948
	
i1l13422:	
;main.c: 948: retvalue = 1U;
	clrf	(bar_decision@retvalue)
	incf	(bar_decision@retvalue),f
	line	949
;main.c: 949: }
	goto	i1l13458
	line	950
	
i1l13424:	
;main.c: 950: else if ( (2 * *bar_unit + *pos_tol > *bar_width) && (*bar_width > 2 * *bar_unit - *neg_tol) )
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(bar_decision@bar_unit),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr0
	movwf	(??_bar_decision+0)+0+1
	lslf	(??_bar_decision+0)+0,f
	rlf	(??_bar_decision+0)+1,f
	moviw	[0]fsr1
	addwf	0+(??_bar_decision+0)+0,w
	movwf	(??_bar_decision+2)+0
	moviw	[1]fsr1
	addwfc	1+(??_bar_decision+0)+0,w
	movwf	(??_bar_decision+2)+0+1
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+4)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+4)+0+1
	movf	1+(??_bar_decision+2)+0,w
	subwf	1+(??_bar_decision+4)+0,w
	skipz
	goto	u116_25
	movf	0+(??_bar_decision+2)+0,w
	subwf	0+(??_bar_decision+4)+0,w
u116_25:
	skipnc
	goto	u116_21
	goto	u116_20
u116_21:
	goto	i1l13438
u116_20:
	
i1l13426:	
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+0)+0+1
	movf	(bar_decision@neg_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(bar_decision@bar_unit),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(??_bar_decision+2)+0
	moviw	[1]fsr0
	movwf	(??_bar_decision+2)+0+1
	lslf	(??_bar_decision+2)+0,f
	rlf	(??_bar_decision+2)+1,f
	moviw	[0]fsr1
	subwf	0+(??_bar_decision+2)+0,w
	movwf	(??_bar_decision+4)+0
	moviw	[1]fsr1
	subwfb	1+(??_bar_decision+2)+0,w
	movwf	(??_bar_decision+4)+0+1
	movf	1+(??_bar_decision+0)+0,w
	subwf	1+(??_bar_decision+4)+0,w
	skipz
	goto	u117_25
	movf	0+(??_bar_decision+0)+0,w
	subwf	0+(??_bar_decision+4)+0,w
u117_25:
	skipnc
	goto	u117_21
	goto	u117_20
u117_21:
	goto	i1l13438
u117_20:
	line	952
	
i1l13428:	
;main.c: 951: {
;main.c: 952: *bar_unit = *bar_width/2;
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+0)+0+1
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	953
	
i1l13430:	
;main.c: 953: *pos_tol = (*bar_unit * 3)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+?i1___wmul),w
	movwf	(??_bar_decision+0)+0
	movf	(1+?i1___wmul),w
	movwf	((??_bar_decision+0)+0+1)
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	954
	
i1l13432:	
;main.c: 954: rdr1_bneg_tol = (*bar_unit * 5)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	05h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol)
	
i1l13434:	
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	line	955
	
i1l13436:	
;main.c: 955: retvalue = 2U;
	movlw	(02h)
	movwf	(bar_decision@retvalue)
	line	956
;main.c: 956: }
	goto	i1l13458
	line	957
	
i1l13438:	
;main.c: 957: else if ( (3 * *bar_unit + *pos_tol > *bar_width) && (*bar_width > 3 * *bar_unit - *neg_tol) )
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(bar_decision@bar_unit),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(?i1___wmul)
	moviw	[1]fsr0
	movwf	(?i1___wmul+1)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	moviw	[0]fsr1
	addwf	(0+(?i1___wmul)),w
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	addwfc	(1+(?i1___wmul)),w
	movwf	(??_bar_decision+0)+0+1
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+2)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+2)+0+1
	movf	1+(??_bar_decision+0)+0,w
	subwf	1+(??_bar_decision+2)+0,w
	skipz
	goto	u118_25
	movf	0+(??_bar_decision+0)+0,w
	subwf	0+(??_bar_decision+2)+0,w
u118_25:
	skipnc
	goto	u118_21
	goto	u118_20
u118_21:
	goto	i1l13448
u118_20:
	
i1l13440:	
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+0)+0+1
	movf	(bar_decision@neg_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(bar_decision@bar_unit),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(?i1___wmul)
	moviw	[1]fsr0
	movwf	(?i1___wmul+1)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	moviw	[0]fsr1
	subwf	(0+(?i1___wmul)),w
	movwf	(??_bar_decision+2)+0
	moviw	[1]fsr1
	subwfb	(1+(?i1___wmul)),w
	movwf	(??_bar_decision+2)+0+1
	movf	1+(??_bar_decision+0)+0,w
	subwf	1+(??_bar_decision+2)+0,w
	skipz
	goto	u119_25
	movf	0+(??_bar_decision+0)+0,w
	subwf	0+(??_bar_decision+2)+0,w
u119_25:
	skipnc
	goto	u119_21
	goto	u119_20
u119_21:
	goto	i1l13448
u119_20:
	line	959
	
i1l13442:	
;main.c: 958: {
;main.c: 959: *bar_unit = *bar_width/3;
	movlw	03h
	movwf	(?i1___lwdiv)
	clrf	(?i1___lwdiv+1)
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	0+(?i1___lwdiv)+02h
	moviw	[1]fsr1
	movwf	1+(?i1___lwdiv)+02h
	fcall	i1___lwdiv
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	(0+(?i1___lwdiv)),w
	movwi	[0]fsr1
	movf	(1+(?i1___lwdiv)),w
	movwi	[1]fsr1
	line	960
;main.c: 960: *pos_tol = (*bar_unit * 3)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+?i1___wmul),w
	movwf	(??_bar_decision+0)+0
	movf	(1+?i1___wmul),w
	movwf	((??_bar_decision+0)+0+1)
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	961
;main.c: 961: rdr1_bneg_tol = (*bar_unit * 5)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	05h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol)
	
i1l13444:	
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	line	962
	
i1l13446:	
;main.c: 962: retvalue = 3U;
	movlw	(03h)
	movwf	(bar_decision@retvalue)
	line	963
;main.c: 963: }
	goto	i1l13458
	line	966
	
i1l13448:	
;main.c: 964: else
;main.c: 965: {
;main.c: 966: *bar_unit = *bar_width/4;
	movf	(bar_decision@bar_width),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(??_bar_decision+0)+0
	moviw	[1]fsr1
	movwf	(??_bar_decision+0)+0+1
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	967
	
i1l13450:	
;main.c: 967: *pos_tol = (*bar_unit * 3)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(0+?i1___wmul),w
	movwf	(??_bar_decision+0)+0
	movf	(1+?i1___wmul),w
	movwf	((??_bar_decision+0)+0+1)
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	lsrf	(??_bar_decision+0)+1,f
	rrf	(??_bar_decision+0)+0,f
	movf	(bar_decision@pos_tol),w
	movwf	fsr1l
	clrf fsr1h	
	
	movf	0+(??_bar_decision+0)+0,w
	movwi	[0]fsr1
	movf	1+(??_bar_decision+0)+0,w
	movwi	[1]fsr1
	line	968
	
i1l13452:	
;main.c: 968: rdr1_bneg_tol = (*bar_unit * 5)/8;
	movf	(bar_decision@bar_unit),w
	movwf	fsr1l
	clrf fsr1h	
	
	moviw	[0]fsr1
	movwf	(?i1___wmul)
	moviw	[1]fsr1
	movwf	(?i1___wmul+1)
	movlw	05h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol)
	
i1l13454:	
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	line	969
	
i1l13456:	
;main.c: 969: retvalue = 4U;
	movlw	(04h)
	movwf	(bar_decision@retvalue)
	line	972
	
i1l13458:	
;main.c: 970: }
;main.c: 972: return retvalue;
	movf	(bar_decision@retvalue),w
	line	973
	
i1l2352:	
	return
	opt stack 0
GLOBAL	__end_of_bar_decision
	__end_of_bar_decision:
;; =============== function _bar_decision ends ============

	signat	_bar_decision,16505
	global	_update_white_bar
psect	text662,local,class=CODE,delta=2
global __ptext662
__ptext662:

;; *************** function _update_white_bar *****************
;; Defined at:
;;		line 875 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;  barwidth        2    7[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         2       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1___lwdiv
;;		i1___wmul
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text662
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	875
	global	__size_of_update_white_bar
	__size_of_update_white_bar	equ	__end_of_update_white_bar-_update_white_bar
	
_update_white_bar:	
	opt	stack 11
; Regs used in _update_white_bar: [wreg+status,2+status,0+pclath+cstack]
	line	876
	
i1l13404:	
;main.c: 876: rdr1_white_bar_unit = rdr1_bar_width/barwidth;
	movf	(update_white_bar@barwidth+1),w
	movwf	(?i1___lwdiv+1)
	movf	(update_white_bar@barwidth),w
	movwf	(?i1___lwdiv)
	movf	(_rdr1_bar_width+1),w
	movwf	1+(?i1___lwdiv)+02h
	movf	(_rdr1_bar_width),w
	movwf	0+(?i1___lwdiv)+02h
	fcall	i1___lwdiv
	movf	(1+(?i1___lwdiv)),w
	movwf	(_rdr1_white_bar_unit+1)
	movf	(0+(?i1___lwdiv)),w
	movwf	(_rdr1_white_bar_unit)
	line	877
;main.c: 877: rdr1_wpos_tol = (rdr1_black_bar_unit * 3)/8 ;
	movf	(_rdr1_black_bar_unit+1),w
	movwf	(?i1___wmul+1)
	movf	(_rdr1_black_bar_unit),w
	movwf	(?i1___wmul)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_wpos_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_wpos_tol)
	
i1l13406:	
	lsrf	(_rdr1_wpos_tol+1),f
	rrf	(_rdr1_wpos_tol),f
	lsrf	(_rdr1_wpos_tol+1),f
	rrf	(_rdr1_wpos_tol),f
	lsrf	(_rdr1_wpos_tol+1),f
	rrf	(_rdr1_wpos_tol),f
	line	878
	
i1l13408:	
;main.c: 878: rdr1_wneg_tol = (rdr1_black_bar_unit * 5)/8;
	movf	(_rdr1_black_bar_unit+1),w
	movwf	(?i1___wmul+1)
	movf	(_rdr1_black_bar_unit),w
	movwf	(?i1___wmul)
	movlw	05h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_wneg_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_wneg_tol)
	lsrf	(_rdr1_wneg_tol+1),f
	rrf	(_rdr1_wneg_tol),f
	lsrf	(_rdr1_wneg_tol+1),f
	rrf	(_rdr1_wneg_tol),f
	lsrf	(_rdr1_wneg_tol+1),f
	rrf	(_rdr1_wneg_tol),f
	line	879
	
i1l2325:	
	return
	opt stack 0
GLOBAL	__end_of_update_white_bar
	__end_of_update_white_bar:
;; =============== function _update_white_bar ends ============

	signat	_update_white_bar,4216
	global	_update_black_bar
psect	text663,local,class=CODE,delta=2
global __ptext663
__ptext663:

;; *************** function _update_black_bar *****************
;; Defined at:
;;		line 868 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;  barwidth        2    7[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         2       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		i1___lwdiv
;;		i1___wmul
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text663
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	868
	global	__size_of_update_black_bar
	__size_of_update_black_bar	equ	__end_of_update_black_bar-_update_black_bar
	
_update_black_bar:	
	opt	stack 11
; Regs used in _update_black_bar: [wreg+status,2+status,0+pclath+cstack]
	line	869
	
i1l13398:	
;main.c: 869: rdr1_black_bar_unit = rdr1_bar_width/barwidth;
	movf	(update_black_bar@barwidth+1),w
	movwf	(?i1___lwdiv+1)
	movf	(update_black_bar@barwidth),w
	movwf	(?i1___lwdiv)
	movf	(_rdr1_bar_width+1),w
	movwf	1+(?i1___lwdiv)+02h
	movf	(_rdr1_bar_width),w
	movwf	0+(?i1___lwdiv)+02h
	fcall	i1___lwdiv
	movf	(1+(?i1___lwdiv)),w
	movwf	(_rdr1_black_bar_unit+1)
	movf	(0+(?i1___lwdiv)),w
	movwf	(_rdr1_black_bar_unit)
	line	870
;main.c: 870: rdr1_bpos_tol = (rdr1_black_bar_unit * 3)/8;
	movf	(_rdr1_black_bar_unit+1),w
	movwf	(?i1___wmul+1)
	movf	(_rdr1_black_bar_unit),w
	movwf	(?i1___wmul)
	movlw	03h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_bpos_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_bpos_tol)
	
i1l13400:	
	lsrf	(_rdr1_bpos_tol+1),f
	rrf	(_rdr1_bpos_tol),f
	lsrf	(_rdr1_bpos_tol+1),f
	rrf	(_rdr1_bpos_tol),f
	lsrf	(_rdr1_bpos_tol+1),f
	rrf	(_rdr1_bpos_tol),f
	line	871
	
i1l13402:	
;main.c: 871: rdr1_bneg_tol = (rdr1_black_bar_unit * 5)/8;
	movf	(_rdr1_black_bar_unit+1),w
	movwf	(?i1___wmul+1)
	movf	(_rdr1_black_bar_unit),w
	movwf	(?i1___wmul)
	movlw	05h
	movwf	0+(?i1___wmul)+02h
	clrf	1+(?i1___wmul)+02h
	fcall	i1___wmul
	movf	(1+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol+1)
	movf	(0+(?i1___wmul)),w
	movwf	(_rdr1_bneg_tol)
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	lsrf	(_rdr1_bneg_tol+1),f
	rrf	(_rdr1_bneg_tol),f
	line	872
	
i1l2322:	
	return
	opt stack 0
GLOBAL	__end_of_update_black_bar
	__end_of_update_black_bar:
;; =============== function _update_black_bar ends ============

	signat	_update_black_bar,4216
	global	i1___lwmod
psect	text664,local,class=CODE,delta=2
global __ptext664
__ptext664:

;; *************** function i1___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwmod.c"
;; Parameters:    Size  Location     Type
;;  __lwmod         2    0[COMMON] unsigned int 
;;  __lwmod         2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  __lwmod         1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         4       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         5       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_checksum
;; This function uses a non-reentrant model
;;
psect	text664
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwmod.c"
	line	5
	global	__size_ofi1___lwmod
	__size_ofi1___lwmod	equ	__end_ofi1___lwmod-i1___lwmod
	
i1___lwmod:	
	opt	stack 11
; Regs used in i1___lwmod: [wreg+status,2+status,0]
	line	8
	
i1l14012:	
	movf	(i1___lwmod@divisor+1),w
	iorwf	(i1___lwmod@divisor),w
	skipnz
	goto	u146_21
	goto	u146_20
u146_21:
	goto	i1l14028
u146_20:
	line	9
	
i1l14014:	
	clrf	(i1___lwmod@counter)
	incf	(i1___lwmod@counter),f
	line	10
	goto	i1l14018
	line	11
	
i1l14016:	
	lslf	(i1___lwmod@divisor),f
	rlf	(i1___lwmod@divisor+1),f
	line	12
	incf	(i1___lwmod@counter),f
	line	10
	
i1l14018:	
	btfss	(i1___lwmod@divisor+1),(15)&7
	goto	u147_21
	goto	u147_20
u147_21:
	goto	i1l14016
u147_20:
	line	15
	
i1l14020:	
	movf	(i1___lwmod@divisor+1),w
	subwf	(i1___lwmod@dividend+1),w
	skipz
	goto	u148_25
	movf	(i1___lwmod@divisor),w
	subwf	(i1___lwmod@dividend),w
u148_25:
	skipc
	goto	u148_21
	goto	u148_20
u148_21:
	goto	i1l14024
u148_20:
	line	16
	
i1l14022:	
	movf	(i1___lwmod@divisor),w
	subwf	(i1___lwmod@dividend),f
	movf	(i1___lwmod@divisor+1),w
	subwfb	(i1___lwmod@dividend+1),f
	line	17
	
i1l14024:	
	lsrf	(i1___lwmod@divisor+1),f
	rrf	(i1___lwmod@divisor),f
	line	18
	
i1l14026:	
	decfsz	(i1___lwmod@counter),f
	goto	u149_21
	goto	u149_20
u149_21:
	goto	i1l14020
u149_20:
	line	20
	
i1l14028:	
	movf	(i1___lwmod@dividend+1),w
	movwf	(?i1___lwmod+1)
	movf	(i1___lwmod@dividend),w
	movwf	(?i1___lwmod)
	line	21
	
i1l10355:	
	return
	opt stack 0
GLOBAL	__end_ofi1___lwmod
	__end_ofi1___lwmod:
;; =============== function i1___lwmod ends ============

	signat	i1___lwmod,90
	global	i1___lwdiv
psect	text665,local,class=CODE,delta=2
global __ptext665
__ptext665:

;; *************** function i1___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  __lwdiv         2    0[COMMON] unsigned int 
;;  __lwdiv         2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  __lwdiv         2    5[COMMON] unsigned int 
;;  __lwdiv         1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         4       0       0       0       0       0       0       0
;;      Locals:         3       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         7       0       0       0       0       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_update_black_bar
;;		_update_white_bar
;;		_bar_decision
;; This function uses a non-reentrant model
;;
psect	text665
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lwdiv.c"
	line	5
	global	__size_ofi1___lwdiv
	__size_ofi1___lwdiv	equ	__end_ofi1___lwdiv-i1___lwdiv
	
i1___lwdiv:	
	opt	stack 11
; Regs used in i1___lwdiv: [wreg+status,2+status,0]
	line	9
	
i1l13986:	
	clrf	(i1___lwdiv@quotient)
	clrf	(i1___lwdiv@quotient+1)
	line	10
	
i1l13988:	
	movf	(i1___lwdiv@divisor+1),w
	iorwf	(i1___lwdiv@divisor),w
	skipnz
	goto	u142_21
	goto	u142_20
u142_21:
	goto	i1l14008
u142_20:
	line	11
	
i1l13990:	
	clrf	(i1___lwdiv@counter)
	incf	(i1___lwdiv@counter),f
	line	12
	goto	i1l13994
	line	13
	
i1l13992:	
	lslf	(i1___lwdiv@divisor),f
	rlf	(i1___lwdiv@divisor+1),f
	line	14
	incf	(i1___lwdiv@counter),f
	line	12
	
i1l13994:	
	btfss	(i1___lwdiv@divisor+1),(15)&7
	goto	u143_21
	goto	u143_20
u143_21:
	goto	i1l13992
u143_20:
	line	17
	
i1l13996:	
	lslf	(i1___lwdiv@quotient),f
	rlf	(i1___lwdiv@quotient+1),f
	line	18
	
i1l13998:	
	movf	(i1___lwdiv@divisor+1),w
	subwf	(i1___lwdiv@dividend+1),w
	skipz
	goto	u144_25
	movf	(i1___lwdiv@divisor),w
	subwf	(i1___lwdiv@dividend),w
u144_25:
	skipc
	goto	u144_21
	goto	u144_20
u144_21:
	goto	i1l14004
u144_20:
	line	19
	
i1l14000:	
	movf	(i1___lwdiv@divisor),w
	subwf	(i1___lwdiv@dividend),f
	movf	(i1___lwdiv@divisor+1),w
	subwfb	(i1___lwdiv@dividend+1),f
	line	20
	
i1l14002:	
	bsf	(i1___lwdiv@quotient)+(0/8),(0)&7
	line	22
	
i1l14004:	
	lsrf	(i1___lwdiv@divisor+1),f
	rrf	(i1___lwdiv@divisor),f
	line	23
	
i1l14006:	
	decfsz	(i1___lwdiv@counter),f
	goto	u145_21
	goto	u145_20
u145_21:
	goto	i1l13996
u145_20:
	line	25
	
i1l14008:	
	movf	(i1___lwdiv@quotient+1),w
	movwf	(?i1___lwdiv+1)
	movf	(i1___lwdiv@quotient),w
	movwf	(?i1___lwdiv)
	line	26
	
i1l10345:	
	return
	opt stack 0
GLOBAL	__end_ofi1___lwdiv
	__end_ofi1___lwdiv:
;; =============== function i1___lwdiv ends ============

	signat	i1___lwdiv,90
	global	i1___wmul
psect	text666,local,class=CODE,delta=2
global __ptext666
__ptext666:

;; *************** function i1___wmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\wmul.c"
;; Parameters:    Size  Location     Type
;;  __wmul          2    0[COMMON] unsigned int 
;;  __wmul          2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  __wmul          2    4[COMMON] unsigned int 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         4       0       0       0       0       0       0       0
;;      Locals:         2       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         6       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;;		_update_black_bar
;;		_update_white_bar
;;		_checksum
;;		_bar_decision
;; This function uses a non-reentrant model
;;
psect	text666
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\wmul.c"
	line	3
	global	__size_ofi1___wmul
	__size_ofi1___wmul	equ	__end_ofi1___wmul-i1___wmul
	
i1___wmul:	
	opt	stack 11
; Regs used in i1___wmul: [wreg+status,2+status,0]
	line	4
	
i1l13970:	
	clrf	(i1___wmul@product)
	clrf	(i1___wmul@product+1)
	line	7
	
i1l13972:	
	btfss	(i1___wmul@multiplier),(0)&7
	goto	u140_21
	goto	u140_20
u140_21:
	goto	i1l13976
u140_20:
	line	8
	
i1l13974:	
	movf	(i1___wmul@multiplicand),w
	addwf	(i1___wmul@product),f
	movf	(i1___wmul@multiplicand+1),w
	addwfc	(i1___wmul@product+1),f
	line	9
	
i1l13976:	
	lslf	(i1___wmul@multiplicand),f
	rlf	(i1___wmul@multiplicand+1),f
	line	10
	
i1l13978:	
	lsrf	(i1___wmul@multiplier+1),f
	rrf	(i1___wmul@multiplier),f
	line	11
	
i1l13980:	
	movf	((i1___wmul@multiplier+1)),w
	iorwf	((i1___wmul@multiplier)),w
	skipz
	goto	u141_21
	goto	u141_20
u141_21:
	goto	i1l13972
u141_20:
	line	12
	
i1l13982:	
	movf	(i1___wmul@product+1),w
	movwf	(?i1___wmul+1)
	movf	(i1___wmul@product),w
	movwf	(?i1___wmul)
	line	13
	
i1l10335:	
	return
	opt stack 0
GLOBAL	__end_ofi1___wmul
	__end_ofi1___wmul:
;; =============== function i1___wmul ends ============

	signat	i1___wmul,90
	global	i1_init_reader1
psect	text667,local,class=CODE,delta=2
global __ptext667
__ptext667:

;; *************** function i1_init_reader1 *****************
;; Defined at:
;;		line 882 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text667
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	882
	global	__size_ofi1_init_reader1
	__size_ofi1_init_reader1	equ	__end_ofi1_init_reader1-i1_init_reader1
	
i1_init_reader1:	
	opt	stack 12
; Regs used in i1_init_reader1: [wreg+status,2]
	line	885
	
i1l13946:	
;main.c: 885: rdr1_first_edge = FALSE;
	movlb 0	; select bank0
	clrf	(_rdr1_first_edge)
	line	886
;main.c: 886: rdr1_bar_count = 0;
	clrf	(_rdr1_bar_count)
	line	887
;main.c: 887: rdr1_byte_count = 0;
	clrf	(_rdr1_byte_count)
	line	888
	
i1l13948:	
;main.c: 888: if (RB3)
	btfss	(107/8),(107)&7
	goto	u139_21
	goto	u139_20
u139_21:
	goto	i1l13960
u139_20:
	line	890
	
i1l13950:	
;main.c: 889: {
;main.c: 890: rdr1_state = WAIT_LOW;
	clrf	(_rdr1_state)
	line	891
	
i1l13952:	
;main.c: 891: { CCP2IE = 0; CCP2CON = 0x04; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	
i1l13954:	
	movlw	(04h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	
i1l13956:	
	movlb 0	; select bank0
	bcf	(144/8),(144)&7
	
i1l13958:	
	movlb 1	; select bank1
	bsf	(1168/8)^080h,(1168)&7
	line	892
;main.c: 892: }
	goto	i1l2330
	line	895
	
i1l13960:	
;main.c: 893: else
;main.c: 894: {
;main.c: 895: rdr1_state_timer = 20;
	movlw	(014h)
	movwf	(_rdr1_state_timer)	;volatile
	line	896
	
i1l13962:	
;main.c: 896: rdr1_state = WAIT_QUIET;
	clrf	(_rdr1_state)
	incf	(_rdr1_state),f
	line	897
	
i1l13964:	
;main.c: 897: { CCP2IE = 0; CCP2CON = 0x05; CCP2IF = 0; CCP2IE = 1; };
	movlb 1	; select bank1
	bcf	(1168/8)^080h,(1168)&7
	movlw	(05h)
	movlb 5	; select bank5
	movwf	(666)^0280h	;volatile
	goto	i1l13956
	line	899
	
i1l2330:	
	return
	opt stack 0
GLOBAL	__end_ofi1_init_reader1
	__end_ofi1_init_reader1:
;; =============== function i1_init_reader1 ends ============

	signat	i1_init_reader1,88
	global	___lmul
psect	text668,local,class=CODE,delta=2
global __ptext668
__ptext668:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[BANK0 ] unsigned long 
;;  multiplicand    4    4[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    7[COMMON] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[BANK0 ] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         0       8       0       0       0       0       0       0
;;      Locals:         4       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         4       8       0       0       0       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text668
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 12
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
i1l13934:	
	clrf	(___lmul@product)
	clrf	(___lmul@product+1)
	clrf	(___lmul@product+2)
	clrf	(___lmul@product+3)
	line	6
	
i1l10407:	
	line	7
	btfss	(___lmul@multiplier),(0)&7
	goto	u137_21
	goto	u137_20
u137_21:
	goto	i1l13938
u137_20:
	line	8
	
i1l13936:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	addwfc	(___lmul@product+1),f
	movf	(___lmul@multiplicand+2),w
	addwfc	(___lmul@product+2),f
	movf	(___lmul@multiplicand+3),w
	addwfc	(___lmul@product+3),f
	line	9
	
i1l13938:	
	lslf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	line	10
	
i1l13940:	
	lsrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u138_21
	goto	u138_20
u138_21:
	goto	i1l10407
u138_20:
	line	12
	
i1l13942:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	line	13
	
i1l10410:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
	global	_lookup_sym
psect	text669,local,class=CODE,delta=2
global __ptext669
__ptext669:

;; *************** function _lookup_sym *****************
;; Defined at:
;;		line 530 in file "C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
;; Parameters:    Size  Location     Type
;;  symbol          4    0[COMMON] unsigned long 
;; Auto vars:     Size  Location     Type
;;  retvalue        1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/1
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6
;;      Params:         4       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0
;;      Totals:         5       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_isr
;; This function uses a non-reentrant model
;;
psect	text669
	file	"C:\Microchip\F1 Eval Kit\F1 Demo Code\F1Eval Demo Code\LCD Driver Demo\main.c"
	line	530
	global	__size_of_lookup_sym
	__size_of_lookup_sym	equ	__end_of_lookup_sym-_lookup_sym
	
_lookup_sym:	
	opt	stack 12
; Regs used in _lookup_sym: [wreg-fsr0h+status,2+status,0]
	line	533
	
i1l13462:	
;main.c: 531: BYTE retvalue;
;main.c: 533: switch(symbol)
	goto	i1l13682
	line	536
	
i1l13464:	
;main.c: 536: retvalue = 0;
	clrf	(lookup_sym@retvalue)
	line	537
;main.c: 537: break;
	goto	i1l13684
	line	538
;main.c: 538: case 222122UL:
	
i1l2211:	
	line	539
;main.c: 539: retvalue = 1;
	clrf	(lookup_sym@retvalue)
	incf	(lookup_sym@retvalue),f
	line	540
;main.c: 540: break;
	goto	i1l13684
	line	542
	
i1l13466:	
;main.c: 542: retvalue = 2;
	movlw	(02h)
	movwf	(lookup_sym@retvalue)
	line	543
;main.c: 543: break;
	goto	i1l13684
	line	545
	
i1l13468:	
;main.c: 545: retvalue = 3;
	movlw	(03h)
	movwf	(lookup_sym@retvalue)
	line	546
;main.c: 546: break;
	goto	i1l13684
	line	548
	
i1l13470:	
;main.c: 548: retvalue = 4;
	movlw	(04h)
	movwf	(lookup_sym@retvalue)
	line	549
;main.c: 549: break;
	goto	i1l13684
	line	551
	
i1l13472:	
;main.c: 551: retvalue = 5;
	movlw	(05h)
	movwf	(lookup_sym@retvalue)
	line	552
;main.c: 552: break;
	goto	i1l13684
	line	554
	
i1l13474:	
;main.c: 554: retvalue = 6;
	movlw	(06h)
	movwf	(lookup_sym@retvalue)
	line	555
;main.c: 555: break;
	goto	i1l13684
	line	557
	
i1l13476:	
;main.c: 557: retvalue = 7;
	movlw	(07h)
	movwf	(lookup_sym@retvalue)
	line	558
;main.c: 558: break;
	goto	i1l13684
	line	560
	
i1l13478:	
;main.c: 560: retvalue = 8;
	movlw	(08h)
	movwf	(lookup_sym@retvalue)
	line	561
;main.c: 561: break;
	goto	i1l13684
	line	563
	
i1l13480:	
;main.c: 563: retvalue = 9;
	movlw	(09h)
	movwf	(lookup_sym@retvalue)
	line	564
;main.c: 564: break;
	goto	i1l13684
	line	566
	
i1l13482:	
;main.c: 566: retvalue = 10;
	movlw	(0Ah)
	movwf	(lookup_sym@retvalue)
	line	567
;main.c: 567: break;
	goto	i1l13684
	line	569
	
i1l13484:	
;main.c: 569: retvalue = 11;
	movlw	(0Bh)
	movwf	(lookup_sym@retvalue)
	line	570
;main.c: 570: break;
	goto	i1l13684
	line	572
	
i1l13486:	
;main.c: 572: retvalue = 12;
	movlw	(0Ch)
	movwf	(lookup_sym@retvalue)
	line	573
;main.c: 573: break;
	goto	i1l13684
	line	575
	
i1l13488:	
;main.c: 575: retvalue = 13;
	movlw	(0Dh)
	movwf	(lookup_sym@retvalue)
	line	576
;main.c: 576: break;
	goto	i1l13684
	line	578
	
i1l13490:	
;main.c: 578: retvalue = 14;
	movlw	(0Eh)
	movwf	(lookup_sym@retvalue)
	line	579
;main.c: 579: break;
	goto	i1l13684
	line	581
	
i1l13492:	
;main.c: 581: retvalue = 15;
	movlw	(0Fh)
	movwf	(lookup_sym@retvalue)
	line	582
;main.c: 582: break;
	goto	i1l13684
	line	584
	
i1l13494:	
;main.c: 584: retvalue = 16;
	movlw	(010h)
	movwf	(lookup_sym@retvalue)
	line	585
;main.c: 585: break;
	goto	i1l13684
	line	587
	
i1l13496:	
;main.c: 587: retvalue = 17;
	movlw	(011h)
	movwf	(lookup_sym@retvalue)
	line	588
;main.c: 588: break;
	goto	i1l13684
	line	590
	
i1l13498:	
;main.c: 590: retvalue = 18;
	movlw	(012h)
	movwf	(lookup_sym@retvalue)
	line	591
;main.c: 591: break;
	goto	i1l13684
	line	593
	
i1l13500:	
;main.c: 593: retvalue = 19;
	movlw	(013h)
	movwf	(lookup_sym@retvalue)
	line	594
;main.c: 594: break;
	goto	i1l13684
	line	596
	
i1l13502:	
;main.c: 596: retvalue = 20;
	movlw	(014h)
	movwf	(lookup_sym@retvalue)
	line	597
;main.c: 597: break;
	goto	i1l13684
	line	599
	
i1l13504:	
;main.c: 599: retvalue = 21;
	movlw	(015h)
	movwf	(lookup_sym@retvalue)
	line	600
;main.c: 600: break;
	goto	i1l13684
	line	602
	
i1l13506:	
;main.c: 602: retvalue = 22;
	movlw	(016h)
	movwf	(lookup_sym@retvalue)
	line	603
;main.c: 603: break;
	goto	i1l13684
	line	605
	
i1l13508:	
;main.c: 605: retvalue = 23;
	movlw	(017h)
	movwf	(lookup_sym@retvalue)
	line	606
;main.c: 606: break;
	goto	i1l13684
	line	608
	
i1l13510:	
;main.c: 608: retvalue = 25;
	movlw	(019h)
	movwf	(lookup_sym@retvalue)
	line	609
;main.c: 609: break;
	goto	i1l13684
	line	614
	
i1l13514:	
;main.c: 614: retvalue = 26;
	movlw	(01Ah)
	movwf	(lookup_sym@retvalue)
	line	615
;main.c: 615: break;
	goto	i1l13684
	line	617
	
i1l13516:	
;main.c: 617: retvalue = 27;
	movlw	(01Bh)
	movwf	(lookup_sym@retvalue)
	line	618
;main.c: 618: break;
	goto	i1l13684
	line	620
	
i1l13518:	
;main.c: 620: retvalue = 28;
	movlw	(01Ch)
	movwf	(lookup_sym@retvalue)
	line	621
;main.c: 621: break;
	goto	i1l13684
	line	623
	
i1l13520:	
;main.c: 623: retvalue = 29;
	movlw	(01Dh)
	movwf	(lookup_sym@retvalue)
	line	624
;main.c: 624: break;
	goto	i1l13684
	line	626
	
i1l13522:	
;main.c: 626: retvalue = 30;
	movlw	(01Eh)
	movwf	(lookup_sym@retvalue)
	line	627
;main.c: 627: break;
	goto	i1l13684
	line	629
	
i1l13524:	
;main.c: 629: retvalue = 31;
	movlw	(01Fh)
	movwf	(lookup_sym@retvalue)
	line	630
;main.c: 630: break;
	goto	i1l13684
	line	632
	
i1l13526:	
;main.c: 632: retvalue = 32;
	movlw	(020h)
	movwf	(lookup_sym@retvalue)
	line	633
;main.c: 633: break;
	goto	i1l13684
	line	635
	
i1l13528:	
;main.c: 635: retvalue = 33;
	movlw	(021h)
	movwf	(lookup_sym@retvalue)
	line	636
;main.c: 636: break;
	goto	i1l13684
	line	638
	
i1l13530:	
;main.c: 638: retvalue = 34;
	movlw	(022h)
	movwf	(lookup_sym@retvalue)
	line	639
;main.c: 639: break;
	goto	i1l13684
	line	641
	
i1l13532:	
;main.c: 641: retvalue = 35;
	movlw	(023h)
	movwf	(lookup_sym@retvalue)
	line	642
;main.c: 642: break;
	goto	i1l13684
	line	644
	
i1l13534:	
;main.c: 644: retvalue = 36;
	movlw	(024h)
	movwf	(lookup_sym@retvalue)
	line	645
;main.c: 645: break;
	goto	i1l13684
	line	647
	
i1l13536:	
;main.c: 647: retvalue = 37;
	movlw	(025h)
	movwf	(lookup_sym@retvalue)
	line	648
;main.c: 648: break;
	goto	i1l13684
	line	650
	
i1l13538:	
;main.c: 650: retvalue = 38;
	movlw	(026h)
	movwf	(lookup_sym@retvalue)
	line	651
;main.c: 651: break;
	goto	i1l13684
	line	653
	
i1l13540:	
;main.c: 653: retvalue = 39;
	movlw	(027h)
	movwf	(lookup_sym@retvalue)
	line	654
;main.c: 654: break;
	goto	i1l13684
	line	656
	
i1l13542:	
;main.c: 656: retvalue = 40;
	movlw	(028h)
	movwf	(lookup_sym@retvalue)
	line	657
;main.c: 657: break;
	goto	i1l13684
	line	659
	
i1l13544:	
;main.c: 659: retvalue = 41;
	movlw	(029h)
	movwf	(lookup_sym@retvalue)
	line	660
;main.c: 660: break;
	goto	i1l13684
	line	662
	
i1l13546:	
;main.c: 662: retvalue = 42;
	movlw	(02Ah)
	movwf	(lookup_sym@retvalue)
	line	663
;main.c: 663: break;
	goto	i1l13684
	line	665
	
i1l13548:	
;main.c: 665: retvalue = 43;
	movlw	(02Bh)
	movwf	(lookup_sym@retvalue)
	line	666
;main.c: 666: break;
	goto	i1l13684
	line	668
	
i1l13550:	
;main.c: 668: retvalue = 44;
	movlw	(02Ch)
	movwf	(lookup_sym@retvalue)
	line	669
;main.c: 669: break;
	goto	i1l13684
	line	671
	
i1l13552:	
;main.c: 671: retvalue = 45;
	movlw	(02Dh)
	movwf	(lookup_sym@retvalue)
	line	672
;main.c: 672: break;
	goto	i1l13684
	line	674
	
i1l13554:	
;main.c: 674: retvalue = 46;
	movlw	(02Eh)
	movwf	(lookup_sym@retvalue)
	line	675
;main.c: 675: break;
	goto	i1l13684
	line	677
	
i1l13556:	
;main.c: 677: retvalue = 47;
	movlw	(02Fh)
	movwf	(lookup_sym@retvalue)
	line	678
;main.c: 678: break;
	goto	i1l13684
	line	680
	
i1l13558:	
;main.c: 680: retvalue = 48;
	movlw	(030h)
	movwf	(lookup_sym@retvalue)
	line	681
;main.c: 681: break;
	goto	i1l13684
	line	683
	
i1l13560:	
;main.c: 683: retvalue = 49;
	movlw	(031h)
	movwf	(lookup_sym@retvalue)
	line	684
;main.c: 684: break;
	goto	i1l13684
	line	686
	
i1l13562:	
;main.c: 686: retvalue = 50;
	movlw	(032h)
	movwf	(lookup_sym@retvalue)
	line	687
;main.c: 687: break;
	goto	i1l13684
	line	689
	
i1l13564:	
;main.c: 689: retvalue = 51;
	movlw	(033h)
	movwf	(lookup_sym@retvalue)
	line	690
;main.c: 690: break;
	goto	i1l13684
	line	692
	
i1l13566:	
;main.c: 692: retvalue = 52;
	movlw	(034h)
	movwf	(lookup_sym@retvalue)
	line	693
;main.c: 693: break;
	goto	i1l13684
	line	695
	
i1l13568:	
;main.c: 695: retvalue = 53;
	movlw	(035h)
	movwf	(lookup_sym@retvalue)
	line	696
;main.c: 696: break;
	goto	i1l13684
	line	698
	
i1l13570:	
;main.c: 698: retvalue = 54;
	movlw	(036h)
	movwf	(lookup_sym@retvalue)
	line	699
;main.c: 699: break;
	goto	i1l13684
	line	701
	
i1l13572:	
;main.c: 701: retvalue = 55;
	movlw	(037h)
	movwf	(lookup_sym@retvalue)
	line	702
;main.c: 702: break;
	goto	i1l13684
	line	704
	
i1l13574:	
;main.c: 704: retvalue = 56;
	movlw	(038h)
	movwf	(lookup_sym@retvalue)
	line	705
;main.c: 705: break;
	goto	i1l13684
	line	707
	
i1l13576:	
;main.c: 707: retvalue = 57;
	movlw	(039h)
	movwf	(lookup_sym@retvalue)
	line	708
;main.c: 708: break;
	goto	i1l13684
	line	710
	
i1l13578:	
;main.c: 710: retvalue = 58;
	movlw	(03Ah)
	movwf	(lookup_sym@retvalue)
	line	711
;main.c: 711: break;
	goto	i1l13684
	line	713
	
i1l13580:	
;main.c: 713: retvalue = 59;
	movlw	(03Bh)
	movwf	(lookup_sym@retvalue)
	line	714
;main.c: 714: break;
	goto	i1l13684
	line	716
	
i1l13582:	
;main.c: 716: retvalue = 60;
	movlw	(03Ch)
	movwf	(lookup_sym@retvalue)
	line	717
;main.c: 717: break;
	goto	i1l13684
	line	719
	
i1l13584:	
;main.c: 719: retvalue = 61;
	movlw	(03Dh)
	movwf	(lookup_sym@retvalue)
	line	720
;main.c: 720: break;
	goto	i1l13684
	line	722
	
i1l13586:	
;main.c: 722: retvalue = 62;
	movlw	(03Eh)
	movwf	(lookup_sym@retvalue)
	line	723
;main.c: 723: break;
	goto	i1l13684
	line	725
	
i1l13588:	
;main.c: 725: retvalue = 63;
	movlw	(03Fh)
	movwf	(lookup_sym@retvalue)
	line	726
;main.c: 726: break;
	goto	i1l13684
	line	728
	
i1l13590:	
;main.c: 728: retvalue = 64;
	movlw	(040h)
	movwf	(lookup_sym@retvalue)
	line	729
;main.c: 729: break;
	goto	i1l13684
	line	731
	
i1l13592:	
;main.c: 731: retvalue = 65;
	movlw	(041h)
	movwf	(lookup_sym@retvalue)
	line	732
;main.c: 732: break;
	goto	i1l13684
	line	734
	
i1l13594:	
;main.c: 734: retvalue = 66;
	movlw	(042h)
	movwf	(lookup_sym@retvalue)
	line	735
;main.c: 735: break;
	goto	i1l13684
	line	737
	
i1l13596:	
;main.c: 737: retvalue = 67;
	movlw	(043h)
	movwf	(lookup_sym@retvalue)
	line	738
;main.c: 738: break;
	goto	i1l13684
	line	740
	
i1l13598:	
;main.c: 740: retvalue = 68;
	movlw	(044h)
	movwf	(lookup_sym@retvalue)
	line	741
;main.c: 741: break;
	goto	i1l13684
	line	743
	
i1l13600:	
;main.c: 743: retvalue = 69;
	movlw	(045h)
	movwf	(lookup_sym@retvalue)
	line	744
;main.c: 744: break;
	goto	i1l13684
	line	746
	
i1l13602:	
;main.c: 746: retvalue = 70;
	movlw	(046h)
	movwf	(lookup_sym@retvalue)
	line	747
;main.c: 747: break;
	goto	i1l13684
	line	749
	
i1l13604:	
;main.c: 749: retvalue = 71;
	movlw	(047h)
	movwf	(lookup_sym@retvalue)
	line	750
;main.c: 750: break;
	goto	i1l13684
	line	752
	
i1l13606:	
;main.c: 752: retvalue = 72;
	movlw	(048h)
	movwf	(lookup_sym@retvalue)
	line	753
;main.c: 753: break;
	goto	i1l13684
	line	755
	
i1l13608:	
;main.c: 755: retvalue = 73;
	movlw	(049h)
	movwf	(lookup_sym@retvalue)
	line	756
;main.c: 756: break;
	goto	i1l13684
	line	758
	
i1l13610:	
;main.c: 758: retvalue = 74;
	movlw	(04Ah)
	movwf	(lookup_sym@retvalue)
	line	759
;main.c: 759: break;
	goto	i1l13684
	line	761
	
i1l13612:	
;main.c: 761: retvalue = 75;
	movlw	(04Bh)
	movwf	(lookup_sym@retvalue)
	line	762
;main.c: 762: break;
	goto	i1l13684
	line	764
	
i1l13614:	
;main.c: 764: retvalue = 76;
	movlw	(04Ch)
	movwf	(lookup_sym@retvalue)
	line	765
;main.c: 765: break;
	goto	i1l13684
	line	767
	
i1l13616:	
;main.c: 767: retvalue = 77;
	movlw	(04Dh)
	movwf	(lookup_sym@retvalue)
	line	768
;main.c: 768: break;
	goto	i1l13684
	line	770
	
i1l13618:	
;main.c: 770: retvalue = 78;
	movlw	(04Eh)
	movwf	(lookup_sym@retvalue)
	line	771
;main.c: 771: break;
	goto	i1l13684
	line	773
	
i1l13620:	
;main.c: 773: retvalue = 79;
	movlw	(04Fh)
	movwf	(lookup_sym@retvalue)
	line	774
;main.c: 774: break;
	goto	i1l13684
	line	776
	
i1l13622:	
;main.c: 776: retvalue = 80;
	movlw	(050h)
	movwf	(lookup_sym@retvalue)
	line	777
;main.c: 777: break;
	goto	i1l13684
	line	779
	
i1l13624:	
;main.c: 779: retvalue = 81;
	movlw	(051h)
	movwf	(lookup_sym@retvalue)
	line	780
;main.c: 780: break;
	goto	i1l13684
	line	782
	
i1l13626:	
;main.c: 782: retvalue = 82;
	movlw	(052h)
	movwf	(lookup_sym@retvalue)
	line	783
;main.c: 783: break;
	goto	i1l13684
	line	785
	
i1l13628:	
;main.c: 785: retvalue = 83;
	movlw	(053h)
	movwf	(lookup_sym@retvalue)
	line	786
;main.c: 786: break;
	goto	i1l13684
	line	788
	
i1l13630:	
;main.c: 788: retvalue = 84;
	movlw	(054h)
	movwf	(lookup_sym@retvalue)
	line	789
;main.c: 789: break;
	goto	i1l13684
	line	791
	
i1l13632:	
;main.c: 791: retvalue = 85;
	movlw	(055h)
	movwf	(lookup_sym@retvalue)
	line	792
;main.c: 792: break;
	goto	i1l13684
	line	794
	
i1l13634:	
;main.c: 794: retvalue = 86;
	movlw	(056h)
	movwf	(lookup_sym@retvalue)
	line	795
;main.c: 795: break;
	goto	i1l13684
	line	797
	
i1l13636:	
;main.c: 797: retvalue = 87;
	movlw	(057h)
	movwf	(lookup_sym@retvalue)
	line	798
;main.c: 798: break;
	goto	i1l13684
	line	800
	
i1l13638:	
;main.c: 800: retvalue = 88;
	movlw	(058h)
	movwf	(lookup_sym@retvalue)
	line	801
;main.c: 801: break;
	goto	i1l13684
	line	803
	
i1l13640:	
;main.c: 803: retvalue = 89;
	movlw	(059h)
	movwf	(lookup_sym@retvalue)
	line	804
;main.c: 804: break;
	goto	i1l13684
	line	806
	
i1l13642:	
;main.c: 806: retvalue = 90;
	movlw	(05Ah)
	movwf	(lookup_sym@retvalue)
	line	807
;main.c: 807: break;
	goto	i1l13684
	line	809
	
i1l13644:	
;main.c: 809: retvalue = 91;
	movlw	(05Bh)
	movwf	(lookup_sym@retvalue)
	line	810
;main.c: 810: break;
	goto	i1l13684
	line	812
	
i1l13646:	
;main.c: 812: retvalue = 92;
	movlw	(05Ch)
	movwf	(lookup_sym@retvalue)
	line	813
;main.c: 813: break;
	goto	i1l13684
	line	815
	
i1l13648:	
;main.c: 815: retvalue = 93;
	movlw	(05Dh)
	movwf	(lookup_sym@retvalue)
	line	816
;main.c: 816: break;
	goto	i1l13684
	line	818
	
i1l13650:	
;main.c: 818: retvalue = 94;
	movlw	(05Eh)
	movwf	(lookup_sym@retvalue)
	line	819
;main.c: 819: break;
	goto	i1l13684
	line	821
	
i1l13652:	
;main.c: 821: retvalue = 95;
	movlw	(05Fh)
	movwf	(lookup_sym@retvalue)
	line	822
;main.c: 822: break;
	goto	i1l13684
	line	824
	
i1l13654:	
;main.c: 824: retvalue = 96;
	movlw	(060h)
	movwf	(lookup_sym@retvalue)
	line	825
;main.c: 825: break;
	goto	i1l13684
	line	827
	
i1l13656:	
;main.c: 827: retvalue = 97;
	movlw	(061h)
	movwf	(lookup_sym@retvalue)
	line	828
;main.c: 828: break;
	goto	i1l13684
	line	830
	
i1l13658:	
;main.c: 830: retvalue = 98;
	movlw	(062h)
	movwf	(lookup_sym@retvalue)
	line	831
;main.c: 831: break;
	goto	i1l13684
	line	833
	
i1l13660:	
;main.c: 833: retvalue = 99;
	movlw	(063h)
	movwf	(lookup_sym@retvalue)
	line	834
;main.c: 834: break;
	goto	i1l13684
	line	836
	
i1l13662:	
;main.c: 836: retvalue = 100;
	movlw	(064h)
	movwf	(lookup_sym@retvalue)
	line	837
;main.c: 837: break;
	goto	i1l13684
	line	839
	
i1l13664:	
;main.c: 839: retvalue = 101;
	movlw	(065h)
	movwf	(lookup_sym@retvalue)
	line	840
;main.c: 840: break;
	goto	i1l13684
	line	842
	
i1l13666:	
;main.c: 842: retvalue = 102;
	movlw	(066h)
	movwf	(lookup_sym@retvalue)
	line	843
;main.c: 843: break;
	goto	i1l13684
	line	845
	
i1l13668:	
;main.c: 845: retvalue = 103;
	movlw	(067h)
	movwf	(lookup_sym@retvalue)
	line	846
;main.c: 846: break;
	goto	i1l13684
	line	848
	
i1l13670:	
;main.c: 848: retvalue = 104;
	movlw	(068h)
	movwf	(lookup_sym@retvalue)
	line	849
;main.c: 849: break;
	goto	i1l13684
	line	851
	
i1l13672:	
;main.c: 851: retvalue = 105;
	movlw	(069h)
	movwf	(lookup_sym@retvalue)
	line	852
;main.c: 852: break;
	goto	i1l13684
	line	854
	
i1l13674:	
;main.c: 854: retvalue = 106;
	movlw	(06Ah)
	movwf	(lookup_sym@retvalue)
	line	855
;main.c: 855: break;
	goto	i1l13684
	line	857
	
i1l13676:	
;main.c: 857: retvalue = 155;
	movlw	(09Bh)
	movwf	(lookup_sym@retvalue)
	line	858
;main.c: 858: break;
	goto	i1l13684
	line	860
	
i1l13678:	
;main.c: 860: retvalue = 255;
	movlw	(0FFh)
	movwf	(lookup_sym@retvalue)
	line	861
;main.c: 861: break;
	goto	i1l13684
	line	533
	
i1l13682:	
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 8 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte            8     6 (fixed)
; jumptable            260     6 (fixed)
; rangetable             5     4 (fixed)
; spacedrange            7     6 (fixed)
; locatedrange           1     3 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+3),w
	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	i1l15844
	goto	i1l13678
	opt asmopt_on
	
i1l15830:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 16 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 16, Range of values is 178 to 229
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           49    25 (average)
; direct_byte          113     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	178^0	; case 178
	skipnz
	goto	i1l15846
	xorlw	179^178	; case 179
	skipnz
	goto	i1l15848
	xorlw	182^179	; case 182
	skipnz
	goto	i1l15850
	xorlw	183^182	; case 183
	skipnz
	goto	i1l15852
	xorlw	185^183	; case 185
	skipnz
	goto	i1l15854
	xorlw	186^185	; case 186
	skipnz
	goto	i1l15856
	xorlw	189^186	; case 189
	skipnz
	goto	i1l15858
	xorlw	190^189	; case 190
	skipnz
	goto	i1l15860
	xorlw	217^190	; case 217
	skipnz
	goto	i1l15862
	xorlw	218^217	; case 218
	skipnz
	goto	i1l15864
	xorlw	221^218	; case 221
	skipnz
	goto	i1l15866
	xorlw	222^221	; case 222
	skipnz
	goto	i1l15868
	xorlw	224^222	; case 224
	skipnz
	goto	i1l15870
	xorlw	225^224	; case 225
	skipnz
	goto	i1l15872
	xorlw	228^225	; case 228
	skipnz
	goto	i1l15874
	xorlw	229^228	; case 229
	skipnz
	goto	i1l15876
	goto	i1l13678
	opt asmopt_on
	
i1l15846:	
; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 39 to 237
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           16     9 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	39^0	; case 39
	skipnz
	goto	i1l13646
	xorlw	120^39	; case 120
	skipnz
	goto	i1l13588
	xorlw	138^120	; case 138
	skipnz
	goto	i1l13622
	xorlw	219^138	; case 219
	skipnz
	goto	i1l13528
	xorlw	237^219	; case 237
	skipnz
	goto	i1l13648
	goto	i1l13678
	opt asmopt_on
	
i1l15848:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 62 to 62
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	62^0	; case 62
	skipnz
	goto	i1l13590
	goto	i1l13678
	opt asmopt_on
	
i1l15850:	
; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 5 to 203
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           16     9 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	5^0	; case 5
	skipnz
	goto	i1l13546
	xorlw	86^5	; case 86
	skipnz
	goto	i1l13600
	xorlw	104^86	; case 104
	skipnz
	goto	i1l13486
	xorlw	185^104	; case 185
	skipnz
	goto	i1l13534
	xorlw	203^185	; case 203
	skipnz
	goto	i1l13548
	goto	i1l13678
	opt asmopt_on
	
i1l15852:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 28 to 28
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	28^0	; case 28
	skipnz
	goto	i1l13602
	goto	i1l13678
	opt asmopt_on
	
i1l15854:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 227 to 245
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           47     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	227^0	; case 227
	skipnz
	goto	i1l13552
	xorlw	245^227	; case 245
	skipnz
	goto	i1l13660
	goto	i1l13678
	opt asmopt_on
	
i1l15856:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 70 to 169
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	70^0	; case 70
	skipnz
	goto	i1l13492
	xorlw	169^70	; case 169
	skipnz
	goto	i1l13554
	goto	i1l13678
	opt asmopt_on
	
i1l15858:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 193 to 211
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           47     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	193^0	; case 193
	skipnz
	goto	i1l13652
	xorlw	211^193	; case 211
	skipnz
	goto	i1l13662
	goto	i1l13678
	opt asmopt_on
	
i1l15860:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 36 to 135
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	36^0	; case 36
	skipnz
	goto	i1l13628
	xorlw	135^36	; case 135
	skipnz
	goto	i1l13654
	goto	i1l13678
	opt asmopt_on
	
i1l15862:	
; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 36 to 234
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           16     9 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	36^0	; case 36
	skipnz
	goto	i1l13592
	xorlw	54^36	; case 54
	skipnz
	goto	i1l13624
	xorlw	135^54	; case 135
	skipnz
	goto	i1l13468
	xorlw	153^135	; case 153
	skipnz
	goto	i1l13626
	xorlw	234^153	; case 234
	skipnz
	goto	i1l13470
	goto	i1l13678
	opt asmopt_on
	
i1l15864:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 77 to 77
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	77^0	; case 77
	skipnz
	goto	i1l13594
	goto	i1l13678
	opt asmopt_on
	
i1l15866:	
; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 2 to 200
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           16     9 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	2^0	; case 2
	skipnz
	goto	i1l13604
	xorlw	20^2	; case 20
	skipnz
	goto	i1l13488
	xorlw	101^20	; case 101
	skipnz
	goto	i1l13474
	xorlw	119^101	; case 119
	skipnz
	goto	i1l13490
	xorlw	200^119	; case 200
	skipnz
	goto	i1l13476
	goto	i1l13678
	opt asmopt_on
	
i1l15868:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 43 to 43
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	43^0	; case 43
	skipnz
	goto	i1l13606
	goto	i1l13678
	opt asmopt_on
	
i1l15870:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 242 to 242
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	242^0	; case 242
	skipnz
	goto	i1l13494
	goto	i1l13678
	opt asmopt_on
	
i1l15872:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 85 to 85
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	85^0	; case 85
	skipnz
	goto	i1l13496
	goto	i1l13678
	opt asmopt_on
	
i1l15874:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 208 to 208
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	208^0	; case 208
	skipnz
	goto	i1l13630
	goto	i1l13678
	opt asmopt_on
	
i1l15876:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 51 to 51
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	51^0	; case 51
	skipnz
	goto	i1l13632
	goto	i1l13678
	opt asmopt_on
	
i1l15832:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 6 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 6, Range of values is 0 to 43
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           19    10 (average)
; direct_byte           94     6 (fixed)
; jumptable            260     6 (fixed)
; rangetable            48     4 (fixed)
; spacedrange           93     6 (fixed)
; locatedrange          44     3 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	i1l15878
	xorlw	4^0	; case 4
	skipnz
	goto	i1l15880
	xorlw	8^4	; case 8
	skipnz
	goto	i1l15882
	xorlw	11^8	; case 11
	skipnz
	goto	i1l15884
	xorlw	39^11	; case 39
	skipnz
	goto	i1l15886
	xorlw	43^39	; case 43
	skipnz
	goto	i1l15888
	goto	i1l13678
	opt asmopt_on
	
i1l15878:	
; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 51 to 249
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	51^0	; case 51
	skipnz
	goto	i1l13530
	xorlw	69^51	; case 69
	skipnz
	goto	i1l13650
	xorlw	150^69	; case 150
	skipnz
	goto	i1l13472
	xorlw	249^150	; case 249
	skipnz
	goto	i1l13532
	goto	i1l13678
	opt asmopt_on
	
i1l15880:	
; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 17 to 215
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	17^0	; case 17
	skipnz
	goto	i1l13536
	xorlw	35^17	; case 35
	skipnz
	goto	i1l13550
	xorlw	116^35	; case 116
	skipnz
	goto	i1l13478
	xorlw	215^116	; case 215
	skipnz
	goto	i1l13538
	goto	i1l13678
	opt asmopt_on
	
i1l15882:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 1 to 1
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	i1l13556
	goto	i1l13678
	opt asmopt_on
	
i1l15884:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 223 to 223
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	223^0	; case 223
	skipnz
	goto	i1l13620
	goto	i1l13678
	opt asmopt_on
	
i1l15886:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 66 to 165
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	66^0	; case 66
	skipnz
	goto	i1l13596
	xorlw	165^66	; case 165
	skipnz
	goto	i1l13598
	goto	i1l13678
	opt asmopt_on
	
i1l15888:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 32 to 131
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	32^0	; case 32
	skipnz
	goto	i1l13608
	xorlw	131^32	; case 131
	skipnz
	goto	i1l13610
	goto	i1l13678
	opt asmopt_on
	
i1l15834:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 17 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 17, Range of values is 57 to 174
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           52    27 (average)
; direct_byte          245     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	57^0	; case 57
	skipnz
	goto	i1l15890
	xorlw	60^57	; case 60
	skipnz
	goto	i1l15892
	xorlw	61^60	; case 61
	skipnz
	goto	i1l15894
	xorlw	64^61	; case 64
	skipnz
	goto	i1l15896
	xorlw	65^64	; case 65
	skipnz
	goto	i1l15898
	xorlw	68^65	; case 68
	skipnz
	goto	i1l15900
	xorlw	95^68	; case 95
	skipnz
	goto	i1l15902
	xorlw	96^95	; case 96
	skipnz
	goto	i1l15904
	xorlw	99^96	; case 99
	skipnz
	goto	i1l15906
	xorlw	100^99	; case 100
	skipnz
	goto	i1l15908
	xorlw	103^100	; case 103
	skipnz
	goto	i1l15910
	xorlw	134^103	; case 134
	skipnz
	goto	i1l15912
	xorlw	135^134	; case 135
	skipnz
	goto	i1l15914
	xorlw	138^135	; case 138
	skipnz
	goto	i1l15916
	xorlw	142^138	; case 142
	skipnz
	goto	i1l15918
	xorlw	173^142	; case 173
	skipnz
	goto	i1l15920
	xorlw	174^173	; case 174
	skipnz
	goto	i1l15922
	goto	i1l13678
	opt asmopt_on
	
i1l15890:	
; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 14 to 212
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           16     9 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	14^0	; case 14
	skipnz
	goto	i1l13670
	xorlw	32^14	; case 32
	skipnz
	goto	i1l13672
	xorlw	113^32	; case 113
	skipnz
	goto	i1l13540
	xorlw	131^113	; case 131
	skipnz
	goto	i1l13560
	xorlw	212^131	; case 212
	skipnz
	goto	i1l13668
	goto	i1l13678
	opt asmopt_on
	
i1l15892:	
; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 155 to 254
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	155^0	; case 155
	skipnz
	goto	i1l13522
	xorlw	173^155	; case 173
	skipnz
	goto	i1l13640
	xorlw	254^173	; case 254
	skipnz
	goto	i1l13464
	goto	i1l13678
	opt asmopt_on
	
i1l15894:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 97 to 97
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	97^0	; case 97
	skipnz
	goto	i1l13524
	goto	i1l13678
	opt asmopt_on
	
i1l15896:	
; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 121 to 220
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	121^0	; case 121
	skipnz
	goto	i1l13564
	xorlw	139^121	; case 139
	skipnz
	goto	i1l13568
	xorlw	220^139	; case 220
	skipnz
	goto	i1l13504
	goto	i1l13678
	opt asmopt_on
	
i1l15898:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 63 to 63
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	63^0	; case 63
	skipnz
	goto	i1l13566
	goto	i1l13678
	opt asmopt_on
	
i1l15900:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 105 to 105
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	105^0	; case 105
	skipnz
	goto	i1l13642
	goto	i1l13678
	opt asmopt_on
	
i1l15902:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 186 to 204
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           47     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	186^0	; case 186
	skipnz
	goto	i1l13614
	xorlw	204^186	; case 204
	skipnz
	goto	i1l13500
	goto	i1l13678
	opt asmopt_on
	
i1l15904:	
; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 29 to 227
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	29^0	; case 29
	skipnz
	goto	i1l13480
	xorlw	47^29	; case 47
	skipnz
	goto	i1l13502
	xorlw	128^47	; case 128
	skipnz
	goto	i1l13482
	xorlw	227^128	; case 227
	skipnz
	goto	i1l13584
	goto	i1l13678
	opt asmopt_on
	
i1l15906:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 170 to 170
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	170^0	; case 170
	skipnz
	goto	i1l2211
	goto	i1l13678
	opt asmopt_on
	
i1l15908:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 13 to 13
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	13^0	; case 13
	skipnz
	goto	i1l13466
	goto	i1l13678
	opt asmopt_on
	
i1l15910:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 136 to 235
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	136^0	; case 136
	skipnz
	goto	i1l13506
	xorlw	235^136	; case 235
	skipnz
	goto	i1l13498
	goto	i1l13678
	opt asmopt_on
	
i1l15912:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 201 to 219
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           47     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	201^0	; case 201
	skipnz
	goto	i1l13542
	xorlw	219^201	; case 219
	skipnz
	goto	i1l13562
	goto	i1l13678
	opt asmopt_on
	
i1l15914:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 44 to 143
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	44^0	; case 44
	skipnz
	goto	i1l13484
	xorlw	143^44	; case 143
	skipnz
	goto	i1l13544
	goto	i1l13678
	opt asmopt_on
	
i1l15916:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 185 to 185
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	185^0	; case 185
	skipnz
	goto	i1l13526
	goto	i1l13678
	opt asmopt_on
	
i1l15918:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 151 to 151
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	151^0	; case 151
	skipnz
	goto	i1l13676
	goto	i1l13678
	opt asmopt_on
	
i1l15920:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 216 to 216
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	216^0	; case 216
	skipnz
	goto	i1l13618
	goto	i1l13678
	opt asmopt_on
	
i1l15922:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 59 to 59
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	59^0	; case 59
	skipnz
	goto	i1l13612
	goto	i1l13678
	opt asmopt_on
	
i1l15836:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 7 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 7, Range of values is 191 to 234
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           22    12 (average)
; direct_byte           97     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	191^0	; case 191
	skipnz
	goto	i1l15924
	xorlw	192^191	; case 192
	skipnz
	goto	i1l15926
	xorlw	195^192	; case 195
	skipnz
	goto	i1l15928
	xorlw	199^195	; case 199
	skipnz
	goto	i1l15930
	xorlw	202^199	; case 202
	skipnz
	goto	i1l15932
	xorlw	230^202	; case 230
	skipnz
	goto	i1l15934
	xorlw	234^230	; case 234
	skipnz
	goto	i1l15936
	goto	i1l13678
	opt asmopt_on
	
i1l15924:	
; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 83 to 182
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	83^0	; case 83
	skipnz
	goto	i1l13570
	xorlw	101^83	; case 101
	skipnz
	goto	i1l13664
	xorlw	182^101	; case 182
	skipnz
	goto	i1l13510
	goto	i1l13678
	opt asmopt_on
	
i1l15926:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 25 to 25
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	25^0	; case 25
	skipnz
	goto	i1l13572
	goto	i1l13678
	opt asmopt_on
	
i1l15928:	
; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 49 to 247
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	49^0	; case 49
	skipnz
	goto	i1l13576
	xorlw	67^49	; case 67
	skipnz
	goto	i1l13508
	xorlw	148^67	; case 148
	skipnz
	goto	i1l13516
	xorlw	247^148	; case 247
	skipnz
	goto	i1l13578
	goto	i1l13678
	opt asmopt_on
	
i1l15930:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 33 to 33
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	33^0	; case 33
	skipnz
	goto	i1l13558
	goto	i1l13678
	opt asmopt_on
	
i1l15932:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 255 to 255
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte            7     5 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	255^0	; case 255
	skipnz
	goto	i1l13582
	goto	i1l13678
	opt asmopt_on
	
i1l15934:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 98 to 197
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	98^0	; case 98
	skipnz
	goto	i1l13510
	xorlw	197^98	; case 197
	skipnz
	goto	i1l13514
	goto	i1l13678
	opt asmopt_on
	
i1l15936:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 64 to 163
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	64^0	; case 64
	skipnz
	goto	i1l13518
	xorlw	163^64	; case 163
	skipnz
	goto	i1l13520
	goto	i1l13678
	opt asmopt_on
	
i1l15838:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 2 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 13 to 17
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           19     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	13^0	; case 13
	skipnz
	goto	i1l15938
	xorlw	17^13	; case 17
	skipnz
	goto	i1l15940
	goto	i1l13678
	opt asmopt_on
	
i1l15938:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 113 to 113
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	113^0	; case 113
	skipnz
	goto	i1l13574
	goto	i1l13678
	opt asmopt_on
	
i1l15940:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 79 to 79
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	79^0	; case 79
	skipnz
	goto	i1l13580
	goto	i1l13678
	opt asmopt_on
	
i1l15840:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 7 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 7, Range of values is 69 to 148
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           22    12 (average)
; direct_byte          169     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	69^0	; case 69
	skipnz
	goto	i1l15942
	xorlw	70^69	; case 70
	skipnz
	goto	i1l15944
	xorlw	73^70	; case 73
	skipnz
	goto	i1l15946
	xorlw	77^73	; case 77
	skipnz
	goto	i1l15948
	xorlw	108^77	; case 108
	skipnz
	goto	i1l15950
	xorlw	109^108	; case 109
	skipnz
	goto	i1l15952
	xorlw	148^109	; case 148
	skipnz
	goto	i1l15954
	goto	i1l13678
	opt asmopt_on
	
i1l15942:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 233 to 251
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte           47     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	233^0	; case 233
	skipnz
	goto	i1l13656
	xorlw	251^233	; case 251
	skipnz
	goto	i1l13666
	goto	i1l13678
	opt asmopt_on
	
i1l15944:	
; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 76 to 175
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            7     4 (average)
; direct_byte          209     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	76^0	; case 76
	skipnz
	goto	i1l13634
	xorlw	175^76	; case 175
	skipnz
	goto	i1l13658
	goto	i1l13678
	opt asmopt_on
	
i1l15946:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 217 to 217
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	217^0	; case 217
	skipnz
	goto	i1l13644
	goto	i1l13678
	opt asmopt_on
	
i1l15948:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 183 to 183
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	183^0	; case 183
	skipnz
	goto	i1l13616
	goto	i1l13678
	opt asmopt_on
	
i1l15950:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 248 to 248
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	248^0	; case 248
	skipnz
	goto	i1l13636
	goto	i1l13678
	opt asmopt_on
	
i1l15952:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 91 to 91
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	91^0	; case 91
	skipnz
	goto	i1l13638
	goto	i1l13678
	opt asmopt_on
	
i1l15954:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 7 to 7
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	7^0	; case 7
	skipnz
	goto	i1l13586
	goto	i1l13678
	opt asmopt_on
	
i1l15842:	
; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 145 to 145
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+1),w
	opt asmopt_off
	xorlw	145^0	; case 145
	skipnz
	goto	i1l15956
	goto	i1l13678
	opt asmopt_on
	
i1l15956:	
; Switch size 1, requested type "space"
; Number of cases is 1, Range of values is 232 to 232
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte           11     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol),w
	opt asmopt_off
	xorlw	232^0	; case 232
	skipnz
	goto	i1l13674
	goto	i1l13678
	opt asmopt_on
	
i1l15844:	
; Switch size 1, requested type "space"
; Number of cases is 7, Range of values is 1 to 35
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           22    12 (average)
; direct_byte           79     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf (lookup_sym@symbol+2),w
	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	i1l15830
	xorlw	2^1	; case 2
	skipnz
	goto	i1l15832
	xorlw	3^2	; case 3
	skipnz
	goto	i1l15834
	xorlw	4^3	; case 4
	skipnz
	goto	i1l15836
	xorlw	5^4	; case 5
	skipnz
	goto	i1l15838
	xorlw	6^5	; case 6
	skipnz
	goto	i1l15840
	xorlw	35^6	; case 35
	skipnz
	goto	i1l15842
	goto	i1l13678
	opt asmopt_on

	line	863
	
i1l13684:	
;main.c: 863: return retvalue;
	movf	(lookup_sym@retvalue),w
	line	865
	
i1l2319:	
	return
	opt stack 0
GLOBAL	__end_of_lookup_sym
	__end_of_lookup_sym:
;; =============== function _lookup_sym ends ============

	signat	_lookup_sym,4217
	global	fptotal
fptotal equ 0
	file ""
	line	#
psect	functab,class=CODE,delta=2,reloc=256
global __pfunctab
__pfunctab:
	global	fpbase
fpbase	equ	0
global	fptable
fptable:
	goto fptable	;no entries
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
