/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	StorageSystem.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Provides centralized storage services for important
				Skoobe parameters. Some parameters are kept in files,
				others are RAM-based but are cleared upon reset,
				others are RAM-based but must survive reset. This
				library keeps almost everything in its place. The
				messages and card files are partially handled by
				other libraries.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _STORAGESYSTEM_C_
#define _STORAGESYSTEM_C_
/*** EndHeader */

/*** BeginHeader Storage_InitFS */
/*	--------------------------------------------------------------
	Function: Storage_InitFS
	Params:
		void
	Returns:
		BOOL - TRUE if storage library initializes the file system
			properly, FALSE if it fails.
	--------------------------------------------------------------
	Purpose:
		Initializes the File System.
	NOTE:
		Failure of this function is critical.
	------------------------------------------------------------ */
// Dynamic C File System library (configuration done in SkoobeConfig.h)
#use "fs2.lib"

// Each file has a checksum in this record (index 0, the checksum file's checksum, is not used)
typedef struct {
	int fXSUMS[SII_NUMFILES];
	int seed;
} filexsums;

// These structures will be zeroed on reset.
typedef struct {
	mssg_ACCT_t	acct;
	int			winnings;
} sk_volatileconfig_t;

// These structures will persist through a reset
// or power cycle. (the RAM is battery-backed).
typedef struct {
	storage_NVGLOB_t globals;
	storage_SNBB_t	snbb;
	storage_CNFG_t	cnfg;
	storage_DEVC_t	devc;
	storage_DIOP_t	diop;
   storage_COLO_t colo;
} sk_nonvolatileconfig_t;

BOOL Storage_InitFS(void);

extern sk_volatileconfig_t g_volatile;
extern sk_nonvolatileconfig_t g_nonvolatile;

// Access macros for the RAM-based storage provided by this library.
// File-based storage is accessed via function calls.
#define G_STOR_ACCT_MSG			g_volatile.acct
#define G_STOR_WINNINGS_MSG	g_volatile.winnings
#define G_STOR_GLOB_MSG			g_nonvolatile.globals.misc
#define G_STOR_GLOB				g_nonvolatile.globals
#define G_STOR_SNBB_MSG			g_nonvolatile.snbb.snbb
#define G_STOR_SNBB				g_nonvolatile.snbb
#define G_STOR_CNFG_MSG			g_nonvolatile.cnfg.cnfg
#define G_STOR_CNFG				g_nonvolatile.cnfg
#define G_STOR_DEVC_MSG			g_nonvolatile.devc.devc
#define G_STOR_DEVC				g_nonvolatile.devc
#define G_STOR_DIOP_MSG			g_nonvolatile.diop.diop
#define G_STOR_DIOP				g_nonvolatile.diop
#define G_STOR_COLO_MSG			g_nonvolatile.colo.colo
#define G_STOR_COLO				g_nonvolatile.colo

/*** EndHeader */
sk_volatileconfig_t		g_volatile;
sk_nonvolatileconfig_t	g_nonvolatile;

BOOL Storage_InitFS(void)
{
	int fserror;
	FSLXnum lx_RAM;

	fserror = fs_init(0,0);
	if (fserror) {
		DebugPrint(STRACE_ERR,("File System Initialization Failed. errno:%d",errno));
		return FALSE;
	}
	lx_RAM = fs_get_ram_lx();
	if (!lx_RAM) {
		DebugPrint(STRACE_ERR,("There is no RAM extent for the file system."));
		return FALSE;
	}

	// Report RAM LX Size if debug tracing is on.
	DebugPrint(STRACE_DBG,("RAM extent size %lu.",fs_get_lx_size(fs_get_ram_lx(),1,0)));

	// Put both files and metadata into the RAM Logical eXtension.
	fserror = fs_set_lx(lx_RAM,lx_RAM);
	if(fserror) {
		DebugPrint(STRACE_ERR,("File System Set LX Failed. errno:%d",errno));
		return FALSE;
	}

	return TRUE;
}

/*** BeginHeader Storage_Init */
/*	--------------------------------------------------------------
	Function: Storage_Init
	Params: void
	Returns:
		BOOL - TRUE if stroage system was initialized successfully,
			FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Initializes the Storage System.
	NOTE:
		Failure of this function is non-critical & may denote
		a new system or one that has been upgraded to an
		incompatible storage layout.
	------------------------------------------------------------ */
BOOL Storage_Init(void);
/*** EndHeader */
BOOL Storage_Init(void)
{
	File tFile;
	filexsums txsums;

	// Marke config as invalid until it is validated.
	g_StorageHaveValidConfig = FALSE;

	// Wipe the volatile RAM.
	memset(&g_volatile,0,sizeof(g_volatile));

	// Check Misc Globals & reset to defaults if corrupt.
	if ( Storage_Checksum(&G_STOR_GLOB_MSG,sizeof(G_STOR_GLOB_MSG)) != G_STOR_GLOB.chksum ) {
		DebugPrint(STRACE_NFO,("Globals Checksum failed. May be first boot. Resetting."));
		G_STOR_GLOB_MSG.skoobe_pricestate = 0;
		G_STOR_GLOB.chksum =  Storage_Checksum(&G_STOR_GLOB_MSG, sizeof(G_STOR_GLOB_MSG));
	}

	// Check DIOP & reset to defaults if corrupt.
	if ( Storage_Checksum(G_STOR_DIOP_MSG,sizeof(G_STOR_DIOP_MSG)) != G_STOR_DIOP.chksum ) {
		DebugPrint(STRACE_NFO,("DIOP Checksum failed. May be first boot. Resetting."));
		Storage_SetDefaultDIOP();
	}

//MSS
	// Check COLO & reset to defaults if corrupt.
	if ( Storage_Checksum(&G_STOR_COLO_MSG,sizeof(G_STOR_COLO_MSG)) != G_STOR_COLO.chksum ) {
		DebugPrint(STRACE_NFO,("COLO Checksum failed. May be first boot. Resetting."));
		Storage_SetDefaultCOLO();
   }
//MSS

	// See if checksum file exists.
	if (fopen_rd(&tFile,SII_CHECKSUMFILE)) {
		DebugPrint(STRACE_NFO,("Checksum file doesn't exist. May be first boot."));
		return FALSE;
	} else {
		// Load checksum structure from checksum file.
		if (fread(&tFile,&txsums,sizeof(txsums)) != sizeof(txsums)) {
			fclose(&tFile);
			DebugPrint(STRACE_DBG,("Can't read checksum file."));
			return FALSE;
		}
		fclose(&tFile);
		// See if checksum file seed value is current. If not, we have probably upgraded to
		// an incompatible file system layout.
		if(SII_STORAGE_SEED != txsums.seed) {
			DebugPrint(STRACE_DBG,("File system format is old."));
			return FALSE;
		}
	}

	return TRUE;
}

/*** BeginHeader Storage_UnInit */
/*	--------------------------------------------------------------
	Function: Storage_UnInit
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Undoes initialization such that Storage_Init() can be called
		again. This is used when the storage system is determined
		to be corrupt & needs formatting.
	------------------------------------------------------------ */
void Storage_UnInit(void);
/*** EndHeader */
void Storage_UnInit(void)
{
}

/*** BeginHeader Storage_NVWipe */
/*	--------------------------------------------------------------
	Function: Storage_NVWipe
	Params: void
	Returns: void
	--------------------------------------------------------------
	Purpose:
		Wipes non-volatile RAM. For debugging purposes.
	------------------------------------------------------------ */
void Storage_NVWipe(void);
/*** EndHeader */
void Storage_NVWipe(void)
{
	memset(&g_nonvolatile,0,sizeof(g_nonvolatile));
}


/*** BeginHeader Storage_ValidateNonVolatileRAMConfig */
/*	--------------------------------------------------------------
	Function: Storage_ValidateNonVolatileRAMConfig
	Params:
		void
	Returns:
		BOOL - TRUE if all NV RAM structures have valid checksums,
			FALSE if they do not.
	--------------------------------------------------------------
	Purpose:
		Checksums all of the Non Volatile configuration structures.
		If a checksum fails, its associated structure is zeroed &
		this function returns zero.
	------------------------------------------------------------ */
BOOL Storage_ValidateNonVolatileRAMConfig(void);
/*** EndHeader */
BOOL Storage_ValidateNonVolatileRAMConfig(void)
{
	BOOL allgood;
	allgood = TRUE;

	// Check SNBB
	if ( Storage_Checksum(&g_nonvolatile.snbb.snbb,sizeof(g_nonvolatile.snbb.snbb)) != g_nonvolatile.snbb.chksum ) {
		DebugPrint(STRACE_NFO,("SNBB Checksum failed. May be first boot."));
		memset(&g_nonvolatile.snbb,0,sizeof(g_nonvolatile.snbb));
		allgood = FALSE;
		g_nonvolatile.snbb.valid = FALSE;
	} else {
		g_nonvolatile.snbb.valid = TRUE;
	}
	// Check CNFG
	if ( Storage_Checksum(&g_nonvolatile.cnfg.cnfg,sizeof(g_nonvolatile.cnfg.cnfg)) != g_nonvolatile.cnfg.chksum ) {
		DebugPrint(STRACE_NFO,("CNFG Checksum failed. May be first boot."));
		memset(&g_nonvolatile.cnfg,0,sizeof(g_nonvolatile.cnfg));
		allgood = FALSE;
	}
	// Check DEVC
	if ( Storage_Checksum(&g_nonvolatile.devc.devc,sizeof(g_nonvolatile.devc.devc)) != g_nonvolatile.devc.chksum ) {
		DebugPrint(STRACE_NFO,("DEVC Checksum failed. May be first boot."));
		memset(&g_nonvolatile.devc,0,sizeof(g_nonvolatile.devc));
		allgood = FALSE;
	}
	// Check DIOP
	if ( Storage_Checksum(g_nonvolatile.diop.diop,sizeof(g_nonvolatile.diop.diop)) != g_nonvolatile.diop.chksum ) {
		DebugPrint(STRACE_NFO,("DIOP Checksum failed."));
		memset(&g_nonvolatile.diop,0,sizeof(g_nonvolatile.diop));
		allgood = FALSE;
	}

	// Check COLO
	if ( Storage_Checksum(&g_nonvolatile.colo.colo,sizeof(g_nonvolatile.colo.colo)) != g_nonvolatile.colo.chksum ) {
		DebugPrint(STRACE_NFO,("COLO Checksum failed."));
		memset(&g_nonvolatile.colo,0,sizeof(g_nonvolatile.colo));
		allgood = FALSE;
	}
	return allgood;
}

/*** BeginHeader Storage_SetPriceState */
/*	--------------------------------------------------------------
	Function: Storage_SetPriceState
	Params:
		pstate - new price state
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets a new price state.
	------------------------------------------------------------ */
void Storage_SetPriceState(int pstate);
/*** EndHeader */
void Storage_SetPriceState(int pstate)
{
	if ((pstate < PRICE_STATE_MIN)||(pstate > PRICE_STATE_MAX))
		return;
	G_STOR_GLOB_MSG.skoobe_pricestate = pstate;
	G_STOR_GLOB.chksum =  Storage_Checksum(&G_STOR_GLOB_MSG, sizeof(G_STOR_GLOB_MSG));
}

/*** BeginHeader Storage_SetDefaultDIOP */
/*	--------------------------------------------------------------
	Function: Storage_SetDefaultDIOP
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Resets the DIOP structures to defaults that work for most
		cases.
	------------------------------------------------------------ */
void Storage_SetDefaultDIOP(void);
/*** EndHeader */
void Storage_SetDefaultDIOP(void)
{
	xmem2root(G_STOR_DIOP_MSG,sII_DefaultDIOP,sizeof(G_STOR_DIOP_MSG));
	// Update the checksum for the configuration record.
	G_STOR_DIOP.chksum =  Storage_Checksum(G_STOR_DIOP_MSG, sizeof(G_STOR_DIOP_MSG));
}

//MSS
/*** BeginHeader Storage_SetDefaultCOLO */
/*	--------------------------------------------------------------
	Function: Storage_SetDefaultCOLO
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Resets the COLO structures to defaults that work for most
		cases.
	------------------------------------------------------------ */
void Storage_SetDefaultCOLO(void);
/*** EndHeader */
void Storage_SetDefaultCOLO(void)
{
	xmem2root(&G_STOR_COLO_MSG,sII_DefaultCOLO,sizeof(G_STOR_COLO_MSG));
	// Update the checksum for the configuration record.
	G_STOR_COLO.chksum =  Storage_Checksum(&G_STOR_COLO_MSG, sizeof(G_STOR_COLO_MSG));
}
//MSS

/*** BeginHeader Storage_ChecksumAFile */
/*	--------------------------------------------------------------
	Function: Storage_ChecksumAFile
	Params:
		tFile - pointer to file handle to checksum
		checksum - location to fill with checksum
	Returns:
		BOOL - TRUE if file was checksummed properly, FALSE if
			an I/O or other error occurs.
	--------------------------------------------------------------
	Purpose:
		Performs a checksum calculation on a file, given a handle
		to the file.
	------------------------------------------------------------ */
BOOL Storage_ChecksumAFile(File* tFile, int* checksum);
/*** EndHeader */
BOOL Storage_ChecksumAFile(File* tFile, int* checksum)
{
	char tBuffer[128];
	int tlen;

	*checksum = SII_STORAGE_SEED;
	if (fseek(tFile,0,SEEK_SET))
		return FALSE;
	do {
		tlen = fread(tFile,tBuffer,128);
		if (tlen > 0)
			*checksum = getcrc(tBuffer,tlen,*checksum);
		//
		// If we didn't read a full buffer, see if it was the end of file (errno == 0)
		// or an I/O error (errno != 0).
		if ((tlen != 128) && (errno != 0)) {
			fclose(tFile);
			return FALSE;
		}
	} while (tlen == 128);
	return TRUE;
}

/*** BeginHeader Storage_UpdateChecksum */
/*	--------------------------------------------------------------
	Function: Storage_UpdateChecksum
	Params:
		filenumber - 2-SII_NUMFILES (1 is reserved for the checksum
			file itself)
		checksum - the checksum to write for this file.
	Returns:
		BOOL - TRUE checksum was updated, FALSE if there was an error.
	--------------------------------------------------------------
	Purpose:
		Updates the checksum for a file.
	------------------------------------------------------------ */
BOOL Storage_UpdateChecksum(int filenumber, int checksum);
/*** EndHeader */
BOOL Storage_UpdateChecksum(int filenumber, int checksum)
{
	File tFile;
	filexsums txsums;

	if ( (filenumber < 2)||(filenumber > SII_NUMFILES) ) {
		DebugPrint(STRACE_DBG,("Bad checksum file number."));
		return FALSE;
	}

	if (fopen_wr(&tFile,SII_CHECKSUMFILE)) {
		DebugPrint(STRACE_DBG,("Can't open checksum file."));
		return FALSE;
	}
	// Load checksum structure from checksum file.
	if (fread(&tFile,&txsums,sizeof(txsums)) != sizeof(txsums)) {
		fclose(&tFile);
		DebugPrint(STRACE_DBG,("Can't read checksum file."));
		return FALSE;
	}
	txsums.fXSUMS[filenumber-1] = checksum;
	// Write out the updated checksum structure.
	if (fseek(&tFile,0,SEEK_SET)) {
		fclose(&tFile);
		DebugPrint(STRACE_DBG,("Can't seek checksum file."));
		return FALSE;
	}
	if (fwrite(&tFile,&txsums,sizeof(txsums)) != sizeof(txsums)) {
		fclose(&tFile);
		DebugPrint(STRACE_DBG,("Can't write checksum file."));
		return FALSE;
	}
	fclose(&tFile);
	return TRUE;
}

/*** BeginHeader Storage_GetChecksum */
/*	--------------------------------------------------------------
	Function: Storage_GetChecksum
	Params:
		filenumber - 2-SII_NUMFILES (1 is reserved for the checksum
			file itself)
		checksum - pointer to the checksum to retrieve for this file.
	Returns:
		BOOL - TRUE checksum was retrieved, FALSE if there was an error.
	--------------------------------------------------------------
	Purpose:
		Updates the checksum for a file.
	------------------------------------------------------------ */
BOOL Storage_GetChecksum(int filenumber, int* checksum);
/*** EndHeader */
BOOL Storage_GetChecksum(int filenumber, int* checksum)
{
	File tFile;
	filexsums txsums;

	if ( (filenumber < 2)||(filenumber > SII_NUMFILES) )
		return FALSE;

	if (fopen_rd(&tFile,SII_CHECKSUMFILE))
		return FALSE;
	// Load checksum structure from checksum file.
	if (fread(&tFile,&txsums,sizeof(txsums)) != sizeof(txsums)) {
		fclose(&tFile);
		return FALSE;
	}
	fclose(&tFile);
	if (checksum)
		*checksum = txsums.fXSUMS[filenumber-1];
	return TRUE;
}


/*** BeginHeader Storage_Format */
/*	--------------------------------------------------------------
	Function: Storage_Format
	Params:
		void
	Returns:
		BOOL - TRUE if format suceeds, FALSE if it fails.
	--------------------------------------------------------------
	Purpose:
		Formats the file system and creates default empty files.
	------------------------------------------------------------ */
BOOL Storage_Format(void);
/*** EndHeader */
BOOL Storage_Format(void)
{
	File tFile;
	filexsums txsums;
	int i;

	// Format the RAM file system.
	if (lx_format(fs_get_ram_lx(),0))
		return FALSE;
	// Zero out the checksum records.
	for (i = 0; i < SII_NUMFILES; i++)
		txsums.fXSUMS[i] = 0;
	// To track the current version of the checksum record.
	txsums.seed = SII_STORAGE_SEED;

	if (fcreate(&tFile, SII_CHECKSUMFILE)) {
		DebugPrint(STRACE_DBG,("Can't create checksum file."));
		return FALSE;
	}
	// Write a zeroed checksum file.
	if (fwrite(&tFile,&txsums,sizeof(txsums)) != sizeof(txsums)) {
		DebugPrint(STRACE_DBG,("Can't write checksum file."));
		fclose(&tFile);
		return FALSE;
	}
	fclose(&tFile);
    return TRUE;
}

/*** BeginHeader Storage_Checksum */
/*	--------------------------------------------------------------
	Function: Storage_Checksum
	Params:
		buffer - buffer of data to checksum.
		len - length of buffer.
	Returns:
		int - checksum of buffer.
	--------------------------------------------------------------
	Purpose:
		Calculates the checksum of a buffer of data.
	------------------------------------------------------------ */
int Storage_Checksum(void* buffer, int len);
/*** EndHeader */
int Storage_Checksum(void* buffer, int len)
{
	int done, txsum;
	txsum = SII_STORAGE_SEED;
	done  = 0;
	while (done < len) {
		if ((len-done) > 255) {
			txsum = getcrc(((char*)buffer)+done,255,txsum);
			done += 255;
		} else {
			txsum = getcrc(((char*)buffer)+done,len,txsum);
			done += (len-done);
		}
	}
    return txsum;
}

/*** BeginHeader Storage_HaveValidConfig */
/*	--------------------------------------------------------------
	Function: Storage_HaveValidConfig
	Params:
		void
	Returns:
		BOOL - TRUE if configuration is valid, FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Returns whether or not the configuration was deemed valid.
	------------------------------------------------------------ */
BOOL Storage_HaveValidConfig(void);
extern BOOL	g_StorageHaveValidConfig;		// TRUE when configuration is completely valid. (good files, good CNFG message, etc.)
/*** EndHeader */
BOOL g_StorageHaveValidConfig;
BOOL Storage_HaveValidConfig(void)
{
	return g_StorageHaveValidConfig;
}

/*** BeginHeader Storage_SetValidConfig */
/*	--------------------------------------------------------------
	Function: Storage_SetValidConfig
	Params:
		valid - TRUE if configuration is valid, FALSE otherwise.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets whether or not the config is valid.
	Notes:
		The Cashless Client library validates the configuration,
		but several libraries need to access the validity state,
		so it is stored in this library.
	------------------------------------------------------------ */
void Storage_SetValidConfig(BOOL valid);
/*** EndHeader */
void Storage_SetValidConfig(BOOL valid)
{
	g_StorageHaveValidConfig = valid;
}

/*** BeginHeader Storage_IsTicketlessTokenInteractive */
/*	--------------------------------------------------------------
	Function: Storage_IsTicketlessTokenInteractive
	Params:
		void
	Returns:
		BOOL - TRUE if this is a ticketless token interactive
			device, FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Helper to determine if this is a ticketless token
		interactive device.
	------------------------------------------------------------ */
BOOL Storage_IsTicketlessTokenInteractive(void);
/*** EndHeader */
BOOL Storage_IsTicketlessTokenInteractive(void)
{
	return ((G_STOR_DEVC_MSG.devicetype == DEVTYPE_TOKINTR) && (G_STOR_CNFG_MSG.nodispense));
}

/*** BeginHeader Storage_GetWinnings */
/*	--------------------------------------------------------------
	Function: Storage_GetWinnings
	Params:
		void
	Returns:
		int - Last ticket winnings latched.
	--------------------------------------------------------------
	Purpose:
		Retrieves the last ticket winnings latched in storage.
	------------------------------------------------------------ */
int Storage_GetWinnings(void);
/*** EndHeader */
int Storage_GetWinnings(void)
{
	return G_STOR_WINNINGS_MSG;
}

/*** BeginHeader Storage_SetWinnings */
/*	--------------------------------------------------------------
	Function: Storage_SetWinnings
	Params:
		winnings - winnings to latch into storage.
	Returns:
		int - Last ticket winnings latched.
	--------------------------------------------------------------
	Purpose:
		Stores current winnings for the UI or other libraries to
		retrieve.
	------------------------------------------------------------ */
void Storage_SetWinnings(int winnings);
/*** EndHeader */
void Storage_SetWinnings(int winnings)
{
	G_STOR_WINNINGS_MSG = winnings;
}


/*** BeginHeader */
#endif // #ifndef _STORAGESYSTEM_C_
/*** EndHeader */

