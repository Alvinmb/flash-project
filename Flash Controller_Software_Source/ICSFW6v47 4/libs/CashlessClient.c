/* --------------------------------------------------------------
	Copyright (C) 2007 Ideal Software Systems. All Rights Reserved.

	Filename:	CashlessClient.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Implements network functionality required to participate as an Ideal
				cashless client.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	03-26-2007	v1.0	<jerry@mindtribe.com>
		Changed the "extra data" seperator to a '@' from '&'
	Update:	05-27-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _CASHLESSCLIENT_C_
#define _CASHLESSCLIENT_C_
#use UDPDOWNL.lib
#define Notch_DIO			11
#define DIOS11				11
#define DIOS10				10
/*** EndHeader */


/*** BeginHeader CClient_Init */
/*	--------------------------------------------------------------
	Function: CClient_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Initialize the Ideal Cashless Client system.
	------------------------------------------------------------ */
void CClient_Init(void);
//void CC_CheckPendingTkts(void);

typedef enum {
	ENET_STATE_FIRSTBRINGUP = 0,
	ENET_STATE_WAITABITFORLINK,
	ENET_STATE_BRINGUP,
	ENET_STATE_STARTING,
	ENET_STATE_IFACEFAILURE,
	ENET_STATE_IFACERETRY,
	ENET_STATE_DCHPFAILURE,
	ENET_STATE_DHCPRETRY,
	ENET_STATE_RESET,
	ENET_STATE_BRINGINGDOWNFORRESET,	// Ethernet is going down for reset.
	ENET_STATE_IDLE
} enet_state;

extern enet_state  	g_ENETState;
extern unsigned long	g_ENETtimer;
extern unsigned long	g_HAPYtimer_s;
extern unsigned long	g_HAPYfreq_s;
extern unsigned char g_sentDisplayIdle;

extern int g_lcd_tkts_count, g_srv_tkts_count, g_srv_tkts_dios;
extern unsigned long g_tkts_timer;
extern BOOL g_srv_tkts_pending, skip_dios;
extern int lcd_inc_tkts;


typedef enum {
	TRANS_STATE_IDLE = 0,	// Transaction state is idle.
	TRANS_STATE_LISTEN,		// Socket is listening for commands.
	TRANS_STATE_CONNECTING,	// Trying to connect to server
	TRANS_STATE_CONNECTED,	// A transaction is currently underway
	TRANS_STATE_CLOSING		// Closing a socket after a transaction.
} cctransaction_state;

typedef struct {
	cctransaction_state tstate;
	tcp_Socket sock;
	char txBuf[SII_NET_TXBUF_LEN];
	char rxBuf[SII_NET_RXBUF_LEN];
	unsigned long  timer;
	BOOL push;
} transaction_ctx;

extern tcp_Socket g_ccTCPListensock;
extern char g_ccMACAddress[18];
extern BOOL g_ccLock;
extern transaction_ctx g_transctx;
extern char	g_CClastbcodescan[BC_MAXBARCODELEN+1];
extern char	g_CClastextradata[BC_MAXEXTRADATALEN+1];
extern SSRV_STATE g_CCLient_SSRVState;
/*** EndHeader */

enet_state		g_ENETState;
unsigned long	g_ENETtimer;
unsigned long	g_HAPYtimer_s;
unsigned long	g_HAPYfreq_s;
unsigned char  g_sentDisplayIdle;
tcp_Socket		g_ccTCPListensock;
char				g_ccMACAddress[18];
BOOL				g_ccLock;
transaction_ctx g_transctx;

int g_lcd_tkts_count, g_srv_tkts_count, g_srv_tkts_dios;
unsigned long g_tkts_timer;
BOOL g_srv_tkts_pending, skip_dios;
int lcd_inc_tkts;

char				g_CClastbcodescan[BC_MAXBARCODELEN+1];
char	        	g_CClastextradata[BC_MAXEXTRADATALEN+1];
SSRV_STATE		g_CCLient_SSRVState;

void CC_GenerateLcdCnfgMessage( int mode);
void CC_GenerateLcdAcctMessage(void);

void CClient_Init(void)
{
	char tmac[6];
	g_ENETState = ENET_STATE_FIRSTBRINGUP;
	g_ccLock = FALSE;
	g_CCLient_SSRVState = SSRVSTATE_DEFAULT;
   g_sentDisplayIdle = 0;


   g_lcd_tkts_count = 0;
   g_srv_tkts_count = 0;
	g_tkts_timer = 0;
	g_srv_tkts_pending = FALSE;
   skip_dios = FALSE;

	sock_init();
	// Get the MAC address and format for transaction use.
	pd_getaddress(0,tmac);
	sprintf(g_ccMACAddress,"%02x:%02x:%02x:%02x:%02x:%02x", tmac[0],tmac[1],tmac[2],tmac[3],tmac[4],tmac[5]);
	DebugPrint(STRACE_DBG,("MAC Address: %s",g_ccMACAddress));

	ifconfig(   IF_ETH0,
				IFS_DOWN,
				IFS_DHCP, true, // Use DHCP
				IFS_ICMP_CONFIG, false,
				IFS_DHCP_TIMEOUT, SII_DHCP_TIMEOUT_S,
				IFS_DHCP_FALLBACK, 1,   // Allow use of fallbacks to static configuration
				IFS_IPADDR, aton("10.1.1.101"),
				IFS_NETMASK, aton("255.255.255.0"),
				IFS_ROUTER_SET, aton("10.1.1.1"),
				IFS_NAMESERVER_SET, aton("10.1.1.1"),
				IFS_END );
	// Interface will be brought up by CClient_Handler().

	// The transaction is initially idle.
	g_transctx.tstate = TRANS_STATE_IDLE;
	g_transctx.push = FALSE;
	// Reset HAPY timer.
	g_HAPYfreq_s = 0;
	g_HAPYtimer_s = SEC_TIMER+g_HAPYfreq_s;
	strcpy(g_CClastbcodescan,"0");
    g_CClastextradata[0] = '\0';

#ifdef ENABLE_EBOOTLOADER
	// Initialize the download manager loader
	UDPDL_Init(g_VersionString);
#endif
}

/*** BeginHeader CCGetMessageID, CCGetMessageHdr */
sn_did_t CCGetMessageID(char* header);
char* CCGetMessageHdr(sn_did_t msgID);
#define NUM_SKOOBE_MESSAGES	37
typedef struct {
	unsigned char	msgstr[5];
	sn_did_t		msgid;
} skoobe_message_t;
extern const skoobe_message_t skoobe_messages[NUM_SKOOBE_MESSAGES];
/*** EndHeader */

const skoobe_message_t skoobe_messages[NUM_SKOOBE_MESSAGES] = {
	{ "ACCT", SN_DID_ACCT},
	{ "BOOT", SN_DID_BOOT},
	{ "BUTN", SN_DID_BUTN},
	{ "CMCD", SN_DID_CMCD},
	{ "CMSG", SN_DID_CMSG},
	{ "CNFG", SN_DID_CNFG},
	{ "CRDS", SN_DID_CRDS},
	{ "DEVC", SN_DID_DEVC},
	{ "DIOP", SN_DID_DIOP},
	{ "DIOS", SN_DID_DIOS},
   { "RIOS", SN_DID_RIOS},
	{ "DISP", SN_DID_DISP},
	{ "EOTX", SN_DID_EOTX},
	{ "HAPY", SN_DID_HAPY},
	{ "LEDS", SN_DID_LEDS},
	{ "LEDX", SN_DID_LEDX},
	{ "LOCK", SN_DID_LOCK},
	{ "MSSG", SN_DID_MSSG},
	{ "PRCE", SN_DID_PRCE},
	{ "PUSH", SN_DID_PUSH},
	{ "REBX", SN_DID_REBX},
	{ "RELY", SN_DID_RELY},
	{ "RQST", SN_DID_RQST},
	{ "SCAN", SN_DID_SCAN},
	{ "SNBB", SN_DID_SNBB},
	{ "TEST", SN_DID_TEST},
   { "COLO", SN_DID_COLO}, //rjt added to support the LED light string
   { "FLIP", SN_DID_FLIP}, //rjt added to support flipping video screen
   { "RSET", SN_DID_RSET}, //rjt added to support resetting video screen
   { "VSET", SN_DID_VSET}, //rjt added to support setting sound volume
};

sn_did_t CCGetMessageID(char* header)
{
	unsigned long thdrval;
	int i;

	// Treat the 4-char message header text as a 4-byte long. This is faster than memcmp or strcmp.
	thdrval = *((unsigned long*)header);
	for(i = 0; i < (sizeof(skoobe_messages)/sizeof(skoobe_message_t)); i++) {
		if ( *((unsigned long*)skoobe_messages[i].msgstr) == thdrval) {
			return skoobe_messages[i].msgid;
		}
	}
	return SN_DID_UNKNOWN;
}

char* CCGetMessageHdr(sn_did_t msgID)
{
	int i;

	for(i = 0; i < (sizeof(skoobe_messages)/sizeof(skoobe_message_t)); i++) {
		if ( skoobe_messages[i].msgid == msgID) {
			return skoobe_messages[i].msgstr;
		}
	}
	return "unkn";
}

/*** BeginHeader CClient_Handler */
/*	--------------------------------------------------------------
	Function: CClient_Handler
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Handles Ideal Cashless Client Ethernet events.
	------------------------------------------------------------ */
void CClient_Handler(void);
/*** EndHeader */
void CClient_Handler(void)
{
	auto word dhcp_ok, dhcp_fb;

	tcp_tick(NULL);
#ifdef ENABLE_EBOOTLOADER
		// Initialize the download manager loader
		if (UDPDL_Tick()) {
			UI_DoManualMsg("Downloading new firmware...",0,FALSE,FALSE);
			while (1)	// Lock up here until the download manager reboots.
				UDPDL_Tick();
		}
#endif
	switch (g_ENETState) {
	case ENET_STATE_FIRSTBRINGUP:
		if (!pd_havelink(0)) {
			DebugPrint(STRACE_DBG,("No initial link. Waiting a bit..."));
			g_ENETtimer = SEC_TIMER+SII_NOINITIALLINKWAIT_S;
			g_ENETState = ENET_STATE_WAITABITFORLINK;
		} else {
			g_ENETState = ENET_STATE_BRINGUP;
		}
		break;
	case ENET_STATE_WAITABITFORLINK:
		if(pd_havelink(0)) {
			g_ENETState = ENET_STATE_BRINGUP;
		} else {
			if (Util_CheckSTimeout(g_ENETtimer)) {
				g_ENETState = ENET_STATE_IFACEFAILURE;
			}
		}
		break;
    case ENET_STATE_BRINGUP:
		if (pd_havelink(0)) {
			if (ifup(IF_ETH0) == IFCTL_FAIL) {
				g_ENETState = ENET_STATE_IFACEFAILURE;
			} else {
				g_ENETState = ENET_STATE_STARTING;
			}
		} else {
			g_ENETState = ENET_STATE_IFACEFAILURE;
		}
        break;
	case ENET_STATE_STARTING:
    	switch (ifpending(IF_ETH0)) {
        case IF_COMING_UP: // Don't do anything until the interface is completely up or down.
        case IF_COMING_DOWN:
			break;
        case IF_UP:
			DebugPrint(STRACE_DBG,("ENET interface came up."));
			if (ifconfig(IF_ETH0, IFG_DHCP_OK, &dhcp_ok, IFG_DHCP_FELLBACK, &dhcp_fb, IFS_END)) {
				// If ifconfig fails, assume the interface failed.
   				g_ENETState = ENET_STATE_IFACEFAILURE;
			} else if (!dhcp_ok || dhcp_fb) {
				// If DHCP did not configure or fell back, it is a DHCP failure.
   				g_ENETState = ENET_STATE_DCHPFAILURE;
			} else {
				EventQ_Push(EVT_ENET_UP,0,0);
				// Note we don't assume we come out of push mode here, to come out of
				// push mode, we must successfully communicate with the SSRV.
				DebugPrint(STRACE_DBG,("DHCP was acquired."));
            //CClient_HAPYSetFreq(1); //MSS
				g_ENETState = ENET_STATE_IDLE;
			}
			break;
        case IF_DOWN:
			// If the interface is down at this point, assume it failed.
   			g_ENETState = ENET_STATE_IFACEFAILURE;
			break;
        }
		break;
	case ENET_STATE_DCHPFAILURE:
		EventQ_Push(EVT_ENET_FAIL,0,0);
		g_transctx.push = TRUE;	// We'll be going into push mode.
		EventQ_Push(EVT_PUSHMODEENTER,0,0);
		DebugPrint(STRACE_NFO,("DHCP was not acquired."));
		ifdown(IF_ETH0);
		g_ENETState = ENET_STATE_DHCPRETRY;
		g_ENETtimer = SEC_TIMER+SII_NODHCP_RETRY_S;
		break;
	case ENET_STATE_DHCPRETRY:
		if (Util_CheckSTimeout(g_ENETtimer)) {
			g_ENETState = ENET_STATE_BRINGUP;
		}
		break;
	case ENET_STATE_IFACEFAILURE:
        if(!pd_havelink(0)) {
    		EventQ_Push(EVT_NETWORKCONN_FAIL,0,0);
        }
		EventQ_Push(EVT_ENET_FAIL,0,0);
		g_transctx.push = TRUE;	// We'll be going into push mode.
		EventQ_Push(EVT_PUSHMODEENTER,0,0);
		DebugPrint(STRACE_ERR,("ENET interface failed."));
		g_ENETState = ENET_STATE_IFACERETRY;
		g_ENETtimer = SEC_TIMER+SII_IFACEDOWN_RETRY_S;
		break;
	case ENET_STATE_IFACERETRY:
		if (Util_CheckSTimeout(g_ENETtimer)) {
			g_ENETState = ENET_STATE_BRINGUP;
		}
		break;
	case ENET_STATE_RESET:
		if (Util_CheckSTimeout(g_ENETtimer)) {
			if (g_transctx.tstate == TRANS_STATE_CONNECTED) {
				sock_abort(&g_transctx.sock);
				tcp_tick(NULL);
				g_transctx.tstate = TRANS_STATE_IDLE;
				SafeReboot_PutSemaphore();
			}
			// Take down the interface.
			ifdown(IF_ETH0);
			// Now, go wait for the interface to completely come down.
			g_ENETState = ENET_STATE_BRINGINGDOWNFORRESET;
		}
		break;
	case ENET_STATE_BRINGINGDOWNFORRESET:
		// Once the interface has come down, bring it back up.
		// Once ifdown() is issued, ifpending() will always eventually return IF_DOWN.
    	if (IF_DOWN == ifpending(IF_ETH0)) {
			g_ENETState = ENET_STATE_BRINGUP;
        }
		break;
	case ENET_STATE_IDLE:
		if (!ifstatus(IF_ETH0)||!pd_havelink(0)) {
   			g_ENETState = ENET_STATE_IFACEFAILURE;
			// Kill any pending transaction connection.
			if ( (g_transctx.tstate == TRANS_STATE_CONNECTING)||(g_transctx.tstate == TRANS_STATE_CONNECTED)) {
				sock_abort(&g_transctx.sock);
				g_transctx.tstate = TRANS_STATE_IDLE;
				g_transctx.push = TRUE;
				EventQ_Push(EVT_TRANSACTIONCOMPLETEFAIL,0,0);
				SafeReboot_PutSemaphore();
			}
		}
		break;
	default:
		g_ENETState = ENET_STATE_BRINGUP;
		break;
	}

	// Check on any pending transactions.
	switch (g_transctx.tstate) {
	case TRANS_STATE_CONNECTING:
		break;
	case TRANS_STATE_CONNECTED:
		if (sock_bytesready(&g_transctx.sock) >= 0) {
			// Received traffic. Reset the HAPY timer.
			g_HAPYtimer_s = SEC_TIMER+g_HAPYfreq_s;
			// Also reset the stale transaction timer.
			g_transctx.timer = SEC_TIMER + SII_TRANS_STALE_TIMEOUT_S;
			sock_gets(&g_transctx.sock,g_transctx.rxBuf,SII_NET_RXBUF_LEN);
			//DebugPrint(STRACE_DBG,("R: %s",g_transctx.rxBuf));
			CC_ProcessMessage(g_transctx.rxBuf);
			if (memcmp(g_transctx.rxBuf,"[EOTX].",7) == 0) {
				sock_close(&g_transctx.sock);
				// Socket close timeout TODO: Is 5 seconds correct?
				g_transctx.timer = MS_TIMER+5000;
				g_transctx.tstate = TRANS_STATE_CLOSING;
				EventQ_Push(EVT_TRANSACTIONCOMPLETESUCCESS,0,0);
				SafeReboot_PutSemaphore();
			}
		} else {
			// The server closed the connection or went away.
			if (!sock_established(&g_transctx.sock) || Util_CheckSTimeout(g_transctx.timer) ) {
				sock_abort(&g_transctx.sock);
				g_transctx.tstate = TRANS_STATE_IDLE;
				EventQ_Push(EVT_TRANSACTIONCOMPLETEFAIL,0,0);
				SafeReboot_PutSemaphore();
			}
		}
		break;
	case TRANS_STATE_CLOSING:
		if (!sock_alive(&g_transctx.sock)) {
			g_transctx.tstate = TRANS_STATE_IDLE;
			SafeReboot_PutSemaphore();
		} else if (Util_CheckmsTimeout(g_transctx.timer)) {
			DebugPrint(STRACE_NFO,("Needed to ABORT a closing socket."));
			sock_abort(&g_transctx.sock);
			g_transctx.tstate = TRANS_STATE_IDLE;
			SafeReboot_PutSemaphore();
		} else if (sock_bytesready(&g_transctx.sock) >= 0) {
			// Get and discard any data received after trying to close the socket.
			sock_gets(&g_transctx.sock,g_transctx.rxBuf,SII_NET_RXBUF_LEN);
		}
		break;
	case TRANS_STATE_IDLE:
		// We can only open a listening command port when the SNBB is valid.
		if (G_STOR_SNBB.valid) {
			tcp_listen(&g_transctx.sock, G_STOR_SNBB_MSG.portcmd, 0, 0, NULL, 0);
			g_transctx.tstate = TRANS_STATE_LISTEN;
		}
		break;
	case TRANS_STATE_LISTEN:
		if (sock_established(&g_transctx.sock) || (sock_bytesready(&g_transctx.sock) >= 0) ) {
			tcp_set_ascii(&g_transctx.sock);
			g_transctx.tstate = TRANS_STATE_CONNECTED;
			// Reset the stale transaction timer
			g_transctx.timer = SEC_TIMER + SII_TRANS_STALE_TIMEOUT_S;
		}
		break;
	}
}

/*** BeginHeader CClient_HAPYHandler */
/*	--------------------------------------------------------------
	Function: CClient_HAPYHandler
	Params:
		statecode - current state machine code
		price - current price mode
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Handles Ideal Cashless Client HAPY messages.
	------------------------------------------------------------ */
void CClient_HAPYHandler(void);
/*** EndHeader */
void CClient_HAPYHandler(void)
{
	if(g_HAPYfreq_s && Util_CheckSTimeout(g_HAPYtimer_s)) {
		g_HAPYtimer_s = SEC_TIMER+g_HAPYfreq_s;
		CClient_SendMessage(SN_DID_HAPY);
		DebugPrint(STRACE_DBG,("Sent HAPY."));
      CC_GenerateLcdCnfgMessage(1); /*Communicate status chage to the LVD */
	}
}

/*** BeginHeader CClient_HAPYSetFreq */
/*	--------------------------------------------------------------
	Function: CClient_HAPYSetFreq
	Params:
		freq - frequency of HAPY heartbeat messages in seconds
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Sets frequency of HAPY messages.
	------------------------------------------------------------ */
void CClient_HAPYSetFreq(int freq);
/*** EndHeader */
void CClient_HAPYSetFreq(int freq)
{
	if (freq >= 0 && freq < 100)
		g_HAPYfreq_s = freq * 60;
	else
		g_HAPYfreq_s = 240;
	g_HAPYtimer_s = SEC_TIMER + g_HAPYfreq_s + (int)(1.0+(rand()*(float)g_HAPYfreq_s));
}


/*** BeginHeader CClient_State */
/*	--------------------------------------------------------------
	Function: CClient_State
	Params:
		void
	Returns:
		enet_state - one of the following:
			enet_pending: Ethernet is still coming up.
			enet_failed: Ethernet or DHCP failure
			enet_up: Ethernet is up and has gotten DHCP.
	--------------------------------------------------------------
	Purpose: Gets the state of the Ethernet interface.
	------------------------------------------------------------ */
typedef enum {
	enet_pending = 0,
	enet_failed,
	enet_up
} enet_status;
enet_status CClient_State(void);
/*** EndHeader */
enet_status CClient_State(void)
{
	switch (g_ENETState) {
	case ENET_STATE_BRINGUP:
	case ENET_STATE_STARTING:
	case ENET_STATE_RESET:
		return enet_pending;
	case ENET_STATE_IFACEFAILURE:
	case ENET_STATE_IFACERETRY:
	case ENET_STATE_DCHPFAILURE:
	case ENET_STATE_DHCPRETRY:
		return enet_failed;
	case ENET_STATE_IDLE:
		return enet_up;
	}
}

/*** BeginHeader CClient_GetSNBB */
/*	--------------------------------------------------------------
	Function: CClient_GetSNBB
	Params:
		void
	Returns:
		BOOL - TRUE if SNBB successfully acquired. FALSE if otherwise.
	--------------------------------------------------------------
	Purpose:
		Blocks and waits for a SNBB message.
	------------------------------------------------------------ */
BOOL CClient_GetSNBB(void);
/*** EndHeader */
BOOL CClient_GetSNBB(void)
{
	udp_Socket		snbbUDPsock;
	unsigned long	ttimer;
	char			trxbuf[SII_NET_RXBUF_LEN];

	// Try getting a SNBB packet.
	if(!udp_open(&snbbUDPsock, SII_SNBBLISTEN_PORT, -1, 0, NULL)) {
		DebugPrint(STRACE_ERR,("Failed to open UDP socket."));
		return FALSE;
	}
	ttimer = SEC_TIMER+SII_SNBBWAIT_S;

	do {
		CClient_Handler();
		if (udp_peek(&snbbUDPsock,NULL) == 1) {
			if (udp_recv(&snbbUDPsock,trxbuf,SII_NET_RXBUF_LEN) > 0) {
				if (CC_DecodeSNBB(trxbuf,&G_STOR_SNBB_MSG)) {
					DebugPrint(STRACE_NFO,("Received valid SNBB. SSRV IP: %s",G_STOR_SNBB_MSG.serveripaddr));
					udp_close(&snbbUDPsock);
					G_STOR_SNBB.valid = TRUE;
					G_STOR_SNBB.chksum =  Storage_Checksum(&G_STOR_SNBB_MSG, sizeof(G_STOR_SNBB_MSG));
					return TRUE;
				} else DebugPrint(STRACE_ERR,("Received invalid SNBB."));
			}
		}
	} while (!Util_CheckSTimeout(ttimer) && (CClient_State()==enet_up));

	DebugPrint(STRACE_ERR,("Did not receive valid SNBB."));
	udp_close(&snbbUDPsock);

    /* Don't need to send an EVT_PUSHMODEENTER here because this function is only called
        while booting & the Skoobe will automatically try going into push mode if this
        function returns FALSE */
    g_transctx.push = TRUE;
	return FALSE;
}

/*** BeginHeader CClient_Reset */
/*	--------------------------------------------------------------
	Function: CClient_Reset
	Params:
		sdelay - number of seconds to wait before resetting.
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Resets the Ethernet interface. This takes it down
		and brings it up again.
	------------------------------------------------------------ */
void CClient_Reset(int sdelay);
/*** EndHeader */
void CClient_Reset(int sdelay)
{
	g_ENETtimer = SEC_TIMER+sdelay;
	g_ENETState = ENET_STATE_RESET;
	CClient_Handler();
}

/*** BeginHeader CClient_SendMessage */
/*	--------------------------------------------------------------
	Function: CClient_SendMessage
	Params:
		msg - message type. See notes for extended parameters.
	Returns:
		BOOL - TRUE if message successfully sent or pushed. FALSE
			if message couldn't be sent or pushed.
	--------------------------------------------------------------
	Purpose:
		Initiates a transaction and sends a message to the
		Skoobe server. If the server isn't available and push mode
		is enabled, the message is stored in the push queue.
	Notes:
		The parameters to this function depend upon the message type.
		BOOT: (SN_DID_BOOT, BOOL)
			void
		DIOS: (SN_DID_DIOS, int 1, int 2)
			int 1 - DIO ID
			int 2 - DIO Count

		HAPY: (SN_DID_HAPY)

		SCAN: (SN_DID_SCAN, char* 1, char* 2)
			char* 1 - card scan
			char* 2 - extra data
		BUTN: (SN_DID_BUTN)
			void
		EOTX: (SN_DID_EOTX)
			void
		MSSG: (SN_DID_MSSG, int 1, char* 1, char* 2) (currently only MSSG type 0, push buffer record, is supported)
			int 1 - DID
			char* 1 - push buffer record info
			char* 2 - push buffer record data
	------------------------------------------------------------ */
BOOL CClient_SendMessage(sn_did_t msg, ...);
/*** EndHeader */
BOOL CClient_SendMessage(sn_did_t msg, ...)
{
	va_list marker;

	BOOL tBool;
	int tInt1,tInt2;
	char *tCharPtr1, *tCharPtr2;
	BOOL shouldtryconnect;
	char tStr[128];
//   unsigned char outBuffer[255];
//   int length;
	unsigned long ttimer;
   int i;

	// Don't reboot until the transaction has completed.
	SafeReboot_GetSemaphore();
	// I/O messages should just quickly be stored in the push
	// queue if the last connection attempt failed.
	shouldtryconnect = TRUE;
	switch (msg) {
	case SN_DID_BUTN:	// I/O
	case SN_DID_DIOS:	// I/O
		if (g_transctx.push)
			shouldtryconnect = FALSE;
		break;
	case SN_DID_EOTX:	// Never connect if we're just trying to end a connection.
		shouldtryconnect = FALSE;
		break;
	}
	// Don't bother trying to connect if the Ethernet is dead.
	if (CClient_State() != enet_up) {
		DebugPrint(STRACE_DBG,("Did not try to connect because enet down."))
		shouldtryconnect = FALSE;
	}
	// Now, see if we should connect and try to do so.
	if ((g_transctx.tstate != TRANS_STATE_CONNECTED) && shouldtryconnect) {
		// If the socket is listening for commands, close it.
		if (g_transctx.tstate == TRANS_STATE_LISTEN) {
			sock_close(&g_transctx.sock);
            // Tick a few times.
            for(i = 0; i < 5; i++) {
			    tcp_tick(&g_transctx.sock);
            }
			g_transctx.tstate = TRANS_STATE_IDLE;
			DebugPrint(STRACE_DBG,("Closed listening socket."));
		} else if (g_transctx.tstate == TRANS_STATE_CLOSING) {
			// If the socket is closing, try giving it one last chance to close & then abort.
			CClient_Handler();
			if(g_transctx.tstate == TRANS_STATE_CLOSING) {
				tcp_tick(&g_transctx.sock);
				sock_abort(&g_transctx.sock);
				tcp_tick(&g_transctx.sock);
				DebugPrint(STRACE_NFO,("Needed to ABORT a closing socket before sending new message."));
				g_transctx.tstate = TRANS_STATE_IDLE;
			}
		}
		DebugPrint(STRACE_DBG,("Connecting..."));
		// Try connecting.
		if (!tcp_open(&g_transctx.sock,0,inet_addr(G_STOR_SNBB_MSG.serveripaddr),G_STOR_SNBB_MSG.porttxn,NULL)) {
			DebugPrint(STRACE_DBG,("Failed to resolve server hardware address."));
			g_transctx.tstate = TRANS_STATE_IDLE;
		} else {
			// If there's a valid configuration, use the push timeout specified; otherwise, use a constant.
			// There really should be a valid configuration at this point for all messages except for BOOT.
            // We were having too many problems where the Skoobe would improperly boot into push mode due to
            // slow server response. It would then miss its configuration. So, we'll always wait 20 seconds
            // for a connection on BOOT.
			if (Storage_HaveValidConfig() && (msg != SN_DID_BOOT)) {
				ttimer = MS_TIMER+G_STOR_CNFG_MSG.pushwait*100;
			} else {
				ttimer = MS_TIMER+20000;
			}
			// Try to connect until we do connect or we timeout.
			g_transctx.tstate = TRANS_STATE_CONNECTING;
			while ((g_transctx.tstate == TRANS_STATE_CONNECTING) && !Util_CheckmsTimeout(ttimer) ) {
				if (sock_established(&g_transctx.sock)) {
					tcp_set_ascii(&g_transctx.sock);
					g_transctx.tstate = TRANS_STATE_CONNECTED;
					// Reset the stale transaction timer
					g_transctx.timer = SEC_TIMER + SII_TRANS_STALE_TIMEOUT_S;
				}
				// CClient_Handler() will set g_transctx.tstate to TRANS_STATE_IDLE and set the push flag
				// if the network connection goes down. It also calls tcp_tick() for us.
				CClient_Handler();
			}
			if (g_transctx.tstate != TRANS_STATE_CONNECTED) {
				DebugPrint(STRACE_DBG,("Failed to connect to server."));
				sock_abort(&g_transctx.sock);
				g_transctx.tstate = TRANS_STATE_IDLE;
			}
		}
	}

	switch (msg) {
	case SN_DID_BOOT:
		sprintf(tStr,"%02X%02X%02X%02X%02X%02X",
			(unsigned char)(SII_FIRMWAREVERSION >> 16)&0xFF,
			(unsigned char)(SII_FIRMWAREVERSION >> 8)&0xFF,
			(unsigned char)(SII_FIRMWAREVERSION)&0xFF,
			(unsigned char)(SII_HARDWAREVERSION >> 16)&0xFF,
			(unsigned char)(SII_HARDWAREVERSION >> 8)&0xFF,
			(unsigned char)(SII_HARDWAREVERSION)&0xFF);
		sprintf(g_transctx.txBuf,"[%s].%s.%u.%ld.%s%c.%d.%d",
			CCGetMessageHdr(SN_DID_BOOT),		// (%s)		BOOT Header
			g_ccMACAddress,						// (%s)		MAC address
			Push_GetCount(),					// (%u)		Number of transactions in push buffer
			G_STOR_SNBB_MSG.tmoffset,			// (%ld)	Time offset determined by SNBB packet
			tStr,								// (%s)		Firmware & Hardware version
			(Storage_HaveValidConfig())?'0':'1',// (%c)		Configuration Status (whether we need configuration sent)
			CClient_GetSSRVState(),				// (%d)		Current state machine state
			G_STOR_GLOB_MSG.skoobe_pricestate	// (%d)		Price thingy.
			);
		break;
	case SN_DID_BUTN:
		sprintf(g_transctx.txBuf,"[%s].%s.%d.0",
			CCGetMessageHdr(SN_DID_BUTN),		// (%s)		BUTN Header
			g_ccMACAddress,						// (%s)		MAC address
			1									// (%d)		Button number
			);
		break;
   case SN_DID_RIOS:
   	va_start(marker,msg);
		tInt1 = va_arg(marker, int);
		tInt2 = va_arg(marker, int);
      sprintf(g_transctx.txBuf,"[%s].%s.%s.%d.%d",
		CCGetMessageHdr(SN_DID_DIOS),		// (%s)		DIOS Header
		g_ccMACAddress,						// (%s)		MAC address
		g_CClastbcodescan,					// (%s)		Last Play Card
      tInt1,									// (%d)		DIO ID
		tInt2										// (%d)		DIO Count
		);
      break;
	case SN_DID_DIOS:
		va_start(marker,msg);
		tInt1 = va_arg(marker, int);
		tInt2 = va_arg(marker, int);

//Sansom
//This is where a ticket event happens.  DIOS is sent to SSRV and a second DIOS is sent to LCD
//Set Timers and prepare to send leftover tickets in main loop


			if (tInt1 == DIOS10 || tInt1 == DIOS11)
         {
          	g_lcd_tkts_count = g_lcd_tkts_count + tInt2; //Increment counts
            g_srv_tkts_count = g_srv_tkts_count + tInt2;
//            DebugPrint(STRACE_DBG,("g_srv_tkts_count = %d", g_srv_tkts_count))

            g_tkts_timer = SEC_TIMER + 3;	//Add 3 seconds to the ticket timer
            g_srv_tkts_pending = TRUE;
            g_srv_tkts_dios = tInt1;
            skip_dios = TRUE;

//            if (g_srv_tkts_count < 5)
//            {
//           		g_srv_tkts_pending = TRUE;  //We've got tickets waiting to go out
//               skip_dios = TRUE;
//            }
//            else
//            {
//            	DebugPrint(STRACE_DBG,("g_srv_tkts_count >= 5"))
//        			if(g_CClastextradata[0] != '\0')
//               {
//		    			sprintf(g_transctx.txBuf,"[%s].%s.%s@%s.%d.%d",
//			    		CCGetMessageHdr(SN_DID_DIOS),		// (%s)		DIOS Header
//			    		g_ccMACAddress,						// (%s)		MAC address
//			    		g_CClastbcodescan,					// (%s)		Last Play Card
//                	g_CClastextradata,               // (%s)		Last Extra Data
//                	tInt1,									// (%d)		DIO ID
//			    		g_srv_tkts_count						// (%d)		DIO Count
//			    		);
//        			}
//        			else
//        			{
//          			sprintf(g_transctx.txBuf,"[%s].%s.%s.%d.%d",
//			   		CCGetMessageHdr(SN_DID_DIOS),		// (%s)		DIOS Header
//			   		g_ccMACAddress,						// (%s)		MAC address
//			   		g_CClastbcodescan,					// (%s)		Last Play Card
//            		tInt1,									// (%d)		DIO ID
//			   		g_srv_tkts_count						// (%d)		DIO Count
//			    		);
//               }
//               g_srv_tkts_count = 0;	//reset server ticket counter
//               g_srv_tkts_pending = FALSE;	//No tickets currently pending
//               skip_dios = FALSE;
//            }

            //Send ticket count to LCD
/*          	memset(outBuffer,0,sizeof(outBuffer));
		    	strncpy(outBuffer,"TKTS|",5);

          	str_xtractstr(G_STOR_ACCT_MSG.cardid,&outBuffer[5],SII_TRANSCARD_MAXLENGTH);
   		 	length = strlen( &outBuffer[0] );
   		 	outBuffer[length++] = '|';
          	itoa(g_lcd_tkts_count,&outBuffer[length]);  //Ticket count converted to ascii string
          	length = strlen( &outBuffer[0] ); //Find new end
          	outBuffer[length++] = '\r';
          	outBuffer[length++] = '\n';

   		 	//Send to the LCD display
          	CommandToLCDScreen(outBuffer,length);
          	DebugPrint(STRACE_DBG,("Sent LCD: TKTS||%d",g_lcd_tkts_count)); */

            CC_SendLCDTkts(g_lcd_tkts_count);
         }
			else
         {
         	DebugPrint(STRACE_DBG,("tInt1 = %d", tInt1))
        		if(g_CClastextradata[0] != '\0')
            {
		    		sprintf(g_transctx.txBuf,"[%s].%s.%s@%s.%d.%d",
			   	CCGetMessageHdr(SN_DID_DIOS),		// (%s)		DIOS Header
			   	g_ccMACAddress,						// (%s)		MAC address
			   	g_CClastbcodescan,					// (%s)		Last Play Card
            	g_CClastextradata,               // (%s)		Last Extra Data
            	tInt1,									// (%d)		DIO ID
			   	tInt2										// (%d)		DIO Count
			   	);
        		}
        		else
            {
	          	sprintf(g_transctx.txBuf,"[%s].%s.%s.%d.%d",
			    	CCGetMessageHdr(SN_DID_DIOS),		// (%s)		DIOS Header
			    	g_ccMACAddress,						// (%s)		MAC address
			    	g_CClastbcodescan,					// (%s)		Last Play Card
               tInt1,									// (%d)		DIO ID
			    	tInt2										// (%d)		DIO Count
			    	);
            }

          	//Send ticket count to LCD
/*   	      memset(outBuffer,0,sizeof(outBuffer));
			   strncpy(outBuffer,"TKTS|",5);

      	   str_xtractstr(G_STOR_ACCT_MSG.cardid,&outBuffer[5],SII_TRANSCARD_MAXLENGTH);
   			length = strlen( &outBuffer[0] );
   	 		outBuffer[length++] = '|';
      	 	itoa(tInt2,&outBuffer[length]);  //Ticket count converted to ascii string
            length = strlen( &outBuffer[0] ); //Find new end
            outBuffer[length++] = '\r';
            outBuffer[length++] = '\n';

   		 	//Send to the LCD display
          	CommandToLCDScreen(outBuffer,length);
          	DebugPrint(STRACE_DBG,("Sent Ticket MSG to LCD")); */

            CC_SendLCDTkts(tInt2);
         }
		break;
	case SN_DID_HAPY:
		va_start(marker,msg);
		tInt1 = va_arg(marker, int);
		tInt2 = va_arg(marker, int);
		Util_tmstr(tStr,-1);
		sprintf(g_transctx.txBuf,"[%s].%s.%s.%d.%u.%d",
			CCGetMessageHdr(SN_DID_HAPY),		// (%s)		HAPY Header
			g_ccMACAddress,						// (%s)		MAC address
			tStr,								// (%s)		Current time
			CClient_GetSSRVState(),				// (%d)		State Machine Code
			Push_GetCount(),					// (%u)		Number of transactions in push buffer
			G_STOR_GLOB_MSG.skoobe_pricestate	// (%d)		Price Mode
			);
		break;
	case SN_DID_SCAN:
		va_start(marker,msg);
		tCharPtr1 = va_arg(marker, char*);
		tCharPtr2 = va_arg(marker, char*);
		Util_tmstr(tStr,-1);
        if(tCharPtr2[0] != '\0') {
		    sprintf(g_transctx.txBuf,"[%s].%s.%s@%s",
			    CCGetMessageHdr(SN_DID_SCAN),		// (%s)		SCAN Header
			    g_ccMACAddress,						// (%s)		MAC address
			    tCharPtr1,							// (%s)		Card Scan
                tCharPtr2							// (%s)		Extra Data
			    );
        } else {
		    sprintf(g_transctx.txBuf,"[%s].%s.%s",
			    CCGetMessageHdr(SN_DID_SCAN),		// (%s)		SCAN Header
			    g_ccMACAddress,						// (%s)		MAC address
			    tCharPtr1							// (%s)		Card Scan
			    );
        }
		break;
	case SN_DID_EOTX:
		sprintf(g_transctx.txBuf,"[%s].",
			CCGetMessageHdr(SN_DID_EOTX)		// (%s)		EOTX Header
			);
		break;
	case SN_DID_MSSG:
		va_start(marker,msg);
		tInt1 = va_arg(marker, int);
		tCharPtr1 = va_arg(marker, char*);
		tCharPtr2 = va_arg(marker, char*);
		sprintf(g_transctx.txBuf,"[%s].%s.%u.%s[%s].%s",
			CCGetMessageHdr(SN_DID_MSSG),		// (%s)		EOTX Header
			g_ccMACAddress,						// (%s)		MAC address
			0,									// (%u)		MSSG type.
			tCharPtr1,							// (%s)		push buffer record info
			CCGetMessageHdr(tInt1),				// (%s)		push buffer record DID
			tCharPtr2							// (%s)		push buffer record data
			);
		break;
	}
   if (skip_dios)
   {
   	skip_dios = FALSE;
		return TRUE;
   }
   else
   {
		if (g_transctx.tstate == TRANS_STATE_CONNECTED)
      {
			DebugPrint(STRACE_DBG,("Send: %s",g_transctx.txBuf));   //Sansom
			sock_write(&g_transctx.sock,g_transctx.txBuf,strlen(g_transctx.txBuf));
	  		CClient_Handler();
			// Sent traffic. Reset the HAPY timer.
			g_HAPYtimer_s = SEC_TIMER+g_HAPYfreq_s;
			if(g_transctx.push)
         {
 				EventQ_Push(EVT_PUSHMODEEXIT,0,0);
				g_transctx.push = FALSE;
			}
			// If this was an EOTX, we should terminate the connection.
			if (SN_DID_EOTX == msg)
      	{
				tcp_tick(&g_transctx.sock);
				tcp_tick(&g_transctx.sock);
				sock_close(&g_transctx.sock);
				tcp_tick(&g_transctx.sock);
				// Socket close timeout TODO: Is 5 seconds correct?
				g_transctx.timer = MS_TIMER+5000;
				g_transctx.tstate = TRANS_STATE_CLOSING;
				EventQ_Push(EVT_TRANSACTIONCOMPLETESUCCESS,0,0);
				CClient_Handler();
				SafeReboot_PutSemaphore();
			}
			return TRUE;
	 	}
   	else
   	{
			switch(msg)
      		{
				case SN_DID_EOTX:	// EOTX should not be pushed nor should it trigger push mode.
				// Even if we can't actually send the EOTX, it's still considered a success.
				EventQ_Push(EVT_TRANSACTIONCOMPLETESUCCESS,0,0);
				SafeReboot_PutSemaphore();
				return TRUE;
			}

			if(!g_transctx.push)
      		{
			EventQ_Push(EVT_PUSHMODEENTER,0,0);
			g_transctx.push = TRUE;
			}
			// Even if event can be pushed, the transaction is considered a failure if it
			// wasn't sent to the server.
			EventQ_Push(EVT_TRANSACTIONCOMPLETEFAIL,0,0);
        	/* Only push the event if push mode is allowed */
        	if (G_STOR_CNFG_MSG.pushenabled)
        	{
            	if (Push_Store(msg,g_transctx.txBuf,CClient_GetSSRVState()))
            	{
                // Transaction is complete, so we can reboot now.
                SafeReboot_PutSemaphore();
                return TRUE;
            	}
        	}
			// Transaction wasn't pushable, but is complete, so we can reboot now.
			SafeReboot_PutSemaphore();
		}
	return FALSE;
	}
}

/*** BeginHeader CClient_IsPush */
/*	--------------------------------------------------------------
	Function: CClient_IsPush
	Params: void
	Returns:
		BOOL - TRUE if client is in push mode (no connection),
			FALSE if not in push mode.
	--------------------------------------------------------------
	Purpose: Determines if client is in push mode or has good
		communications with server.
	------------------------------------------------------------ */
BOOL CClient_IsPush(void);
/*** EndHeader */
BOOL CClient_IsPush(void)
{
	return g_transctx.push;
}


/*** BeginHeader CClient_TransactionComplete */
/*	--------------------------------------------------------------
	Function: CClient_TransactionComplete
	Params:
		void
	Returns:
		BOOL - TRUE if transaction is finished. FALSE if
			transaction is still in progress.
	--------------------------------------------------------------
	Purpose:
		Determines if a transaction has completed (though it may
		have failed).
	------------------------------------------------------------ */
BOOL CClient_TransactionComplete(void);
/*** EndHeader */
BOOL CClient_TransactionComplete(void)
{
	if ((g_transctx.tstate == TRANS_STATE_IDLE) || (g_transctx.tstate == TRANS_STATE_LISTEN) || (g_transctx.tstate == TRANS_STATE_CLOSING)) {
		return TRUE;
	}
	return FALSE;
}

/*** BeginHeader str_xtracttol */
/*	--------------------------------------------------------------
	Function: str_xtracttol
	Params:
		c - buffer containing string to convert
		n - number of characters to convert as a long.
			This can't be greater than 11. (-2147483648)
	Returns:
		long - representation of n characters from source string
			as a long.
	--------------------------------------------------------------
	Purpose:
		Converts 'n' characters from a string to a long.
	------------------------------------------------------------ */
long str_xtracttol(char *c, int n);
/*** EndHeader */
long str_xtracttol(char *c, int n)
{
	char cXfr[12];
	if (n > 11) n = 11;
	memset(cXfr,0x00,sizeof(cXfr));
	memcpy(cXfr,c,n);
	return(atol(cXfr));
}

/*** BeginHeader str_xtractstr */
/*	--------------------------------------------------------------
	Function: str_xtractstr
	Params:
		dest - buffer to extract string to.
		src - buffer containing string to extract
		n - maximum number of characters to extract
	Returns:
		char* - Pointer to the terminator that halted the
				conversion or NULL if there are too many
				characters before a terminator.
	--------------------------------------------------------------
	Purpose:
		Extracts up to n characters and adds a NULL terminator.
		If there are more than n characters before a terminator
		('.', or '\0') is reached, this function returns NULL.
		A NULL terminator is NOT appended to the destination string.
	------------------------------------------------------------ */
char* str_xtractstr(char *dest, char *src, int n);
/*** EndHeader */
char* str_xtractstr(char *dest, char *src, int n)
{
	while (n--) {
		if ((*src == '.' || *src == '\0')) {
			*dest = '\0';
			return src;
		}
		*dest = *src;
		src++;
		dest++;
	}
	return NULL;
}


/*** BeginHeader CC_DecodeSNBB */
/*	--------------------------------------------------------------
	Function: CC_DecodeSNBB
	Params:
		buffer - buffer containing SNBB packet to process
		snbb - SNBB structure to fill with processing results
	Returns:
		void
	--------------------------------------------------------------
	Purpose: decodes an SNBB packet buffer into a SNBB struct.
	------------------------------------------------------------ */
BOOL CC_DecodeSNBB(char* buffer, mssg_SNBB_t* snbb);
/*** EndHeader */
BOOL CC_DecodeSNBB(char* buffer, mssg_SNBB_t* snbb)
{
	struct tm ttm;
	long tmdiff, tmnow;

	memset(snbb,0x00,sizeof(*snbb));

	if (strlen(buffer) <= 10)
		return FALSE;
	if ( (buffer[0] != '[') || (buffer[5] != ']') || (buffer[6] != '.') )
		return FALSE;
	// Ensure the Datagram ID is SNBB
	if (CCGetMessageID(buffer+1) != SN_DID_SNBB)
		return FALSE;
	// Decode the Broadcast
	ttm.tm_year		= (char)(str_xtracttol(&buffer[7],4)-1900);
	ttm.tm_mon		= (char)(str_xtracttol(&buffer[11],2));
	ttm.tm_mday		= (char)(str_xtracttol(&buffer[13],2));
	ttm.tm_hour		= (char)(str_xtracttol(&buffer[15],2));
	ttm.tm_min		= (char)(str_xtracttol(&buffer[17],2));
	ttm.tm_sec		= (char)(str_xtracttol(&buffer[19],2));
	snbb->porttxn	= (word)(str_xtracttol(&buffer[21],5));
	snbb->portsnbb	= (word)(str_xtracttol(&buffer[26],5));
	snbb->portcmd	= (word)(str_xtracttol(&buffer[31],5));
	// The following IP strings will be NULL terminated because structure is zeroed above.
	memcpy(snbb->serveripaddr, &buffer[36], 15);
	memcpy(snbb->broadcastip, &buffer[51], 15);

	// Determine the Time Variance
	// Note: variance is the difference between the current SEC_TIMER value and the converted.
	tmnow = mktime(&ttm);
	// Store Variance in SNBB structure
	snbb->tmoffset = (SEC_TIMER-tmnow);
	// Reset the RTC. Note: the SEC_TIMER variable is not reset until the next reboot!
	tm_wr(&ttm);
	return TRUE;
}

/*** BeginHeader CC_ProcessMessage */
/*	--------------------------------------------------------------
	Function: CC_ProcessMessage
	Params:
		buffer - buffer to process as message.
	Returns:
	--------------------------------------------------------------
	Purpose:
	------------------------------------------------------------ */
void CC_ProcessMessage(char* buffer);
/*** EndHeader */
void CC_ProcessMessage(char* buffer)
{
	char* pktdata;
	// Check that the message has the "[XXXX]." header.
	if ( (buffer[0] != '[') || (buffer[5] != ']') || (buffer[6] != '.') )
		return;

	pktdata = buffer+7;	// Pointer to data past the header.

	switch (CCGetMessageID(buffer+1)) {
	case SN_DID_UNKNOWN:
		return;
	case SN_DID_ACCT:
		if (CC_isLocked()) return;
		CC_DoACCT(pktdata);
		break;
	case SN_DID_CMSG:
		CC_DoCMSG(pktdata);
		break;
	case SN_DID_CNFG:
		CC_DoCNFG(pktdata);
		break;
	case SN_DID_CRDS:
		CC_DoCRDS(pktdata);
		break;
	case SN_DID_DEVC:
		CC_DoDEVC(pktdata);
		break;
	case SN_DID_DIOP:
		CC_DoDIOP(pktdata);
		break;
	case SN_DID_DISP:
		CC_DoDISP(pktdata);
		break;
	case SN_DID_EOTX:
		break;
	case SN_DID_LEDS:
		CC_DoLEDS(pktdata);
		break;
	case SN_DID_LEDX:
		CC_DoLEDX(pktdata);
		break;
	case SN_DID_LOCK:
		if (pktdata[0] == '0') {
			EventQ_Push(EVT_UNLOCK,0,0);
		}
		else if (pktdata[0] == '1') {
			EventQ_Push(EVT_LOCK,0,0);
		}
		break;
	case SN_DID_MSSG:
		if (CC_isLocked()) return;
		CC_DoMSSG(pktdata);
		break;
	case SN_DID_PRCE:
		CC_DoPRCE(pktdata);
		break;
	case SN_DID_PUSH:
		CC_DoPUSH(pktdata);
		break;
	case SN_DID_REBX:
		CC_DoREBX(pktdata);
		break;
	case SN_DID_RELY:
// Obsoleted.
//		CC_DoRELY(pktdata);
		break;
	case SN_DID_RQST:
		if (CC_isLocked()) return;
		CC_DoRQST(pktdata);
		break;
	case SN_DID_TEST:
		if (CC_isLocked()) return;
		CC_DoTEST(pktdata);
		break;
   case SN_DID_COLO:
      CC_DoCOLO(pktdata);
   	break;
   case SN_DID_FLIP:
      CC_DoFLIP(pktdata);
      break;
   case SN_DID_RSET:
      CC_DoRSET(pktdata);
      break;
   case SN_DID_VSET:
      CC_DoVSET(pktdata);
      break;

	default:
		break;
	}
}

/*** BeginHeader CC_isLocked */
/*	--------------------------------------------------------------
	Function: CC_isLocked
	Params:
		void
	Returns:
		BOOL - TRUE if Skoobe is locked, FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Used to determine if Skoobe is locked.
	------------------------------------------------------------ */
BOOL CC_isLocked(void);
/*** EndHeader */
BOOL CC_isLocked(void)
{
	return g_ccLock;
}

/*** BeginHeader CC_Lock */
/*	--------------------------------------------------------------
	Function: CC_Lock
	Params:
		BOOL - TRUE if Skoobe should be locked, FALSE should be
			unlocked.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Used to lock or unlock Skoobe.
	------------------------------------------------------------ */
void CC_Lock(BOOL lock);
/*** EndHeader */
void CC_Lock(BOOL lock)
{
	g_ccLock = lock;
}

/*** BeginHeader CC_DoMSSG */
/*	--------------------------------------------------------------
	Function: CC_DoMSSG
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a MSSG packet.
	------------------------------------------------------------ */
void CC_DoMSSG(char* buffer);
/*** EndHeader */
void CC_DoMSSG(char* buffer)
{
	int msgid, newstate;
	// Extract Message ID
	msgid = (int)(str_xtracttol(&buffer[18],2));
	switch (msgid) {
	case 0: // Push buffer record.
		// TODO: Ignore this for now as we don't support loading of the push buffer.
		break;
	case 1: // Set state machine state
		buffer = strchr(&buffer[18],'.');
		if(buffer) {
	        buffer++;
			EventQ_Push(EVT_CHANGESTATE,(int)(str_xtracttol(buffer,1)),0);
		}
		break;
	case 2: // Set push buffer fill mode
		// TODO: Ignore this for now as we don't support loading of the push buffer.
		break;
	case 80: // Get push buffer offsets
		// TODO: Ignore this for now as we don't support it.
		break;
	case 98: // Clear the push buffer
		Push_Clear();
		break;
	case 99:
		// TODO: Ignore this for now as we don't support loading of the push buffer.
		break;
	}
}

/*** BeginHeader CC_DoCMSG */
/*	--------------------------------------------------------------
	Function: CC_DoCMSG
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a CMSG packet.
	------------------------------------------------------------ */
void CC_DoCMSG(char* buffer);
/*** EndHeader */
void CC_DoCMSG(char* buffer)
{
	int msgid;
	recd_MSSG_t t_msg;
	// Write Messages to file sequentially
	// Extract Message ID
	msgid = (int)(str_xtracttol(&buffer[0],5));
	// Card 0 indicates start of a new table
	if (0 == msgid) {
		// Reuse msgid as message count.
		msgid = (int)(str_xtracttol(&buffer[6],strlen(buffer)-6));
		DebugPrint(STRACE_DBG,("Got new message list count: %d",msgid));
		Messages_PrepareForNewList(msgid);
	} else {
		// Extract Info
		t_msg.mssgid	= msgid;
		t_msg.timedisp	= (int)(str_xtracttol(&buffer[5],4));
		t_msg.isscroll	= (buffer[9]=='S')?TRUE:FALSE;
		t_msg.isblink	= (buffer[10]=='B')?TRUE:FALSE;
		strncpy(t_msg.mssgtext,&buffer[11],CMSG_TEXT_LEN);
		t_msg.mssgtext[CMSG_TEXT_LEN-1] = '\0';
		//DebugPrint(STRACE_DBG,("Got new message: %s",t_msg.mssgtext));
		Messages_AddMessageToList(&t_msg);
	}
}

/*** BeginHeader CC_DoCNFG */
/*	--------------------------------------------------------------
	Function: CC_DoCNFG
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a CNFG packet.
	------------------------------------------------------------ */
void CC_DoCNFG(char* buffer);
/*** EndHeader */
void CC_DoCNFG(char* buffer)
{
	int tBuflen;
   char price4[13];
   char pricealt[13];   //VIP price

   //unsigned char lcdBuffer[255];
   //int length;
   //unsigned char temp, scratch[64];

   CClient_SendDisplayIdle();

	tBuflen = strlen(buffer);

   // Store any previously set alternative and scheduled price.
   memcpy(price4,G_STOR_CNFG_MSG.prices[4],13);
   memcpy(pricealt,G_STOR_CNFG_MSG.pricealt,13);

    // Zero out the current configuration & checksum
    memset(&G_STOR_CNFG_MSG,0x00,sizeof(G_STOR_CNFG_MSG));
	// Copy over the configuration
	G_STOR_CNFG_MSG.exptime.tm_year	= (char)(str_xtracttol(&buffer[0],4)-1900);
	G_STOR_CNFG_MSG.exptime.tm_mon	= (char)(str_xtracttol(&buffer[4],2));
	G_STOR_CNFG_MSG.exptime.tm_mday	= (char)(str_xtracttol(&buffer[6],2));
	G_STOR_CNFG_MSG.exptime.tm_hour	= (char)(str_xtracttol(&buffer[8],2));
	G_STOR_CNFG_MSG.exptime.tm_min	= (char)(str_xtracttol(&buffer[10],2));
	G_STOR_CNFG_MSG.exptime.tm_sec	= 0;
   memcpy(&G_STOR_CNFG_MSG.prices[0],&buffer[12],12);
   memcpy(&G_STOR_CNFG_MSG.prices[1],&buffer[24],12);
   memcpy(&G_STOR_CNFG_MSG.prices[2],&buffer[36],12);
   memcpy(&G_STOR_CNFG_MSG.prices[3],&buffer[48],12);
   // Restore any previously set alternative and scheduled price.
   memcpy(G_STOR_CNFG_MSG.prices[4],price4,13);
   memcpy(G_STOR_CNFG_MSG.pricealt,pricealt,13);
   G_STOR_CNFG_MSG.prices[0][12] = 0;
   G_STOR_CNFG_MSG.prices[1][12] = 0;
   G_STOR_CNFG_MSG.prices[2][12] = 0;
   G_STOR_CNFG_MSG.prices[3][12] = 0;
   G_STOR_CNFG_MSG.prices[4][12] = 0;
   G_STOR_CNFG_MSG.pricealt[12] = 0;

	G_STOR_CNFG_MSG.pushenabled		= buffer[60]=='1'?TRUE:FALSE;
	G_STOR_CNFG_MSG.nodispense		= buffer[61]=='1'?TRUE:FALSE;
	G_STOR_CNFG_MSG.cardtypes		= (char)(str_xtracttol(&buffer[62],1));
	G_STOR_CNFG_MSG.timeplay		= buffer[63]=='1'?TRUE:FALSE;
	G_STOR_CNFG_MSG.freeplay		= buffer[64]=='1'?TRUE:FALSE;
	G_STOR_CNFG_MSG.portcmd			= (word)(str_xtracttol(&buffer[65],5));
   memcpy(G_STOR_CNFG_MSG.denomination,&buffer[70],5);
   memcpy(G_STOR_CNFG_MSG.battdate,&buffer[75],8);
	G_STOR_CNFG_MSG.porthb			= (word)(str_xtracttol(&buffer[83],5));
	G_STOR_CNFG_MSG.hbfreq			= (char)(str_xtracttol(&buffer[88],2));
	G_STOR_CNFG_MSG.hbtype			= (char)(str_xtracttol(&buffer[90],1));
    // Trap older CNFG format
	if (tBuflen > 90) {
	    G_STOR_CNFG_MSG.pushwait = (word)(str_xtracttol(&buffer[91],5));
    }
    // Set a default Push Wait time of 7 seconds
    if (G_STOR_CNFG_MSG.pushwait<2 || G_STOR_CNFG_MSG.pushwait>200) {
		G_STOR_CNFG_MSG.pushwait = 70;
    }
	if (tBuflen > 95) {
		G_STOR_CNFG_MSG.sitepmpayout = (char)(str_xtracttol(&buffer[96],1));
		G_STOR_CNFG_MSG.gamepmpayout = (char)(str_xtracttol(&buffer[97],1));
    }
    // Added for version 4.04
	if (tBuflen > 97) {
		G_STOR_CNFG_MSG.pricemode = (char)(str_xtracttol(&buffer[98],1));
		memcpy(&G_STOR_CNFG_MSG.pricealt,&buffer[99],12);
        G_STOR_CNFG_MSG.pricealt[12] = 0;
		G_STOR_CNFG_MSG.cyclewait = (word)(str_xtracttol(&buffer[111],5));
    }

	// Update the checksum for the configuration record.
	G_STOR_CNFG.chksum =  Storage_Checksum(&G_STOR_CNFG_MSG, sizeof(G_STOR_CNFG_MSG));
   DebugPrint(STRACE_DBG,("Got CNFG packet")); //MSS
#if 0
   //Build message for config info to LCD screen
   memset(lcdBuffer,0,sizeof(lcdBuffer));
   strncpy(lcdBuffer,"CNFG|",5);
   length = 5;

   itoa(G_STOR_DEVC_MSG.devicetype,&lcdBuffer[length]);  //Device type converted to ascii string
   length = strlen( &lcdBuffer[0] ); //Find new end
   lcdBuffer[length++] = '|';

   itoa(CClient_GetSSRVState(),&lcdBuffer[length]);  //Play state converted to ascii string
   length = strlen( &lcdBuffer[0] ); //Find new end
   lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.prices[0]);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   //strcpy(&lcdBuffer[length],(char*)&G_STOR_CNFG_MSG.prices[0]); //Price
   //length = strlen( &lcdBuffer[0] ); //Find new end
   //lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.pricealt);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);   //Find new end

   //strcpy(&lcdBuffer[length],(char*)&G_STOR_CNFG_MSG.pricealt); //VIP price
   //length = strlen( &lcdBuffer[0] ); //Find new end


   lcdBuffer[length++] = '|';
   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.pricemode);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);

   lcdBuffer[length++] = '|';
   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.denomination);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);

   lcdBuffer[length++] = '\r';
   lcdBuffer[length++] = '\n';
   lcdBuffer[length++] = '\0';

   //Send to the LCD display
   CommandToLCDScreen(lcdBuffer,length);

#endif

}

/*** BeginHeader CC_DoACCT */
/*	--------------------------------------------------------------
	Function: CC_DoACCT
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches an ACCT packet.
	------------------------------------------------------------ */
void CC_DoACCT(char* buffer);
/*** EndHeader */
void CC_DoACCT(char* buffer)
{
	char* tbufptr;
	char* tbufptr2;
   unsigned char temp;

   //Print the ACCT message we just received

   //DebugPrint(STRACE_DBG,("Received from SSRV: [ACCT]%s",buffer));

	// Zero out the current message
	memset(&G_STOR_ACCT_MSG,0x00,sizeof(G_STOR_ACCT_MSG));

   // Copy the Card ID
	tbufptr = str_xtractstr(G_STOR_ACCT_MSG.cardid,buffer,SII_TRANSCARD_MAXLENGTH);
	if (!tbufptr)
		return;
   tbufptr++;

	G_STOR_ACCT_MSG.reasoncode = (char)(str_xtracttol(tbufptr,2));
	tbufptr += 2;

   //Check what color to flash the LEDs
   if(G_STOR_ACCT_MSG.reasoncode == 0)
   {
      //Got a successful barcode read, flash LEDs green
      SetLightStringColor(3,0,255,0);
   }
   else
   {
      //Got a successful barcode read, flash LEDs red
      SetLightStringColor(4,255,0,0);
   }

	// See if there is Reason Data & get it.
	if (*tbufptr != '.')
   {
		tbufptr = str_xtractstr(G_STOR_ACCT_MSG.reasondata,buffer,17);
		if (!tbufptr)
			return;
	}

	tbufptr++;
	memcpy(&G_STOR_ACCT_MSG.balplay,tbufptr,12);
	memcpy(&G_STOR_ACCT_MSG.balcash,tbufptr+12,12);
	memcpy(&G_STOR_ACCT_MSG.balcomp,tbufptr+24,12);
	memcpy(&G_STOR_ACCT_MSG.balbons,tbufptr+36,12);
	memcpy(&G_STOR_ACCT_MSG.baltick,tbufptr+48,12);
	// Offset the pointer
	tbufptr += 60;
	// Get remaining stuff
	G_STOR_ACCT_MSG.nodispense = *tbufptr=='1'?TRUE:FALSE;
	tbufptr++;
	G_STOR_ACCT_MSG.approvetype = (char)(str_xtracttol(tbufptr,2));
	tbufptr += 2;
	memcpy(&G_STOR_ACCT_MSG.baltotl,tbufptr,12);
	memcpy(&G_STOR_ACCT_MSG.baltime,tbufptr+12,12);
	memcpy(&G_STOR_ACCT_MSG.balentt,tbufptr+24,5);
	// And finally...
	G_STOR_ACCT_MSG.acctvalid = TRUE;
	// Send the event
	EventQ_Push(EVT_GOTVALIDACCT,0,0);

   CC_GenerateLcdAcctMessage();

}

/*** BeginHeader CC_DoCRDS */
/*	--------------------------------------------------------------
	Function: CC_DoCRDS
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a CRDS packet.
	------------------------------------------------------------ */
void CC_DoCRDS(char* buffer);
/*** EndHeader */
void CC_DoCRDS(char* buffer)
{
	recd_CARD_t t_crd;

	// Extract Card ID
	t_crd.cardid = (int)(str_xtracttol(&buffer[0],5));
	// Card ID 0 indicates start of a new table
	if (0 == t_crd.cardid) {
		// Reuse t_crd.cardid as card count.
		t_crd.cardid = (int)(str_xtracttol(&buffer[6],strlen(buffer)-6));
		DebugPrint(STRACE_DBG,("Got new card list count: %d",t_crd.cardid));
		Cards_PrepareForNewList(t_crd.cardid);
	} else {
		// Extract Info
		t_crd.isbrdcast = (buffer[5]=='1')?TRUE:FALSE;
		t_crd.isdiscount = (buffer[6]=='1')?TRUE:FALSE;
		memcpy(&t_crd.cardnumber,&buffer[7],18);
		//DebugPrint(STRACE_DBG,("Got new card."));
		Cards_AddCardToList(&t_crd);
	}
}

/*** BeginHeader CC_DoDEVC */
/*	--------------------------------------------------------------
	Function: CC_DoDEVC
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a DEVC packet.
	------------------------------------------------------------ */
void CC_DoDEVC(char* buffer);
/*** EndHeader */
void CC_DoDEVC(char* buffer)
{
	// Validate the Size
	// FUP - Invalid size should return an error to the server
	// Zero out the current configuration
	memset(&G_STOR_DEVC_MSG,0x00,sizeof(G_STOR_DEVC_MSG));
	// Copy over the configuration
	memcpy(&G_STOR_DEVC_MSG.siteid,&buffer[0],5);
	G_STOR_DEVC_MSG.devicetype =  (char)(str_xtracttol(&buffer[5],2));
	memcpy(&G_STOR_DEVC_MSG.devicename,&buffer[7],60);
	G_STOR_DEVC_MSG.saenabled =   buffer[67]=='1'?TRUE:FALSE;
	G_STOR_DEVC_MSG.sw1 =         buffer[68];
	G_STOR_DEVC_MSG.sw2 =         buffer[69];
	G_STOR_DEVC_MSG.sw3 =         buffer[70];
	G_STOR_DEVC_MSG.sw4 =         buffer[71];
	G_STOR_DEVC_MSG.scanner1 =    buffer[72];
	G_STOR_DEVC_MSG.scanner2 =    buffer[73];
	G_STOR_DEVC_MSG.displaytype = (char)(str_xtracttol(&buffer[74],1));
	G_STOR_DEVC_MSG.gameacttype = (char)(str_xtracttol(&buffer[75],1));
	G_STOR_DEVC_MSG.MustHaveNetwork = buffer[76]=='1'?TRUE:FALSE;
	G_STOR_DEVC_MSG.unused2 =     (char)(str_xtracttol(&buffer[77],1));
	G_STOR_DEVC_MSG.unused3 =     (char)(str_xtracttol(&buffer[78],1));
	G_STOR_DEVC_MSG.dispenstype = (char)(str_xtracttol(&buffer[79],2));
	G_STOR_DEVC_MSG.ticketcap =   str_xtracttol(&buffer[81],8);
	G_STOR_DEVC_MSG.ticketlow =   str_xtracttol(&buffer[89],6);
	// Update the checksum for the record.
	G_STOR_DEVC.chksum =  Storage_Checksum(&G_STOR_DEVC_MSG, sizeof(G_STOR_DEVC_MSG));
}

/*** BeginHeader CC_DoDIOP */
/*	--------------------------------------------------------------
	Function: CC_DoDIOP
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a DIOP packet.
	------------------------------------------------------------ */
void CC_DoDIOP(char* buffer);
/*** EndHeader */
void CC_DoDIOP(char* buffer)
{
	int diopnum, tdiopnum;
	diopnum = (int)str_xtracttol(&buffer[0],3);
	tdiopnum = IOSys_DIOPNum2Phys(diopnum);

	if(tdiopnum >= 0) {
		G_STOR_DIOP_MSG[tdiopnum].dioid = diopnum;
		G_STOR_DIOP_MSG[tdiopnum].xormask = buffer[3]=='0'?0xFF:0x00;
		G_STOR_DIOP_MSG[tdiopnum].cycles = str_xtracttol(&buffer[5],5);
		G_STOR_DIOP_MSG[tdiopnum].timeon = str_xtracttol(&buffer[10],5);
		G_STOR_DIOP_MSG[tdiopnum].timeoff = str_xtracttol(&buffer[15],5);
		G_STOR_DIOP_MSG[tdiopnum].eosdetect = str_xtracttol(&buffer[20],5);

		G_STOR_DIOP.chksum =  Storage_Checksum(&G_STOR_DIOP_MSG, sizeof(G_STOR_DIOP_MSG));
	}
}


/*** BeginHeader CC_DoDISP */
/*	--------------------------------------------------------------
	Function: CC_DoDISP
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a DISP packet.
	------------------------------------------------------------ */
void CC_DoDISP(char* buffer);
/*** EndHeader */
void CC_DoDISP(char* buffer)
{
	/*
	wfd LCDClear();
	wfd LCDWrite(&tRcvBuf[7]);
	*/
	UI_DoManualMsg(buffer,5,FALSE,FALSE);
}

/*** BeginHeader CC_DoLEDS */
/*	--------------------------------------------------------------
	Function: CC_DoLEDS
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a LEDS packet.
	------------------------------------------------------------ */
void CC_DoLEDS(char* buffer);
/*** EndHeader */
void CC_DoLEDS(char* buffer)
{
	/*
	gnLEDNumber = (int)(str_xtracttol(&tRcvBuf[7],1));
	gnLEDState = (int)(str_xtracttol(&tRcvBuf[9],1));
	gnLEDTimeout = (unsigned long)(str_xtracttol(&tRcvBuf[11],5));
	*/
}

/*** BeginHeader CC_DoLEDX */
/*	--------------------------------------------------------------
	Function: CC_DoLEDX
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a LEDX packet.
	------------------------------------------------------------ */
void CC_DoLEDX(char* buffer);
/*** EndHeader */
void CC_DoLEDX(char* buffer)
{
}

/*** BeginHeader CC_DoPRCE */
/*	--------------------------------------------------------------
	Function: CC_DoPRCE
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a PRCE packet.
	------------------------------------------------------------ */
void CC_DoPRCE(char* buffer);
/*** EndHeader */
void CC_DoPRCE(char* buffer)
{
	int tpricetype;
	int tactivatemode;
	// Extract Price Type (0=Std, 1-3=Disc, 4=Sched, 5=Alt)
	// Note: Standard, Discount, and Schedule types map to their respective
	// prices in the CNFG struct, which allows the 'price type' to act as
	// an index. If the order changes this will not work!
	tpricetype = (int)(str_xtracttol(&buffer[0],1));
	// Extract Activate Mode (0=Set Only, 1=Set+Activate, 2=Activate Only)
	tactivatemode = (int)(str_xtracttol(&buffer[1],1));
	// Validate Price Type and Activate Mode
	if ( (tpricetype < 0) || (tpricetype > 5) || (tactivatemode < 0) || (tactivatemode > 2) )
		return;
	// Set New Price
	if (tactivatemode != 2) {
        if (tpricetype==5) {
			memcpy(&G_STOR_CNFG_MSG.pricealt,&buffer[2],12);
            G_STOR_CNFG_MSG.pricealt[12] = 0;
        } else {
	        memcpy(&G_STOR_CNFG_MSG.prices[tpricetype],&buffer[2],12);
            G_STOR_CNFG_MSG.prices[tpricetype][12] = 0;
        }
		// We changed CNFG, so update the CNFG checksum.
		G_STOR_CNFG.chksum =  Storage_Checksum(&G_STOR_CNFG_MSG, sizeof(G_STOR_CNFG_MSG));
	}
	// Change State to trigger a display update
	if (tactivatemode != 0) {
		// Set Price selection only if not Alt price
		if (tpricetype < 5) {
			Storage_SetPriceState(tpricetype);
		}
		EventQ_Push(EVT_PRICECHANGED,0,0);
	}
}

/*** BeginHeader CC_DoPUSH */
/*	--------------------------------------------------------------
	Function: CC_DoPUSH
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a PUSH packet.
	------------------------------------------------------------ */
void CC_DoPUSH(char* buffer);
/*** EndHeader */
void CC_DoPUSH(char* buffer)
{
	char tmode;
	int thours;
	int tminutes;

	tmode = (char)(str_xtracttol(&buffer[0],1));

	switch (tmode) {
	case 0:	// Exit Push Mode
		//TODO: Exit Push mode.
		break;
	case 1:	// Enter Push Mode
		thours = (char)(str_xtracttol(&buffer[1],2));
		tminutes = (char)(str_xtracttol(&buffer[3],2));
		//TODO: Enter Push mode.
		break;
	case 2:	// Send Push Buffer
		EventQ_Push(EVT_PUSHXFERENTER,0,0);
		break;
	case 3:	// Ack last push buffer record sent.
		EventQ_Push(EVT_PUSHACKXFER,0,0);
		break;
	case 4:	// Send push buffer count.
		break;
	}
}

/*** BeginHeader CC_DoREBX */
/*	--------------------------------------------------------------
	Function: CC_DoREBX
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a REBX packet.
	------------------------------------------------------------ */
void CC_DoREBX(char* buffer);
/*** EndHeader */
void CC_DoREBX(char* buffer)
{
	int i;
	i = (int)(str_xtracttol(&buffer[0],1));
	if (i==1) {
		DebugPrint(STRACE_DBG,("Hard Reboot!"));
		forceSoftReset();
	} else {
		DebugPrint(STRACE_DBG,("Starting Soft Reboot!"));
		SafeReboot_SoftReboot();
		DebugPrint(STRACE_DBG,("Soft Reboot Scheduled!"));
	}
}

/*** BeginHeader CC_DoRQST */
/*	--------------------------------------------------------------
	Function: CC_DoRQST
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a RQST packet.
	------------------------------------------------------------ */
void CC_DoRQST(char* buffer);
/*** EndHeader */
void CC_DoRQST(char* buffer)
{
	/*
	// WIP - Work in Progress
	// Get Type Code
	cpPtr++;
	cpEnd = strchr(cpPtr,'.');
	cpEnd++;
	lnI = (int)(str_xtracttol(cpEnd,2));
	gnDIDFlavor = lnI;
	gnStateTransition = STATE_RUN_REQUEST;
	gnStateCancel = TRUE;
	*/
}

/*** BeginHeader CC_DoTEST */
/*	--------------------------------------------------------------
	Function: CC_DoTEST
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes and dispatches a TEST packet.
	------------------------------------------------------------ */
void CC_DoTEST(char* buffer);
/*** EndHeader */
void CC_DoTEST(char* buffer)
{
	/*
	// WIP - Work in Progress
	lnI = (int)(str_xtracttol(&tRcvBuf[7],1));
	//gnStateTransition = STATE_CLEAR;
	if (lnI) {
		// Ignore if we're already in Test
		if (gnLoopPhase != PHASE_LOOP_TEST) {
			gnStateRun = STATE_CLEAR;
			gnLoopPhase = PHASE_LOOP_TEST;
		}
	} else {
		gnLEDTimeout = 0;
		waitfor(DelaySec(3));
		forceSoftReset();
	}
	*/
}

/*** BeginHeader CC_DoCOLO */
/*	--------------------------------------------------------------
	Function: CC_DoCOLO
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes a COLO packet, sets LED light string.
	------------------------------------------------------------ */
void CC_DoCOLO(char* buffer);
/*** EndHeader */
void CC_DoCOLO(char* buffer)
{
   long red1, green1, blue1, red2, green2, blue2;

   CClient_SendDisplayIdle();

	// Extract color elements for sequence1 & sequence2
   G_STOR_COLO_MSG.red1 = (int)str_xtracttol(&buffer[1],3); //Have to step around '.' delimiters
   G_STOR_COLO_MSG.green1 = (int)str_xtracttol(&buffer[5],3);
   G_STOR_COLO_MSG.blue1 = (int)str_xtracttol(&buffer[9],3);

   G_STOR_COLO_MSG.red2 = (int)str_xtracttol(&buffer[13],3);
   G_STOR_COLO_MSG.green2 = (int)str_xtracttol(&buffer[17],3);
   G_STOR_COLO_MSG.blue2 = (int)str_xtracttol(&buffer[21],3);

   G_STOR_COLO.chksum =  Storage_Checksum(&G_STOR_COLO_MSG, sizeof(G_STOR_COLO_MSG));

	//Send the sequences to the light string handler
   SetLightStringColor(1, (unsigned char)G_STOR_COLO_MSG.red1, (unsigned char)G_STOR_COLO_MSG.green1, (unsigned char)G_STOR_COLO_MSG.blue1);
   SetLightStringColor(2, (unsigned char)G_STOR_COLO_MSG.red2, (unsigned char)G_STOR_COLO_MSG.green2, (unsigned char)G_STOR_COLO_MSG.blue2);

}

/*** BeginHeader CC_DoFLIP */
/*	--------------------------------------------------------------
	Function: CC_DoFLIP
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes a FLIP packet, Flip the video display.
	------------------------------------------------------------ */
void CC_DoFLIP(char* buffer);
/*** EndHeader */
void CC_DoFLIP(char* buffer)
{
   unsigned char outBuffer[10];

   memset(outBuffer,0,sizeof(outBuffer));

   strncpy(outBuffer,"FLIP|",5);
   outBuffer[5] = *buffer;  //Will be an ascii value either 1 or 2

   outBuffer[6] = '\r';
   outBuffer[7] = '\n';

   //Send to the LCD display
   CommandToLCDScreen(outBuffer,8);

}

/*** BeginHeader CC_DoRSET */
/*	--------------------------------------------------------------
	Function: CC_DoRSET
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes a RSET packet, resets the video display.
	------------------------------------------------------------ */
void CC_DoRSET(char* buffer);
/*** EndHeader */
void CC_DoRSET(char* buffer)
{
   ResetLCDScreen();
}

/*** BeginHeader CC_DoVSET */
/*	--------------------------------------------------------------
	Function: CC_DoVSET
	Params:
		buffer - packet buffer.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decodes a VSET packet, adjusts the sound volume.
	------------------------------------------------------------ */
void CC_DoVSET(char* buffer);
/*** EndHeader */
void CC_DoVSET(char* buffer)
{
   unsigned char outBuffer[10];
   int length;

   CClient_SendDisplayIdle();

   memset(outBuffer,0,sizeof(outBuffer));

   strncpy(outBuffer,"VSET|",5);
   strcpy(&outBuffer[5], buffer);  //Will be an ascii value between 8 and 128

   length = strlen( &outBuffer[0] ); //Find new length
   outBuffer[length++] = '\r';
   outBuffer[length++] = '\n';

   //Send to the LCD display
   CommandToLCDScreen(outBuffer,length);

}

/*** BeginHeader CClient_IsValidCard */
/*	--------------------------------------------------------------
	Function: CClient_IsValidCard
	Params:
		cardID - Card to validate.
	Returns:
		BOOL - TRUE if card is valid, FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Determines if a Card ID is valid. This is used if the
		controller is operating in Push Mode.
	Note:
		ARI had cards that were in two lengths: 18 and 24 digits
	------------------------------------------------------------ */
int CClient_IsValidCard(char *cardID);
/*** EndHeader */
int CClient_IsValidCard(char *cardID)
{
	int IDLen;

	// Check the card length
	IDLen = strlen(cardID);
	switch (IDLen) {
	case 18: // Length suggests one of ours
		// If the Site ID matches, or other card type is allowed, then validate
		if (memcmp(cardID,G_STOR_DEVC_MSG.siteid,5)==0 || G_STOR_CNFG_MSG.cardtypes == 1 || G_STOR_CNFG_MSG.cardtypes == 2)
			return TRUE;
		break;
	case 24: // Probably an ARI card
		if (G_STOR_CNFG_MSG.cardtypes == 1 || G_STOR_CNFG_MSG.cardtypes == 2)
			return TRUE;
		break;
	default: // Some other type?
		if (G_STOR_CNFG_MSG.cardtypes == 2)
			return TRUE;
		break;
	}
	return FALSE;
}

/*** BeginHeader CClient_SetLastScan */
/*	--------------------------------------------------------------
	Function: CClient_SetLastScan
	Params:
		cardID - null-terminated Barcode string.
		extradata - null-terminated extra data.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets the last barcode scan. The Cashless Client library
		holds this instead of the main loop, since it is needed
		for several messages such as DIOS that are sent by
		libraries that don't have access to the scan data. A
		get function is provided for when the main loop needs
		the scan data again.
	------------------------------------------------------------ */
void CClient_SetLastScan(char *cardID, char *extradata);
/*** EndHeader */
void CClient_SetLastScan(char *cardID, char *extradata)
{
	strncpy(g_CClastbcodescan,cardID,sizeof(g_CClastbcodescan));
	g_CClastbcodescan[sizeof(g_CClastbcodescan)-1] = '\0';

	strncpy(g_CClastextradata,extradata,sizeof(g_CClastextradata));
	g_CClastextradata[sizeof(g_CClastextradata)-1] = '\0';
}

/*** BeginHeader CClient_GetLastScan */
/*	--------------------------------------------------------------
	Function: CClient_GetLastScan
	Params:
		void
	Returns:
		char* - pointer to string representing last scanned card.
	--------------------------------------------------------------
	Purpose:
		Gets the last barcode scan.
	------------------------------------------------------------ */
char* CClient_GetLastScan(void);
/*** EndHeader */
char* CClient_GetLastScan(void)
{
	return g_CClastbcodescan;
}

/*** BeginHeader CClient_SetSSRVState */
/*	--------------------------------------------------------------
	Function: CClient_SetSSRVState
	Params:
		sstate - run state to report to SSRV when needed.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets run state to report to SSRV when needed.
	------------------------------------------------------------ */
void CClient_SetSSRVState(SSRV_STATE sstate);
/*** EndHeader */
void CClient_SetSSRVState(SSRV_STATE sstate)
{
	g_CCLient_SSRVState = sstate;
}

/*** BeginHeader CClient_GetSSRVState */
/*	--------------------------------------------------------------
	Function: CClient_GetSSRVState
	Params:
		sstate - run state to report to SSRV when needed.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets run state to report to SSRV when needed.
	------------------------------------------------------------ */
int CClient_GetSSRVState(void);
/*** EndHeader */
int CClient_GetSSRVState(void)
{
	return (int)g_CCLient_SSRVState;
}

/*** BeginHeader CClient_SendDisplayIdle */
/*	--------------------------------------------------------------
	Function: CClient_SendDisplayIdle
	Params:
	   void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Puts the LCD display into IDLE mode it not already done so
	------------------------------------------------------------ */
void CClient_SendDisplayIdle(void);
/*** EndHeader */
void CClient_SendDisplayIdle(void)
{
	if(g_sentDisplayIdle == 0)
   {
   	g_sentDisplayIdle = 1;
      CommandToLCDScreen("IDLE|\r\n\0", 8);	//Put LCD into "ticket man mode"
      Util_Delay(200);
      //CC_GenerateLcdCnfgMessage(1);
   }
}

/*** BeginHeader CC_GenerateLcdCnfgMessage */
/*	--------------------------------------------------------------
	Function: CC_GenerateLcdCnfgMessage
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Generates an LCD CNFG packet and sends it to the display.
	------------------------------------------------------------ */
void CC_GenerateLcdCnfgMessage(int mode);
/*** EndHeader */
void CC_GenerateLcdCnfgMessage(int mode)
{
   unsigned char lcdBuffer[255];
   int length;
   unsigned char temp, scratch[64];

   if (mode == 1){
   //Build message for config info to LCD screen
   memset(lcdBuffer,0,sizeof(lcdBuffer));
   strncpy(lcdBuffer,"CNFG|",5);
   length = 5;

   itoa(G_STOR_DEVC_MSG.devicetype,&lcdBuffer[length]);  //Device type converted to ascii string
   length = strlen( &lcdBuffer[0] ); //Find new end
   lcdBuffer[length++] = '|';

   		itoa(CClient_GetSSRVState(),&lcdBuffer[length]);  //Play state converted to ascii string
   		length = strlen( &lcdBuffer[0] ); //Find new end
   		lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.prices[0]);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   //strcpy(&lcdBuffer[length],(char*)&G_STOR_CNFG_MSG.prices[0]); //Price
   //length = strlen( &lcdBuffer[0] ); //Find new end
   //lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.pricealt);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);   //Find new end

   //strcpy(&lcdBuffer[length],(char*)&G_STOR_CNFG_MSG.pricealt); //VIP price
   //length = strlen( &lcdBuffer[0] ); //Find new end


   lcdBuffer[length++] = '|';
   memset(scratch,0,sizeof(scratch));

   lcdBuffer[length++] = G_STOR_CNFG_MSG.pricemode + 0x30; //Convert to ascii

   lcdBuffer[length++] = '|';
   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_CNFG_MSG.denomination);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);

   lcdBuffer[length++] = '\r';
   lcdBuffer[length++] = '\n';

   //Send to the LCD display
   CommandToLCDScreen(lcdBuffer,length);

}else {

//Generate a CNFG pkt to force display into standby mode.
  memset(lcdBuffer,0,sizeof(lcdBuffer));
  //Build message for config info to LCD screen
   memset(lcdBuffer,0,sizeof(lcdBuffer));
   strncpy(lcdBuffer,"CNFG|",5);
   length = 5;

   itoa(G_STOR_DEVC_MSG.devicetype,&lcdBuffer[length]);  //Device type converted to ascii string
   length = strlen( &lcdBuffer[0] ); //Find new end
   lcdBuffer[length++] = '|';

   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '0';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '|';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '$';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '0';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '|';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '$';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '0';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '|';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '0';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '|';
   length = strlen( &lcdBuffer[0] );
   lcdBuffer[length++] = '0';
   length = strlen( &lcdBuffer[0] );

   lcdBuffer[length++] = '\r';
   lcdBuffer[length++] = '\n';

   length = strlen( &lcdBuffer[0] );
    //Send to the LCD display
   CommandToLCDScreen(lcdBuffer,length);
}
}



/*** BeginHeader CC_GenerateLcdAcctMessage */
/*	--------------------------------------------------------------
	Function: CC_GenerateLcdAcctMessage
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Generates an LCD ACCT packet and sends it to the display.
	------------------------------------------------------------ */
//MSS549
void CC_GenerateLcdAcctMessage(void);
/*** EndHeader */
void CC_GenerateLcdAcctMessage(void)
{
   unsigned char lcdBuffer[255];  // was 255
   int length;
   unsigned char temp, scratch[64]; // was 64

//Send select ACCT info to LCD
   //build message to send to LCD
   memset(lcdBuffer,0,sizeof(lcdBuffer));
   strncpy(lcdBuffer,"ACCT|",5);

   strcpy(&lcdBuffer[5], (char*)&G_STOR_ACCT_MSG.cardid);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   //brute force to get the format right...
   temp = G_STOR_ACCT_MSG.reasoncode /10;
   lcdBuffer[length++] = temp + 0x30;
   temp = G_STOR_ACCT_MSG.reasoncode % 10;
   lcdBuffer[length++] = temp + 0x30;

   //length += 2;
   lcdBuffer[length++] = '|';

   temp = G_STOR_ACCT_MSG.approvetype /10;
   lcdBuffer[length++] = temp + 0x30;
   temp = G_STOR_ACCT_MSG.approvetype % 10;
   lcdBuffer[length++] = temp + 0x30;

   lcdBuffer[length++] = '|';

   if(G_STOR_ACCT_MSG.reasondata[0] == 0)
      lcdBuffer[length] = '0';
   else
     	strcpy(&lcdBuffer[length],G_STOR_ACCT_MSG.reasondata);

   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_ACCT_MSG.balplay);

   //Find the '$' in the string (trying to quickly get passed all of the blank spaces
   for(temp = 0; temp < sizeof(scratch); temp++)
   {
   	if(scratch[temp] == '$')
      	break;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_ACCT_MSG.baltime);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_ACCT_MSG.balentt);

   //Find the last ' ' in the string (trying to quickly get passed all of the blank spaces
   temp = 0;
   while(scratch[temp] == ' ')
   {
      temp++;
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   memset(scratch,0,sizeof(scratch));
   strcpy(scratch,(char*)&G_STOR_ACCT_MSG.baltick);

   //Find the ':' at the end of "tkts:" in the string (trying to quickly get passed all of the blank spaces
   for(temp = 0; temp < sizeof(scratch); temp++)
   {
   	if(scratch[temp] == ':')
      {
         temp++;
         break;
      }
   }

   if(temp >= sizeof(scratch))
   	temp = 0;

   strcpy(&lcdBuffer[length],&scratch[temp]);
   length = strlen(&lcdBuffer[0]);
   lcdBuffer[length++] = '|';

   if(G_STOR_ACCT_MSG.nodispense == TRUE)
   	lcdBuffer[length++] = '1';  //tickets to account
   else
      lcdBuffer[length++] = '0';  //dispense tickets

   lcdBuffer[length++] = '\r';
   lcdBuffer[length++] = '\n';
   //lcdBuffer[length++] = '\0';


   //Send to the LCD display
   CommandToLCDScreen(lcdBuffer,length);
   //DebugPrint(STRACE_DBG,("Sent to LCD: %s",lcdBuffer));
}

/*** BeginHeader CC_CheckPendingTkts */
/*	--------------------------------------------------------------
	Function: CC_CheckPendingTkts
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		If 2 sec timer g_tkts_timer has expired, send any remaining
      tickets
	------------------------------------------------------------ */
void CC_CheckPendingTkts(void);
/*** EndHeader */
void CC_CheckPendingTkts(void)
{
	if(Util_CheckSTimeout(g_tkts_timer) && g_srv_tkts_pending)
   {
      DebugPrint(STRACE_DBG,("Sending remaining %d tickets",g_srv_tkts_count))
      CClient_SendMessage(SN_DID_RIOS,g_srv_tkts_dios,g_srv_tkts_count);
      CC_SendLCDTkts(g_lcd_tkts_count);

      g_srv_tkts_count = 0;	//reset server ticket counter
      g_lcd_tkts_count = 0;	//reset LCD ticket counter
      g_srv_tkts_pending = FALSE;	//No tickets currently pending
      lcd_inc_tkts = 0;			//reset the incremental LCD ticket count

   }
}

/*** BeginHeader CC_SendLCDTkts */
/*	--------------------------------------------------------------
	Function: CC_SendLCDTkts
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		If 2 sec timer g_tkts_timer has expired, send any remaining
      tickets
	------------------------------------------------------------ */
void CC_SendLCDTkts(int num_tkts);
/*** EndHeader */
void CC_SendLCDTkts(int num_tkts)
{

	unsigned char outBuffer[255];
   int length;

	memset(outBuffer,0,sizeof(outBuffer));
	strncpy(outBuffer,"TKTS|",5);

   str_xtractstr(G_STOR_ACCT_MSG.cardid,&outBuffer[5],SII_TRANSCARD_MAXLENGTH);
   length = strlen( &outBuffer[0] );
   outBuffer[length++] = '|';
   itoa(num_tkts,&outBuffer[length]);  //Ticket count converted to ascii string
   length = strlen( &outBuffer[0] ); //Find new end
   outBuffer[length++] = '\r';
   outBuffer[length++] = '\n';

   //Send to the LCD display
   CommandToLCDScreen(outBuffer,length);
   DebugPrint(STRACE_DBG,("Sent LCD: TKTS||%d",num_tkts));

}
/*** BeginHeader */
#endif // #ifndef _CASHLESSCLIENT_C_
/*** EndHeader */

