/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	DIOPDefaults.h
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Skoobe II hard-coded DIOP default settings.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	08-02-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _DIOPDEFAULTS_H_
#define _DIOPDEFAULTS_H_
/*** EndHeader */

/*** BeginHeader */
/* --------------------------------------------------------------
	DIOP Defaults
   ------------------------------------------------------------- */
xdata sII_DefaultDIOP {
/* INPUTS */
	// DIO ID 1
	(int)			1,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 2
	(int)			2,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 3
	(int)			3,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 4
	(int)			4,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 5
	(int)			5,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 6
	(int)			6,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 7
	(int)			7,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 8
	(int)			8,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 9
	(int)			9,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 10
	(int)			10,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			100,		// EOS detect (ms)
	// DIO ID 11
	(int)			11,			// DIO ID
	(unsigned char)	0xFF,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			1200,		// EOS detect (ms)
	// DIO ID 12
	(int)			12,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 13
	(int)			13,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 14
	(int)			14,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
/* OUTPUTS */
	// DIO ID 20
	(int)			20,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 21
	(int)			21,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 22
	(int)			22,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 23
	(int)			23,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 24 ("RELAY" 1)
	(int)			24,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	100,		// Time on (ms)
	(unsigned long)	100,		// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 25 ("RELAY" 2)
	(int)			25,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	15,			// Time on (ms)
	(unsigned long)	15,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 26
	(int)			26,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10,			// EOS detect (ms)
	// DIO ID 27
	(int)			27,			// DIO ID
	(unsigned char)	0x00,		// XOR Mask
	(long)			1,			// Cycles
	(unsigned long)	10,			// Time on (ms)
	(unsigned long)	10,			// Time off (ms)
	(long)			10			// EOS detect (ms)
};
/*** EndHeader */

/*** BeginHeader */
#endif // #ifndef _DIOPDEFAULTS_H_
/*** EndHeader */


/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	DIOPDefaults.h
	By:			Scott Sansom
	Purpose:	Skoobe II hard-coded DIOP default settings.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	06-06-2012	v1.0	<scott.sansom@mchsi.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _COLODEFAULTS_H_
#define _COLODEFAULTS_H_
/*** EndHeader */

/*** BeginHeader */
/* --------------------------------------------------------------
	COLO Defaults
   ------------------------------------------------------------- */
xdata sII_DefaultCOLO {
	(int)			255,	// red1
	(int)			0,		// green1
	(int)			0,		// blue1
	(int)			0,		// red2
	(int)			0,		// green2
	(int)			0		// blue2
};
/*** EndHeader */

/*** BeginHeader */
#endif // #ifndef _COLODEFAULTS_H_
/*** EndHeader */