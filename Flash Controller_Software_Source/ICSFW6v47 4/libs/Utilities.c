/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Utilities.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Misc utility functions.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _UTILITIES_C_
#define _UTILITIES_C_

// Handy variable argument typedefs and macros that Dynamic C doesn't support for some reason:
typedef char *  va_list;

#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )
#define va_start(ap,v)  ( ap = (va_list)(&v) + _INTSIZEOF(v) )
#define va_arg(ap,t)    ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define va_end(ap)      ( ap = (va_list)0 )


/*** EndHeader */

/*** BeginHeader Util_Init */
/*	--------------------------------------------------------------
	Function: Util_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the utilities library.
	------------------------------------------------------------ */
void Util_Init(void);
/*** EndHeader */
nodebug
void Util_Init(void)
{
	// Set debug LED lines off and make them outputs
	BitWrPortI(PBDR, &PBDRShadow, 0, 5);
	BitWrPortI(PBDR, &PBDRShadow, 0, 7);
	BitWrPortI(PBDDR, &PBDDRShadow, 1, 5);
	BitWrPortI(PBDDR, &PBDDRShadow, 1, 7);
}

/*** BeginHeader Util_Delay */
/*	--------------------------------------------------------------
	Function: Util_Delay
	Params:
		milliseconds - number of milliseconds to block
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Blocks program execution for a specified number of
		milliseconds.
	------------------------------------------------------------ */
void Util_Delay(unsigned int milliseconds);
/*** EndHeader */
nodebug
void Util_Delay(unsigned int milliseconds)
{
	auto unsigned long done_time;

	done_time = MS_TIMER + milliseconds;
	while( (long) (MS_TIMER - done_time) < 0 );
}

/*** BeginHeader Util_CheckSTimeout */
/*	--------------------------------------------------------------
	Function: Util_CheckSTimeout
	Params:
		stimeout - second timeout variable to check against.
	Returns:
		int - nonzero if timeout has expired, zero otherwise.
	--------------------------------------------------------------
	Purpose:
		Determines if a timeout has expired. A second timeout
		is an unsigned long variable calculated from SEC_TIMER.
		For example, to wait 1s, the following could be used:
			unsigned long mytimout;
			mytimout = SEC_TIMER+30;	// Timeout 30 seconds from now.
			while(!Util_CheckSTimeout(mytimout)) {
				// Do something until we timeout.
			}
		This is used instead of a blocking delay function if
		something else needs to occur while waiting for the
		timeout.
	------------------------------------------------------------ */
int Util_CheckSTimeout(unsigned long stimeout);
/*** EndHeader */
nodebug
int Util_CheckSTimeout(unsigned long stimeout)
{
	return ((long)(SEC_TIMER-stimeout))>=0;
}

/*** BeginHeader Util_CheckmsTimeout */
/*	--------------------------------------------------------------
	Function: Util_CheckmsTimeout
	Params:
		mstimeout - millisecond timeout variable to check against.
	Returns:
		int - nonzero if timeout has expired, zero otherwise.
	--------------------------------------------------------------
	Purpose:
		Determines if a timeout has expired. A millisecond timeout
		is an unsigned long variable calculated from MS_TIMER.
		For example, to wait 1s, the following could be used:
			unsigned long mytimout;
			mytimout = MS_TIMER+1000;	// Timeout 1 second from now.
			while(!Util_CheckmsTimeout(mytimout)) {
				// Do something until we timeout.
			}
		This is used instead of a blocking delay function if
		something else needs to occur while waiting for the
		timeout. This mechanism has limited precision, which
		largely depends upon what else is happening while waiting
		for a timeout. It is most appropriate for measuring things
		in user time, or ensuring that some processing task
		eventually times out.
	------------------------------------------------------------ */
int Util_CheckmsTimeout(unsigned long mstimeout);
/*** EndHeader */
nodebug
int Util_CheckmsTimeout(unsigned long mstimeout)
{
	return ((long)(MS_TIMER-mstimeout))>=0;
}

/*** BeginHeader Util_DebugLED */
/*	--------------------------------------------------------------
	Function: Util_DebugLED
	Params:
		LED - LED to change: debug_led1 or debug_led2
		turnon - TRUE to turn on LED, FALSE to turn off LED.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Turns on or off one of the debug LEDs.
	------------------------------------------------------------ */
typedef enum {
	debug_led1 = 0,
	debug_led2
} debug_leds;

void Util_DebugLED(debug_leds LED, BOOL turnon);
/*** EndHeader */
void Util_DebugLED(debug_leds LED, BOOL turnon)
{
	char LEDstate;
	LEDstate = 0;
	if(turnon)
		LEDstate = 1;
	switch(LED){
	case debug_led1:
		BitWrPortI(PBDR, &PBDRShadow, LEDstate, 5);
		break;
	case debug_led2:
		BitWrPortI(PBDR, &PBDRShadow, LEDstate, 7);
		break;
	}
}

/*** BeginHeader Util_Stopwatch */
/*	--------------------------------------------------------------
	Function: Util_Stopwatch
	Params:
		void
	Returns:
		long - time (in mS) elapsed since Util_Stopwatch() was
			last called.
	--------------------------------------------------------------
	Purpose:
		Turns on or off one of the debug LEDs.
	------------------------------------------------------------ */
long Util_Stopwatch(void);
extern unsigned long g_util_snap;
/*** EndHeader */
unsigned long g_util_snap;
nodebug root
long Util_Stopwatch(void)
{
	long timediff;
#GLOBAL_INIT{g_util_snap = 0;}
	timediff = MS_TIMER-g_util_snap;
	g_util_snap = MS_TIMER;
	return timediff;
}

/*** BeginHeader Util_tmstr */
/*	--------------------------------------------------------------
	Function: Util_tmstr
	Params:
		tC - Buffer to hold the resulting string
		tTime - if < 0, this function reads and formats the
			current time, otherwise it formats tTime.
	Returns: void
	--------------------------------------------------------------
	Purpose: Reads the current RTC setting and creates a string
		formatted for the Ideal Server.
	------------------------------------------------------------ */
void Util_tmstr(char *tC, long tTime);
/*** EndHeader */
void Util_tmstr(char *tC, long tTime)
{
	long tmrtc;
	struct tm utc;
	if (tTime<0) {
		tmrtc = read_rtc();
	} else {
		tmrtc = tTime;
	}
	mktm(&utc,tmrtc);
	sprintf(tC,"%4d%02d%02d%02d%02d%02d", (utc.tm_year+1900), utc.tm_mon, utc.tm_mday, utc.tm_hour, utc.tm_min, utc.tm_sec);
}

/*** BeginHeader Util_2ASCIIBytesTo1HexByte */
/*	--------------------------------------------------------------
	Function: Util_2ASCIIBytesTo1HexByte
	Params:
        bytes - pointer to string of at least 2 bytes.
	Returns: 
        unsigned char - the conversion result.
	--------------------------------------------------------------
	Purpose:
        Converts a 2-byte ASCII representation of a hex number
        into the actual 1-byte hex number.
	------------------------------------------------------------ */
unsigned char Util_2ASCIIBytesTo1HexByte(char *bytes);
/*** EndHeader */
unsigned char Util_2ASCIIBytesTo1HexByte(char *bytes)
{
    char tb[3];
    char tresult;

    tb[0] = bytes[0];
    tb[1] = bytes[1];
    tb[2] = '\0';
    tresult = 0;
    sscanf(tb,"%2X",&tresult);
    return tresult;
}

/*** BeginHeader */
#endif // #ifndef _UTILITIES_C_
/*** EndHeader */