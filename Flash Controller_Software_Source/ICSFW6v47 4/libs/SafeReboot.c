/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	SafeReboot.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Provides services to ensure requested soft reboots
				happen safely.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.
   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _SAFEREBOOT_C_
#define _SAFEREBOOT_C_
/*** EndHeader */

/*** BeginHeader SafeReboot_Init */
/*	--------------------------------------------------------------
	Function: SafeReboot_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the Safe Reboot library.
	------------------------------------------------------------ */
void SafeReboot_Init(void);
typedef enum {
	safereboot_idle = 0,
	safereboot_softboot_pending
} safereboot_state;
extern int g_safereboot_semaphore;
extern safereboot_state g_safereboot_state;
/*** EndHeader */
int g_safereboot_semaphore;
safereboot_state g_safereboot_state;
void SafeReboot_Init(void)
{
	ipset(3);
	g_safereboot_semaphore = 0;
	g_safereboot_state = safereboot_idle;
	ipres();
}

/*** BeginHeader SafeReboot_GetSemaphore */
/*	--------------------------------------------------------------
	Function: SafeReboot_GetSemaphore
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Increments the reboot semaphore. Reboots can't happen until
		the count is back at 0.
	------------------------------------------------------------ */
void SafeReboot_GetSemaphore(void);
/*** EndHeader */
void SafeReboot_GetSemaphore(void)
{
	ipset(3);
	// There should never be this many nested semaphore gets,
	// but we cap it so that the integer doesn't accidentally roll around to
	// 0 and allow a reboot.
	if (g_safereboot_semaphore < 1000)
		g_safereboot_semaphore++;
	ipres();
}

/*** BeginHeader SafeReboot_PutSemaphore */
/*	--------------------------------------------------------------
	Function: SafeReboot_PutSemaphore
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decrements the reboot semaphore. Reboots can't happen until
		the count is back at 0.
	------------------------------------------------------------ */
void SafeReboot_PutSemaphore(void);
/*** EndHeader */
void SafeReboot_PutSemaphore(void)
{
	ipset(3);
	if (g_safereboot_semaphore > 0) {
		g_safereboot_semaphore--;
	}
	if ( (g_safereboot_state==safereboot_softboot_pending) && (g_safereboot_semaphore == 0) ) {
		forceSoftReset();
	}
	ipres();
}

/*** BeginHeader SafeReboot_SoftReboot */
/*	--------------------------------------------------------------
	Function: SafeReboot_SoftReboot
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Decrements the reboot semaphore. Reboots can't happen until
		the count is back at 0.
	------------------------------------------------------------ */
void SafeReboot_SoftReboot(void);
/*** EndHeader */
void SafeReboot_SoftReboot(void)
{
	ipset(3);
	if(g_safereboot_semaphore == 0) {
		forceSoftReset();
	} else {
		g_safereboot_state = safereboot_softboot_pending;
	}
	ipres();
}

/*** BeginHeader */
#endif // #ifndef _SAFEREBOOT_C_
/*** EndHeader */

