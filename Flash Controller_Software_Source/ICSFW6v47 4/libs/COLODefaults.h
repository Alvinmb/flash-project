/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	DIOPDefaults.h
	By:			Scott Sansom
	Purpose:	Skoobe II hard-coded DIOP default settings.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	06-06-2012	v1.0	<scott.sansom@mchsi.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _COLODEFAULTS_H_
#define _COLODEFAULTS_H_
/*** EndHeader */

/*** BeginHeader */
/* --------------------------------------------------------------
	COLO Defaults
   ------------------------------------------------------------- */
xdata sII_DefaultCOLO {
/* INPUTS */
	// DIO ID 1
	(int)			255,	// red1
	(int)			0,		// green1
	(int)			0,		// blue1
	(int)			0,		// red2
	(int)			0,		// green2
	(int)			0		// blue2
};
/*** EndHeader */

/*** BeginHeader */
#endif // #ifndef _COLODEFAULTS_H_
/*** EndHeader */