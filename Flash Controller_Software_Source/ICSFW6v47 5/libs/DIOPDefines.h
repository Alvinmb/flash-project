/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	DIOPDefines.h
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Skoobe II hard-coded DIOP definitions.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	08-02-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _DIOPDEFINES_H_
#define _DIOPDEFINES_H_
/*** EndHeader */

/*** BeginHeader */
// These are indexes into the input or output arrays.  
#define DIOP_ORELAY1						4	// DIOP 24
#define DIOP_ORELAY2						5	// DIOP 25

#define DIOP_ONOTCHSIGNALTOGAME				0	// DIOP 20
#define DIOP_ODISPENSESIGNALTODISPENSER		1	// DIOP 21
#define DIOP_INOTCHSIGNALFROMDISPENSER		10	// DIOP 11
#define DIOP_IDISPENSESIGNALFROMGAME		9	// DIOP 10

/*** EndHeader */

/*** BeginHeader */
#endif // #ifndef _DIOPDEFINES_H_
/*** EndHeader */
