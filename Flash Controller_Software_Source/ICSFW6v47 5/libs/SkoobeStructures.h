/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	SkoobeStructures.h
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Skoobe II data structures.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _SKOOBESTRUCTURES_H_
#define _SKOOBESTRUCTURES_H_
/*** EndHeader */

/**********************************************************
			Volatile Configuration Structures
 **********************************************************/
/*** BeginHeader mssg_ACCT_t */
typedef struct {
	char cardid[SII_TRANSCARD_MAXLENGTH];// Card ID for Transaction
	char reasoncode;			// Txn Reason code, 0 = Approved
	char nodispense;			// Do not dispense tickets, 0 = Dispense, 1 = To Acct
	char reasondata[17];		// Txn Reason Data (optional)
	char balplay[13];			// Play Balance
	char balcash[13];			// Cash Balance
	char balcomp[13];			// Comp Balance
	char balbons[13];			// Bonus Balance
	char baltick[13];			// Ticket Balance
	char approvetype;			// Approval Type, 0 = Cash, 1 = Time, 2 = Entitlement
	char baltotl[13];			// Total Balance (Cash + Comp + Bonus)
	char baltime[13];			// Time Balance
	char balentt[6];			// Entitlement Balance
	BOOL acctvalid;				// Set to True when message is valid (after an ACCT message has been received.)
} mssg_ACCT_t;
/*** EndHeader */

/**********************************************************
			Persistent Configuration Structures
 **********************************************************/
/*** BeginHeader mssg_SNBB_t, storage_SNBB_t */
typedef struct {
	long tmoffset;				// Calculated RTC prior offset
	word porttxn;				// Server Transaction Port
	word portsnbb;				// SNBB Listening Port
	word portcmd;				// Listening Command Port
	char serveripaddr[16];		// Address of the Server
	char broadcastip[16];		// Address to use for Broadcasts
} mssg_SNBB_t;

typedef struct {
	mssg_SNBB_t snbb;
	int			chksum;
	BOOL		valid;
} storage_SNBB_t;

/*** EndHeader */

/*** BeginHeader mssg_CNFG_t, storage_CNFG_t */
typedef struct {
	struct tm exptime;			// Configuration Expire Time Structure
	char prices[5][13];			// Play Prices, Standard, Discounted 1-3, Alternate
	char pricealt[13];			// Alternate Play Price (for Dual Pricing)
	char pushenabled;			// Push Enabled, 0 = False, 1 = True
	char nodispense;			// Do not dispense tickets, 0 = False, 1 = True
	char cardtypes;				// Acceptable Cards: 0, 1, or 2
	char timeplay;				// Time Play Enabled, 0 = False, 1 = True
	char freeplay;				// Free Play Enabled, 0 = False, 1 = True
	word portcmd;				// Listen-to TCP Port for Override
	char denomination[6];		// Denomination for Device
	char battdate[9];			// Date Battery Changed, yyyymmdd
	word porthb;				// Heartbeat TCP Port for Override
	char hbfreq;				// Heartbeat frequency in minutes
	char hbtype;				// Heartbeat type: 0 - Idle Only, 1 - On Freq
	long pushwait;				// Wait time in 100ms before entering Push Mode
	char sitepmpayout;			// Site Push Mode Payout
									// 0 = Dispense
									// 1 = Ticketless
									// 2 = No Payout
									// 3 = Up to Game
	char gamepmpayout;			// Game Push Mode Payout
									// 0 = Dispense
									// 1 = Ticketless
									// 2 = No Payout
	char pricemode;				// Pricing Mode
									// 0 = Standard
									// 1 = Dual Pricing
	long cyclewait;				// Wait time after engaging device
} mssg_CNFG_t;

typedef struct {
	mssg_CNFG_t cnfg;
	int			chksum;
} storage_CNFG_t;
/*** EndHeader */

/*** BeginHeader mssg_DEVC_t, storage_DEVC_t */
#define DEVTYPE_GAME		0	// Standard Game
#define DEVTYPE_RESTRICT	1	// GRS Device
#define DEVTYPE_TIMEFRZ		2	// Time Freeze
#define DEVTYPE_REDEMPT		3	// Standard Redemption
#define DEVTYPE_TOKINT		4	// Token Interactive
#define DEVTYPE_TOKINTR		5	// Token Interactive w/Redemption
#define DEVTYPE_CHANGER		6	// Token Changer
#define DEVTYPE_CRANE		7	// Crane
#define DEVTYPE_PUSHER		8	// Pusher
#define DEVTYPE_SLOTMACHINE	9	// Slot Machine
#define DEVTYPE_DENOMDEV	10	// Denominated Device
#define DEVTYPE_MULTITCKT	11	// Multi-Ticket Dispenser
#define DEVTYPE_TIKEATER	12	// Ticket Eater
#define DEVTYPE_ACCESS		13	// ACM Device/Turnstile

#define DISPENSETYPE_NONE	0	// No dispenser installed.
#define DISPENSETYPE_PULSE	1	// Pulse dispenser (DL-4, TD-963PR)
#define DISPENSETYPE_MOTOR	2	// Motor drive dispenser (DL-1275, DT-963CR)

typedef struct {
	char siteid[6];				// Site ID
	char devicetype;			// Device Type, 0 - 99, see tech ref
	char devicename[61];		// Device Name
	char saenabled;				// Standalone Enabled, 0 = False, 1 = True
	char sw1;					// Switch 1 Installed, 0 = False, 1 = True
	char sw2;					// Switch 2 Installed, 0 = False, 1 = True
	char sw3;					// Switch 3 Installed, 0 = False, 1 = True
	char sw4;					// Switch 4 Installed, 0 = False, 1 = True
	char scanner1;				// Device Type Port C, 0 = None, 1 = P3 Scanner
	char scanner2;				// Device Type Port D, 0 = None, 1 = P3 Scanner
	char displaytype;			// Display Type, 0 = None, 1 = P3 Type
	char gameacttype;			// Game Action Type
	char MustHaveNetwork;       // Whether or not game is enabled when network cable is unplugged. 0 = False, 1 = True
	char unused2;				// (unused)
	char unused3;				// (unused)
	char dispenstype;			// Dispenser Type, 0 - 99
	long ticketcap;				// Ticket Capacity
	long ticketlow;				// Tickets Low Level sense
} mssg_DEVC_t;

typedef struct {
	mssg_DEVC_t devc;
	int			chksum;
} storage_DEVC_t;
/*** EndHeader */

/*** BeginHeader mssg_DIOP_t, storage_DIOP_t */
typedef struct {
	int dioid;					// ID of DIO (see DIOP message in Tech Ref)
	unsigned char xormask;		// if the signal is active low, this is set to 0xFF, otherwise 0x00.
	long cycles;				// # of cycles in a signal, usually 1
	unsigned long timeon;		// Min time in ms for On portion
	unsigned long timeoff;		// Min time in ms for Off portion
	long eosdetect;				// Time to wait for End of Signal detection
} mssg_DIOP_t;

#define MAXINPUTS 14
#define MAXOUTPUTS 8
#define MAXIO MAXINPUTS+MAXOUTPUTS
typedef struct {
	mssg_DIOP_t diop[MAXIO];
	int			chksum;
} storage_DIOP_t;
/*** EndHeader */

/*** BeginHeader mssg_COLO_t, storage_COLO_t */
typedef struct {
	int red1;
   int green1;
   int blue1;
   int red2;
   int green2;
   int blue2;
} mssg_COLO_t;

typedef struct {
	mssg_COLO_t colo;
   int chksum;
} storage_COLO_t;

/*** EndHeader */

/*** BeginHeader misc_NVGLOB_t, storage_NVGLOB_t */
#define PRICE_STATE_MIN			0
#define PRICE_STATE_STANDARD	0
#define PRICE_STATE_DISCNT1		1
#define PRICE_STATE_DISCNT2		2
#define PRICE_STATE_DISCNT3		3
#define PRICE_STATE_SCHEDULE	4
#define PRICE_STATE_MAX			4
// Miscellaneous non-volatile globals.
typedef struct {
	int		skoobe_pricestate;	// Current Price Mode State, 0=Std, 1-3=Discount
} misc_NVGLOB_t;

typedef struct {
	misc_NVGLOB_t misc;
	int			chksum;
} storage_NVGLOB_t;
/*** EndHeader */

/*** BeginHeader sn_did_t */
typedef enum {
	SN_DID_UNKNOWN = 0,
	SN_DID_ACCT,
	SN_DID_BOOT,
	SN_DID_BUTN,
	SN_DID_CMCD,
	SN_DID_CMSG,
	SN_DID_CNFG,
	SN_DID_CRDS,
	SN_DID_DEVC,
	SN_DID_DIOP,
   SN_DID_RIOS,
	SN_DID_DIOS,
	SN_DID_DISP,
	SN_DID_EOTX,
	SN_DID_HAPY,
	SN_DID_LEDS,
	SN_DID_LEDX,
	SN_DID_LOCK,
	SN_DID_MSSG,
	SN_DID_PRCE,
	SN_DID_PUSH,
	SN_DID_REBX,
	SN_DID_RELY,
	SN_DID_RQST,
	SN_DID_SCAN,
	SN_DID_SNBB,
	SN_DID_TEST,
   SN_DID_COLO,
   SN_DID_FLIP,
   SN_DID_RSET,
   SN_DID_VSET,

} sn_did_t;
/*** EndHeader */

/**********************************************************
				File Record Structures
 **********************************************************/

/*** BeginHeader recd_CARD_t */
typedef struct {
	int  cardid;
	char isbrdcast;				// Broadcast Enabled, 0 = False, 1 = True
	char isdiscount;			// Discount, 0 = False, 1-3 = Discount #
	char cardnumber[SII_COMMANDCARD_CMDLENGTH];	// Card number text
} recd_CARD_t;
/*** EndHeader */

/*** BeginHeader recd_MSSG_t */
#define CMSG_TEXT_LEN	80
typedef struct {
	int		mssgid;						// Message ID
	char	isblink;					// Blink Enabled, 0 = False, 1 = True
	char	isscroll;					// Scroll Enabled, 0 = False, 1 = True
	int		timedisp;					// Time in Seconds to Display Message
	char	mssgtext[CMSG_TEXT_LEN];	// Message
} recd_MSSG_t;
/*** EndHeader */

/*** BeginHeader recd_TXNS_t */
typedef struct {
	unsigned long txntime;		// Transaction Time (UTC)
	char txntype;				// Transaction Type: T, E, U, X (Alpha Only!)
	char txndid;				// Transaction DID (from Macros)
	char actiontype;			// Transaction Action Type
	char txndata[45];			// Data/Card ID (encoded)
} recd_TXNS_t;
/*** EndHeader */

/*** BeginHeader SSRV_STATE */
typedef enum {
	SSRVSTATE_STANDBY			= 0,
	SSRVSTATE_DEFAULT			= 1,
	SSRVSTATE_FREEPLAY			= 2,
	SSRVSTATE_TIMEPLAY			= 3,
	SSRVSTATE_NEWCONTROLLER		= 4,
	SSRVSTATE_TESTMODE			= 5,
	SSRVSTATE_OFFLINE			= 6,
	SSRVSTATE_ROBOPLAY			= 7,
	SSRVSTATE_BUFFERTRANSFER	= 8,
	SSRVSTATE_REQUEST			= 9
} SSRV_STATE;
/*** EndHeader */


/*** BeginHeader */
#define _SKOOBESTRUCTURES_H_
#endif // #ifndef
/*** EndHeader */

