/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Push.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Push mode library
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	09-12-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _PUSH_C_
#define _PUSH_C_
/*** EndHeader */

/*** BeginHeader Push_Init */
/*	--------------------------------------------------------------
	Function: Push_Init
	Params: void
	Returns:
		BOOL - TRUE if push library initializes properly, FALSE if
			it fails.
	--------------------------------------------------------------
	Purpose: Initializes the Push library
	NOTE: Requires Storage_Init() to have been called first to
		set up the file system.
	------------------------------------------------------------ */
BOOL Push_Init(void);
extern File		g_pushfile;
extern File		g_pushptrfile;

// Structure to hold the push buffer pointers
typedef struct {
	unsigned int	g_pushBCount;
	unsigned int	g_pushBRead;
	unsigned int	g_pushBWrite;
} pushptr_rec;
extern pushptr_rec g_pushptrs;
extern unsigned int	g_pushBXfer;

/*** EndHeader */
File	g_pushfile;
File	g_pushptrfile;
pushptr_rec g_pushptrs;
unsigned int g_pushBXfer;

BOOL Push_Init(void)
{
	BOOL initerr;
	memset(&g_pushptrs,0,sizeof(g_pushptrs));
	memset(&g_pushfile,0,sizeof(g_pushfile));
	memset(&g_pushptrfile,0,sizeof(g_pushptrfile));
	g_pushBXfer = 0;
	initerr = FALSE;
	if (fopen_wr(&g_pushptrfile, SII_PUSHPTRFILE)) {
		return Push_CreateAndInitFiles();
	}
	if (fopen_wr(&g_pushfile, SII_PUSHFILE)) {
		return Push_CreateAndInitFiles();
	}
	if(fseek(&g_pushptrfile,0,SEEK_SET)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
	if(fread(&g_pushptrfile,&g_pushptrs,sizeof(g_pushptrs)) != sizeof(g_pushptrs)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
	return TRUE;
}

/*** BeginHeader Push_UnInit */
/*	--------------------------------------------------------------
	Function: Push_UnInit
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Undoes initialization such that Push_Init() can be called
		again. This is used when the storage system is determined
		to be corrupt & needs formatting.
	------------------------------------------------------------ */
void Push_UnInit(void);
/*** EndHeader */
void Push_UnInit(void)
{
	fclose(&g_pushptrfile);
	fclose(&g_pushfile);
}

/*** BeginHeader Push_CreateAndInitFiles */
/*	--------------------------------------------------------------
	Function: Push_CreateAndInitFiles
	Params:
		void
	Returns:
		BOOL - TRUE if file was created successfully. FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Upon first boot or corruption of the file, this will
		create new push files.
	------------------------------------------------------------ */
BOOL Push_CreateAndInitFiles(void);
/*** EndHeader */
BOOL Push_CreateAndInitFiles(void)
{
	recd_TXNS_t ttxn;
	unsigned int i;

	DebugPrint(STRACE_NFO,("Creating push files..."));

	// Delete old/corrupt push files.
	fdelete(SII_PUSHPTRFILE);
	fdelete(SII_PUSHFILE);

	// Create new push files.
	if (fcreate(&g_pushptrfile, SII_PUSHPTRFILE)) {
		return FALSE;
	}
	if (fcreate(&g_pushfile, SII_PUSHFILE)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
	// Write out push pointers.
	memset(&g_pushptrs,0,sizeof(g_pushptrs));
	if(fwrite(&g_pushptrfile,&g_pushptrs,sizeof(g_pushptrs)) != sizeof(g_pushptrs)) {
		fclose(&g_pushfile);
		return FALSE;
	}
	// Write out empty transactions to reserve push buffer room.
	memset(&ttxn,0,sizeof(ttxn));
	for(i = 0; i < SII_MAX_PUSHBUFFER; i++) {
		if(fwrite(&g_pushfile,&ttxn,sizeof(ttxn)) != sizeof(ttxn)) {
			DebugPrint(STRACE_ERR,("Error creating room for push buffer. Check allocations."));
			fclose(&g_pushptrfile);
			fclose(&g_pushfile);
			return FALSE;
		}
	}
	fclose(&g_pushfile);
	if(fopen_wr(&g_pushfile, SII_PUSHFILE)) {
		return FALSE;
	}
	return TRUE;
}



/*** BeginHeader Push_Store */
/*	--------------------------------------------------------------
	Function: Push_Store
	Params: void
	Returns:
		BOOL - TRUE if message was successfully pushed, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose: Stores a message in the push buffer.
	------------------------------------------------------------ */
BOOL Push_Store(sn_did_t mID, char *tMssg, char sstate);
/*** EndHeader */
BOOL Push_Store(sn_did_t mID, char *tMssg, char sstate)
{
	recd_TXNS_t ttxn;
	int tresult;

	if(g_pushptrs.g_pushBCount >= SII_MAX_PUSHBUFFER)
		return FALSE;

	memset(&ttxn,0x00,sizeof(ttxn));
	ttxn.txndid = (char)mID;

	switch (mID) {
	default:
		return FALSE;
    /*
	default:
		ttxn.txntype = tType;
		if (tType=='E') {
			// Save data component
			lpCptr = tMssg;
			strncpy(ttxn.txndata,lpCptr,sizeof(ttxn.txndata));
		} else {
		// Force it to Unknown Txn Type
		ttxn.txntype = 'U';
		}
		// Unsupported Txn Type
		ttxn.txntype = 'X';
		break;
	*/
	case SN_DID_DIOS:
		ttxn.txntype = 'D';
		// Save data component
		// Note: all of these datagrams begin with a MAC address
		strncpy(ttxn.txndata,&tMssg[25],sizeof(ttxn.txndata));
		ttxn.txndata[sizeof(ttxn.txndata)-1] = '\0';
		break;
	case SN_DID_BUTN:
	case SN_DID_SCAN:
		ttxn.txntype = 'T';
		// Save data component
		// Note: all of these datagrams begin with a MAC address
		strncpy(ttxn.txndata,&tMssg[25],sizeof(ttxn.txndata));
		ttxn.txndata[sizeof(ttxn.txndata)-1] = '\0';
		break;
	}

	ttxn.txntime = read_rtc();
	ttxn.actiontype = sstate;

	// Write out the transaction to the push buffer.
	fseek(&g_pushfile,g_pushptrs.g_pushBWrite*sizeof(recd_TXNS_t),SEEK_SET);
	if(fwrite(&g_pushfile,&ttxn,sizeof(recd_TXNS_t)) == sizeof(recd_TXNS_t)) {
		// Increment write and count pointers.
		g_pushptrs.g_pushBCount++;
		g_pushptrs.g_pushBWrite++;
		if(g_pushptrs.g_pushBWrite >= SII_MAX_PUSHBUFFER)
			g_pushptrs.g_pushBWrite = 0;
		// Write out the pointers.
		if(fseek(&g_pushptrfile,0,SEEK_SET)) {
			fclose(&g_pushptrfile);
			return FALSE;
		}
		if(fwrite(&g_pushptrfile,&g_pushptrs,sizeof(g_pushptrs)) != sizeof(g_pushptrs)) {
			fclose(&g_pushptrfile);
			return FALSE;
		}
	} else {
		return FALSE;
	}
	return TRUE;
}


/*** BeginHeader Push_GetCount */
/*	--------------------------------------------------------------
	Function: Push_GetCount
	Params: void
	Returns:
		unsigned int - number of transactions in the push buffer.
	--------------------------------------------------------------
	Purpose: Retrieves the number of transactions in the push
		buffer.
	------------------------------------------------------------ */
unsigned int Push_GetCount(void);
/*** EndHeader */
unsigned int Push_GetCount(void)
{
	return g_pushptrs.g_pushBCount;
}

/*** BeginHeader Push_SendNextRecord */
/*	--------------------------------------------------------------
	Function: Push_SendNextRecord
	Params: void
	Returns:
		BOOL - TRUE if record successfully sent, FALSE otherwise.
	--------------------------------------------------------------
	Purpose: Sends the next pending record. If no records are
		pending, nothing is sent and this returns FALSE.
	------------------------------------------------------------ */
BOOL Push_SendNextRecord(void);
/*** EndHeader */
BOOL Push_SendNextRecord(void)
{
	recd_TXNS_t ttxn;
	char ttimestr[24];
	char ttempstr[64];

	if(g_pushptrs.g_pushBCount == 0)
		return FALSE;
	fseek(&g_pushfile,g_pushptrs.g_pushBRead*sizeof(recd_TXNS_t),SEEK_SET);
	if(fread(&g_pushfile,&ttxn,sizeof(recd_TXNS_t)) == sizeof(recd_TXNS_t)) {
		// Convert the time
		Util_tmstr(ttimestr,ttxn.txntime);
		// Render the record info.
		sprintf(ttempstr,"%c%02u%s",ttxn.txntype,ttxn.actiontype,ttimestr);
		// Send the message.
		return CClient_SendMessage(SN_DID_MSSG,(int)ttxn.txndid,ttempstr,ttxn.txndata);
	} else {
		return FALSE;
	}
	return TRUE;
}

/*** BeginHeader Push_AckPreviousSendNext */
/*	--------------------------------------------------------------
	Function: Push_AckPreviousSendNext
	Params: void
	Returns:
		BOOL - TRUE if acked & sent properly, FALSE otherwise.
	--------------------------------------------------------------
	Purpose: Acks the last push record and sends the next if
		there are more pending.
	------------------------------------------------------------ */
BOOL Push_AckPreviousSendNext(void);
/*** EndHeader */
BOOL Push_AckPreviousSendNext(void)
{
	g_pushptrs.g_pushBCount--;
	g_pushptrs.g_pushBRead++;
	if(g_pushptrs.g_pushBRead >= SII_MAX_PUSHBUFFER)
		g_pushptrs.g_pushBRead = 0;
	// Write out the pointers.
	if(fseek(&g_pushptrfile,0,SEEK_SET)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
	if(fwrite(&g_pushptrfile,&g_pushptrs,sizeof(g_pushptrs)) != sizeof(g_pushptrs)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
	if(g_pushptrs.g_pushBCount == 0)
		return FALSE;
	Push_SendNextRecord();
	return TRUE;
}

/*** BeginHeader Push_Clear */
/*	--------------------------------------------------------------
	Function: Push_Clear
	Params: void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Clears the push buffer.
	------------------------------------------------------------ */
BOOL Push_Clear(void);
/*** EndHeader */
BOOL Push_Clear(void)
{
	memset(&g_pushptrs,0,sizeof(g_pushptrs));
	// Write out the pointers.
	if(fseek(&g_pushptrfile,0,SEEK_SET)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
	if(fwrite(&g_pushptrfile,&g_pushptrs,sizeof(g_pushptrs)) != sizeof(g_pushptrs)) {
		fclose(&g_pushptrfile);
		return FALSE;
	}
}

/*** BeginHeader */
#endif // #ifndef _PUSH_C_
/*** EndHeader */

