/* --------------------------------------------------------------
	Copyright (C) 2011 Ideal Software Systems. All Rights Reserved.

	Filename:	LCDScreen.c
	By:			Rob Toth
	Purpose:	   Run serially connected LCD screen.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	09-12-2011	v1.0
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _LCD_SCREEN_C_
#define _LCD_SCREEN_C_

/*** EndHeader */

/*** BeginHeader */

#define LCD_SERIAL_BUFFER_SIZE	( 256 )

/*	-----------------------------------------------------------------------------
	Macros:	LCD_seropen, LCD_serclose, LCD_serread, LCD_serwrite,
			LCD_serrdFlush, LCD_serwrFlush, LCD_serrdUsed, LCD_serwrUsed
	Desc:	These macros map the Dynamic C serial port functions that should be
			used for communicating with the LCD screen.
	Note:	These should be changed if a different serial port is used.
	---------------------------------------------------------------------------- */
#define LCD_seropen		serDopen
#define LCD_serclose		serDclose
#define LCD_serread		serDread
#define LCD_serwrite		serDwrite
#define LCD_serrdFlush	serDrdFlush
#define LCD_serwrFlush	serDwrFlush
#define LCD_serrdUsed	serDrdUsed
#define LCD_serwrUsed	serDwrUsed
#define LCD_serdatabits	serDdatabits
/*	-----------------------------------------------------------------------------
	Macro:	EINBUFSIZE, EOUTBUFSIZE
	Desc:	These macros are used by the included Rabbit / Dynamic C serial port
			libraries.  They determine the size of the serial input/output buffers.
	Note:	These must be defined before serEOpen is called.
	Note:	These should be changed if a different serial port is used.
	---------------------------------------------------------------------------- */
//#define DINBUFSIZE		LCD_SERIAL_BUFFER_SIZE-1
//#define DOUTBUFSIZE		LCD_SERIAL_BUFFER_SIZE-1

#define LCD_SERIAL_DEFAULT_BAUD_RATE	( 115200 )
/*** EndHeader */

/*** BeginHeader LCD_Startup */
/*	--------------------------------------------------------------
	Function: LCD_Startup
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the LCD screen
	------------------------------------------------------------ */
extern unsigned char g_LCD_outBuffer[LCD_SERIAL_BUFFER_SIZE];
extern char g_LCD_State;
extern unsigned long g_LCD_timer;
extern unsigned char rjt_test_character;

typedef enum {
	LCD_STATE_RESET_START = 0,
	LCD_STATE_RESET_STOP,
	LCD_STATE_ACCEPT_COMMANDS
} lcd_state;

void LCD_Startup(void);
/*** EndHeader */
unsigned char g_LCD_outBuffer[LCD_SERIAL_BUFFER_SIZE];
char g_LCD_State;
unsigned long g_LCD_timer;
unsigned char rjt_test_character;

void LCD_Startup(void)
{
   memset(g_LCD_outBuffer,0,sizeof(g_LCD_outBuffer));

	LCD_seropen(LCD_SERIAL_DEFAULT_BAUD_RATE);
   LCD_serdatabits(PARAM_8BIT);

   //Configure the reset line I/O
   //  PA2 is output.
   WrPortI(SPCR, &SPCRShadow, 0x84); //Make PortA a byte wide output
   BitWrPortI(PADR, &PADRShadow, 1, 2);   //Bring reset line high
   g_LCD_State = LCD_STATE_ACCEPT_COMMANDS;

   DebugPrint(STRACE_NFO,("Video Screen - RS232 Started."));

}

/*** BeginHeader ResetLCDScreen */
/*	--------------------------------------------------------------
	Function: ResetLCDScreen
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Resets the serial LCD Screen

	------------------------------------------------------------ */
void ResetLCDScreen(void);
/*** EndHeader */
void ResetLCDScreen(void)
{
	g_LCD_State = LCD_STATE_RESET_START;	//Gets asserted in HandleLCDScreen()
}

/*** BeginHeader CommandToLCDScreen */
/*	--------------------------------------------------------------
	Function: CommandToLCDScreen
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Sends a string to the serial LCD Screen

	------------------------------------------------------------ */
void CommandToLCDScreen(unsigned char *cmd, unsigned char length);
/*** EndHeader */
void CommandToLCDScreen(unsigned char *cmd, unsigned char length)
{
   strncpy(g_LCD_outBuffer,cmd,length);

   LCD_serwrFlush();
	LCD_serrdFlush();
   LCD_serwrite(g_LCD_outBuffer, length);

}

/*** BeginHeader HandleLCDScreen */
/*	--------------------------------------------------------------
	Function: HandleLCDScreen
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Control the reset line timing of the serial LCD Screen

	------------------------------------------------------------ */
void HandleLCDScreen(void);
/*** EndHeader */
void HandleLCDScreen(void)
{

   switch(g_LCD_State)
   {
   	case LCD_STATE_RESET_START:
         BitWrPortI(PADR, &PADRShadow, 0, 2);   //Bring reset line low
   		g_LCD_timer = (MS_TIMER + 1000);
         g_LCD_State = LCD_STATE_RESET_STOP;
      	break;
      case LCD_STATE_RESET_STOP:
         if( Util_CheckmsTimeout(g_LCD_timer) )	//Wait 1 second
         {
            BitWrPortI(PADR, &PADRShadow, 1, 2);   //Bring reset line high
         	g_LCD_State = LCD_STATE_ACCEPT_COMMANDS;
         }
	     	break;
      case LCD_STATE_ACCEPT_COMMANDS:
         //Don't do anything for normal operation, data passes through serial port
       	break;
   }

}

/*** BeginHeader */
#endif // #ifndef _LCD_SCREEN_C_
/*** EndHeader */

