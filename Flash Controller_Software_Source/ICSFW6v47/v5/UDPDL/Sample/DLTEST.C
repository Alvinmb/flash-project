#define UDP_SOCKETS 4	// allow enough for downloader and DHCP
#define MAX_UDP_SOCKET_BUFFERS 4
#define USE_DHCP 1
#use dcrtcp.lib
// define this string to inlcude the RAM loader in FLASH,
// RAM loader must be unencrypted.
#define UDPDL_LOADER "p:\\shdesigns\\rabbit\\udpdwnld\\UDPDNLD.bin"

#use UDPDOWNL.lib

void main()
{
	tcp_Socket socket;
	sock_init();
	UDPDL_Init("Test app to download over");
	printf("Ready to download over me!\n");
	
	while(1)
	{
		tcp_tick(NULL);
		if (UDPDL_Tick())
			printf("Download request pending!\n");
	}
}
