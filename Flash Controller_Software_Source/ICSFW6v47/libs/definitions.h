/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Definitions.h
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	General commonly-used type definitions
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_

typedef unsigned char BOOL;
#define FALSE	0
#define TRUE	1

typedef unsigned char BYTE;

/*** EndHeader */


/*** BeginHeader */
#endif // #ifndef _DEFINITIONS_H_
/*** EndHeader */