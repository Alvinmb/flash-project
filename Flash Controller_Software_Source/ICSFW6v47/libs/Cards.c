/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Cards.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Provides Command Cards services.
	Target(s):  Rabbit 3000
	Requires:	This library uses StorageSystem library, which must
				be included first.

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _CARDS_C_
#define _CARDS_C_
/*** EndHeader */

/*** BeginHeader Cards_Init */
/*	--------------------------------------------------------------
	Function: Cards_Init
	Params:
		void
	Returns:
		BOOL - TRUE if cards library initializes properly, FALSE if
			it fails.
	--------------------------------------------------------------
	Purpose:
		Initializes the cards library.
	NOTE: Requires Storage_Init() to have been called first to
		set up the file system.
	------------------------------------------------------------ */
BOOL Cards_Init(void);

typedef enum {
	crdstate_invalid = 0,	// Cards are corrupt.
	crdstate_idle,			// Cards file handle is open and valid.
	crdstate_updating		// Cards are being updated, so the file handle should not be used.
} cardssys_state;

extern File				g_cardsfile;
extern cardssys_state	g_cards_state;
extern int				g_crdsupdatelistsize;

/*** EndHeader */

File			g_cardsfile;
cardssys_state	g_cards_state;
int				g_crdsupdatelistsize;

BOOL Cards_Init(void)
{
	g_cards_state = crdstate_invalid;
	memset(&g_cardsfile,0,sizeof(g_cardsfile));
	if(fopen_rd(&g_cardsfile, SII_CRDSFILE)){
		if(!Cards_CreateAndInitFile())
			return FALSE;
	}
	g_cards_state = crdstate_idle;
	return TRUE;
}

/*** BeginHeader Cards_UnInit */
/*	--------------------------------------------------------------
	Function: Cards_UnInit
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Undoes initialization such that Cards_Init() can be called
		again. This is used when the storage system is determined
		to be corrupt & needs formatting.
	------------------------------------------------------------ */
void Cards_UnInit(void);
/*** EndHeader */
void Cards_UnInit(void)
{
    if(crdstate_updating == g_cards_state) {
		SafeReboot_PutSemaphore();
    }
	g_cards_state = crdstate_invalid;
	fclose(&g_cardsfile);
}


/*** BeginHeader Cards_ValidateFile */
/*	--------------------------------------------------------------
	Function: Cards_ValidateFile
	Params:
		void
	Returns:
		BOOL - TRUE if file is valid. FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Determines if the Cards file was properly written or
		if it is corrupt.
	------------------------------------------------------------ */
BOOL Cards_ValidateFile(void);
/*** EndHeader */
BOOL Cards_ValidateFile(void)
{
	int tchecksum, tproperchecksum;
	// Force close the list in case it was left open by a bad update.
	Cards_ForceCloseList();
	if(g_cards_state == crdstate_invalid)
		return FALSE;
	if(!Storage_ChecksumAFile(&g_cardsfile,&tchecksum))
		return FALSE;
	if(!Storage_GetChecksum(SII_CRDSFILE,&tproperchecksum))
		return FALSE;
	if(tchecksum == tproperchecksum)
		return TRUE;
	return FALSE;
}

/*** BeginHeader Cards_CreateAndInitFile */
/*	--------------------------------------------------------------
	Function: Cards_CreateAndInitFile
	Params:
		void
	Returns:
		BOOL - TRUE if file was created successfully. FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Upon first boot or corruption of the file, this will
		create a new file.
	------------------------------------------------------------ */
BOOL Cards_CreateAndInitFile(void);
/*** EndHeader */
BOOL Cards_CreateAndInitFile(void)
{
	int i;
	if(g_cards_state == crdstate_updating)
		return FALSE;
	DebugPrint(STRACE_NFO,("Creating cards file..."));

	if(g_cards_state == crdstate_idle){
		fclose(&g_cardsfile);
	}
	g_cards_state = crdstate_invalid;
	if (fcreate(&g_cardsfile, SII_CRDSFILE)) {
		return FALSE;
	}
	fclose(&g_cardsfile);
	if(fopen_rd(&g_cardsfile, SII_CRDSFILE)) {
		return FALSE;
	}
	g_cards_state = crdstate_idle;
	return TRUE;
}

/*** BeginHeader Cards_PrepareForNewList */
/*	--------------------------------------------------------------
	Function: Cards_PrepareForNewList
	Params:
		ncards - number of cards that will be received.
	Returns:
		BOOL - TRUE if ready for new list, FALSE if something
			went wrong when creating new cards list file.
	--------------------------------------------------------------
	Purpose:
		Deletes the existing cards list and creates a fresh
		file for the new one.
	------------------------------------------------------------ */
BOOL Cards_PrepareForNewList(int ncards);
/*** EndHeader */
BOOL Cards_PrepareForNewList(int ncards)
{
	int i;
	switch(g_cards_state) {
	case crdstate_updating:	// Already updating
		SafeReboot_PutSemaphore();
        /* FALLTHROUGH */
	case crdstate_invalid:	// We couldn't open the file before, try recreating it
	case crdstate_idle:		// File is open
		SafeReboot_GetSemaphore();
		// First, close the existing file handle.
		fclose(&g_cardsfile);
		g_cards_state = crdstate_invalid;
		// Delete the existing file (don't check return value here).
		fdelete(SII_CRDSFILE);
		// Create new file.
		if(fcreate(&g_cardsfile, SII_CRDSFILE)) {
			SafeReboot_PutSemaphore();
			return FALSE;
		}
		g_crdsupdatelistsize = ncards;
		g_cards_state = crdstate_updating;
		return TRUE;
	}
	return FALSE;
}

/*** BeginHeader Cards_AddCardToList */
/*	--------------------------------------------------------------
	Function: Cards_AddCardToList
	Params:
	Returns:
		BOOL - TRUE if card was added, FALSE if something
			went wrong when writing card to file, or if
			the file wasn't prepared with a call to
			Cards_PrepareForNewList().
	--------------------------------------------------------------
	Purpose:
		Adds a card to the card list file. File must first
		be prepared with Cards_PrepareForNewList(). When the
		expected number of cards has been written, this function
		will automatically close the card file, update its
		checksum, and then re-open it for reading.
	------------------------------------------------------------ */
BOOL Cards_AddCardToList(recd_CARD_t* card);
/*** EndHeader */
BOOL Cards_AddCardToList(recd_CARD_t* card)
{
	int tchecksum;
	switch(g_cards_state) {
	case crdstate_invalid:
	case crdstate_idle:
		return FALSE;
	case crdstate_updating:
		if ( fwrite(&g_cardsfile, card, sizeof(*card)) != sizeof(*card) ) {
			DebugPrint(STRACE_ERR,("Error writing to file."));
			fclose(&g_cardsfile);
			g_cards_state = crdstate_invalid;
			SafeReboot_PutSemaphore();
			return FALSE;
		}
		if(card->cardid >= g_crdsupdatelistsize) {
			fclose(&g_cardsfile);
			g_cards_state = crdstate_invalid;
			DebugPrint(STRACE_DBG,("Found last card and closed file."));
			if(fopen_rd(&g_cardsfile, SII_CRDSFILE)) {
				DebugPrint(STRACE_ERR,("Error reopening file."));
				SafeReboot_PutSemaphore();
				return FALSE;
			}
			if(!Storage_ChecksumAFile(&g_cardsfile,&tchecksum)) {
				DebugPrint(STRACE_ERR,("Error checksumming file."));
				SafeReboot_PutSemaphore();
				return FALSE;
			}
			if(!Storage_UpdateChecksum(SII_CRDSFILE,tchecksum)) {
				DebugPrint(STRACE_ERR,("Error writing to file."));
				SafeReboot_PutSemaphore();
				return FALSE;
			}
			DebugPrint(STRACE_DBG,("Updated card checksum."));
			// Leave card file open for future fetching of cards.
			g_cards_state = crdstate_idle;
		}
		return TRUE;
	}
	return FALSE;
}

/*** BeginHeader Cards_ForceCloseList */
/*	--------------------------------------------------------------
	Function: Cards_ForceCloseList
	Params:
	Returns:
	--------------------------------------------------------------
	Purpose:
		Forces the card list file closed during an update. Can
		be used to release the semaphore if the update fails.
	------------------------------------------------------------ */
void Cards_ForceCloseList(void);
/*** EndHeader */
void Cards_ForceCloseList(void)
{
	switch(g_cards_state) {
	case crdstate_invalid:
	case crdstate_idle:
		break;
	case crdstate_updating:
		fclose(&g_cardsfile);
		g_cards_state = crdstate_invalid;
		SafeReboot_PutSemaphore();
		break;
	}
}

/*** BeginHeader Cards_GetCommandCardID */
/*	--------------------------------------------------------------
	Function: Cards_GetCommandCardID
	Params:
		scan - null terminated card scan.
		fwdToServer - pointer to receive whether the card should
			be sent to the Skoobe Server.
	Returns: 0 if scan is not a command card, otherwise the
		command card ID.
	--------------------------------------------------------------
	Purpose:
	------------------------------------------------------------ */
int Cards_GetCommandCardID(char* scan, BOOL* fwdToServer);
/*** EndHeader */
int Cards_GetCommandCardID(char* scan, BOOL* fwdToServer)
{
	recd_CARD_t tcard;
	int i;

	*fwdToServer = FALSE;
	if(g_cards_state != crdstate_idle)
		return 0;

	if (fseek(&g_cardsfile,0,SEEK_SET))
		return 0;
	//
	// We don't store the table identifier like the old firmware did,
	// so the first dynamic command card ID is 101.
	i = 101;
	while (fread(&g_cardsfile, &tcard, sizeof(tcard)) == sizeof(tcard)) {
		if (memcmp(scan,tcard.cardnumber,SII_COMMANDCARD_CMDLENGTH) == 0 ) {
			// Evidently all cards in the list sent by the server are "broadcast"
			*fwdToServer = TRUE;
			return i;
		}
		i++;
	}
	for (i = 0; i < SII_NUM_BUILTINCARDS; i++) {
 		if (strcmp(scan,sII_BuiltInCmdCards[i])==0) {
        	*fwdToServer = FALSE;
    		return (i+1);
		}
	}
	// Not a command card.
	return 0;
}

/*** BeginHeader */
#endif // #ifndef _CARDS_C_
/*** EndHeader */

