/* --------------------------------------------------------------
	Copyright (C) 2006 Ideal Software Systems. All Rights Reserved.

	Filename:	RFID.h
	By:			Scott Nortman, MindTribe Product Engineering. Modified for
				integration with Skoobe Firmware by Jerry Ryle, MindTribe
				Product Engineering
	Purpose:	Macros for RFID Library.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	04-02-2006	v1.0	<jerry@mindtribe.com>
		Split macros from rfid.c into this header file.
	Update:	01-23-2006	v1.0	<scott@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _RFID_H_
#define _RFID_H_
/*** EndHeader */

/*** BeginHeader */

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SERIAL_MS_DELAY
	Desc:	This is the amount of time, in ms, that the host should wait
			before checking for a received response from the RFID reader.
	Notes:	This value was determined experimentally.
	---------------------------------------------------------------------------- */
#define RFID_SERIAL_MS_DELAY						( 250 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SERIAL_MS_RETRY
	Desc:	This is the amount of time, in ms, that the host should wait
			between RFID reader polls.
	---------------------------------------------------------------------------- */
#define RFID_SERIAL_MS_RETRY						( 50 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SERIAL_BYTE_TIMEOUT
	Desc:	The delay timeout, in ms, between received characters.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_SERIAL_BYTE_TIMEOUT					( 10 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_MAX_BYTES
	Desc:	This macro determines how much space is allocated to the outgoing
			packet array.
	Notes:	This max value is 128 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define	RFID_PACKET_MAX_BYTES						( RFID_SERIAL_BUFFER_SIZE - 8 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_SOF
	Desc:	This is the Start Of Frame value for the RFID serial packet.
	Notes:	This value should be 0x01 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_SOF								( 0x01 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_SOF_OFFSET
	Desc:	This is the packet offset for the Start Of Frame value for the
			RFID serial packet.
	Notes:	This value should be 0 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_SOF_OFFSET						( 0 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_LENGTH_LSB_OFFSET
	Desc:		This is the packet offset of the Length LSB for the RFID serial
				packet.
	Notes:	This value should be 1 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_LENGTH_LSB_OFFSET				( 1 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_LENGTH_MSB_OFFSET
	Desc:	This is the packet offset of the Length MSB for the RFID serial
			packet.
	Notes:	This value should be 2 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_LENGTH_MSB_OFFSET				( 2 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_DEVICE_ID
	Desc:	This is the Device ID value for the RFID serial packet.
	Notes:	This value should be 0x03 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_DEVICE_ID						( 0x03 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_DEVICE_ID_OFFSET
	Desc:	This is the packet offset of the Device ID value for the RFID
			serial packet.
	Notes:	This value should be 3 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_DEVICE_ID_OFFSET				( 3 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_CMD1_OFFSET
	Desc:	This is the packet offset of the CMD1 value for the RFID serial
			packet.
	Notes:	This value should be 4 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_CMD1_OFFSET						( 4 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD1_APPLICATION_LAYER
	Desc:    Indicates that the CMD2 value is directed at the application
				layer.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD1_APPLICATION_LAYER    				( 0x01 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD1_ISO14443A_LAYER
	Desc:	Indicates that the CMD2 value is directed at the ISO14443A layer.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD1_ISO14443A_LAYER					( 0x02 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD1_ISO14443B_LAYER
	Desc:	Indicates that the CMD2 value is directed at the ISO14443B layer.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD1_ISO14443B_LAYER					( 0x03 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD1_ISO15693_LAYER
	Desc:    Indicates that the CMD2 value is directed at the ISO15693 layer.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD1_ISO15693_LAYER    				( 0x04 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_PACKET_CMD2_OFFSET
	Desc:	This is the packet offset of the CMD2 value for the RFID serial
			packet.
	Notes:	This value should be 5 as per the Application Protocol Reference
	---------------------------------------------------------------------------- */
#define RFID_PACKET_CMD2_OFFSET				 		( 5 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_TX_PACKET_DATA_OFFSET
	Desc:    Array index of RFID_TxBuffer which indicates the start of the data.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_TX_PACKET_DATA_OFFSET			 		( 6 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_RX_PACKET_DATA_OFFSET
	Desc:    Array index of RFID_RxBuffer which indicates the start of the data.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_RX_PACKET_DATA_OFFSET			 		( 7 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_RX_PACKET_STATUS_OFFSET
	Desc:    Array index which indicates the received status byte location.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_RX_PACKET_STATUS_OFFSET		 		( 6 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_RX_PACKET_ISO15693_ID_OFFSET
	Desc:    Array index which indicates the LSB of the 8 byte unique value.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_RX_PACKET_ISO15693_ID_OFFSET			( 10 )


/*	-----------------------------------------------------------------------------
	Macro:	RFID_ISO15693_ID_SIZE
	Desc:	Size of the unique ID of an ISO 15693 packet
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ISO15693_ID_SIZE						( 8 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_RX_PACKET_ISO15693_DATABLOCK_OFFSET
	Desc:    Array index which indicates the beginning of the 4 byte data block
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_RX_PACKET_ISO15693_DATABLOCK_OFFSET	( 9 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ISO15693_DATABLOCK_SIZE
	Desc:	Size of a data block in an ISO 15693 tag
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ISO15693_DATABLOCK_SIZE				( 4 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_DATA_MAX_BYTES
	Desc:    Maximum allowed number of data bytes in an RFID communication
				packet.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_DATA_MAX_BYTES					  		( RFID_SERIAL_BUFFER_SIZE - 6 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_DWNLD_MODE
	Desc:    CMD2 value requesting to out the RFID module into download mode.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_DWNLD_MODE						( 0x20 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_GET_VERSION
	Desc:    CMD2 value requesting the RFID version information.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_GET_VERSION						( 0x40 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_FIND_TOKEN
	Desc:    CMD2 value requesting that the RFID module look for the tokens as
				listed in the token priority table.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_FIND_TOKEN						( 0x41 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_TOKEN_PRIORITY
	Desc:    CMD2 value allowing the user to set the token priority table.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_TOKEN_PRIORITY					( 0x42 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_READ_SINGLE_BLOCK
	Desc:    CMD2 value allowing the user to read a block of data
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_READ_SINGLE_BLOCK					( 0x65 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_TOKEN_TYPE_15693
	Desc:    CMD2 value indicated that the RFID module should look for an
				ISO15693 token.
	Notes:   The SKOOBE RFID bands are ISO15693, and therefore use this value
				when indicating the type of token to find.
	---------------------------------------------------------------------------- */
#define RFID_TOKEN_TYPE_15693						( 0x04 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_PASS_THROUGH
	Desc:    CMD2 value requesting a pass through command.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_PASS_THROUGH						( 0x45 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_SET_BAUD_RATE
	Desc:    CMD2 value requesting a baud rate change.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_SET_BAUD_RATE						( 0x46 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SET_BAUD_9600
	Desc:    Data byte for RFID_CMD2_SET_BAUD_RATE requesting 9600 bps
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_SET_BAUD_9600							( 0 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SET_BAUD_19200
	Desc:    Data byte for RFID_CMD2_SET_BAUD_RATE requesting 19200 bps
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_SET_BAUD_19200							( 1 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SET_BAUD_38400
	Desc:    Data byte for RFID_CMD2_SET_BAUD_RATE requesting 38400 bps
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_SET_BAUD_38400							( 4 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SET_BAUD_57600
	Desc:    Data byte for RFID_CMD2_SET_BAUD_RATE requesting 57600 bps
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_SET_BAUD_57600							( 2 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_SET_BAUD_115200
	Desc:    Data byte for RFID_CMD2_SET_BAUD_RATE requesting 115200 bps
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_SET_BAUD_115200						( 3 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_TRANSMITTER_ON
	Desc:    CMD2 value requesting that the RF antenna system be turned on.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_TRANSMITTER_ON					( 0x48 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_CMD2_TRANSMITTER_OFF
	Desc:    CMD2 value requesting that the RF antenna system be turned off.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_CMD2_TRANSMITTER_OFF					( 0x49 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_NONE
	Desc:    Error code indicating no error occurred.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_NONE								( 0x00 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_TOKEN_NOT_PRESENT
	Desc:    Error code indicating that no token was found.
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_TOKEN_NOT_PRESENT				( 0x01 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_INVALID_RF_FORMAT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_RF_FORMAT				( 0x05 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_DEVICE_ID_INVALID
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_DEVICE_ID_INVALID				( 0x0E )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_ILLEGAL_ACTION
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_ILLEGAL_ACTION					( 0x10 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_WRONG_DOWNLOAD_STATE
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_WRONG_DOWNLOAD_STATE				( 0x11 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_WRITE_FAILED
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_WRITE_FAILED						( 0x12 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_INVALID_ADDRESS
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_ADDRESS					( 0x13 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_INVALID_BAUD
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_BAUD						( 0x14 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_INVALID_CHECK_DIGITS
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_CHECK_DIGITS				( 0x15 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_NO_TIMER_AVAILABLE
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_NO_TIMER_AVAILABLE				( 0x16 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_INVALID_ENTITY_ID
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_ENTITY_ID				( 0x17 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_DATA_TRUNCATED
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_DATA_TRUNCATED					( 0x18 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_NO_DATA_READ						( 0x19 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_START_BYTE				( 0x1A )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_INVALID_CRC						( 0x1B )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_CMD_REPLY_MISMATCH				( 0x1C )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_A_DATA_INCORRECT			( 0x20 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_B_DATA_INCORRECT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_B_DATA_INCORRECT			( 0x21 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_B_TOKEN_NOT_FOUND
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_B_TOKEN_NOT_FOUND			( 0x22 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_RECEIVE_TIMEOUT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_RECEIVE_TIMEOUT			( 0x23 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_ABORTED
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_ABORTED					( 0x24 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_ATQB_ERR1
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_ATQB_ERR1				( 0x25 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_ATQB_PROT_TYPE
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_ATQB_PROT_TYPE			( 0x26 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_INVALID_CID
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_INVALID_CID				( 0x27 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_INVALID_NAD
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_INVALID_NAD				( 0x28 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_CID_LOW_POWER
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_CID_LOW_POWER			( 0x29 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_B_HLTB_ERROR
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_B_HLTB_ERROR				( 0x2A )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_B_INVALID_BLK_TYPE
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_B_INVALID_BLK_TYPE			( 0x2B )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_NOT_IBLOCK
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_NOT_IBLOCK				( 0x2C )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_HF_ASIC_NOT_RBLOCK
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_HF_ASIC_NOT_RBLOCK				( 0x2D )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_B_SDESELECT_ERROR
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_B_SDESELECT_ERROR			( 0x2E )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_DATA_INCORRECT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_DATA_INCORRECT				( 0x2F )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_MANY_CID_NO_SUPRT_TRANSPONDERS
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_MANY_CID_NO_SUPRT_TRANSPONDERS	( 0x30 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_COLISN_BPSK_AND_OR_CID
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_COLISN_BPSK_AND_OR_CID			( 0x31 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_COLISN_BPSK_DECODE
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_COLISN_BPSK_DECODE				( 0x32 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_B_ABORTED
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_B_ABORTED					( 0x33 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_TOKEN_BUFFER_FULL
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_TOKEN_BUFFER_FULL				( 0x34 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_A_UPLINK_PARITY
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_A_UPLINK_PARITY			( 0x35 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_A_ATS
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_A_ATS						( 0x36 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_A_PPS
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_A_PPS						( 0x37 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_A_CASCADE_LEVEL
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_A_CASCADE_LEVEL			( 0x38 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_14443_A_SAK_CRC
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_14443_A_SAK_CRC					( 0x39 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_NO_ERROR
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_NO_ERROR					( 0x40 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_BAD_FRAME_WAIT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_BAD_FRAME_WAIT				( 0x41 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_BAD_VAL_BAUD
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_BAD_VAL_BAUD				( 0x42 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_CANCELLED
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_CANCELLED					( 0x43 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_SUBCARRIER
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_SUBCARRIER					( 0x44 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_TR0_TIMEOUT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_TR0_TIMEOUT					( 0x45 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_RCV_OVERFLOW
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_RCV_OVERFLOW				( 0x46 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_NO_SOF
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_NO_SOF						( 0x47 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_NO_EOF
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_NO_EOF						( 0x48 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_TR1_TIMEOUT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_TR1_TIMEOUT					( 0x49 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_CRC_ERROR
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_CRC_ERROR					( 0x4A )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_BPSK_FRAME_ERROR
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_BPSK_FRAME_ERROR					( 0x4B )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_MODULE_ABORTED
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_MODULE_ABORTED					( 0x4C )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_PARAMETER_ERROR
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_PARAMETER_ERROR					( 0x4D )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_COLLISION_DETECT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_COLLISION_DETECT					( 0x57 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_APOLLO_LIFE_CYCLE
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_APOLLO_LIFE_CYCLE				( 0x60 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_APOLLO_DATA_INCORRECT
	Desc:
	Notes:
	---------------------------------------------------------------------------- */
#define RFID_ERROR_APOLLO_DATA_INCORRECT			( 0x61 )

/*	-----------------------------------------------------------------------------
	Macro:	RFID_ERROR_UNDEFINED
	Desc:	This value is not transmitted by the RFID module.  It is used
			internally as a return value.
	Notes:	This error code is not defined in the TI RFID Base Application
			Protocol Reference guide.  It is used if there is an undefined
			error detected.
	---------------------------------------------------------------------------- */
#define RFID_ERROR_UNDEFINED						( 0xFF )
/*** EndHeader */

/*** BeginHeader */
#endif // #ifndef _RFID_H_
/*** EndHeader */
