/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	LCD_LM4052.c
	By:			Scott Nortman, MindTribe Product Engineering
	Purpose:	Contains functions for writing to the character LCD.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v2.0	<jerry@mindtribe.com>
		Unified API and comment style.
	Update:	04-05-2005	v1.1	<scott@mindtribe.com>
		First working version.
	Update:	04-01-2005	v1.0	<scott@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _LCD_LM4052_C_
#define _LCD_LM4052_C_
/*** EndHeader */

/*** BeginHeader */
//Hardware references
#define LCD_BUS_OUTPUT	(WrPortI(SPCR, &SPCRShadow, 0x84))
#define LCD_BUS_INPUT	(WrPortI(SPCR, &SPCRShadow, 0x80))
#define LCD_BUS_DR		PADR
#define LCD_BUS_DRSH	PADRShadow
#define LCD_CNTL_DDR	PBDDR
#define LCD_CNTL_DDRSH	PBDDRShadow
#define LCD_CNTL_DR		PBDR
#define LCD_CNTL_DRSH	PBDRShadow
#define LCD_CNTL_RS		2
#define LCD_CNTL_RDWR	3
#define LCD_CNTL_E		4
#define LCD_DLY_CNTS	40
#define LCD_ROW_MIN		0 	// Limits are for a 2x20 display
#define LCD_ROW_MAX		1	// change accordingly
#define LCD_COL_MIN		0
#define LCD_COL_MAX		19

//LCD Commands
#define LCD_REG_CMD		0
#define LCD_REG_DATA	1
#define LCD_RDWR_WR		0
#define LCD_RDWR_RD		1
#define LCD_CMD_CLS		0x01	//Clears display, sends cursor home
#define LCD_CMD_FNCT	0x3B	//Sets 8 bit data, 1/16 duty, 5x8 dots
#define LCD_CMD_MODE	0x06	//Inc. data address after each wite
#define LCD_CMD_DISPCTL	0x08	//Disp. on, blink and cursor off
#define		LCD_DISP_ON		0x04
#define		LCD_DISP_OFF	0x00

#define LCD_STAT_BF		7
/*** EndHeader */

/*** BeginHeader LCD_LM4052_Init */
/*	--------------------------------------------------------------
	Function: LCD_LM4052_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the LCD Library. Clears screen and homes
		cursor to (0, 0) position.
	Notes: Uses PORTA as a bi-directional IO bus.
		Sets PORTB, pins 2, 3, 4 as outputs.
			PORTA[7:0] <=> LCD 8 Bit data bus
			PORTB[2]    => LCD RS
			PORTB[3]    => LCD RD, /WR
			PORTB[4]    => LCD E
	------------------------------------------------------------ */
void LCD_LM4052_Init(void);
/*** EndHeader */

void LCD_LM4052_Init(void)
{
	//Set PORTB DDR
	WrPortI(LCD_CNTL_DDR, &LCD_CNTL_DDRSH, LCD_CNTL_DDRSH|0x1C);
	//Set E line low
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, FALSE, LCD_CNTL_E);
	//Set RD /WR low
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, FALSE, LCD_CNTL_RDWR);
	//Set RS low
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, FALSE, LCD_CNTL_RS);

	//Initialize the display module
	//Delay > 15ms
	Util_Delay(50);
	//Write command byte (execute 4 times as recommended in datasheet)
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_FNCT);
//	Util_Delay(2);
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_FNCT);
//	Util_Delay(2);
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_FNCT);
//	Util_Delay(2);
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_FNCT);
//	Util_Delay(2);
	//Write additional init. commands
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_DISPCTL|LCD_DISP_ON);
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_MODE);
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_CLS);
//	Util_Delay(2);
	//Display should be initialized and ready for use
}

/*** BeginHeader LCD_LM4052_Write */
/*	--------------------------------------------------------------
	Function: LCD_LM4052_Write
	Params:
		reg - 0=instruction register, 1=data register
		tbyte - data to write
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Writes a byte of data to the LCD as an instruction or data.
	------------------------------------------------------------ */
void LCD_LM4052_Write(BYTE reg, BYTE tbyte);
/*** EndHeader */
void LCD_LM4052_Write(BYTE reg, BYTE tbyte)
{
	//Select CMD or DATA register
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, reg, LCD_CNTL_RS);
	//Set RD /WR for write operation
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, LCD_RDWR_WR, LCD_CNTL_RDWR);
	//Set LCD_DATA_DDR as output
	LCD_BUS_OUTPUT;
	//Write byte to LCD_BUS_DR
	WrPortI(LCD_BUS_DR, NULL, tbyte);
	//Assert E
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, TRUE, LCD_CNTL_E);
	// a short delay
#asm
	nop
#endasm
	//Deassert E
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, FALSE, LCD_CNTL_E);
	//Block while busy flag is asserted
	while(  LCD_LM4052_Read(LCD_REG_CMD) & (1<<LCD_STAT_BF) ){};
}

/*** BeginHeader LCD_LM4052_Read */
BYTE LCD_LM4052_Read(BYTE reg);
/*** EndHeader */
BYTE LCD_LM4052_Read(BYTE reg)
{
/*	Desc:		Reads the 8 bit value from the inst. or data register
*	Args:    BYTE reg	   => 0 indicates instruction register
*										1 indicates data register
*  Returns:	BYTE, 8 bit value read from register
*	PreReq:	LCD_LM4052_Init() must be called prior to this function.
*	Globals:	None.
*	Notes:	Changes the data direction of LCD_DATA_DDR
*/
	BYTE temp;

	//Select CMD or DATA register
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, reg, LCD_CNTL_RS);

	//Set RD /WR for read operation
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, LCD_RDWR_RD, LCD_CNTL_RDWR);

	//Set LCD_DATA_DDR as input
	LCD_BUS_INPUT;

	//Assert E
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, TRUE, LCD_CNTL_E);
#asm
		nop
		nop
#endasm

	//Grab data
	temp = (BYTE)RdPortI(LCD_BUS_DR);

	//Deassert E
	BitWrPortI(LCD_CNTL_DR, &LCD_CNTL_DRSH, FALSE, LCD_CNTL_E);

	//Return data
	return temp;
}

/*** BeginHeader LCD_LM4052_DispChar */
void LCD_LM4052_DispChar(BYTE row, BYTE col, char c);
/*** EndHeader */
void LCD_LM4052_DispChar(BYTE row, BYTE col, char c)
{
/*	Desc:		Displays a character at a specified location.
*	Args:    BYTE	row, 0 or 1 indicating which row to write to
*				BYTE	col, 0 through 19 indicating column
*				char c, character to write at location
*  Returns:	None.
*	PreReq:	LCD_Init() must be called prior to calling this function.
*	Globals: None.
*	Notes:	This funciton blocks.
*/

	//First perform limit checks
	//row
	if (row < LCD_ROW_MIN)
		row = LCD_ROW_MIN;
	else if (row > LCD_ROW_MAX)
		row = LCD_ROW_MAX;

	//col
	if (col < LCD_COL_MIN)
		col = LCD_COL_MIN;
	else if(col > LCD_COL_MAX)
		col = LCD_COL_MAX;

	//Set the position of the cursor
	switch(row){
	case 0:
		LCD_LM4052_Write(LCD_REG_CMD, (0x80 + col));
		break;
	case 1:
		LCD_LM4052_Write(LCD_REG_CMD, (0xC0 + col));
		break;
	default:
		break;
	}

	//Write character
	LCD_LM4052_Write(LCD_REG_DATA, c);
}

/*** BeginHeader LCD_LM4052_GotoXY */
void LCD_LM4052_GotoXY(BYTE X, BYTE Y);
/*** EndHeader */
void LCD_LM4052_GotoXY(BYTE X, BYTE Y)
{
	if (Y < LCD_ROW_MIN)
		Y = LCD_ROW_MIN;
	else if (Y > LCD_ROW_MAX)
		Y = LCD_ROW_MAX;
	if (X < LCD_COL_MIN)
		X = LCD_COL_MIN;
	else if(X > LCD_COL_MAX)
		X = LCD_COL_MAX;

	//Set the position of the cursor
	if(Y == 0) {
		LCD_LM4052_Write(LCD_REG_CMD, (0x80 + X));
	} else {
		LCD_LM4052_Write(LCD_REG_CMD, (0xC0 + X));
	}
}

/*** BeginHeader LCD_LM4052_ClrLine */
void LCD_LM4052_ClrLine(BYTE Y);
/*** EndHeader */
void LCD_LM4052_ClrLine(BYTE Y)
{
	//Local variables
	BYTE temp;

	//First perform limit checks
	//row
	if (Y < LCD_ROW_MIN)
		Y = LCD_ROW_MIN;
	else if (Y > LCD_ROW_MAX)
		Y = LCD_ROW_MAX;

	LCD_LM4052_GotoXY(0,Y);
	//Function Implementation
	for(temp = 0; temp <= LCD_COL_MAX; temp++)
		LCD_LM4052_OutChar(' ');
}


/*** BeginHeader LCD_LM4052_OutChar */
void LCD_LM4052_OutChar(char c);
/*** EndHeader */
void LCD_LM4052_OutChar(char c)
{
	LCD_LM4052_Write(LCD_REG_DATA, c);
}

/*** BeginHeader LCD_LM4052_ClrScr */
void LCD_LM4052_ClrScr(void);
/*** EndHeader */
void LCD_LM4052_ClrScr(void)
{
/*	Desc:		Clear the entire (ie, both lines) LCD
*	Args:    None.
*  Returns:	None.
*	PreReq:	LCD_LM4052_Init() must be called prior to calling this function
*	Globals:	None.
*	Notes:	Clears the screen with the LCD_CMD_CLS
*/
	//Write clear screen command
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_CLS);
	//Delay as specified in datasheet
//	Util_Delay(2);
}

/*** BeginHeader LCD_LM4052_DispStr */
void LCD_LM4052_DispStr(BYTE row, BYTE col, char *s);
/*** EndHeader */
void LCD_LM4052_DispStr(BYTE row, BYTE col, char *s)
{
/*	Desc:		Displays a string, passed as a pointer, at the location
*				specified by the row, col arguments.
*	Args:    BYTE row	=> 0 or 1, indicating what row string should be witten
*				BYTE col => 0 through 19, indicating the start column
*  Returns:	None.
*	PreReq:  LCD_LM4052_Init() must be called prior to calling this function
*	Globals:	None.
*	Notes:	String must be null terminated, function does NOT check to make sure
*				the entire string will fit on a line.
*/

	BYTE colTemp, index;

	//First perform limit checks
	//row
	if (row < LCD_ROW_MIN)
		row = LCD_ROW_MIN;
	else if (row > LCD_ROW_MAX)
		row = LCD_ROW_MAX;

	//col
	if (col < LCD_COL_MIN)
		col = LCD_COL_MIN;
	else if (col > LCD_COL_MAX)
		col = LCD_COL_MAX;

	//Assign passed values to temps
	colTemp	= col;
	index	= 0;
	while( (*s != (char)NULL) && (colTemp <= LCD_COL_MAX)) {
		LCD_LM4052_DispChar(row, colTemp++, *s++);
	}
}

/*** BeginHeader */
#endif // #ifndef _LCD_LM4052_C_
/*** EndHeader */

