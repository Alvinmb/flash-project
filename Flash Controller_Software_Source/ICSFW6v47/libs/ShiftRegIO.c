/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	ShiftRegIO.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Library of functions for accessing the I/O attached to shift registers.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _SHIFTREGIO_C_
#define _SHIFTREGIO_C_
/*** EndHeader */

/*** BeginHeader SRIO_Init */
/*	--------------------------------------------------------------
	Function: SRIO_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the Shift Register IO library. Must be called
		before other "SRIO_" functions can be used.
	------------------------------------------------------------ */
#define SKOOBESPI_SER_C
#define SKOOBESPI_CLOCK_MODE		01
#define SKOOBESPI_CLOCK_DIVISOR  	10
#use "SkoobeSPI.c"

#define SRIO_OUTPUT_OE		4
#define SRIO_OUTPUT_STROBE	5
#define SRIO_INPUT_LATCH	7

extern unsigned char g_SRIO_outshadow;

void SRIO_Init(void);
/*** EndHeader */
unsigned char g_SRIO_outshadow;
void SRIO_Init(void)
{
	SKOOBESPIinit();

	// De-assert the various latches and enables.
	BitWrPortI(PEDR, &PEDRShadow, 0, SRIO_OUTPUT_OE);		// Output OE low to de-assert
	BitWrPortI(PEDR, &PEDRShadow, 0, SRIO_OUTPUT_STROBE);	// Input Strobe low to de-assert
	BitWrPortI(PEDR, &PEDRShadow, 1, SRIO_INPUT_LATCH);		// Input latch high to de-assert

	// Set various latches and enables to outputs.
	BitWrPortI(PEDDR, &PEDDRShadow, 1, SRIO_OUTPUT_OE);
	BitWrPortI(PEDDR, &PEDDRShadow, 1, SRIO_OUTPUT_STROBE);
	BitWrPortI(PEDDR, &PEDDRShadow, 1, SRIO_INPUT_LATCH);
	SRIO_OutputWrite(0);
}

/*** BeginHeader SRIO_InputRead */
/*	--------------------------------------------------------------
	Function: SRIO_InputRead
	Params:
		void
	Returns:
		unsigned char - 8 bits representing the 8 inputs.
	--------------------------------------------------------------
	Purpose:
		Reads the 8 inputs attached to the input shift register.
	------------------------------------------------------------ */
unsigned char SRIO_InputRead(void);
/*** EndHeader */
unsigned char SRIO_InputRead(void)
{
	unsigned char tdata;
	tdata = 0;
	// Pulse the latch to latch the inputs to the input shift register's buffer.
	BitWrPortI(PEDR, &PEDRShadow, 0, SRIO_INPUT_LATCH);		// Input latch low to assert
	BitWrPortI(PEDR, &PEDRShadow, 1, SRIO_INPUT_LATCH);		// Input latch high to de-assert
	SKOOBESPIRead(&tdata,1);
	return (tdata ^ 0xFF);
}

/*** BeginHeader SRIO_OutputWrite */
/*	--------------------------------------------------------------
	Function: SRIO_OutputWrite
	Params:
		tdata - 8 bits to write to the 8 outputs.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets the state of the 8 outputs attached to the output
		shift register.
	------------------------------------------------------------ */
void SRIO_OutputWrite(unsigned char tdata);
/*** EndHeader */
void SRIO_OutputWrite(unsigned char tdata)
{
	g_SRIO_outshadow = tdata;
	SKOOBESPIWrite(&tdata,1);
	// Pulse the strobe to latch the data to the output lines.
	BitWrPortI(PEDR, &PEDRShadow, 1, SRIO_OUTPUT_STROBE);		// Output Strobe high to assert
	BitWrPortI(PEDR, &PEDRShadow, 0, SRIO_OUTPUT_STROBE);		// Output Strobe low to de-assert
}

/*** BeginHeader SRIO_OutputSetBit */
/*	--------------------------------------------------------------
	Function: SRIO_OutputSetBit
	Params:
		bit - 0-7, the bit to set.
		bstate - 0/1, state to set the bit as.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Sets the state of 1 output attached to the output
		shift register.
	------------------------------------------------------------ */
void SRIO_OutputSetBit(char bit, char bstate);
/*** EndHeader */
void SRIO_OutputSetBit(char bit, char bstate)
{
	bit &= 0x07;

	if(bstate)
		g_SRIO_outshadow |= (0x01 << bit);
	else 
		g_SRIO_outshadow &= ~(0x01 << bit);

	SKOOBESPIWrite(&g_SRIO_outshadow,1);
	// Pulse the strobe to latch the data to the output lines.
	BitWrPortI(PEDR, &PEDRShadow, 1, SRIO_OUTPUT_STROBE);		// Output Strobe high to assert
	BitWrPortI(PEDR, &PEDRShadow, 0, SRIO_OUTPUT_STROBE);		// Output Strobe low to de-assert
}

/*** BeginHeader SRIO_OutputEnable */
/*	--------------------------------------------------------------
	Function: SRIO_OutputEnable
	Params:
		EnableOutpus - TRUE to drive the outputs, FALSE to
			tristate the outputs.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Allows the outputs to be driven or tristated.
	------------------------------------------------------------ */
void SRIO_OutputEnable(BOOL EnableOutputs);
/*** EndHeader */
void SRIO_OutputEnable(BOOL EnableOutputs)
{
	if(EnableOutputs)
		BitWrPortI(PEDR, &PEDRShadow, 1, SRIO_OUTPUT_OE);		// Output OE high to assert
	else
		BitWrPortI(PEDR, &PEDRShadow, 0, SRIO_OUTPUT_OE);		// Output OE low to de-assert
}

/*** BeginHeader */
#endif // #ifndef _SHIFTREGIO_C_
/*** EndHeader */