/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Button.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Cool pulsing button stuff.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _BUTTON_C_
#define _BUTTON_C_

#define BUTTON_INPUTPIN		1	// Button is on PORTC[1].
#define BUTTON_PWMCHANNEL	0	// Button is on PWM channel 0.
#define BUTTON_PWM_BASE		500	// Use 500Hz base frequency for PWM
/*** EndHeader */

/*** BeginHeader Button_Init */
/*	--------------------------------------------------------------
	Function: Button_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the button library.
	------------------------------------------------------------ */
void Button_Init(void);

typedef	enum {
	button_off = 0,
	button_on,
	button_atintensity,
	button_fading,
	button_flashing
} buttoncontextstate;

typedef struct {
	buttoncontextstate	contextstate;
	buttoncontextstate	lastcontextstate;	// Holds the context state while flashing the button.
	// Fading:
	int				buttonFadeCounter;
	int				buttonFadeDirection;
	BOOL			buttonFadeStop;
	// Intensity:
	char			buttonIntensity;
	// Flashing:
	BOOL			buttonOn;
	unsigned long	buttonTimer;
	unsigned long	buttonTimeOn;
	unsigned long	buttonTimeOff;
	unsigned char	buttonFlashCount;
} ButtonContext;

extern ButtonContext g_button_context;

/*** EndHeader */
ButtonContext g_button_context;
void Button_Init(void)
{
	// Button input is on PORTC[1], which is always an input. no register setup needed.

	// Make PORTF[4] act as I/O.
	BitWrPortI(PFFR, &PFFRShadow, 0, 4+BUTTON_PWMCHANNEL);
	// Set PORTF[4] low (turn off button LED).
	BitWrPortI(PFDR, &PFDRShadow, 0, 4+BUTTON_PWMCHANNEL);
	// Make PORTF[4] an output.
	BitWrPortI(PFDDR, &PFDDRShadow, 1, 4+BUTTON_PWMCHANNEL);

	memset(&g_button_context,0,sizeof(g_button_context));
}

/*** BeginHeader Button_Handler */
/*	--------------------------------------------------------------
	Function: Button_Handler
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Turns on/off button LED.
	------------------------------------------------------------ */
void Button_Handler(void);
/*** EndHeader */
void Button_Handler(void)
{
	if(g_button_context.contextstate == button_flashing) {
		if(g_button_context.buttonOn) {
			if(Util_CheckmsTimeout(g_button_context.buttonTimer)) {
				BitWrPortI(PFDR, &PFDRShadow, 0, BUTTON_PWMCHANNEL + 4); //always low
				g_button_context.buttonTimer = MS_TIMER+g_button_context.buttonTimeOff;
				g_button_context.buttonOn = FALSE;
			}
		} else {
			if(Util_CheckmsTimeout(g_button_context.buttonTimer)) {
				BitWrPortI(PFDR, &PFDRShadow, 1, BUTTON_PWMCHANNEL + 4); //always high
				g_button_context.buttonTimer = MS_TIMER+g_button_context.buttonTimeOn;
				g_button_context.buttonOn = TRUE;
				g_button_context.buttonFlashCount--;
				// Restore the context;
				if(g_button_context.buttonFlashCount == 0) {
					g_button_context.contextstate = g_button_context.lastcontextstate;
					switch(g_button_context.contextstate) {
					case button_off:
						Button_LEDState(buttonLED_off);
						break;
					case button_on:
						Button_LEDState(buttonLED_on);
						break;
					case button_atintensity:
						Button_Intensity(g_button_context.buttonIntensity);
						break;
					case button_fading:
						// Fading holds its state. It just needs to be re-enabled.
						BitWrPortI(PFFR, &PFFRShadow, 1, BUTTON_PWMCHANNEL + 4); // PWM output
						ISR_EnableFade(TRUE);
						break;
					}
				}
			}
		}
	}
}

/*** BeginHeader Button_Flash */
/*	--------------------------------------------------------------
	Function: Button_Flash
	Params:
		flashon - time in milliseconds the button should be on
			when flashing
		flashoff - time in milliseconds the button should be off
			when flashing
		counts - number of times to flash the button.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Flashes the button while storing the state of whatever
		the button was previously doing. This is used for a brief
		attention grab. For example, you should flash the button
		when a card is successfully swiped.
	------------------------------------------------------------ */
void Button_Flash(unsigned long flashon, unsigned long flashoff, unsigned char counts);
/*** EndHeader */
void Button_Flash(unsigned long flashon, unsigned long flashoff, unsigned char counts)
{
	if(counts == 0)
		return;
	// If we're already flashing, just override the settings. Don't store the state.
	if(g_button_context.contextstate != button_flashing) {
		g_button_context.lastcontextstate = g_button_context.contextstate;
	}
	g_button_context.contextstate = button_flashing;
	g_button_context.buttonOn = TRUE;
	g_button_context.buttonTimeOff = flashoff;
	g_button_context.buttonTimeOn = flashoff;
	g_button_context.buttonFlashCount = counts;
	// Stop any fading
	ISR_EnableFade(FALSE);
	// Turn button on.
	BitWrPortI(PFDR, &PFDRShadow, 1, BUTTON_PWMCHANNEL + 4); //always high
	BitWrPortI(PFFR, &PFFRShadow, 0, BUTTON_PWMCHANNEL + 4); //normal output (turn off PWM)
	g_button_context.buttonTimer = MS_TIMER+flashon;
}


/*** BeginHeader Button_isDown */
/*	--------------------------------------------------------------
	Function: Button_isDown
	Params:
		void
	Returns:
		BOOL - TRUE if button is currently held down.
	--------------------------------------------------------------
	Purpose:
		Turns on/off button LED.
	------------------------------------------------------------ */
BOOL Button_isDown(void);
/*** EndHeader */
BOOL Button_isDown(void)
{
	BitRdPortI(PCDR, BUTTON_INPUTPIN);
}


/*** BeginHeader Button_LEDState */
/*	--------------------------------------------------------------
	Function: Button_LEDState
	Params:
		state - 0 = turn LED off, buttonLED_on = turn LED on.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Turns on/off button LED.
	------------------------------------------------------------ */
typedef enum {
	buttonLED_off = 0,
	buttonLED_on
} buttonLEDstate;
void Button_LEDState(buttonLEDstate state);
/*** EndHeader */
void Button_LEDState(buttonLEDstate state)
{
	_button_StopFade();
	if(buttonLED_on == state) {
		g_button_context.contextstate = button_on;
		BitWrPortI(PFDR, &PFDRShadow, 1, BUTTON_PWMCHANNEL + 4); //always high
	} else {
		g_button_context.contextstate = button_off;
		BitWrPortI(PFDR, &PFDRShadow, 0, BUTTON_PWMCHANNEL + 4); //always low
	}
	BitWrPortI(PFFR, &PFFRShadow, 0, BUTTON_PWMCHANNEL + 4); //normal output (turn off PWM)
}

/*** BeginHeader Button_Intensity */
/*	--------------------------------------------------------------
	Function: Button_Intensity
	Params:
		val - 0-100, button brightness in percent. (0 = full off,
			1024 = full on)
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Turns on/off button LED.
	------------------------------------------------------------ */
void Button_Intensity(char percent);
/*** EndHeader */
void Button_Intensity(char percent)
{
	short duty;
	_button_StopFade();
	g_button_context.buttonIntensity = percent;
	g_button_context.contextstate = button_atintensity;
	if (percent <= 0) {
		BitWrPortI(PFDR, &PFDRShadow, 0, BUTTON_PWMCHANNEL + 4); //always low
		BitWrPortI(PFFR, &PFFRShadow, 0, BUTTON_PWMCHANNEL + 4); //normal output
	} else if (percent >= 100) {
		BitWrPortI(PFDR, &PFDRShadow, 1, BUTTON_PWMCHANNEL + 4); //always high
		BitWrPortI(PFFR, &PFFRShadow, 0, BUTTON_PWMCHANNEL + 4); //normal output
	} else {
		duty = (short)(1023*((float)percent/100.0));
		WrPortI(PWM0R + (2*BUTTON_PWMCHANNEL), NULL, (duty >> 2));
		WrPortI(PWL0R + (2*BUTTON_PWMCHANNEL), NULL, (duty << 6));
		BitWrPortI(PFFR, &PFFRShadow, 1, BUTTON_PWMCHANNEL + 4); // PWM output
	}
}

/*** BeginHeader Button_StartFade */
/*	--------------------------------------------------------------
	Function: Button_StartFade
	Params:
		faderate - 0-100, fade rate. 0 = slowest, 100 = fastest
		startFademode - determines initial fade mode:
			buttonfade_start_in: starts fading "in" from off.
			buttonfade_start_out: starts fading "out" from on.
			buttonfade_start_leftoff: continues where last left off.
		continuous - if TRUE, button fades in & out. If FALSE, button
		    fades to max or min (depending upon mode) & stops.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Makes the button fade in/out.
	------------------------------------------------------------ */
typedef enum {
	buttonfade_start_in	= 0,
	buttonfade_start_out,
	buttonfade_start_leftoff
} buttonfademode;
void Button_StartFade(char faderate, buttonfademode startFademode, BOOL continuous);
/*** EndHeader */
void Button_StartFade(char faderate, buttonfademode startFademode, BOOL continuous)
{
	if(faderate > 100)
		faderate = 100;
	else if(faderate < 0)
		faderate = 0;
	faderate = 100-faderate;
	ISR_SetFadeSpeed(faderate);

	g_button_context.contextstate = button_fading;

	if(continuous)
		g_button_context.buttonFadeStop = FALSE;
	else
		g_button_context.buttonFadeStop = TRUE;

	switch(startFademode){
	case buttonfade_start_in:
		g_button_context.buttonFadeCounter = 0;
		g_button_context.buttonFadeDirection = 5;
		break;
	case buttonfade_start_out:
		g_button_context.buttonFadeCounter = 1024;
		g_button_context.buttonFadeDirection = -5;
		break;
	}
	// Set duty cycle
	WrPortI(PWM0R + (2*BUTTON_PWMCHANNEL), NULL, (g_button_context.buttonFadeCounter >> 2));
	WrPortI(PWL0R + (2*BUTTON_PWMCHANNEL), NULL, (g_button_context.buttonFadeCounter << 6));
	// Set output to PWM
	BitWrPortI(PFFR, &PFFRShadow, 1, BUTTON_PWMCHANNEL + 4); // PWM output
	ISR_EnableFade(TRUE);
}

/*** BeginHeader _button_StopFade */
/*	--------------------------------------------------------------
	Function: _button_StopFade
	Params:
		none
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Stops the button from fading.
	------------------------------------------------------------ */
void _button_StopFade(void);
/*** EndHeader */
void _button_StopFade(void)
{
	ISR_EnableFade(FALSE);
}

/*** BeginHeader FadeRoutine */
/*--------------------------------------------------------------
  Function: FadeRoutine
  --------------------------------------------------------------
  Purpose:	Handles button fading.
  ------------------------------------------------------------*/
void FadeRoutine(void);
/*** EndHeader */
nodebug
root void FadeRoutine(void)
{
	short x;
    x = (short)(((long)g_button_context.buttonFadeCounter*(long)g_button_context.buttonFadeCounter)>>10);
	g_button_context.buttonFadeCounter += g_button_context.buttonFadeDirection;
    if ((g_button_context.buttonFadeCounter >= 1024)||(g_button_context.buttonFadeCounter <= 0)) {
		if (g_button_context.buttonFadeStop) {
			ISR_EnableFade(FALSE);
			return;
		}
        g_button_context.buttonFadeDirection = -g_button_context.buttonFadeDirection;
    }
	if (x > 1023)
		x = 1023;
	else if (x < 0)
		x = 0;
	WrPortI(PWM0R + (2*BUTTON_PWMCHANNEL), NULL, (x >> 2));
	WrPortI(PWL0R + (2*BUTTON_PWMCHANNEL), NULL, (x << 6));
}

/*** BeginHeader */
#endif // #ifndef _BUTTON_C_
/*** EndHeader */

