/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Barcode.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Low level IO functions for capturing TTL barcode data and decoding it.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	04-13-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */

/*** BeginHeader */
#ifndef _BARCODE_C_
#define _BARCODE_C_

#if (CPU_ID_MASK(_CPU_ID_) < R3000)
#warnt "The barcode library will only work on a Rabbit 3000 or later processor"
#endif

#ifdef WIN32
#include "libs\RFID.c"
#else
#use "RFID.c"
#endif

/*** EndHeader */

/*** BeginHeader Barcode_Init */
/*	--------------------------------------------------------------
	Function: Barcode_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the barcode library. This must be called before
		any other functions in the library.
	Notes:
		This does not configure the barcode hardware as that is
		dependent upon the configuration sent by the server to
		tell us what type of hardware is attached. Barcode_Startup()
		performs the task of inspecting the configuration and
		starting the reader(s).
	------------------------------------------------------------ */
void Barcode_Init(void);


/* Constants, Flags, and variables for the TTL reader algorithms */
#define BC_MAXBARCODELEN	32		// Maximum length of decoded barcodes.
									//  Barcodes are 18-24 chars long and RFID IDs are 16 chars

#define BC_MAXEXTRADATALEN  8       // Maximum length of "extra" barcode data. "Extra" data comes from
                                    //  devices such as RFIDm, which may have additional storage space.

#define BC_SCANTIMEOUTPERIOD 100	// Once scanning starts, it is aborted if not sucessfully
									//		completed in this amount of time. (ms)
#define BC_SCANARRAYSIZE	192		// Size of the scan buffer(in words). Should be at least 6*BC_MAXBARCODELEN

#define BC_SFSCANENABLE		0x01
#define BC_SFSCANNING		0x02

#define IC2_START_OCCURRED		0x80
#define IC2_STOP_OCCURRED		0x40
#define IC1_START_OCCURRED		0x20
#define IC1_STOP_OCCURRED		0x10
#define IC2_COUNTER_ROLLOVER	0x08
#define IC1_COUNTER_ROLLOVER	0x04

//#define IC2_COUNTER_ROLLOVER	0x00  //rjt a test to turn this off
//#define IC1_COUNTER_ROLLOVER	0x00


#define IC1_ACTIVITY		0x34
#define IC2_ACTIVITY		0xC8


extern unsigned char	 g_BC1scanflags;
extern unsigned short g_BC1lastscanptr;
extern unsigned long	 g_BC1scantimer;
extern char				 g_BC1decodedlength;

extern unsigned short g_BC1scanarray[BC_SCANARRAYSIZE];
extern unsigned char	 g_BC1digitizeddata[BC_SCANARRAYSIZE];
extern unsigned short g_BC1scanptr;


extern unsigned char	 g_BC2scanflags;
extern unsigned short g_BC2lastscanptr;
extern unsigned long	 g_BC2scantimer;
extern char				 g_BC2decodedlength;

extern unsigned short g_BC2scanarray[BC_SCANARRAYSIZE];
extern unsigned char	 g_BC2digitizeddata[BC_SCANARRAYSIZE];
extern unsigned short g_BC2scanptr;

extern unsigned char  g_copyIccsr;
extern unsigned char  readIccsr;

extern unsigned char	 g_enableIC1interrupt;
extern unsigned char	 g_enableIC2interrupt;

/* Constants, Flags, and variables for the RS232 reader algorithms */

/* Constants, Flags, and variables for non-hardware-specific stuff */
typedef enum {
	BCR_NONE = 0,				// No reader installed
	BCR_TTL_BCODE,				// TTL barcode reader
	BCR_RS232_BCODE,			// RS-232 barcode reader
	BCR_RS232_RFID				// RS-232 RFID reader (TTL voltage levels)
} bcreadertype;

// These hold the barcode reader types parsed from the configuration information
// sent by the server. We currently have two ports on which readers can be installed.
// The TTL reader can only be installed on port 1.
extern bcreadertype g_BCreader1;
extern bcreadertype g_BCreader2;

#ifdef WIN32
typedef BOOL(*BARCODEHANDLERFUNC)(int readernumber);
typedef int(*BARCODEGETFUNC)(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen);
#else
typedef BOOL(*BARCODEHANDLERFUNC)();
typedef int(*BARCODEGETFUNC)();
#endif

extern BARCODEHANDLERFUNC	g_BChandler1;
extern BARCODEHANDLERFUNC	g_BChandler2;
extern BARCODEGETFUNC		g_BCGet1;
extern BARCODEGETFUNC		g_BCGet2;

/*** EndHeader */

/* Globals used by interrupt routine. Must be protected
   when accessing from non-interrupt processing routine. */
//rjt Bar code scanner 1
unsigned short	g_BC1scanarray[BC_SCANARRAYSIZE];
unsigned char	g_BC1digitizeddata[BC_SCANARRAYSIZE];
unsigned short	g_BC1scanptr;
unsigned char	g_BC1scanflags;

//rjt Bar code scanner 2
unsigned short	g_BC2scanarray[BC_SCANARRAYSIZE];
unsigned char	g_BC2digitizeddata[BC_SCANARRAYSIZE];
unsigned short	g_BC2scanptr;
unsigned char	g_BC2scanflags;


/* Globals used only by non-interrupt processing routine. */
//rjt Bar code scanner 1
unsigned short	g_BC1lastscanptr;
unsigned long	g_BC1scantimer;
char				g_BC1decodedlength;

//rjt Bar code scanner 2
unsigned short	g_BC2lastscanptr;
unsigned long	g_BC2scantimer;
char				g_BC2decodedlength;

unsigned char  g_copyIccsr;
unsigned char  readIccsr;

/*Need to have a way for each individual barcode reader to "disable" its
interrupt even though they both share a single hardware interrupt. These
flags will allow the portion of the interrupt handler to be skipped for a
particular reader if necessary. */
unsigned char	 g_enableIC1interrupt;
unsigned char	 g_enableIC2interrupt;

bcreadertype	g_BCreader1;
bcreadertype	g_BCreader2;

BARCODEHANDLERFUNC	g_BChandler1;
BARCODEHANDLERFUNC	g_BChandler2;
BARCODEGETFUNC			g_BCGet1;
BARCODEGETFUNC			g_BCGet2;

void Barcode_Init(void)
{
	g_BC1scanptr = 0;
	g_BC1lastscanptr = 0;
	g_BC1decodedlength = 0;
	g_BC1scanflags = 0;

   g_BC2scanptr = 0;
	g_BC2lastscanptr = 0;
	g_BC2decodedlength = 0;
   g_BC2scanflags = 0;

	g_BCreader1 = BCR_TTL_BCODE;
	g_BCreader2 = BCR_TTL_BCODE;
	g_BChandler1 = Barcode_NULLHandler;
	g_BChandler2 = Barcode_NULLHandler;
	g_BCGet1 = Barcode_NULLGet;
	g_BCGet2 = Barcode_NULLGet;

   g_copyIccsr = 0;

   g_enableIC1interrupt = 0;
   g_enableIC2interrupt = 0;
}

/*** BeginHeader Barcode_Startup */
/*	--------------------------------------------------------------
	Function: Barcode_Startup
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Configures the barcode reader depending upon the current
		device configuration. After this function is called,
		Barcode_Handler() and Barcode_Get() will begin processing
		barcode scans.
	Notes:
		There are several TTL and Serial
		reader options that can be specified in this configuration.
		If the configuration is invalid, the code defaults to
		looking for 1 TTL barcode reader and 1 Serial barcode
		reader. This should allow utility cards to be used in
		most cases, even if there is no configuration.
	------------------------------------------------------------ */
void Barcode_Startup(void);
/*** EndHeader */
void Barcode_Startup(void)
{

   g_BCreader1 = BCR_TTL_BCODE;
   //g_BCreader2 = BCR_TTL_BCODE;
   g_BCreader2 = BCR_NONE;

   switch(g_BCreader1)
   {
		case BCR_RS232_BCODE:
			serDopen(9600);
			serDdatabits(PARAM_8BIT);
			g_BChandler1 = Barcode_RS232Handler;
			g_BCGet1 = Barcode_RS232Get;
			DebugPrint(STRACE_NFO,("Reader 1 - RS232 Started."));
			break;
		case BCR_TTL_BCODE:
			g_BC1scanflags = BC_SFSCANENABLE;

         // Ensure PD5 is input.
			BitWrPortI(PDDDR, &PDDDRShadow, 0, 5);
			// Set the Timer A8 time constant
			WrPortI(TAT8R, &TAT8RShadow, 32);
			// Trigger START & STOP on PD5
			WrPortI(ICS1R, &ICS1RShadow, 0x66);
			// Counter runs continuously.
			// Latch count on start only for now.
			// START is rising, STOP is falling
			WrPortI(ICT1R, &ICT1RShadow, 0xA6);
         g_copyIccsr |= (IC1_START_OCCURRED | IC1_COUNTER_ROLLOVER);
			WrPortI(ICCSR, &ICCSRShadow, g_copyIccsr); // zero out counter and enable START interrupt

	#if __SEPARATE_INST_DATA__
			interrupt_vector inputcap_intvec bc_isr;
	#else
			SetVectIntern(0x1A, bc_isr);
	#endif

			// Interrupt at priority 1
			WrPortI(ICCR, &ICCRShadow, 0x01);
			g_BChandler1 = Barcode_TTLHandler;
			g_BCGet1 = Barcode_TTLGet;
         g_enableIC1interrupt = 1;
			DebugPrint(STRACE_NFO,("Reader 1 - TTL Started."));
			break;
		default:
			DebugPrint(STRACE_NFO,("Reader 1 - Not Installed."));
			g_BChandler1 = Barcode_NULLHandler;
			g_BCGet1 = Barcode_NULLGet;
			break;
	}

   switch(g_BCreader2)
   {
		case BCR_RS232_BCODE:
			serEopen(9600);
			serEdatabits(PARAM_8BIT);
			g_BChandler2 = Barcode_RS232Handler;
			g_BCGet2 = Barcode_RS232Get;
			DebugPrint(STRACE_NFO,("Reader 2 - RS232 Started."));
			break;
      case BCR_TTL_BCODE:
			g_BC2scanflags = BC_SFSCANENABLE;
         // Ensure PG7 is input.
			BitWrPortI(PGDDR, &PDDDRShadow, 0, 7);
			// Set the Timer A8 time constant
			WrPortI(TAT8R, &TAT8RShadow, 32);
			// Trigger START & STOP on PG7
			WrPortI(ICS2R, &ICS2RShadow, 0xFF);
			// Counter runs continuously.
			// Latch count on start only for now.
			// START is rising, STOP is falling
			WrPortI(ICT2R, &ICT2RShadow, 0xA6);
         g_copyIccsr |= (IC2_START_OCCURRED | IC2_COUNTER_ROLLOVER);
			WrPortI(ICCSR, &ICCSRShadow, g_copyIccsr); // zero out counter and enable START interrupt for both counters

	#if __SEPARATE_INST_DATA__
			interrupt_vector inputcap_intvec bc_isr;
	#else
			SetVectIntern(0x1A, bc_isr);
	#endif

			// Interrupt at priority 1
			WrPortI(ICCR, &ICCRShadow, 0x01);
			g_BChandler2 = Barcode_TTLHandler;
			g_BCGet2 = Barcode_TTLGet;
         g_enableIC2interrupt = 1;
			DebugPrint(STRACE_NFO,("Reader 2 - TTL Started."));
			break;
		case BCR_RS232_RFID:
			RFID_Startup();
			g_BChandler2 = RFID_Handler;
			g_BCGet2 = RFID_Get;
			DebugPrint(STRACE_NFO,("Reader 2 - RFID Started."));
			break;
		default:
			g_BChandler2 = Barcode_NULLHandler;
			g_BCGet2 = Barcode_NULLGet;
			DebugPrint(STRACE_NFO,("Reader 2 - Not Installed."));
			break;
	}

}

/*** BeginHeader Barcode_Shutdown */
/*	--------------------------------------------------------------
	Function: Barcode_Shutdown
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Un-configures the barcode readers. After this function is
		called, Barcode_Startup() may be called again to
		reconfigure the readers based upon the most recent
		device configuration data. Also after this function is
		called, Barcode_Handler() and Barcode_Get() will stop
		processing barcode scans.
	------------------------------------------------------------ */
int Barcode_Shutdown(void);
/*** EndHeader */
int Barcode_Shutdown(void)
{
	switch(g_BCreader1)
   {
		case BCR_RS232_BCODE:
			serDclose();
			break;
		case BCR_TTL_BCODE:
			ipset(2);
			g_BC1scanflags = 0;
			ipres();
			// Disable Input Capture interrupt.
			WrPortI(ICCR, &ICCRShadow, 0x00);
			// Disable the counter
			WrPortI(ICT1R, &ICT1RShadow, 0x00);
         g_enableIC1interrupt = 0;
			break;
		default:
			break;
	}

	switch(g_BCreader2)
   {
		case BCR_RS232_BCODE:
			serEclose();
			break;
      case BCR_TTL_BCODE:
			ipset(2);
			g_BC2scanflags = 0;
			ipres();
			// Disable Input Capture interrupt.
			WrPortI(ICCR, &ICCRShadow, 0x00);
			// Disable the counter
			WrPortI(ICT2R, &ICT2RShadow, 0x00);
         g_enableIC2interrupt = 0;
			break;
		case BCR_RS232_RFID:
			RFID_Shutdown();
			break;
		default:
			break;
	}

   DebugPrint(STRACE_NFO,("Barcode readers shut down."));

	g_BChandler1 = Barcode_NULLHandler;
	g_BChandler2 = Barcode_NULLHandler;
	g_BCGet1 = Barcode_NULLGet;
	g_BCGet2 = Barcode_NULLGet;
}

/*** BeginHeader Barcode_Get */
/*	--------------------------------------------------------------
	Function: Barcode_Get
	Params:
		reader - 1 or 2. Which reader to fetch a barcode from.
		barcode - a character pointer of at least BC_MAXBARCODELEN
			characters long to hold the decoded barcode. This
			string will be null terminated.
		buflen - the length of the barcode buffer, including room
			for a NULL terminator.
		extradata - a character pointer of at least BC_MAXEXTRADATALEN
			characters long to hold the extra barcode data. This
			string will be null terminated.
		edbuflen - the length of the extra data buffer, including room
			for a NULL terminator.
	Returns:
		int - number of barcode characters copied into the
			provided buffer, not including the NULL terminator.
	--------------------------------------------------------------
	Purpose:
		Fetches a decoded barcode.
	Notes:
		This function will only process and retrieve scans after
		Barcode_Startup() has been called. It may safely be called
		before Barcode_Startup(), but it will never return a scan.
        Not all readers return extra data. If there is none, the
        first byte of the extradata buffer will be '\0'.
	------------------------------------------------------------ */
int Barcode_Get(int reader, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen);
/*** EndHeader */
int Barcode_Get(int reader, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen)
{
	if(reader == 1)
   {
		return g_BCGet1(1,barcode,buflen,extradata,edbuflen);
	}
   else if(reader == 2)
   {
		return g_BCGet2(2,barcode,buflen,extradata,edbuflen);
	}
	return Barcode_NULLGet(0,barcode,buflen,extradata,edbuflen);
}

/*** BeginHeader Barcode_Handler */
/*	--------------------------------------------------------------
	Function: Barcode_Handler
	Params: none
	Returns:
		int - 0 until a barcode is sucessfully decoded. The number
		of barcodes waiting when one or more complete barcodes
		have sucessfully been decoded.
	--------------------------------------------------------------
	Purpose:
		Called periodically by the main loop to run the
		barcode decoding mechanism. Once the return value is
		non-zero, a barcode can be fetched with Barcode_Get().
	------------------------------------------------------------ */
int Barcode_Handler(void);
/*** EndHeader */
int Barcode_Handler(void)
{
	int ncodes;
	ncodes = 0;
	if(g_BChandler1(1))
		ncodes++;
	if(g_BChandler2(2))
	 	ncodes++;
	return ncodes;
}

/*** BeginHeader Barcode_NULLGet */
/*	--------------------------------------------------------------
	Function: Barcode_NULLGet
	Params:
		readernumber - 1 or 2, which reader to get a barcode from.
		barcode - a character pointer of at least BC_MAXBARCODELEN
			characters long to hold the decoded barcode. This
			string will be null terminated.
		buflen - the length of the barcode buffer, including room
			for a NULL terminator.
		extradata - a character pointer of at least BC_MAXEXTRADATALEN
			characters long to hold the extra barcode data. This
			string will be null terminated.
		edbuflen - the length of the extra data buffer, including room
			for a NULL terminator.
	Returns:
		int - number of barcode characters copied into the
			provided buffer, not including the NULL terminator.
	--------------------------------------------------------------
	Purpose:
		Fetches nothing.
	Notes:
		This is a NULL handler for channels with no hardware
		installed.
        Not all readers return extra data. If there is none, the
        first byte of the extradata buffer will be '\0'.
	------------------------------------------------------------ */
int Barcode_NULLGet(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen);
/*** EndHeader */
int Barcode_NULLGet(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen)
{
    if(edbuflen > 0)
        extradata[0] = '\0';
    if(buflen > 0)
	    barcode[0] = '\0';
	return 0;
}

/*** BeginHeader Barcode_NULLHandler */
/*	--------------------------------------------------------------
	Function: Barcode_NULLHandler
	Params:
		readernumber - 1 or 2, which reader this should handle.
	Returns:
		BOOL - Always returns FALSE.
	--------------------------------------------------------------
	Purpose:
		Called periodically to run the barcode decoding mechanism.
		This is a NULL handler which is used for a channel with
		no device installed.
	------------------------------------------------------------ */
BOOL Barcode_NULLHandler(int readernumber);
/*** EndHeader */
BOOL Barcode_NULLHandler(int readernumber)
{
	return FALSE;
}

/*** BeginHeader Barcode_RS232Get */
/*	--------------------------------------------------------------
	Function: Barcode_RS232Get
	Params:
		readernumber - 1 or 2, which reader to get a barcode from.
		barcode - a character pointer of at least BC_MAXBARCODELEN
			characters long to hold the decoded barcode. This
			string will be null terminated.
		buflen - the length of the barcode buffer, including room
			for a NULL terminator.
		extradata - a character pointer of at least BC_MAXEXTRADATALEN
			characters long to hold the extra barcode data. This
			string will be null terminated.
		edbuflen - the length of the extra data buffer, including room
			for a NULL terminator.
	Returns:
		int - number of barcode characters copied into the
			provided buffer, not including the NULL terminator.
	--------------------------------------------------------------
	Purpose:
		Fetches a decoded barcode from a RS232 reader.
        Not all readers return extra data. If there is none, the
        first byte of the extradata buffer will be '\0'.
	------------------------------------------------------------ */
int Barcode_RS232Get(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen);
/*** EndHeader */
int Barcode_RS232Get(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen)
{
    if(edbuflen > 0)
        extradata[0] = '\0';
    if(buflen > 0)
	    barcode[0] = '\0';
	return 0;
}

/*** BeginHeader Barcode_RS232Handler */
/*	--------------------------------------------------------------
	Function: Barcode_RS232Handler
	Params:
		readernumber - 1 or 2, which reader this should handle.
	Returns:
		BOOL - TRUE when a barcode is sucessfully decoded, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose:
		Called periodically to run the barcode decoding mechanism
		for RS232 readers. Once the return value is TRUE, a
		barcode can be fetched with Barcode_Get().
	------------------------------------------------------------ */
BOOL Barcode_RS232Handler(int readernumber);
/*** EndHeader */
BOOL Barcode_RS232Handler(int readernumber)
{
	//TODO: Write the RS232 handler.
	return FALSE;
}

/*** BeginHeader Barcode_TTLGet */
/*	--------------------------------------------------------------
	Function: Barcode_TTLGet
	Params:
		readernumber - 1 or 2, which reader to get a barcode from.
		barcode - a character pointer of at least BC_MAXBARCODELEN
			characters long to hold the decoded barcode. This
			string will be null terminated.
		buflen - the length of the barcode buffer, including room
			for a NULL terminator.
		extradata - a character pointer of at least BC_MAXEXTRADATALEN
			characters long to hold the extra barcode data. This
			string will be null terminated.
		edbuflen - the length of the extra data buffer, including room
			for a NULL terminator.
	Returns:
		int - number of barcode characters copied into the
			provided buffer, not including the NULL terminator.
	--------------------------------------------------------------
	Purpose:
		Fetches a decoded barcode from a TTL reader.
	Notes:

	------------------------------------------------------------ */
int Barcode_TTLGet(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen);
/*** EndHeader */
int Barcode_TTLGet(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen)
{
	int i, j;
	unsigned char tuschar;

    if(edbuflen)
        extradata[0] = '\0';
    if(buflen > 0)
    	barcode[0] = '\0';

	 if(readernumber == 1)
    {
      ipset(2);
		tuschar = g_BC1scanflags;
		ipres();
		// Only try to get a barcode when a barcode has been received and scanning is disabled.
		if(tuschar & BC_SFSCANENABLE)
			return 0;
		j = 0;
		if ((g_BC1decodedlength > 0) && (buflen > 0))
      {
			// Format the decoded barcode into the user's character buffer. Don't overrun buffer.
			// Assume last character is always STOP, so only traverse g_BC1decodedlength-1
			for(i = 0; (i < g_BC1decodedlength-1) && (j < buflen-1); i++)
         {
				// Don't copy control characters, which are > 99. Also, if the next
				// character is STOP (106), then the current character is a checksum. Don't copy
				// the checksum.
				if((g_BC1scanarray[i] <= 99)&&(g_BC1scanarray[i+1] != 106))
            {
					tuschar = (unsigned char)g_BC1scanarray[i];
					sprintf(barcode+j,"%02u",tuschar);
					j+=2;
				}
			}
			barcode[j] = 0;
		}
		// Scanning is disabled. Now that the decoded barcode has been retrieved,
		//	prepare scanning variables and re-enable scanning.
		g_BC1scanptr = 0;
		g_BC1lastscanptr = 0;
		g_BC1decodedlength = 0;
		g_BC1scanflags |= BC_SFSCANENABLE;
		// zero out counter and enable START interrupt
      g_copyIccsr |= (IC1_START_OCCURRED | IC1_COUNTER_ROLLOVER);
		WrPortI(ICCSR, NULL, g_copyIccsr);
		// Enable IC interrupt at priority 1
		//rjt WrPortI(ICCR, &ICCRShadow, 0x01);
      g_enableIC1interrupt = 1;
		return j;
	}
   else if(readernumber == 2)
   {
		ipset(2);
		tuschar = g_BC2scanflags;
		ipres();
		// Only try to get a barcode when a barcode has been received and scanning is disabled.
		if(tuschar & BC_SFSCANENABLE)
			return 0;
		j = 0;
		if ((g_BC2decodedlength > 0) && (buflen > 0))
      {
			// Format the decoded barcode into the user's character buffer. Don't overrun buffer.
			// Assume last character is always STOP, so only traverse g_BC2decodedlength-1
			for(i = 0; (i < g_BC2decodedlength-1) && (j < buflen-1); i++)
         {
				// Don't copy control characters, which are > 99. Also, if the next
				// character is STOP (106), then the current character is a checksum. Don't copy
				// the checksum.
				if((g_BC2scanarray[i] <= 99)&&(g_BC2scanarray[i+1] != 106))
            {
					tuschar = (unsigned char)g_BC2scanarray[i];
					sprintf(barcode+j,"%02u",tuschar);
					j+=2;
				}
			}
			barcode[j] = 0;
		}
		// Scanning is disabled. Now that the decoded barcode has been retrieved,
		//	prepare scanning variables and re-enable scanning.
		g_BC2scanptr = 0;
		g_BC2lastscanptr = 0;
		g_BC2decodedlength = 0;
		g_BC2scanflags |= BC_SFSCANENABLE;
		// zero out counter and enable START interrupt
      g_copyIccsr |= (IC2_START_OCCURRED | IC2_COUNTER_ROLLOVER);
		WrPortI(ICCSR, NULL, g_copyIccsr);
		// Enable IC interrupt at priority 1
		//rjt WrPortI(ICCR, &ICCRShadow, 0x01);
      g_enableIC2interrupt = 1;
		return j;
	}

	return 0;
}

/*** BeginHeader Barcode_TTLHandler */
/*	--------------------------------------------------------------
	Function: Barcode_TTLHandler
	Params:
		readernumber - 1 or 2, which reader this should handle.

	Returns:
		BOOL - TRUE when a barcode is sucessfully decoded, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose:
		Called periodically to run the barcode decoding mechanism
		for the TTL reader.	Once the return value is TRUE, a
		barcode can be fetched with Barcode_Get().
	------------------------------------------------------------ */
BOOL Barcode_TTLHandler(int readernumber);
/*** EndHeader */
int _BC1Barcode_Decode(void);
int _BC2Barcode_Decode(void);
BOOL Barcode_TTLHandler(int readernumber)
{
	unsigned char	tflags;
	unsigned char	tscanptr;
	unsigned char	i;
	int				tresult;

   if(readernumber == 1)
   {
      ipset(2);
		tflags = g_BC1scanflags;
		tscanptr = g_BC1scanptr;
		ipres();

		//rjt a test
      //g_BC1scanptr = 0;
      //return(TRUE);

		if(tflags & BC_SFSCANNING)
      {
			// At least one transition has been scanned.
			if(tscanptr)
         {
            //DebugPrint(STRACE_NFO,("reader 1 A"));

            if (tscanptr != g_BC1lastscanptr)
            {
               //DebugPrint(STRACE_NFO,("reader 1 B"));
               g_BC1lastscanptr = tscanptr;
					g_BC1scantimer = MS_TIMER+BC_SCANTIMEOUTPERIOD;
				}

            else if(Util_CheckmsTimeout(g_BC1scantimer))
            {
               //DebugPrint(STRACE_NFO,("reader 1 C"));

               // Disable IC interrupt
					//rjt WrPortI(ICCR, &ICCRShadow, 0x00);


					// Disable scanning and reset scanning flag.
					ipset(2);
					g_BC1scanflags = 0;
               g_enableIC1interrupt = 0;
					ipres();
					// Process the barcode data.
					tresult = _BC1Barcode_Decode();
               // If decoding failed, re-enable the scanner. Otherwise, leave it disabled so the
					// application can retrieve the scan before it is overwritten.
					if(!tresult)
               {
                  //DebugPrint(STRACE_NFO,("reader 1 D"));
                  g_BC1scanptr = 0;
						g_BC1lastscanptr = 0;
						g_BC1decodedlength = 0;
						g_BC1scanflags |= BC_SFSCANENABLE;
                  // zero out counter and enable START interrupt
               	g_copyIccsr |= (IC1_START_OCCURRED | IC1_COUNTER_ROLLOVER);
						WrPortI(ICCSR, NULL, g_copyIccsr);
						// Enable IC interrupt at priority 1
						//rjt WrPortI(ICCR, &ICCRShadow, 0x01);
                  g_enableIC1interrupt = 1;
					}

               //DebugPrint(STRACE_NFO,("reader 1 E"));
               g_BC1scanptr = 0;
					g_BC1lastscanptr = 0;
					if(tresult)
						return TRUE;

					return FALSE;
				}
			}
		}
      else if(!tflags)
      {
         //DebugPrint(STRACE_NFO,("reader 1 F"));

         // If scanning is disabled, see if anything is sitting in the scan buffer.
			if(g_BC1decodedlength > 0)
         {
            //DebugPrint(STRACE_NFO,("reader 1 G"));
            return TRUE;
			}
         else
         {
            //DebugPrint(STRACE_NFO,("reader 1 H"));
            // Nothing in the scan buffer, but scanning disabled? Re-enable it.
				g_BC1scanptr = 0;
				g_BC1lastscanptr = 0;
				g_BC1decodedlength = 0;
				g_BC1scanflags |= BC_SFSCANENABLE;
            // zero out counter and enable START interrupt
         	g_copyIccsr |= (IC1_START_OCCURRED | IC1_COUNTER_ROLLOVER);
				WrPortI(ICCSR, NULL, g_copyIccsr);
				// Enable IC interrupt at priority 1
				//rjt WrPortI(ICCR, &ICCRShadow, 0x01);
            g_enableIC1interrupt = 1;
			}
		}
      //DebugPrint(STRACE_NFO,("reader 1 I"));

   	return FALSE;
   } //if(readernumber == 1)
   else if(readernumber == 2)
   {
      ipset(2);
		tflags = g_BC2scanflags;
		tscanptr = g_BC2scanptr;
		ipres();
		if(tflags & BC_SFSCANNING)
      {
			// At least one transition has been scanned.
			if(tscanptr)
         {
				if (tscanptr != g_BC2lastscanptr)
            {
					g_BC2lastscanptr = tscanptr;
					g_BC2scantimer = MS_TIMER+BC_SCANTIMEOUTPERIOD;
				}
            else if(Util_CheckmsTimeout(g_BC2scantimer))
            {
					// Disable IC interrupt
					//rjt WrPortI(ICCR, &ICCRShadow, 0x00);
               g_enableIC2interrupt = 0;
					// Disable scanning and reset scanning flag.
					ipset(2);
					g_BC2scanflags = 0;
					ipres();
					// Process the barcode data.
					tresult = _BC2Barcode_Decode();
					// If decoding failed, re-enable the scanner. Otherwise, leave it disabled so the
					// application can retrieve the scan before it is overwritten.
					if(!tresult)
               {
						g_BC2scanptr = 0;
						g_BC2lastscanptr = 0;
						g_BC2decodedlength = 0;
						g_BC2scanflags |= BC_SFSCANENABLE;
                  // zero out counter and enable START interrupt
               	g_copyIccsr |= (IC2_START_OCCURRED | IC2_COUNTER_ROLLOVER);
						WrPortI(ICCSR, NULL, g_copyIccsr);
						// Enable IC interrupt at priority 1
						//rjt WrPortI(ICCR, &ICCRShadow, 0x01);
                  g_enableIC2interrupt = 1;
					}

               g_BC2scanptr = 0;
					g_BC2lastscanptr = 0;
					if(tresult)
						return TRUE;
					return FALSE;
				}
			}
		}
      else if(!tflags)
      {
      	// If scanning is disabled, see if anything is sitting in the scan buffer.
			if(g_BC2decodedlength > 0)
         {
				return TRUE;
			}
         else
         {
				// Nothing in the scan buffer, but scanning disabled? Re-enable it.
				g_BC2scanptr = 0;
				g_BC2lastscanptr = 0;
				g_BC2decodedlength = 0;
				g_BC2scanflags |= BC_SFSCANENABLE;
            // zero out counter and enable START interrupt
         	g_copyIccsr |= (IC2_START_OCCURRED | IC2_COUNTER_ROLLOVER);
				WrPortI(ICCSR, NULL, g_copyIccsr);
				// Enable IC interrupt at priority 1
				//rjt WrPortI(ICCR, &ICCRShadow, 0x01);
            g_enableIC2interrupt = 1;
			}
		}

   	return FALSE;
   }//else if(readernumber == 2)

   return FALSE;
}


typedef struct {
	unsigned short code;
	char val;
} datarec;

#define CODE128TABLELEN 108
static const datarec arraydata[CODE128TABLELEN] = {
	{14,92},
	{23,63},
	{29,80},
	{38,33},
	{44,93},
	{53,64},
	{74,42},
	{83,69},
	{89,12},
	{98,36},
	{104,43},
	{113,70},
	{134,45},
	{140,99},
	{149,15},
	{164,46},
	{194,95},
	{200,100},
	{209,83},
	{224,96},
	{263,65},
	{269,81},
	{278,3},
	{284,82},
	{293,4},
	{308,66},
	{323,71},
	{329,13},
	{338,6},
	{344,14},
	{353,7},
	{368,72},
	{389,16},
	{404,17},
	{449,84},
	{464,85},
	{518,34},
	{524,94},
	{533,5},
	{548,35},
	{578,37},
	{584,44},
	{593,8},
	{608,38},
	{644,47},
	{704,79},
	{773,67},
	{788,68},
	{833,73},
	{848,74},
	{1034,106},
	{1043,104},
	{1049,105},
	{1058,39},
	{1064,49},
	{1073,103},
	{1094,30},
	{1100,89},
	{1109,0},
	{1124,31},
	{1154,51},
	{1160,53},
	{1169,21},
	{1184,52},
	{1220,90},
	{1283,76},
	{1289,19},
	{1298,9},
	{1304,20},
	{1313,10},
	{1328,61},
	{1349,1},
	{1364,2},
	{1409,22},
	{1424,18},
	{1538,40},
	{1544,50},
	{1553,11},
	{1568,41},
	{1604,32},
	{1664,106},
	{1793,78},
	{1808,75},
	{2054,54},
	{2060,101},
	{2069,24},
	{2084,55},
	{2114,57},
	{2120,23},
	{2129,27},
	{2144,58},
	{2180,48},
	{2240,60},
	{2309,25},
	{2324,26},
	{2369,28},
	{2384,29},
	{2564,56},
	{2624,59},
	{3074,97},
	{3080,102},
	{3089,86},
	{3104,98},
	{3140,91},
	{3200,77},
	{3329,87},
	{3344,88},
	{3584,62}
};

static const unsigned char code128_startf[6] = {2,1,1,2,3,2};
static const unsigned char code128_startb[6] = {2,1,1,1,3,3};

/*	--------------------------------------------------------------
	Function: _BCCode128ConvertToValue
	Params:
		digitizedarray - array containing scan data.
	Returns:
		unsigned char - the Code 128 character that corresponds to
		a digitized bar/space pattern. Or, 0xFF if the pattern
		was not recognized.
	--------------------------------------------------------------
	Purpose:
		This function converts a digitized bar/space pattern into
		the corresponding Code 128 character. If the scan was bad
		and parity could not be corrected, it is possible to
		pass an invalid pattern into this function. This function
		will handle invalid patterns by returning 0xFF.
	------------------------------------------------------------ */
unsigned char _BCCode128ConvertToValue(unsigned char* digitizedarray)
{
	unsigned char tval;
	unsigned short l,u,i;
	unsigned short code;

	// First, pack the digitized values into a single short for quick lookup.
	code = 0;
	for (i = 0; i < 6; i++) {
		code = code << 2;
		code |= (unsigned short)(digitizedarray[i]-1);
	}

	// Binary search through the lookup table.
	tval = 0xFF;
	if((code >= arraydata[0].code)&&(code <= arraydata[CODE128TABLELEN-1].code))
	{
		l = 0;
		u = CODE128TABLELEN-1;
		while(l <= u)
		{
			i = (l+u)>>1;	// midpoint = (l+u)/2
			if(code == arraydata[i].code)
			{
				tval = arraydata[i].val;
				break;
			}
			else if(code < arraydata[i].code)
				u = i-1;
			else
				l = i+1;
		}
	}
	return tval;
}

/*	--------------------------------------------------------------
	Function: _BCCode128CheckFixParity
	Params:
		digitizedarray - array containing scan data.
	Returns:
		TRUE if parity check/correct succeeded, FALSE if parity is
		still wrong.
	--------------------------------------------------------------
	Purpose:
		This magical algorithm attempts to fix the digitized array
		by checking parity. It takes advantage of the fact that
		the sum of all bars & spaces should be 11, the sum of all
		bars should be even, and the sum of all spaces should be
		odd. There is only so much error that can be corrected by
		this algorithm, so it will return FALSE if the scan was
		so bad it was unrepairable.
	------------------------------------------------------------ */
BOOL _BCCode128CheckFixParity(unsigned char* digitizedarray)
{
	int bparity;
	int wparity;
	int total;
	int diff;
	int i;
	char doW;
	char doB;
	char eliminate;

	total = digitizedarray[0]+digitizedarray[1]+digitizedarray[2]+digitizedarray[3]+digitizedarray[4]+digitizedarray[5];
	wparity = digitizedarray[1]+digitizedarray[3]+digitizedarray[5];
	bparity = digitizedarray[0]+digitizedarray[2]+digitizedarray[4];
	if (total > 11) {
		diff = total-11;
		if(diff > 4)
			return FALSE;
		eliminate = 4;
		while(diff){
			if( (wparity & 0x01) && !(bparity & 0x01) && !(diff&0x01)  ) {	// if both parities are correct and the diff is even.
				for(i = 1; (i <= 5)&&(diff); i+=2)
					if(digitizedarray[i] >= eliminate) {
						digitizedarray[i]--;
						diff--;
						wparity--;
					}
				for(i = 0; (i <= 4)&&(diff); i+=2)
					if(digitizedarray[i] >= eliminate){
						digitizedarray[i]--;
						diff--;
						bparity--;
					}
				if(diff){
					eliminate--;
					if(eliminate == 1)	// Nothing left to eliminate.
						return FALSE;
				}
			}
			else if(bparity & 0x01){ // Black parity is wrong.
				for(i = 0; i < 6; i+=2)
					if(digitizedarray[i] >= eliminate){
						digitizedarray[i]--;
						diff--;
						bparity--;
						break;
					}
				if(diff){
					eliminate--;
					if(eliminate == 1)	// Nothing left to eliminate.
						return FALSE;
				}
			} else if(!(wparity & 0x01)) { // White parity is wrong.
				for(i = 1; i <= 5; i+=2)
					if(digitizedarray[i] >= eliminate) {
						digitizedarray[i]--;
						diff--;
						wparity--;
						break;
					}
				if(diff){
					eliminate--;
					if(eliminate == 1)	// Nothing left to eliminate.
						return FALSE;
				}
			} else	// Both parities are correct, but the diff is odd. Can't do anything.
				return FALSE;
		}
	} else if (total < 11){
		diff = 11-total;
		if(diff > 4)
			return FALSE;
		eliminate = 1;
		while(diff){
			if( (wparity & 0x01) && !(bparity & 0x01) && !(diff&0x01)  ) {	// if both parities are correct and the diff is even.
				for(i = 1; (i <= 5)&&(diff); i+=2)
					if(digitizedarray[i] <= eliminate) {
						digitizedarray[i]++;
						diff--;
						wparity++;
					}
				for(i = 0; (i <= 4)&&(diff); i+=2)
					if(digitizedarray[i] <= eliminate){
						digitizedarray[i]++;
						diff--;
						bparity++;
					}
				if(diff){
					eliminate++;
					if(eliminate == 4)	// Nothing left to eliminate.
						return FALSE;
				}
			}
			else if(bparity & 0x01){ // Black parity is wrong.
				for(i = 0; i < 6; i+=2)
					if(digitizedarray[i] <= eliminate){
						digitizedarray[i]++;
						diff--;
						bparity++;
						break;
					}
				if(diff){
					eliminate++;
					if(eliminate == 4)	// Nothing left to eliminate.
						return FALSE;
				}
			} else if(!(wparity & 0x01)) { // White parity is wrong.
				for(i = 1; i <= 5; i+=2)
					if(digitizedarray[i] <= eliminate) {
						digitizedarray[i]++;
						diff--;
						wparity++;
						break;
					}
				if(diff){
					eliminate++;
					if(eliminate == 4)	// Nothing left to eliminate.
						return FALSE;
				}
			} else	// Both parities are correct, but the diff is odd. Can't do anything.
				return FALSE;
		}
	}
	return TRUE;
}

/*	--------------------------------------------------------------
	Function: _BCFindSmallestWidth
	Params:
		scandata - array containing scan data.
		smallestwidth - the smallest width
		isBar - whether the smallest width belonged to a bar or
			a space.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Each digit, composed of three bars & three spaces, has at
		least one unit bar or space. This function finds the
		smallest width and returns whether it was a bar or a
		space.
	------------------------------------------------------------ */
void _BCFindSmallestWidth(unsigned short* scandata, unsigned short* smallestwidth, BOOL* isBar)
{
	int	i;
	unsigned short smallestbar;
	unsigned short smallestspace;
	unsigned short largestbar;
	unsigned short largestspace;

	smallestbar = scandata[0];
	largestbar = scandata[0];
	smallestspace = scandata[1];
	largestspace = scandata[1];
	for(i = 2; i < 6; i++) {
		if((i&0x01)==0) {	// We're looking at a bar
			if(scandata[i] < smallestbar) {
				smallestbar = scandata[i];
			} else if(scandata[i] > largestbar) {
				largestbar = scandata[i];
			}
		} else {	// We're looking at a space
			if(scandata[i] < smallestspace) {
				smallestspace = scandata[i];
			} else if(scandata[i] > largestspace) {
				largestspace = scandata[i];
			}
		}
	}
	if(smallestbar < smallestspace){
		*smallestwidth = smallestbar;
		*isBar = TRUE;
	} else {
		*smallestwidth = smallestspace;
		*isBar = FALSE;
	}
}

/*	--------------------------------------------------------------
	Function: _BCDigitizeCheckParity
	Params:
		scandata - array containing the raw bar/space width data
		ubar - unit bar width to use for digitization. This value
			is modified by this function.
		uspace - unit space width to use for digitization. This
			value is modified by this function.
		digitizeddata - array to hold digitized bar/space pattern
		processreverse - whether the scandata should be processed
			in reverse (in case of backward scan)
	Returns:
		BOOL - TRUE if resulting digitized parity is correct,
			FALSE if parity is invalid.
	--------------------------------------------------------------
	Purpose:
		This function digitizes raw bar/space width times into
		Code 128 patterns consisting of the numbers 1-4. It also
		checks/corrects the parity by calling
		_BCCode128CheckFixParity().
	------------------------------------------------------------ */
BOOL _BCDigitizeCheckParity(unsigned short* scandata, float* ubar, float* uspace, unsigned char* digitizeddata, BOOL processreverse)
{
	unsigned short	minwidth;
	float			unitwidth_average;
	float			unitwidth_bar;
	float			unitwidth_space;
	BOOL			unitisBar;
	float			minaveragewidth_difference;
	float			evenscansum;
	float			oddscansum;
	float			evendigitsum;
	float			odddigitsum;

	// First, find the smallest width. This is considered a '1'.
	_BCFindSmallestWidth(scandata,&minwidth,&unitisBar);
	// If we're processing backwards, the positions of the bars and spaces are flipped.
	if(processreverse)
		unitisBar = !unitisBar;

	// Now, find the average width. If bars and spaces were printed with identical widths,
	// this would be considered a '1'.
	unitwidth_average = (float)(scandata[0]+scandata[1]+scandata[2]+scandata[3]+scandata[4]+scandata[5]);
	unitwidth_average /= 11.0;

	// Find the difference between the "real" unit width and the average unit width.
	minaveragewidth_difference = unitwidth_average-(float)minwidth;

	if(unitisBar) {
		// If the minimum width is a bar, assume the minimum's deviance from the average is caused by incorrectly-sized spaces.
		unitwidth_bar = (float)minwidth;
		unitwidth_space = (float)minwidth+minaveragewidth_difference;
	} else {
		// If the minimum width is a space, assume the minimum's deviance from the average is caused by incorrectly-sized bars.
		unitwidth_bar =	(float)minwidth+minaveragewidth_difference;
		unitwidth_space = (float)minwidth;
	}

	if((*ubar != (float)0.0)&&(*uspace != (float)0.0)) {
		unitwidth_bar =	(unitwidth_bar+*ubar)/(float)2.0;
		unitwidth_space = (unitwidth_space+*uspace)/(float)2.0;
	}

	if(processreverse) {
		digitizeddata[0] = (unsigned char)(((float)scandata[5]/unitwidth_bar)+0.5);
		digitizeddata[1] = (unsigned char)(((float)scandata[4]/unitwidth_space)+0.5);
		digitizeddata[2] = (unsigned char)(((float)scandata[3]/unitwidth_bar)+0.5);
		digitizeddata[3] = (unsigned char)(((float)scandata[2]/unitwidth_space)+0.5);
		digitizeddata[4] = (unsigned char)(((float)scandata[1]/unitwidth_bar)+0.5);
		digitizeddata[5] = (unsigned char)(((float)scandata[0]/unitwidth_space)+0.5);
	} else {
		digitizeddata[0] = (unsigned char)(((float)scandata[0]/unitwidth_bar)+0.5);
		digitizeddata[1] = (unsigned char)(((float)scandata[1]/unitwidth_space)+0.5);
		digitizeddata[2] = (unsigned char)(((float)scandata[2]/unitwidth_bar)+0.5);
		digitizeddata[3] = (unsigned char)(((float)scandata[3]/unitwidth_space)+0.5);
		digitizeddata[4] = (unsigned char)(((float)scandata[4]/unitwidth_bar)+0.5);
		digitizeddata[5] = (unsigned char)(((float)scandata[5]/unitwidth_space)+0.5);
	}
	if(!_BCCode128CheckFixParity(digitizeddata))
		return FALSE;
	if(processreverse) {
		evenscansum	= (float)(scandata[1]+scandata[3]+scandata[5]);
		oddscansum	= (float)(scandata[0]+scandata[2]+scandata[4]);
	} else {
		oddscansum	= (float)(scandata[1]+scandata[3]+scandata[5]);
		evenscansum	= (float)(scandata[0]+scandata[2]+scandata[4]);
	}

	evendigitsum = (float)(digitizeddata[0]+digitizeddata[2]+digitizeddata[4]);
	odddigitsum  = (float)(digitizeddata[1]+digitizeddata[3]+digitizeddata[5]);

	*ubar = evenscansum/evendigitsum;
	*uspace = oddscansum/odddigitsum;
	return TRUE;
}

/*	--------------------------------------------------------------
	Function: _BCDigitizeFirst
	Params:
		scandata - array containing the raw bar/space width data
		digitizeddata - array to hold digitized bar/space pattern
		reverse - TRUE if the data should be processed as backwards
			scan. This function determines this.
	Returns:
		BOOL - TRUE if the scan data could be digitized.
	--------------------------------------------------------------
	Purpose:
		This function uses the first 6 scan values and known info
		about the start and stop patterns to calculate whether the
		scan data represents a forward or a reverse scan.
	------------------------------------------------------------ */
BOOL _BCDigitizeFirst(unsigned short* scandata, unsigned char* digitizeddata, BOOL* reverse)
{
	unsigned short tuspace;
	unsigned long forwarderror;
	unsigned long reverseerror;

	// Forward pattern: 2 1 1 2 3 2
	// Reverse pattern: 2 1 1 1 3 3

	// A unit width for space is known to be at position 1 for both
	// forward and reverse patterns.
	tuspace = scandata[1];

	// Both forward and reverse patterns share the following values:
	digitizeddata[0] = 2;
	digitizeddata[1] = 1;
	digitizeddata[2] = 1;
	digitizeddata[4] = 3;
	// digitizeddata[3] = 2 or 1. Don't know yet, so leave out.
	// digitizeddata[5] = 2 or 3. Don't know yet, so leave out.

	// Calculate the error for assuming the pattern is forward. We only
	// look at positions 3 & 5, because the rest are the same for both
	// forward and reverse patterns.
	forwarderror = abs((tuspace*2)-scandata[3]);
	forwarderror += abs((tuspace*2)-scandata[5]);

	// Calculate the error for assuming the pattern is reverse. We only
	// look at positions 3 & 5, because the rest are the same for both
	// forward and reverse patterns.
	reverseerror = abs((tuspace*1)-scandata[3]);
	reverseerror += abs((tuspace*3)-scandata[5]);

	// Now see which assumption produced a smaller error.
	if (forwarderror < reverseerror) {
		digitizeddata[3] = 2;
		digitizeddata[5] = 2;
		*reverse = FALSE;
	} else {
		digitizeddata[3] = 1;
		digitizeddata[5] = 3;
		*reverse = TRUE;
	}
	return TRUE;
}

/*	--------------------------------------------------------------
	Function: _BC1Barcode_Decode
	Params:
		void
	Returns:
		int - the number of characters in a successfully decoded
			barocde, or 0 if the barcode was invalid.
	--------------------------------------------------------------
	Purpose:
		This function attemtps to decode the global g_BC1scanarray.
		If a valid barcode is decoded, it is put back into
		g_BC1scanarray and the g_BC1decodedlength is set to the
		number of characters successfully decoded.
	------------------------------------------------------------ */
int _BC1Barcode_Decode(void)
{
	int i, j;
	long tlval;
	unsigned short tusval;
	int decodelen;
	int barcodelen;
	float ubar, uspace;
	int startchar;
	BOOL reverse;

   if(g_BC1scanptr > 10) {
		ubar = 0.0;
		uspace = 0.0;

		/*
		for(i = 0; i < g_BC1scanptr; i++){
			printf("%u,",g_BC1scanarray[i]);
		}
		printf("\n");
		*/

		g_BC1scanptr--;

		for(i = 0; i < g_BC1scanptr; i++){
			tlval = g_BC1scanarray[i+1]-g_BC1scanarray[i];
			if(tlval < 0)
				tlval = tlval+65535;
			tusval = (unsigned short)tlval;
			g_BC1scanarray[i] = tusval;
		}

		if(!_BCDigitizeFirst(g_BC1scanarray,g_BC1digitizeddata,&reverse))
			return 0;
		if (!reverse) {
			// Array position: 0 1 2 3 4 5
			// Bar/Space:      B S B S B S
			// Start pattern:  2 1 1 2 3 2
			// The first 6 bars comprise the "start" pattern. Calculate initial bar/space
			// values from this known pattern.
			ubar = (float)g_BC1scanarray[0]+(float)g_BC1scanarray[2]+(float)g_BC1scanarray[4];
			ubar /= (float)6.0;
			uspace = (float)g_BC1scanarray[1]+(float)g_BC1scanarray[3]+(float)g_BC1scanarray[5];
			uspace /= (float)5.0;
			for (i = 6; i < g_BC1scanptr; i+=6) {
				_BCDigitizeCheckParity(&g_BC1scanarray[i],&ubar,&uspace,&g_BC1digitizeddata[i],FALSE);
			}
		} else {
			j = g_BC1scanptr-7;
			// Array position (j+): 5 4 3 2 1 0
			// Bar/Space:           B S B S B S
			// Start pattern:       2 1 1 2 3 2
			// The last 6 bars comprise the "start" pattern. Calculate initial bar/space
			// values from this known pattern.
			ubar = (float)g_BC1scanarray[j+5]+(float)g_BC1scanarray[j+3]+(float)g_BC1scanarray[j+1];
			ubar /= (float)6.0;
			uspace = (float)g_BC1scanarray[j+4]+(float)g_BC1scanarray[j+2]+(float)g_BC1scanarray[j+0];
			uspace /= (float)5.0;
			j = 0;
			for (i = g_BC1scanptr-7; i >= 0; i-=6) {
				_BCDigitizeCheckParity(&g_BC1scanarray[i],&ubar,&uspace,&g_BC1digitizeddata[j],TRUE);
				j+=6;
			}
		}

//		printf("reverse: %u, g_BC1scanptr: %u\n",reverse,g_BC1scanptr);

		decodelen = 0;
		for(i = 0; i < (g_BC1scanptr-6); i+=6) {
			g_BC1scanarray[decodelen] = _BCCode128ConvertToValue(&g_BC1digitizeddata[i]);

/*
			if(g_BC1scanarray[decodelen] > 99) {
				printf("0x%02X ",g_BC1scanarray[decodelen]);
			} else {
				printf("%02d ",g_BC1scanarray[decodelen]);
			}
*/
			decodelen++;
		}
//		printf("\n");

		// Find the START character. If the barcode was scanned backwards,
		// it's possible this isn't the first character in the array. There
		// might have been some garbage.
		startchar = -1;
		for (i = 0; i < decodelen; i++) {
			if (g_BC1scanarray[i] == 105) {
				startchar = i;
				break;
			}
		}
		// If the START character wasn't found, quit.
		if (startchar < 0)
			return 0;
		// Shift the array down if the start character was not zero.
		if(startchar > 0) {
			for (i = startchar; i < decodelen; i++) {
				g_BC1scanarray[i-startchar] = g_BC1scanarray[i];
			}
		}
		// Adjust the decode length if we shrunk the scan array by advancing the start.
		decodelen = decodelen-startchar;

		// Now, make sure there's a valid STOP character at the end.
		barcodelen = 0;
		for(i = 0; i < decodelen; i++) {
			if(g_BC1scanarray[i] == 106) {
				barcodelen = i+1;
				break;
			}
		}
		// Check that the STOP character was found and that the barcode is not too long
		if((barcodelen < 4)||(barcodelen > BC_MAXBARCODELEN))
			return 0;
		tlval = g_BC1scanarray[0];
		for(i = 1; i < barcodelen-2; i++)
			tlval += (g_BC1scanarray[i]*i);
//		printf("  Length: %u, sum mod 103: %lu\n\n",barcodelen,tlval%103);
		if((tlval % 103) != g_BC1scanarray[barcodelen-2])
			return 0;

		// Copy decoded length to global buffer.
		g_BC1decodedlength = barcodelen;
		return barcodelen;
	}
	return 0;
}

/*	--------------------------------------------------------------
	Function: _BC2Barcode_Decode
	Params:
		void
	Returns:
		int - the number of characters in a successfully decoded
			barocde, or 0 if the barcode was invalid.
	--------------------------------------------------------------
	Purpose:
		This function attemtps to decode the global g_BC2scanarray.
		If a valid barcode is decoded, it is put back into
		g_BC2scanarray and the g_BC2decodedlength is set to the
		number of characters successfully decoded.
	------------------------------------------------------------ */
int _BC2Barcode_Decode(void)
{
	int i, j;
	long tlval;
	unsigned short tusval;
	int decodelen;
	int barcodelen;
	float ubar, uspace;
	int startchar;
	BOOL reverse;

	if(g_BC2scanptr > 10) {
		ubar = 0.0;
		uspace = 0.0;

		/*
		for(i = 0; i < g_BC1scanptr; i++){
			printf("%u,",g_BC1scanarray[i]);
		}
		printf("\n");
		*/

		g_BC2scanptr--;

		for(i = 0; i < g_BC2scanptr; i++){
			tlval = g_BC2scanarray[i+1]-g_BC2scanarray[i];
			if(tlval < 0)
				tlval = tlval+65535;
			tusval = (unsigned short)tlval;
			g_BC2scanarray[i] = tusval;
		}

		if(!_BCDigitizeFirst(g_BC2scanarray,g_BC2digitizeddata,&reverse))
			return 0;
		if (!reverse) {
			// Array position: 0 1 2 3 4 5
			// Bar/Space:      B S B S B S
			// Start pattern:  2 1 1 2 3 2
			// The first 6 bars comprise the "start" pattern. Calculate initial bar/space
			// values from this known pattern.
			ubar = (float)g_BC2scanarray[0]+(float)g_BC2scanarray[2]+(float)g_BC2scanarray[4];
			ubar /= (float)6.0;
			uspace = (float)g_BC2scanarray[1]+(float)g_BC2scanarray[3]+(float)g_BC2scanarray[5];
			uspace /= (float)5.0;
			for (i = 6; i < g_BC2scanptr; i+=6) {
				_BCDigitizeCheckParity(&g_BC2scanarray[i],&ubar,&uspace,&g_BC2digitizeddata[i],FALSE);
			}
		} else {
			j = g_BC2scanptr-7;
			// Array position (j+): 5 4 3 2 1 0
			// Bar/Space:           B S B S B S
			// Start pattern:       2 1 1 2 3 2
			// The last 6 bars comprise the "start" pattern. Calculate initial bar/space
			// values from this known pattern.
			ubar = (float)g_BC2scanarray[j+5]+(float)g_BC2scanarray[j+3]+(float)g_BC2scanarray[j+1];
			ubar /= (float)6.0;
			uspace = (float)g_BC2scanarray[j+4]+(float)g_BC2scanarray[j+2]+(float)g_BC2scanarray[j+0];
			uspace /= (float)5.0;
			j = 0;
			for (i = g_BC2scanptr-7; i >= 0; i-=6) {
				_BCDigitizeCheckParity(&g_BC2scanarray[i],&ubar,&uspace,&g_BC2digitizeddata[j],TRUE);
				j+=6;
			}
		}

//		printf("reverse: %u, g_BC1scanptr: %u\n",reverse,g_BC1scanptr);

		decodelen = 0;
		for(i = 0; i < (g_BC2scanptr-6); i+=6) {
			g_BC2scanarray[decodelen] = _BCCode128ConvertToValue(&g_BC2digitizeddata[i]);

/*
			if(g_BC1scanarray[decodelen] > 99) {
				printf("0x%02X ",g_BC1scanarray[decodelen]);
			} else {
				printf("%02d ",g_BC1scanarray[decodelen]);
			}
*/
			decodelen++;
		}
//		printf("\n");

		// Find the START character. If the barcode was scanned backwards,
		// it's possible this isn't the first character in the array. There
		// might have been some garbage.
		startchar = -1;
		for (i = 0; i < decodelen; i++) {
			if (g_BC2scanarray[i] == 105) {
				startchar = i;
				break;
			}
		}
		// If the START character wasn't found, quit.
		if (startchar < 0)
			return 0;
		// Shift the array down if the start character was not zero.
		if(startchar > 0) {
			for (i = startchar; i < decodelen; i++) {
				g_BC2scanarray[i-startchar] = g_BC2scanarray[i];
			}
		}
		// Adjust the decode length if we shrunk the scan array by advancing the start.
		decodelen = decodelen-startchar;

		// Now, make sure there's a valid STOP character at the end.
		barcodelen = 0;
		for(i = 0; i < decodelen; i++) {
			if(g_BC2scanarray[i] == 106) {
				barcodelen = i+1;
				break;
			}
		}
		// Check that the STOP character was found and that the barcode is not too long
		if((barcodelen < 4)||(barcodelen > BC_MAXBARCODELEN))
			return 0;
		tlval = g_BC2scanarray[0];
		for(i = 1; i < barcodelen-2; i++)
			tlval += (g_BC2scanarray[i]*i);
//		printf("  Length: %u, sum mod 103: %lu\n\n",barcodelen,tlval%103);
		if((tlval % 103) != g_BC2scanarray[barcodelen-2])
			return 0;

		// Copy decoded length to global buffer.
		g_BC2decodedlength = barcodelen;
		return barcodelen;
	}
	return 0;
}




/*** BeginHeader bc_isr */
root void bc_isr();
/*** EndHeader */

#asm debug
bc_isr::
	push	af
	push	bc
	push	de
	push	hl
	push	iy

	ioi	ld		a, (ICCSR)		; see which scanner triggered/clear ICCSR
   ld		(readIccsr), a       ; Save a copy of the current ICCSR status

	; Check for activity on IC1
   ;rjt and   IC1_ACTIVITY			; see if interrupted because of IC1 stuff
   ;rjt jr    nz, checkIc1
   jp	   checkIc2

checkIc1:
	ld		a, (g_enableIC1interrupt)
   and	1
   jr		nz, contIc1
   jp		checkIc2

contIc1:
	ld		a, (g_BC1scanflags)
	and	BC_SFSCANENABLE		; should we continue to get samples?
	jr		z, checkIc2          ; if z (zero) then exit

	ld		a, (g_BC1scanflags)
	and	BC_SFSCANNING		; are we continuing a scan?
	jr		nz, scancontinue

	; if starting a scan, set the scanning flag and enable both START and STOP interrupts
	ld		a, (g_BC1scanflags)
	or		BC_SFSCANNING
	ld		(g_BC1scanflags), a
	ld		a, 0xB6				; latch both START and STOP conditions
	ioi	ld		(ICT1R), a
   ld    a, (g_copyIccsr)
	or		0x34					; enable both IC1 START, STOP, and overflow interrupts
   ;rjt or		0x30					; enable both IC1 START, STOP interrupts
	ioi	ld		(ICCSR), a
	bool	hl						; clear hl
	rr		hl
	jr		skipread

scancontinue:
	ld		de, ICL1R
	ioi	ld		a, (de)	; Read the Input Capture LSB
	ld		l, a
	inc	de
	ioi	ld		a, (de)	; Read the Input Capture MSB
	ld		h,a
skipread:
	push	hl

	ld    hl, (g_BC1scanptr)	; Load scan pointer
	add	hl, hl				; Multiply by two since the array is 16-bit
   ex		de, hl				; store in de, since hl can not be added to iy
	ld		iy, g_BC1scanarray
	add	iy, de				; add offset to scan array
	pop	hl
	ld		(iy+0),hl

	; increment scan pointer
	ld		de, (g_BC1scanptr)
	inc	de
	ld		(g_BC1scanptr), de
	ld		hl, BC_SCANARRAYSIZE
	scf
	sbc	hl, de
	jp		nc, keepscanning
	; if we've scanned the max number of transitions, disable scanning.
	ld		a, (g_BC1scanflags)
	and	a, ~BC_SFSCANENABLE
	ld		(g_BC1scanflags), a
keepscanning:

	;Check for activity on IC2
checkIc2:
   ld		a, (g_enableIC2interrupt)
   and	1
   jr		nz, contIc2
   jp		alldone

contIc2:
	ld		a, (readIccsr)			; reload the reasons for this interrupt
   and   IC2_ACTIVITY			; see if interrupted because of IC2 stuff
   jr    nz, continueIc2
   jp		alldone

continueIc2:
   ld		a, (g_BC2scanflags)
	and	BC_SFSCANENABLE		; should we continue to get samples?
	jr		z, alldone           ; if z (zero) then exit

	ld		a, (g_BC2scanflags)
	and	BC_SFSCANNING			; are we continuing a scan?
	jr		nz, scan2continue

	; if starting a scan, set the scanning flag and enable both START and STOP interrupts
	ld		a, (g_BC2scanflags)
	or		BC_SFSCANNING
	ld		(g_BC2scanflags), a
	ld		a, 0xB6				; latch both START and STOP conditions
	ioi	ld		(ICT2R), a
   ld    a, (g_copyIccsr)
	;rjt or		0xC8					; enable both IC2 START, STOP, and overflow interrupts
   or		0xC0					; enable both IC2 START, STOP interrupts
   ioi	ld		(ICCSR), a
	bool	hl						; clear hl
	rr		hl
	jr		skip2read

scan2continue:
	ld		de, ICL2R
	ioi	ld		a, (de)				; Read the Input Capture LSB
	ld		l, a
	inc	de
	ioi	ld		a, (de)				; Read the Input Capture MSB
	ld		h,a
skip2read:
	push	hl

	ld    hl, (g_BC2scanptr)	; Load scan pointer
	add	hl, hl				; Multiply by two since the array is 16-bit
   ex		de, hl				; store in de, since hl can not be added to iy
	ld		iy, g_BC2scanarray
	add	iy, de				; add offset to scan array
	pop	hl
	ld		(iy+0),hl

	; increment scan pointer
	ld		de, (g_BC2scanptr)
   inc	de
	ld		(g_BC2scanptr), de
	ld		hl, BC_SCANARRAYSIZE
	scf
	sbc	hl, de
	jp		nc, keep2scanning
	; if we've scanned the max number of transitions, disable scanning.
	ld		a, (g_BC2scanflags)
	and	a, ~BC_SFSCANENABLE
	ld		(g_BC2scanflags), a
keep2scanning:

alldone:
	ipres
	pop		iy
	pop		hl
	pop		de
	pop		bc
	pop		af
	ret

#endasm

/*** BeginHeader */
#endif //#ifndef _BARCODE_C_
/*** EndHeader */


