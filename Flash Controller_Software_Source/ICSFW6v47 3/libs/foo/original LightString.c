/* --------------------------------------------------------------
	Copyright (C) 2011 Ideal Software Systems. All Rights Reserved.

	Filename:	LightString.c
	By:			Rob Toth
	Purpose:	   Run the daisy chained LED light string using the WS2801.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	08-31-2011	v1.0
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _LIGHT_STRING_C_
#define _LIGHT_STRING_C_

#define LS_SET_BIT_HIGH		1
#define LS_SET_BIT_LOW  	0
#define LS_CLOCK_LINE  		7    //Port G pin 7
#define LS_DATA_LINE   		6    //Port G pin 6

#define LS_CLOCKS_PER_DEVICE 		24
#define LS_NUMBER_WS2801_DEVICES 8
#define LS_REFRESH_PERIOD 			2000 //2 seconds
#define LS_BIT24				0x00800000

/*** EndHeader */

/*** BeginHeader LightString_Init */
/*	--------------------------------------------------------------
	Function: LightString_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the Light String library.
	------------------------------------------------------------ */
void LightString_Init(void);
/*** EndHeader */

extern unsigned long g_colorOne;
extern unsigned long g_colorTwo;
extern unsigned long	g_lightStringTimer;

unsigned long g_colorOne;
unsigned long g_colorTwo;
unsigned long g_lightStringTimer;

void LightString_Init(void)
{
   //Setup the RFID port to excerise the LED control circuits
   // Ensure PG6 is output.
   BitWrPortI(PGDDR, &PGDDRShadow, 1, LS_LED_DATA_LINE); //LED Data out
   // Ensure PG7 is output.
   BitWrPortI(PGDDR, &PGDDRShadow, 1, LS_LED_CLOCK_LINE); //LED clock out

   BitWrPortI(PGDCR, &PGDDRShadow, 0, LS_LED_DATA_LINE); //LED Data out can be driven high or low
   BitWrPortI(PGDCR, &PGDCRShadow, 0, LS_LED_CLOCK_LINE); //LED clock out can be driven high or low

   BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_LOW, LS_LED_DATA_LINE);
   BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_LOW, LS_LED_CLOCK_LINE);

   g_colorOne = 0;
   g_colorTwo = 0;
   g_lightStringTimer = (MS_TIMER + LS_REFRESH_PERIOD);

   DebugPrint(STRACE_NFO,("LED Test harness on connector P4"));
}

/*** BeginHeader HandleLightString */
/*	--------------------------------------------------------------
	Function: HandleLightString
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Control the daisy chained LEDs

	------------------------------------------------------------ */
void HandleLightString(void);
/*** EndHeader */
void HandleLightString(void)
{
   unsigned short clockCount;
   unsigned long mask;

   mask = LS_BIT24;

   if(Util_CheckmsTimeout(g_lightStringTimer)
   {
   	g_lightStringTimer = MS_TIMER + LS_REFRESH_PERIOD; //Reload for next time
      clockCount = 0;

   	while( clockCount++ < (LS_CLOCKS_PER_DEVICE * LS_NUMBER_WS2801_DEVICES) )
   	{
      	//Bring clock line low
      	BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_LOW, LS_LED_CLOCK_LINE);

      	//Get next bit to send out
      	if( g_colorOne & mask )
         {
      		BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_HIGH, LS_LED_DATA_LINE);
         }
      	else
         {
         	BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_LOW, LS_LED_DATA_LINE);
         }

      	//Adjust mask for next time through
      	mask = mask >> 1;
      	if(mask == 0)
      		mask = LS_BIT24;

      	//Bring clock line high
      	BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_HIGH, LS_LED_CLOCK_LINE);
   	}
      //Clock line low to start latch data time out
   	BitWrPortI(PGDR, &PGDRShadow, LS_SET_BIT_LOW, LS_LED_CLOCK_LINE);
   }

}



/*** BeginHeader */
#endif // #ifndef _LIGHT_STRING_C_
/*** EndHeader */

