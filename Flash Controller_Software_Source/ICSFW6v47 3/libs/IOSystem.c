/*	--------------------------------------------------------------
	Copyright (C) 2006 Ideal Software Systems. All Rights Reserved.

	Filename:	IOSystem.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Skoobe I/O System.
	Target(s):	Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

	------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _IOSYSTEM_C_
#define _IOSYSTEM_C_
/*** EndHeader */

/*** BeginHeader IOSys_Init */
/*	--------------------------------------------------------------
	Function: IOSys_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the IO System.
	------------------------------------------------------------ */
#ifdef WIN32
#include "libs\ShiftRegIO.c"
#include "libs\DIOPDefines.h"
#else
#use "ShiftRegIO.c"
#use "DIOPDefines.h"
#endif

void IOSys_Init(void);

typedef struct IO_INPUT_TYPE;
typedef struct IO_OUTPUT_TYPE;

#ifdef WIN32
typedef void(*IOInputStateRoutine)(struct IO_INPUT_TYPE* inputtoproc);
typedef void(*IOOutputStateRoutine)(struct IO_OUTPUT_TYPE* outputtoproc);
#else
typedef void(*IOInputStateRoutine)();
typedef void(*IOOutputStateRoutine)();
#endif

typedef struct IO_OUTPUT_TYPE {
	// Static setup, based upon hardware
	mssg_DIOP_t*			diop;			// The DIOP associated with this output
	mssg_DIOP_t*			defaultdiop;	// The default DIOP associated with this output
   												//	(diop may be temporarily overridden)
	unsigned char			outputbit;		// The hardware bit number of this output
	// Dynamic setup, based upon configuration
	struct IO_OUTPUT_TYPE*	next;			// Next output in the processing chain
	BOOL					dioenable;		// True = DIO processing is enabled
	// State variables
	unsigned long			timr;			// Signal transition timer
	IOInputStateRoutine		stateroutine;	// Current signal timing state
	int						patterncount;	// Current pattern count
} io_output_type;

typedef struct IO_INPUT_TYPE {
	// Static setup, based upon hardware
	mssg_DIOP_t*			diop;			// The DIOP associated with this input
	unsigned char*			iosrc;			// The hardware location of this input
	unsigned char			iomask;			// The bit mask for the hardware location of this input
	// Dynamic setup, based upon configuration
	struct IO_INPUT_TYPE*	next;			// Next input in the processing chain
	io_output_type*			EchoOutput;		// Output to echo signal to, if echoing required
	BOOL					sendDIOS;		// Whether a DIOS message should be sent for this input
	BOOL					dioenable;		// True = DIO processing is enabled
	// State variables
	unsigned long			timr;			// Signal transition timer
	unsigned long			EOStimr;		// End of signal timer
	IOInputStateRoutine		stateroutine;	// Current signal timing state
	int						patterncount;	// Current pattern count
} io_input_type;

extern char g_InputSHIFTREG;
extern char g_InputPORTE;
extern char g_InputPORTF;

extern io_input_type g_inputs[MAXINPUTS];	// Storage for all inputs
extern io_input_type* g_HSInputs;			// List of high speed inputs
extern io_input_type* g_LSInputs;			// List of low speed inputs
extern io_input_type* g_AAInputs;			// List of all active inputs (high and low speed)

extern io_output_type g_outputs[MAXOUTPUTS];// Storage for all outputs
extern io_output_type* g_AAOutputs;			// List of all active outputs

extern mssg_DIOP_t g_RelayOverrideDiop;

extern int* g_winningscounter;				// This points to where we should look to collect
														//  winnings. It changes based upon the dispenser type

extern unsigned long* g_dispensingEOStimer;	// This points to where we should look to see if
											// the dispense signal has finished. This may change based
                                 // upon dispenser type.

extern BOOL g_dispensing;					// This is set to TRUE while dispensing.

extern BOOL g_sent_notch;
extern int lcd_inc_tkts;

/*** EndHeader */
char g_InputSHIFTREG;
char g_InputPORTE;
char g_InputPORTF;

io_input_type g_inputs[MAXINPUTS];
io_input_type* g_HSInputs;
io_input_type* g_LSInputs;
io_input_type* g_AAInputs;

io_output_type g_outputs[MAXOUTPUTS];
io_output_type* g_AAOutputs;

mssg_DIOP_t g_RelayOverrideDiop;

int* g_winningscounter;
unsigned long* g_dispensingEOStimer;
BOOL g_dispensing;
BOOL g_sent_notch;
//int lcd_inc_tkts;

void IOSys_Init(void)
{
	int i;

	SRIO_Init();
	SRIO_OutputEnable(FALSE);

	WrPortI(PEDDR,&PEDDRShadow,PEDDRShadow & ~0x03);
	WrPortI(PEFR,&PEFRShadow,PEFRShadow & ~0x03);

	WrPortI(PFDDR,&PFDDRShadow,PFDDRShadow & ~0xE1);
	WrPortI(PFFR,&PFFRShadow,PFFRShadow & ~0xE1);

	g_InputSHIFTREG = SRIO_InputRead();
	g_InputPORTE = RdPortI(PEDR);
	g_InputPORTF = RdPortI(PFDR);

	g_HSInputs = NULL;
	g_LSInputs = NULL;
	g_AAInputs = NULL;
	g_AAOutputs = NULL;
	g_winningscounter = NULL;
	g_dispensing = FALSE;
   g_sent_notch = FALSE;
   lcd_inc_tkts = 0;

	// Setup input DIOPS and initialize pattern counts
	for (i = 0; i < MAXINPUTS; i++) {
		g_inputs[i].diop			= &G_STOR_DIOP_MSG[i];
		g_inputs[i].patterncount	= 0;
		g_inputs[i].stateroutine	= IOISWaitOn;
		g_inputs[i].EchoOutput		= NULL;	// Default to no echoing for this input
		g_inputs[i].sendDIOS		= TRUE;	// Default to sending a DIOS message for this input
		g_inputs[i].dioenable		= TRUE;	// Default to processing the I/O
	}
	// Setup input sources & masks. These don't change. Each input corresponds to a
	// specific port. The ports are read in the interrupt routine & the values are
	// placed into globals that are associated with each input.
	g_inputs[0].iosrc	= &g_InputSHIFTREG;
	g_inputs[0].iomask	= 0x01;
	g_inputs[1].iosrc	= &g_InputPORTE;
	g_inputs[1].iomask	= 0x02;
	g_inputs[2].iosrc	= &g_InputPORTF;
	g_inputs[2].iomask	= 0x40;
	g_inputs[3].iosrc	= &g_InputSHIFTREG;
	g_inputs[3].iomask	= 0x10;
	g_inputs[4].iosrc	= &g_InputSHIFTREG;
	g_inputs[4].iomask	= 0x20;
	g_inputs[5].iosrc	= &g_InputPORTF;
	g_inputs[5].iomask	= 0x20;
	g_inputs[6].iosrc	= &g_InputPORTF;
	g_inputs[6].iomask	= 0x80;
	g_inputs[7].iosrc	= &g_InputPORTE;
	g_inputs[7].iomask	= 0x01;
	g_inputs[8].iosrc	= &g_InputSHIFTREG;
	g_inputs[8].iomask	= 0x02;
	g_inputs[9].iosrc	= &g_InputSHIFTREG;
	g_inputs[9].iomask	= 0x04;
	g_inputs[10].iosrc	= &g_InputSHIFTREG;
	g_inputs[10].iomask	= 0x08;
	g_inputs[11].iosrc	= &g_InputSHIFTREG;
	g_inputs[11].iomask	= 0x40;
	g_inputs[12].iosrc	= &g_InputSHIFTREG;
	g_inputs[12].iomask	= 0x80;
	g_inputs[13].iosrc	= &g_InputPORTF;
	g_inputs[13].iomask	= 0x01;

	// Setup output DIOPS and initialize pattern counts
	for (i = 0; i < MAXOUTPUTS; i++) {
		g_outputs[i].diop = &G_STOR_DIOP_MSG[MAXINPUTS+i];
		g_outputs[i].defaultdiop	= g_outputs[i].diop;
		g_outputs[i].patterncount	= 0;
		g_outputs[i].stateroutine	= IOOSTurnOn;
		g_outputs[i].dioenable		= TRUE;	// Default to processing the I/O
	}
	// Setup output sources & masks. These don't change. Each output corresponds to
	// a specific shift register bit.
	g_outputs[0].outputbit = 3;
	g_outputs[1].outputbit = 4;
	g_outputs[2].outputbit = 1;
	g_outputs[3].outputbit = 2;
	g_outputs[4].outputbit = 0;
	g_outputs[5].outputbit = 5;
	g_outputs[6].outputbit = 7;
	g_outputs[7].outputbit = 6;

	g_dispensingEOStimer = &g_inputs[DIOP_IDISPENSESIGNALFROMGAME].EOStimr;

	memset(&g_RelayOverrideDiop,0,sizeof(g_RelayOverrideDiop));
}

/*** BeginHeader IOAddToInputProcessingChain */
/*	--------------------------------------------------------------
	Function: IOAddToInputProcessingChain
	--------------------------------------------------------------
	Purpose:	Adds an input to a processing chain
	------------------------------------------------------------ */
void IOAddToInputProcessingChain(io_input_type** chain, io_input_type* inputtoadd);
/*** EndHeader */
void IOAddToInputProcessingChain(io_input_type** chain, io_input_type* inputtoadd)
{
	io_input_type* tchain;
	if(!(*chain)) {
		*chain = inputtoadd;
		inputtoadd->next = NULL;
	} else {
		tchain = *chain;
		while (tchain->next) {
			tchain = tchain->next;
		}
		tchain->next = inputtoadd;
		inputtoadd->next = NULL;
	}
}

/*** BeginHeader IOAddToOutputProcessingChain */
/*	--------------------------------------------------------------
	Function: IOAddToOutputProcessingChain
	--------------------------------------------------------------
	Purpose: Adds an output to a processing chain
	------------------------------------------------------------ */
void IOAddToOutputProcessingChain(io_output_type** chain, io_output_type* outputtoadd);
/*** EndHeader */
void IOAddToOutputProcessingChain(io_output_type** chain, io_output_type* outputtoadd)
{
	io_output_type* tchain;
	if(!(*chain)) {
		*chain = outputtoadd;
		outputtoadd->next = NULL;
	} else {
		tchain = *chain;
		while (tchain->next) {
			tchain = tchain->next;
		}
		tchain->next = outputtoadd;
		outputtoadd->next = NULL;
	}
}

/*** BeginHeader IOSys_GetDispenserWinnings */
/*	--------------------------------------------------------------
	Function: IOSys_GetDispenserWinnings
	Returns:
		int - the number of tickets dispensed or spoofed.
	--------------------------------------------------------------
	Purpose:
		Retrieves the number of tickets dispensed or spoofed.
	------------------------------------------------------------ */
int IOSys_GetDispenserWinnings(void);
/*** EndHeader */
int IOSys_GetDispenserWinnings(void)
{
	if(g_winningscounter)
		return *g_winningscounter;
	return 0;
}

/*** BeginHeader IOSys_IsDispensing */
/*	--------------------------------------------------------------
	Function: IOSys_IsDispensing
	Returns:
		BOOL - TRUE if currently dispensing.
	--------------------------------------------------------------
	Purpose:
		Retrieves whether or not game is currently dispensing.
	------------------------------------------------------------ */
BOOL IOSys_IsDispensing(void);
/*** EndHeader */
BOOL IOSys_IsDispensing(void)
{
	return g_dispensing;
}

/*** BeginHeader IOSys_Dispense */
/*	--------------------------------------------------------------
	Function: IOSys_Dispense
		ntickets - number of tickets to dispense.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Manually dispenses tickets.
	------------------------------------------------------------ */
void IOSys_Dispense(int ntickets);
/*** EndHeader */
void IOSys_Dispense(int ntickets)
{
	g_outputs[DIOP_ONOTCHSIGNALTOGAME].patterncount
   	+= (int)(g_outputs[DIOP_ONOTCHSIGNALTOGAME].diop->cycles * ntickets);
}

/*** BeginHeader IOSys_SetupDispenser */
/*	--------------------------------------------------------------
	Function: IOSys_SetupDispenser
	--------------------------------------------------------------
	Purpose: Sets up echoing or spoofing based upon dispenser type
		and ticketless options. This needs to happen once before
		we get an ACCT because we might be in token interactive
		mode, which needs the dispenser configured before a user
		swipes a card. It also needs to happen after an ACCT
		because the ACCT might request that we go from dispensing
		tickets to ticketless.
	------------------------------------------------------------ */
void IOSys_SetupDispenser(void);
/*** EndHeader */
void IOSys_SetupDispenser(void)
{
	BOOL ticketless;

	ticketless = G_STOR_CNFG_MSG.nodispense;

	if(G_STOR_DEVC_MSG.dispenstype == DISPENSETYPE_NONE) {
		DebugPrint(STRACE_DBG,("No dispenser."));
      // Use normal Input states.
		g_inputs[DIOP_IDISPENSESIGNALFROMGAME].stateroutine = IOISWaitOn;
		g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].stateroutine	= IOISWaitOn;
		g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].dioenable		= TRUE;
		g_dispensing = FALSE;
		return;
	}

	// First default to sending DIOS messages for all dispenser-related signals.
	// This will be disabled based upon the dispenser type.
	g_inputs[DIOP_IDISPENSESIGNALFROMGAME].sendDIOS = TRUE;
	g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].sendDIOS = TRUE;

	// If ticketless token interactive, the ACCT message must be ignored.
	if(Storage_IsTicketlessTokenInteractive()) {
		ticketless = TRUE;
	} else {
		// Otherwise, a valid ACCT determines whether we're ticketless.
		if(G_STOR_ACCT_MSG.acctvalid)
			ticketless = G_STOR_ACCT_MSG.nodispense;
		// If ACCT is not valid, default to the configuration's setting.
	}
	if(ticketless) {
		DebugPrint(STRACE_DBG,("Ticketless."));

		// Input 11 is the notch signal from the dispenser.
		// There is no notch signal in ticketless mode, so don't process it.
		g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].dioenable = FALSE;

		// We watch the dispense signal from the game to determine whether
		// we're currently dispensing.
		g_dispensingEOStimer = &g_inputs[DIOP_IDISPENSESIGNALFROMGAME].EOStimr;

		// If ticketless, we need to pretend to be a dispenser.
      //sansom
		switch(G_STOR_DEVC_MSG.dispenstype) {
		case DISPENSETYPE_PULSE:
			DebugPrint(STRACE_DBG,("Pulse dispenser."));
			// Input 10 is the pulsed dispense signal from the game. Generate the fake
			// notch signal on DIO ID 20, the notch signal to the game.
			g_inputs[DIOP_IDISPENSESIGNALFROMGAME].EchoOutput
         	= &g_outputs[DIOP_ONOTCHSIGNALTOGAME];
			g_inputs[DIOP_IDISPENSESIGNALFROMGAME].stateroutine
         	= IOISPulseDispenseSpoofWaitOn; // Use the pulse dispenser states.
			g_inputs[DIOP_IDISPENSESIGNALFROMGAME].sendDIOS	= TRUE;

			// In pulsed mode, we get one pulse per ticket the game wishes to dispense.
			// Winnings will be accumulated by counting the pulses from the game.
			g_winningscounter = &g_inputs[DIOP_IDISPENSESIGNALFROMGAME].patterncount;
			break;
		case DISPENSETYPE_MOTOR:
			DebugPrint(STRACE_DBG,("Motor dispenser."));
			// Input 10 is the motor dispense signal from the game. Generate the fake
			// notch signal on DIO ID 20, the notch signal to the game.
			g_inputs[DIOP_IDISPENSESIGNALFROMGAME].EchoOutput
         	= &g_outputs[DIOP_ONOTCHSIGNALTOGAME];
			g_inputs[DIOP_IDISPENSESIGNALFROMGAME].stateroutine
         	= IOISDriveDispenseSpoofWhileOn; // Use the motor dispenser states.
			g_inputs[DIOP_IDISPENSESIGNALFROMGAME].sendDIOS	= TRUE;

			// In drive mode, we get a constant high or low signal & we send spoofed ticket
         // pulses back to the game.
			// The game deactivates the signal when we have sent enough pulses. Winnings
         // will be accumulated by counting the spoofed pulses we create. Because
         // the spoofing mechanism decrements the output's pattern count as output
         // pulses are generated, the winnings are stored in the input's storage.
			g_winningscounter = &g_inputs[DIOP_IDISPENSESIGNALFROMGAME].patterncount;
			break;
		}
	} else {
		switch(G_STOR_DEVC_MSG.dispenstype) {
		case DISPENSETYPE_PULSE:
			DebugPrint(STRACE_DBG,("Pulse dispenser."));
			break;
		case DISPENSETYPE_MOTOR:
			DebugPrint(STRACE_DBG,("Motor dispenser."));
			break;
		}
		// If not ticketless, just pass the game and dispenser signals through to one another.
		// If we're not ticketless, it's okay to generate a DIOS automatically from a
      // non-zero pulsecount

		// Input 10 is the dispense signal from the game. Echo this to DIO ID 21, the
      // dispense signal to the dispenser.
		g_inputs[DIOP_IDISPENSESIGNALFROMGAME].EchoOutput
      	= &g_outputs[DIOP_ODISPENSESIGNALTODISPENSER];
		g_inputs[DIOP_IDISPENSESIGNALFROMGAME].stateroutine
      	= IOISDispenseWaitOnEcho; // Use the echoing states.
		// Don't send DIOS for dispense signal from game. We should only send a DIOS for
      // tickets dispensed.
		g_inputs[DIOP_IDISPENSESIGNALFROMGAME].sendDIOS	= FALSE;

		// Input 11 is the notch signal from the dispenser. Echo this to DIO ID 20, the
      // notch signal to the game.
		g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].EchoOutput
      	= &g_outputs[DIOP_ONOTCHSIGNALTOGAME];
		g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].stateroutine
      	= IOISDispenseWaitOnEcho; // Use the echoing states.
		// Enable processing of the notch signal from the dispenser.
      // (ticketless mode may have disabled it)
		g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].dioenable = TRUE;
		// Winnings will be accumulated by counting the pulses from the notch sensor.
		g_winningscounter = &g_inputs[DIOP_INOTCHSIGNALFROMDISPENSER].patterncount;

		// We watch the dispense signal from the game to determine whether
		// we're currently dispensing.
		g_dispensingEOStimer = &g_inputs[DIOP_IDISPENSESIGNALFROMGAME].EOStimr;
	}
}

/*** BeginHeader IOSys_SetupProcessingFromConfig */
/*	--------------------------------------------------------------
	Function: IOSys_SetupProcessingFromConfig
	--------------------------------------------------------------
	Purpose: Sets up the list of high and low speed inputs.
	------------------------------------------------------------ */
void IOSys_SetupProcessingFromConfig(void);
/*** EndHeader */
void IOSys_SetupProcessingFromConfig(void)
{
	int i;
	int hsctr;

	ipset(3);

	g_HSInputs = NULL;
	g_LSInputs = NULL;
	g_AAInputs = NULL;
	g_AAOutputs = NULL;

	hsctr = 0;
	for (i = 0; i < MAXINPUTS; i++) {
		if (g_inputs[i].dioenable) {
			if ( ((g_inputs[i].diop->timeon < 10) || (g_inputs[i].diop->timeoff < 10)
         		|| (g_inputs[i].diop->eosdetect < 10))
					&& (hsctr < 4) ) {
				// Input is considered high-speed. Add to high-speed chain. (to be processed
            // via interrupt)
				// Note that, due to processing resources, we can only process 4 high
            // speed inputs. Therefore, only the first 4 high speed inputs are added
            // to this chain. The rest go on the low speed chain.
				IOAddToInputProcessingChain(&g_HSInputs,&g_inputs[i]);
				hsctr++;
			} else {
				// Input is considered low-speed. Add to low-speed chain. (to be processed
            // via main loop)
				IOAddToInputProcessingChain(&g_LSInputs,&g_inputs[i]);
			}
			// Add to chain of all active inputs.
			IOAddToInputProcessingChain(&g_AAInputs,&g_inputs[i]);
		}
	}

	for (i = 0; i < MAXOUTPUTS; i++) {
		if (g_outputs[i].dioenable)
			IOAddToOutputProcessingChain(&g_AAOutputs,&g_outputs[i]);
	}

	ipres();
	DebugPrint(STRACE_DBG,("Configured %d inputs for High Speed",hsctr));
}

/*** BeginHeader IOSys_OutputsOff */
/*	--------------------------------------------------------------
	Function: IOSys_OutputsOff
	--------------------------------------------------------------
	Purpose: Put all outputs in an off state (according to
		configuration) and enable outputs.
	------------------------------------------------------------ */
void IOSys_OutputsOff(void);
/*** EndHeader */
void IOSys_OutputsOff(void)
{
	int i;
	// Turn all outputs "off". They will be set high or low depending upon their signal type.
	for(i = 0; i < MAXOUTPUTS; i++) {
		SRIO_OutputSetBit(g_outputs[i].outputbit,0^g_outputs[i].diop->xormask);
	}
	SRIO_OutputEnable(TRUE);
}

/*** BeginHeader IOSys_DeviceStart */
/*	--------------------------------------------------------------
	Function: IOSys_DeviceStart
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to assert.
	------------------------------------------------------------ */
void IOSys_DeviceStart(int tMode);
/*** EndHeader */
void IOSys_DeviceStart(int tMode)
{
	char selectedRelay;
	selectedRelay = DIOP_ORELAY1;
	if(tMode > 0) {
		switch (G_STOR_DEVC_MSG.gameacttype) {
		case 0:
			// Game isn't set to start anything
			return;
		case 1:
		case 3:	/*TODO: This is supposed to be a 2-player setting, but this Skoobe
      						doesn't support two players. */
			// One Player, Relay 1
			selectedRelay = DIOP_ORELAY1;
			break;
		case 2:
			// One Player, Relay 2
			selectedRelay = DIOP_ORELAY2;
			break;
		default:
			return;
		}
		g_outputs[selectedRelay].patterncount
      	+= (int)g_outputs[selectedRelay].defaultdiop->cycles;
	} else if(tMode < 0) {
		// Test mode always selects relay 1.
		selectedRelay = DIOP_ORELAY1;
		g_RelayOverrideDiop.cycles = 1;
		g_RelayOverrideDiop.xormask = g_outputs[selectedRelay].defaultdiop->xormask;
		if (tMode == -3) {	// Cycle Relay 500ms
			g_RelayOverrideDiop.timeon = 500;
			g_RelayOverrideDiop.timeoff = 500;
		} else {			// Cycle Relay 200ms
			g_RelayOverrideDiop.timeon = 200;
			g_RelayOverrideDiop.timeoff = 200;
		}
		// Override the default DIOP with the test settings.
		g_outputs[selectedRelay].diop = &g_RelayOverrideDiop;
		g_outputs[selectedRelay].patterncount += (int)g_RelayOverrideDiop.cycles;
	} else {	// Cancel the relay.
		if(g_outputs[selectedRelay].patterncount > 1)
			// Set to 1 so the current pattern can finish.
			g_outputs[selectedRelay].patterncount = 1;
	}
}

/*** BeginHeader IOISWaitOn, IOISCheckOn, IOISWaitOff, IOISCheckOff */
/*	--------------------------------------------------------------
	Function: IOISWaitOn
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to assert.
		This state machine function set simply verifies that an
		input's pulse timing is correct & counts the pulses.
	------------------------------------------------------------ */
void IOISWaitOn(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISCheckOn
	--------------------------------------------------------------
	Purpose: IO State machine function. Ensures IO remained
		asserted for required amount of time. This state machine
		function set simply verifies that an input's pulse timing
		is correct & counts the pulses.
	------------------------------------------------------------ */
void IOISCheckOn(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISWaitOff
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to deassert.
	This state machine function set simply verifies that an
	input's pulse timing is correct & counts the pulses.
	------------------------------------------------------------ */
void IOISWaitOff(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISCheckOff
	--------------------------------------------------------------
	Purpose: IO State machine function. Ensures IO remains
		de-asserted for required amount of time. This state machine
		function set simply verifies that an input's pulse timing
		is correct & counts the pulses.
	------------------------------------------------------------ */
void IOISCheckOff(io_input_type* inputtoproc);
/*** EndHeader */

nodebug
root void IOISWaitOn(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(curstate) {
		inputtoproc->timr = MS_TIMER+inputtoproc->diop->timeon;
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		inputtoproc->stateroutine = IOISCheckOn;
	}
}
nodebug
root void IOISCheckOn(io_input_type* inputtoproc)
{
	unsigned char curstate;
	if(Util_CheckmsTimeout(inputtoproc->timr)) {
		curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
		if(curstate) {
			inputtoproc->stateroutine = IOISWaitOff;
		} else {
			inputtoproc->stateroutine = IOISWaitOn;
		}
	}
}
nodebug
root void IOISWaitOff(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(!curstate) {
		inputtoproc->timr = MS_TIMER+inputtoproc->diop->timeoff;
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		inputtoproc->stateroutine = IOISCheckOff;
	}
}
nodebug
root void IOISCheckOff(io_input_type* inputtoproc)
{
	unsigned char curstate;
	if(Util_CheckmsTimeout(inputtoproc->timr)) {
		curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
		if(!curstate) {
			inputtoproc->stateroutine = IOISWaitOn;
			inputtoproc->patterncount++;
		}
// [BUG: 05.00.22, rwc, 8/1/08] commented out and moved up to reset after pulse is counted
//		inputtoproc->stateroutine = IOISWaitOn;
	}
}

/*** BeginHeader IOISDispenseWaitOnEcho, IOISDispenseCheckOnEcho, IOISDispenseWaitOffEcho,
IOISDispenseCheckOffEcho */
/*	--------------------------------------------------------------
	Function: IOISDispenseWaitOnEcho
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to assert.
		This state machine function set verifies that an input's
		pulse timing is correct, counts the pulses, and echos
		them to the echo port.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISDispenseWaitOnEcho(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISDispenseCheckOnEcho
	--------------------------------------------------------------
	Purpose: IO State machine function. Ensures IO remained
		asserted for required amount of time. This state machine
		function set verifies that an input's pulse timing is
		correct, counts the pulses, and echos them to the echo port.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISDispenseCheckOnEcho(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISDispenseWaitOffEcho
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to deassert.
		This state machine function set verifies that an input's
		pulse timing is correct, counts the pulses, and echos
		them to the echo port.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISDispenseWaitOffEcho(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISDispenseCheckOffEcho
	--------------------------------------------------------------
	Purpose: IO State machine function. Ensures IO remains
		de-asserted for required amount of time. This state machine
		function set verifies that an input's pulse timing is
		correct, counts the pulses, and echos them to the echo port.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISDispenseCheckOffEcho(io_input_type* inputtoproc);
/*** EndHeader */
nodebug
root void IOISDispenseWaitOnEcho(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(curstate) {
		DebugPrint(STRACE_DBG,("IO ID %d came on.",inputtoproc->diop->dioid));
		SRIO_OutputSetBit(inputtoproc->EchoOutput->outputbit,
      	1^inputtoproc->EchoOutput->diop->xormask);
		inputtoproc->timr = MS_TIMER+inputtoproc->diop->timeon;
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		inputtoproc->stateroutine = IOISDispenseCheckOnEcho;
	}
}
nodebug
root void IOISDispenseCheckOnEcho(io_input_type* inputtoproc)
{
	unsigned char curstate;
	if(Util_CheckmsTimeout(inputtoproc->timr)) {
		curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask)
      	& inputtoproc->iomask);
		if(curstate) {
			// If dispense signal has been on for the minimum time required,
			// it looks like we've started dispensing. This routine is used for
			// echoing both the dispense signal from the game and the notch signal
			// from the dispenser. Normally, we'd only set dispensing TRUE while getting
			// a dispense signal from the game, but since they're related, it's okay
			// to set it TRUE for either signal. This way we don't need separate state
			// machines for the two signals.
			if(g_dispensingEOStimer == &inputtoproc->EOStimr) {
				g_dispensing = TRUE;
			}
			inputtoproc->stateroutine = IOISDispenseWaitOffEcho;
		} else {
			DebugPrint(STRACE_DBG,("IO ID %d turned off prematurely.",inputtoproc->diop->dioid));
			SRIO_OutputSetBit(inputtoproc->EchoOutput->outputbit,
         	0^inputtoproc->EchoOutput->diop->xormask);
			inputtoproc->stateroutine = IOISDispenseWaitOnEcho;
		}
	}
}
nodebug
root void IOISDispenseWaitOffEcho(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(!curstate) {
		DebugPrint(STRACE_DBG,("IO ID %d turned off.",inputtoproc->diop->dioid));
		SRIO_OutputSetBit(inputtoproc->EchoOutput->outputbit,
      	0^inputtoproc->EchoOutput->diop->xormask);
		inputtoproc->timr = MS_TIMER+inputtoproc->diop->timeoff;
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		inputtoproc->stateroutine = IOISDispenseCheckOffEcho;
	} else if (G_STOR_DEVC_MSG.dispenstype == DISPENSETYPE_MOTOR) {
		// For motor drive dispensers, keep extending the dispense EOS timer.
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
	}
}
nodebug
root void IOISDispenseCheckOffEcho(io_input_type* inputtoproc)
{
	unsigned char curstate;
	if(Util_CheckmsTimeout(inputtoproc->timr)) {
		curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
		if(!curstate) {
			inputtoproc->patterncount++;
			inputtoproc->stateroutine = IOISDispenseWaitOnEcho;
		} else {
			DebugPrint(STRACE_DBG,("IO ID %d turned on prematurely.",inputtoproc->diop->dioid));
			SRIO_OutputSetBit(inputtoproc->EchoOutput->outputbit,
         	1^inputtoproc->EchoOutput->diop->xormask);
		}
// [BUG: 05.00.22, rwc, 8/1/08] commented out and moved up to reset after pulse is counted
//		inputtoproc->stateroutine = IOISDispenseWaitOnEcho;
	}
}

/*** BeginHeader IOISPulseDispenseSpoofWaitOn, IOISPulseDispenseSpoofCheckOn,
	IOISPulseDispenseSpoofWaitOff, IOISPulseDispenseSpoofCheckOff */
/*	--------------------------------------------------------------
	Function: IOISPulseDispenseSpoofWaitOn
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to assert.
		This state machine function set verifies that an input's
		pulse timing is correct, counts the pulses, and then
		increment's the echo output's pattern count. The echo
		output is serviced by the output handler. This will
		cause one output pulse for every input pulse. The pattern
		count of the input is also incremented & is used to
		determine how many input pulses were received & thus how
		many tickets were won.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISPulseDispenseSpoofWaitOn(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISPulseDispenseSpoofCheckOn
	--------------------------------------------------------------
	Purpose: IO State machine function. Ensures IO remained
		asserted for required amount of time. This state machine
		function set verifies that an input's pulse timing is
		correct, counts the pulses, and then increment's the echo
		output's pattern count. The echo output is serviced by the
		output handler. This will cause one output pulse for every
		input pulse. The pattern count of the input is also
		incremented & is used to determine how many input pulses
		were received & thus how many tickets were won.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISPulseDispenseSpoofCheckOn(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISPulseDispenseSpoofWaitOff
	--------------------------------------------------------------
	Purpose: IO State machine function. Waits for IO to deassert.
		This state machine function set verifies that an input's
		pulse timing is correct, counts the pulses, and then
		increment's the echo output's pattern count. The echo
		output is serviced by the output handler. This will
		cause one output pulse for every input pulse. The pattern
		count of the input is also incremented & is used to
		determine how many input pulses were received & thus how
		many tickets were won.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISPulseDispenseSpoofWaitOff(io_input_type* inputtoproc);
/*	--------------------------------------------------------------
	Function: IOISPulseDispenseSpoofCheckOff
	--------------------------------------------------------------
	Purpose: IO State machine function. Ensures IO remains
		de-asserted for required amount of time. This state machine
		function set verifies that an input's pulse timing is
		correct, counts the pulses, and then increment's the echo
		output's pattern count. The echo output is serviced by the
		output handler. This will cause one output pulse for every
		input pulse. The pattern count of the input is also
		incremented & is used to determine how many input pulses
		were received & thus how many tickets were won.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
void IOISPulseDispenseSpoofCheckOff(io_input_type* inputtoproc);
/*** EndHeader */
nodebug
root void IOISPulseDispenseSpoofWaitOn(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(curstate) {
		inputtoproc->timr = MS_TIMER+inputtoproc->diop->timeon;
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		inputtoproc->stateroutine = IOISPulseDispenseSpoofCheckOn;
	}
}
nodebug
root void IOISPulseDispenseSpoofCheckOn(io_input_type* inputtoproc)
{
	unsigned char curstate;
	if(Util_CheckmsTimeout(inputtoproc->timr)) {
		curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
		if(curstate) {
			// If dispense signal has been on for the minimum time required,
			// it looks like we've started dispensing.
			g_dispensing = TRUE;
			inputtoproc->stateroutine = IOISPulseDispenseSpoofWaitOff;
		} else {
			inputtoproc->stateroutine = IOISPulseDispenseSpoofWaitOn;
		}
	}
}
nodebug
root void IOISPulseDispenseSpoofWaitOff(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(!curstate) {
		inputtoproc->timr = MS_TIMER+inputtoproc->diop->timeoff;
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		inputtoproc->stateroutine = IOISPulseDispenseSpoofCheckOff;
	}
}
nodebug
root void IOISPulseDispenseSpoofCheckOff(io_input_type* inputtoproc)
{
	unsigned char curstate;
	if(Util_CheckmsTimeout(inputtoproc->timr)) {
		curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
		if(!curstate) {
			inputtoproc->patterncount++;
			inputtoproc->EchoOutput->patterncount += (int)inputtoproc->EchoOutput->diop->cycles;
		}
		inputtoproc->stateroutine = IOISPulseDispenseSpoofWaitOn;
	}
}

/*** BeginHeader IOISDriveDispenseSpoofWhileOn */
/*	--------------------------------------------------------------
	Function: IOISDriveDispenseSpoofWhileOn
	--------------------------------------------------------------
	Purpose: This I/O handler function spoofs a drive-type ticket
		dispenser. While the input is high, this function sets to 1
		the pattern count of the output designated as the echo output.
		It then waits for the count to go back to zero (which
		is done by the output handler) before setting it to 1 again.
		This causes a pulse train on the output for as long as the
		input remains high. The pattern count of the input is also
		incremented & is used to determine how many output pulses
		were generated & thus how many tickets were won.
	Notes: An echo port must be specified for an input using this
		function set.
	------------------------------------------------------------ */
//sansom
void IOISDriveDispenseSpoofWhileOn(io_input_type* inputtoproc);
/*** EndHeader */
nodebug
root void IOISDriveDispenseSpoofWhileOn(io_input_type* inputtoproc)
{
	unsigned char curstate;
	curstate = ( (*(inputtoproc->iosrc) ^ inputtoproc->diop->xormask) & inputtoproc->iomask);
	if(curstate) {
		// If dispense signal is on, it looks like we've started dispensing.
		g_dispensing = TRUE;
		// Just keep extending the EOS timer until we stop getting the dispense signal.
		inputtoproc->EOStimr = MS_TIMER+inputtoproc->diop->eosdetect;
		if(inputtoproc->EchoOutput->patterncount == 0) {
			inputtoproc->EchoOutput->patterncount = (int)inputtoproc->EchoOutput->diop->cycles;
			inputtoproc->patterncount++;
//         DebugPrint(STRACE_NFO,("Notch");
			g_sent_notch = TRUE;
		}
	}
}

/*** BeginHeader IOOSTurnOn, IOOSWaitOn, IOOSTurnOff, IOOSWaitOff */
/*	--------------------------------------------------------------
	Function: IOOSTurnOn
	--------------------------------------------------------------
	Purpose: IO Output State machine function.
	------------------------------------------------------------ */
void IOOSTurnOn(io_output_type* outputtoproc);
/*	--------------------------------------------------------------
	Function: IOOSWaitOn
	--------------------------------------------------------------
	Purpose: IO Output State machine function.
	------------------------------------------------------------ */
void IOOSWaitOn(io_output_type* outputtoproc);
/*	--------------------------------------------------------------
	Function: IOOSTurnOff
	--------------------------------------------------------------
	Purpose: IO Output State machine function.
	------------------------------------------------------------ */
void IOOSTurnOff(io_output_type* outputtoproc);
/*	--------------------------------------------------------------
	Function: IOOSWaitOff
	--------------------------------------------------------------
	Purpose: IO Output State machine function.
	------------------------------------------------------------ */
void IOOSWaitOff(io_output_type* outputtoproc);
/*** EndHeader */
nodebug
root void IOOSTurnOn(io_output_type* outputtoproc)
{
	// Assert Output.
	SRIO_OutputSetBit(outputtoproc->outputbit,1^outputtoproc->diop->xormask);
	outputtoproc->timr = MS_TIMER + outputtoproc->diop->timeon;
	outputtoproc->stateroutine = IOOSWaitOn;
}
nodebug
root void IOOSWaitOn(io_output_type* outputtoproc)
{
	if(Util_CheckmsTimeout(outputtoproc->timr)) {
		outputtoproc->stateroutine = IOOSTurnOff;
	}
}
nodebug
root void IOOSTurnOff(io_output_type* outputtoproc)
{
	// De-Assert Output.
	SRIO_OutputSetBit(outputtoproc->outputbit,0^outputtoproc->diop->xormask);
	outputtoproc->timr = MS_TIMER + outputtoproc->diop->timeoff;
	outputtoproc->stateroutine = IOOSWaitOff;
}
nodebug
root void IOOSWaitOff(io_output_type* outputtoproc)
{
	if(Util_CheckmsTimeout(outputtoproc->timr)) {
		outputtoproc->stateroutine = IOOSTurnOn;
		// Decrement the outgoing count.
		if(outputtoproc->patterncount > 0) {
			outputtoproc->patterncount--;
		} else {
			// Revert to the default diop in case it was a manual override.
			outputtoproc->diop = outputtoproc->defaultdiop;
		}
	}
}

/*** BeginHeader IOHandler */
/*	--------------------------------------------------------------
	Function: IOHandler
	--------------------------------------------------------------
	Purpose: Services low speed inputs and sends DIOS when either
		a low or high speed input's count is nonzero.
	------------------------------------------------------------*/
void IOHandler(void);
/*** EndHeader */
void IOHandler(void)
{
	io_input_type* tInput;
	io_output_type* tOutput;
	int i;

	// Process low-speed inputs.
	tInput = g_LSInputs;
	while(tInput) {
		tInput->stateroutine(tInput);
		tInput = tInput->next;
	}
	// Process outputs.
	tOutput = g_AAOutputs;
	while(tOutput) {
		if(tOutput->patterncount) {
			tOutput->stateroutine(tOutput);
		}
		tOutput = tOutput->next;
	}

	// Now check all inputs for counts that should be reported via DIOS.
	tInput = g_AAInputs;
	while (tInput) {
		if (tInput->patterncount) {
			if(Util_CheckmsTimeout(tInput->EOStimr)) {
				ipset(3);
				if(tInput->sendDIOS) {	// If we should send a DIOS, send it.
					// If we can push the event, clear the pattern count. Otherwise, just keep
					// accumulating the count in the input's storage.
					if(EventQ_PushLimited(EVT_DIOS,tInput->diop->dioid,tInput->patterncount)) {
						if(&tInput->patterncount == g_winningscounter) {
							EventQ_PushLimited(EVT_TOTALWINNINGS,tInput->patterncount,0);
						}
						tInput->patterncount = 0;
					}
				} else { // If we should not send a DIOS, just clear the pattern count.
					tInput->patterncount = 0;
				}
				ipres();
			}
		}
		tInput = tInput->next;
	}
	// Now check whether we're dispensing tickets. We don't allow play card swipes while
	// dispensing.
	if (g_dispensing) {

		if(Util_CheckmsTimeout(*g_dispensingEOStimer)) {
			ipset(3);
			g_dispensing = FALSE;
			ipres();
		}
	}

   if (g_sent_notch && G_STOR_CNFG_MSG.nodispense && G_STOR_DEVC_MSG.dispenstype==DISPENSETYPE_MOTOR) {
	   g_sent_notch = FALSE;
   	lcd_inc_tkts++;
   	CC_SendLCDTkts(lcd_inc_tkts);
   }
}

/*** BeginHeader IORoutine */
/*	--------------------------------------------------------------
	Function: IORoutine
	--------------------------------------------------------------
	Purpose: Handles I/O Polling and high speed inputs from
		timer ISR
	------------------------------------------------------------ */
void IORoutine(void);
/*** EndHeader */
nodebug
root void IORoutine(void)
{
	io_input_type* tHSInputs;
	g_InputSHIFTREG = SRIO_InputRead();
	g_InputPORTE = RdPortI(PEDR);
	g_InputPORTF = RdPortI(PFDR);
	tHSInputs = g_HSInputs;
	while(tHSInputs) {
		tHSInputs->stateroutine(tHSInputs);
		tHSInputs = tHSInputs->next;
	}
}

/*** BeginHeader IOSys_DIOPNum2Phys */
/*	--------------------------------------------------------------
	Function: IOSys_DIOPNum2Phys
	Params:
		diopnum - Virtual DIOP number
	Returns:
		int - physical DIOP array location of virtual DIOP number
	--------------------------------------------------------------
	Purpose: Translates a virtual DIOP address to a physical
		DIOP array location.
	------------------------------------------------------------ */
int IOSys_DIOPNum2Phys(int diopnum);
/*** EndHeader */
int IOSys_DIOPNum2Phys(int diopnum)
{
	if(diopnum < 1)
		return -1;
	if(diopnum < MAXINPUTS)
		return diopnum-1;

	diopnum = diopnum-20;	// Outputs start at DIOP ID 20.
	if(diopnum < 0)
		return -1;
	if(diopnum < MAXOUTPUTS)
		return MAXINPUTS+diopnum;
	return -1;
}

/*** BeginHeader */
#endif // #ifndef _IOSYSTEM_C_
/*** EndHeader */

