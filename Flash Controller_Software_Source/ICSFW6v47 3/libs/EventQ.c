/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	EventQ.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Provides Event Queue routines.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	01-01-2006	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _EVENTQ_C_
#define _EVENTQ_C_
/*** EndHeader */

/*** BeginHeader */
typedef enum {
	EVT_NULL = 0,						// NULL Event. Do nothing.
	EVT_TRANSACTIONCOMPLETESUCCESS,		// A transaction completed successfully
	EVT_TRANSACTIONCOMPLETEFAIL,		// A transaction failed, but is considered complete
	EVT_PUSHXFERENTER,					// Push buffer transfer mode is requested.
	EVT_PUSHACKXFER,					// The last push buffer record sent has been acked.
	EVT_ENET_FAIL,						// The Ethernet interface failed.
	EVT_ENET_UP,						// The Ethernet interface came up.
	EVT_NETWORKCONN_FAIL,   			// The network cable was unplugged or lost connection to switch/hub.
	EVT_PUSHMODEENTER,					// Push Mode was just entered due to server failure.
	EVT_PUSHMODEEXIT,					// Push Mode was just exited due to proper server communications.
	EVT_GOTCMDBARCODE,					// A command barcode was received.
											// data1 = command ID
	EVT_GOTCMDFWDBARCODE,				// A command barcode that needs forwarding was received.
											// data1 = command ID
	EVT_GOTPLAYBARCODE,					// A play barcode was received.
	EVT_DIOS,							// A DIOS event was received.
											// data1 = DIO ID
											// data2 = count
	EVT_LOCK,							// A LOCK request was received.
	EVT_UNLOCK,							// An UNLOCK request was received.
	EVT_FREEPLAY,						// A request to enter free play was received.
	EVT_VALUEPLAY,						// A request to go back to value play was received.
	EVT_BLINKLEDS,						// A request to flash the LED was received.
	EVT_SHOWVERSIONINFO,				// A request to show the version info was received.
	EVT_SETDISPENSERTYPE,				// A request to change the dispenser type was received.
											// data1 = 1 if type should be DL-1275. 2 if type should be DL-4
	EVT_DISPENSE,						// A request to manually dispense tickets was received.
											// data1 = number of tickets to dispense.
	EVT_PRICECHANGED,					// A request to change the price was received.
	EVT_CHANGESTATE,					// A request to change state was received.
											// data1 = new state.
	EVT_REENTER,						// The current state should re-enter itself to redraw, if appropriate.
	EVT_TOTALWINNINGS,					// The total winnings should be displayed.
											// data1 = total winnings.
	EVT_GOTVALIDACCT					// Received a valid Account. Play may proceed.
} EQ_event_type;

typedef struct {
	EQ_event_type	evt;	// Event type
	int				data1;	// Event data 1. Some events may use this to pass a parameter.
	int				data2;	// Event data 2. Some events may use this to pass a parameter.
} EQ_event;

#ifndef MAX_EQ_EVENTS
	#define MAX_EQ_EVENTS	100
#endif
/*** EndHeader */

/*** BeginHeader EventQ_Init */
/*	--------------------------------------------------------------
	Function: EventQ_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the Event Queue library.
	------------------------------------------------------------ */
EQ_event		g_EventQueue[MAX_EQ_EVENTS];
int				g_EQNumEvents;
EQ_event*		g_EQEventRead;
EQ_event*		g_EQEventWrite;
void EventQ_Init(void);
/*** EndHeader */
void EventQ_Init(void)
{
	g_EQEventRead = g_EQEventWrite = g_EventQueue;
	g_EQNumEvents = 0;
}

/*** BeginHeader EventQ_Push, EventQ_PushLimited, EventQ_PushRec, EventQ_Pop, EventQ_Flush */
/*	--------------------------------------------------------------
	Function: EventQ_Push
	Params:
		e - event ID
		data1 - data associated with event (if none, just use 0)
		data2 - data associated with event (if none, just use 0)
	Returns:
		BOOL - TRUE if event was successfully put on queue, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose:
		Pushes event information onto the event queue.
	------------------------------------------------------------ */
BOOL EventQ_Push(EQ_event_type e, int data1, int data2);
/*	--------------------------------------------------------------
	Function: EventQ_PushLimited
	Params:
		e - event ID
		data1 - data associated with event (if none, just use 0)
		data2 - data associated with event (if none, just use 0)
	Returns:
		BOOL - TRUE if event was successfully put on queue, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose:
		This only pushes an event on the queue if there is 50% of
		the queue's capacity available. This was specifically
		added for the DIOS events, which could potentially fill
		up the queue, causing other important events to be dropped.
		Since DIOS events can be aggregated by the I/O handler &
		sent in bulk later, it is very easy for the I/O handler
		to deal with queue push failures.
	------------------------------------------------------------ */
BOOL EventQ_PushLimited(EQ_event_type e, int data1, int data2);
/*	--------------------------------------------------------------
	Function: EventQ_PushRec
	Params:
		e - a filled-in event record
	Returns:
		BOOL - TRUE if event was successfully put on queue, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose:
		Pushes an event record onto the event queue. This is just
		an alternative to pushing the individual data components
		if you already have an event structure.
	------------------------------------------------------------ */
BOOL EventQ_PushRec(EQ_event* e);
/*	--------------------------------------------------------------
	Function: EventQ_Pop
	Params:
		e - a retrieved event record
	Returns:
		BOOL - TRUE if event was successfully retrieved from the
			queue, FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Retrieves an event record from the event queue.
	------------------------------------------------------------ */
BOOL EventQ_Pop(EQ_event* e);
/*	--------------------------------------------------------------
	Function: EventQ_Flush
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Clears the event queue.
	------------------------------------------------------------ */
void EventQ_Flush(void);
/*** EndHeader */
BOOL EventQ_Push(EQ_event_type e, int data1, int data2)
{
	EQ_event tevent;
	tevent.evt = e;
	tevent.data1 = data1;
	tevent.data2 = data2;
	return EventQ_PushRec(&tevent);
}

BOOL EventQ_PushLimited(EQ_event_type e, int data1, int data2)
{
	if(g_EQNumEvents > (MAX_EQ_EVENTS>>1))
		return FALSE;
	return EventQ_Push(e, data1, data2);
}

BOOL EventQ_PushRec(EQ_event* e)
{
	if (g_EQNumEvents < MAX_EQ_EVENTS) {
		g_EQNumEvents++;
		*(g_EQEventWrite++) = *e;
		if(g_EQEventWrite >= g_EventQueue + MAX_EQ_EVENTS)
			g_EQEventWrite = g_EventQueue;
		return TRUE;
	}
	return FALSE;
}

BOOL EventQ_Pop(EQ_event* e)
{
	if (g_EQNumEvents == 0)
		return FALSE;
	g_EQNumEvents--;
	*e = *(g_EQEventRead++);
	if(g_EQEventRead >= g_EventQueue + MAX_EQ_EVENTS)
		g_EQEventRead = g_EventQueue;
	return TRUE;
}

void EventQ_Flush(void)
{
	g_EQEventRead = g_EQEventWrite = g_EventQueue;
	g_EQNumEvents = 0;
}

/*** BeginHeader */
#endif // #ifndef _MESSAGES_C_
/*** EndHeader */

