/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	Messages.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Provides Message Display services.
	Target(s):  Rabbit 3000
	Requires:	This library uses StorageSystem library, which must
				be included first.

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _MESSAGES_C_
#define _MESSAGES_C_
/*** EndHeader */

/*** BeginHeader Messages_Init */
/*	--------------------------------------------------------------
	Function: Messages_Init
	Params:
		void
	Returns:
		BOOL - TRUE if message library initializes properly, FALSE if
			it fails.
	--------------------------------------------------------------
	Purpose:
		Initializes the Message library.
	NOTE: Requires Storage_Init() to have been called first to
		set up the file system.
	------------------------------------------------------------ */
BOOL Messages_Init(void);

typedef enum {
	msgstate_invalid = 0,	// Messages are corrupt.
	msgstate_idle,			// Message file handle is open and valid.
	msgstate_updating		// Messages are being updated, so the file handle should not be used.
} messagesys_state;

extern File				g_messagefile;
extern messagesys_state g_messages_state;
extern int				g_msgsupdatelistsize;
extern int				g_msgreasonoffset;

/*** EndHeader */

File				g_messagefile;
messagesys_state	g_messages_state;
int					g_msgsupdatelistsize;
int					g_msgreasonoffset;

BOOL Messages_Init(void)
{
	recd_MSSG_t t_message;

	g_messages_state = msgstate_invalid;
	memset(&g_messagefile,0,sizeof(g_messagefile));
	if(fopen_rd(&g_messagefile, SII_CMSGFILE)) {
		if(!Messages_CreateAndInitFile())
			return FALSE;
	}
	g_messages_state = msgstate_idle;
	g_msgreasonoffset = 0;

	while (fread(&g_messagefile,&t_message, sizeof(t_message)) == sizeof(t_message)) {
		if (t_message.mssgid < SII_MSG_RSNOFFSET)
			g_msgreasonoffset = t_message.mssgid;
		else
			return TRUE;
	}
	return TRUE;
}

/*** BeginHeader Messages_UnInit */
/*	--------------------------------------------------------------
	Function: Messages_UnInit
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Undoes initialization such that Messages_Init() can be called
		again. This is used when the storage system is determined
		to be corrupt & needs formatting.
	------------------------------------------------------------ */
void Messages_UnInit(void);
/*** EndHeader */
void Messages_UnInit(void)
{
	if(msgstate_updating == g_messages_state) 
		SafeReboot_PutSemaphore();
	g_messages_state = msgstate_invalid;
	fclose(&g_messagefile);
}

/*** BeginHeader Messages_ValidateFile */
/*	--------------------------------------------------------------
	Function: Messages_ValidateFile
	Params:
		void
	Returns:
		BOOL - TRUE if file is valid. FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Determines if the Messages file was properly written or
		if it is corrupt.
	------------------------------------------------------------ */
BOOL Messages_ValidateFile(void);
/*** EndHeader */
BOOL Messages_ValidateFile(void)
{
	int tchecksum, tproperchecksum;
	Messages_ForceCloseList();
	if(g_messages_state == msgstate_invalid)
		return FALSE;
	if(!Storage_ChecksumAFile(&g_messagefile,&tchecksum))
		return FALSE;
	if(!Storage_GetChecksum(SII_CMSGFILE,&tproperchecksum))
		return FALSE;
	if(tchecksum == tproperchecksum)
		return TRUE;
	return FALSE;
}

/*** BeginHeader Messages_CreateAndInitFile */
/*	--------------------------------------------------------------
	Function: Messages_CreateAndInitFile
	Params:
		void
	Returns:
		BOOL - TRUE if file was created successfully. FALSE otherwise.
	--------------------------------------------------------------
	Purpose:
		Upon first boot or corruption of the file, this will
		create a new file.
	------------------------------------------------------------ */
BOOL Messages_CreateAndInitFile(void);
/*** EndHeader */
BOOL Messages_CreateAndInitFile(void)
{
	int i;
	recd_MSSG_t t_message;

	DebugPrint(STRACE_NFO,("Creating messages file..."));
	if (fcreate(&g_messagefile, SII_CMSGFILE)) {
		return FALSE;
	}
	// Write out a few blank messages just to ensure storage is working.
	memset(&t_message,0,sizeof(t_message));
	for(i = 0; i < 32; i++) {
		if ( fwrite(&g_messagefile, &t_message, sizeof(t_message)) != sizeof(t_message) ) {
			fclose(&g_messagefile);
			return FALSE;
		}
	}
	fclose(&g_messagefile);
	if(fopen_rd(&g_messagefile, SII_CMSGFILE)) {
		return FALSE;
	}
	return TRUE;
}

/*** BeginHeader Messages_PrepareForNewList */
/*	--------------------------------------------------------------
	Function: Messages_PrepareForNewList
	Params:
		nmessages - number of messages that will be received.
	Returns:
		BOOL - TRUE if ready for new list, FALSE if something
			went wrong when creating new message list file.
	--------------------------------------------------------------
	Purpose:
		Deletes the existing message list and creates an emtpy
		file for the new one.
	------------------------------------------------------------ */
BOOL Messages_PrepareForNewList(int nmessages);
/*** EndHeader */
BOOL Messages_PrepareForNewList(int nmessages)
{
	switch(g_messages_state) {
	case msgstate_updating:	// Already updating
		return FALSE;
	case msgstate_invalid:	// We couldn't open the file before, try recreating it
	case msgstate_idle:		// File is open
		SafeReboot_GetSemaphore();
		// First, close the existing file handle.
		fclose(&g_messagefile);
		g_messages_state = msgstate_invalid;
		// Delete the existing file (don't check return value here).
		fdelete(SII_CMSGFILE);
		// Create new file.
		if(fcreate(&g_messagefile, SII_CMSGFILE)) {
			SafeReboot_PutSemaphore();
			return FALSE;
		}
		g_msgsupdatelistsize = nmessages;
		g_messages_state = msgstate_updating;
		return TRUE;
	}
	return FALSE;
}

/*** BeginHeader Messages_AddMessageToList */
/*	--------------------------------------------------------------
	Function: Messages_AddMessageToList
	Params:
		message - message to add
	Returns:
		BOOL - TRUE if message was added, FALSE if something
			went wrong when writing message to file, or if
			the file wasn't prepared with a call to
			Messages_PrepareForNewList().
	--------------------------------------------------------------
	Purpose:
		Adds a message to the message list file. File must first
		be prepared with Messages_PrepareForNewList(). When the
		expected number of messages has been written, this function
		will automatically close the message file, update its
		checksum, and then re-open it for reading.
	------------------------------------------------------------ */
BOOL Messages_AddMessageToList(recd_MSSG_t* message);
/*** EndHeader */
BOOL Messages_AddMessageToList(recd_MSSG_t* message)
{
	int tchecksum;
	switch(g_messages_state) {
	case msgstate_invalid:
	case msgstate_idle:
		return FALSE;
	case msgstate_updating:
		// Update the offset to the last non-reason message.
		if (message->mssgid < SII_MSG_RSNOFFSET)
			g_msgreasonoffset = message->mssgid;

		if ( fwrite(&g_messagefile, message, sizeof(*message)) != sizeof(*message) ) {
			DebugPrint(STRACE_ERR,("Error writing to file."));
			fclose(&g_messagefile);
			g_messages_state = msgstate_invalid;
			SafeReboot_PutSemaphore();
			return FALSE;
		}
		g_msgsupdatelistsize--;
		if (g_msgsupdatelistsize <= 0) {
			fclose(&g_messagefile);
			g_messages_state = msgstate_invalid;
			DebugPrint(STRACE_DBG,("Found last message and closed file."));
			if (fopen_rd(&g_messagefile, SII_CMSGFILE)) {
				DebugPrint(STRACE_ERR,("Error reopening file."));
				SafeReboot_PutSemaphore();
				return FALSE;
			}
			if (!Storage_ChecksumAFile(&g_messagefile,&tchecksum)) {
				DebugPrint(STRACE_ERR,("Error checksumming file."));
				SafeReboot_PutSemaphore();
				return FALSE;
			}
			if (!Storage_UpdateChecksum(SII_CMSGFILE,tchecksum)) {
				DebugPrint(STRACE_ERR,("Error writing to file."));
				SafeReboot_PutSemaphore();
				return FALSE;
			}
			DebugPrint(STRACE_DBG,("Updated message checksum."));
			// Leave message file open for future fetching of messages.
			g_messages_state = msgstate_idle;
		}
		return TRUE;
	}
	return FALSE;
}

/*** BeginHeader Messages_ForceCloseList */
/*	--------------------------------------------------------------
	Function: Messages_ForceCloseList
	Params:
	Returns:
	--------------------------------------------------------------
	Purpose:
		Forces the message list file closed during an update. Can
		be used to release the semaphore if the update fails.
	------------------------------------------------------------ */
void Messages_ForceCloseList(void);
/*** EndHeader */
void Messages_ForceCloseList(void)
{
	switch(g_messages_state) {
	case msgstate_invalid:
	case msgstate_idle:
		break;
	case msgstate_updating:
		fclose(&g_messagefile);
		g_messages_state = msgstate_invalid;
		SafeReboot_PutSemaphore();
		break;
	}
}


/*** BeginHeader Messages_GetMessage */
/*	--------------------------------------------------------------
	Function: Messages_GetMessage
	Params:
		nmsg - message number to retrieve
		message - structure to fill with retrieved message
	Returns:
		BOOL - TRUE if message was retrieved, FALSE if unable
			to get message.
	--------------------------------------------------------------
	Purpose:
		Retrieves a CMSG from the list.
	------------------------------------------------------------ */
BOOL Messages_GetMessage(int msgid, recd_MSSG_t* message);
/*** EndHeader */
BOOL Messages_GetMessage(int msgid, recd_MSSG_t* message)
{
	BOOL gotmessage;
	if(g_messages_state == msgstate_idle) {
		if(msgid >= SII_MSG_RSNOFFSET) {
			//TODO: Is this correct for finding reason offset?
			msgid = g_msgreasonoffset+(msgid-SII_MSG_RSNOFFSET);
		}
		if (fseek(&g_messagefile,(msgid-1)*sizeof(*message),SEEK_SET))
			return FALSE;
		if (fread(&g_messagefile,message, sizeof(*message)) == sizeof(*message)){
			return TRUE;
		}
	}
	return FALSE;
}


/*** BeginHeader */
#endif // #ifndef _MESSAGES_C_
/*** EndHeader */

