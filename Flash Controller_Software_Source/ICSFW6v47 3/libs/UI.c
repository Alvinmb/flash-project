/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	UI.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Provides Skoobe UI Routines.
	Target(s):  Rabbit 3000

	Revisions
	----------
   Update:  10-25-2011  v2.0  Rob Toth
   	Modified to operate with the SkoobeIII platform. Commented out the "guts"
      of the functions but left the original calls throughout the code, an excellent
      source of documentation. If the SkoobeIII wants to display some of the messages,
      then just need to modify these functions to talk to the serial LCD driver

	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _UI_C_
#define _UI_C_
/*** EndHeader */

/*** BeginHeader UI_Init */
/*	--------------------------------------------------------------
	Function: UI_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the UI library.
	------------------------------------------------------------ */
#ifdef WIN32
//rjt #include "libs\LCD_LM4052.c"
#else
//rjt #use "LCD_LM4052.c"
#endif

void UI_Init(void);

// Message Table CMSG Selection
#define MSG_M_UNDEF		-1		// Undefined or Manual Message (not in table)
#define MSG_M_DEFAULT	1		// Default Attract (%s = Current Play Price)
#define MSG_M_PLAYBAL	2		// Play Instruction (%s = Play Balance)
#define MSG_M_NEWBAL	3		// Balance After Play (%s = Play Balance)
#define MSG_M_FREEPLAY	4		// Free Play Instruction
#define MSG_M_DISCPLAY	5		// Discount Attract (%s = Current Play Price)
#define MSG_M_PUSHPLAY	6		// Push Mode Play Instruction
#define MSG_M_PUSHSTRT	7		// Push/Free Mode Game Started message
#define MSG_M_2PLAY		8		// 2 Player Instruction (%s = Play Balance)
#define MSG_M_PLAYBTKS	9		// Play Instr w/Tickets (%s=Play Bal, %s=Tick Bal)
#define MSG_M_2PLAYBTK	10		// 2 Player Instr w/Tickets (%s=Play Bal, %s=Tick Bal)
#define MSG_M_NOSERVC	11		// Out of Service
#define MSG_M_TODATTR1	12		// Time of Day Attract 1
#define MSG_M_TODATTR2	13		// Time of Day Attract 2
#define MSG_M_TODATTR3	14		// Time of Day Attract 3
#define MSG_M_CHANGER	15		// Default Token Changer (%s = Price, %s = Tokens)
#define MSG_M_PLAYENT	16		// Play Instruction, Entitlement (%s = Entitlements)
#define MSG_M_PLAYTIM	17		// Play Instruction, Time (%s = Time)
#define MSG_M_2PLAYENT	18		// 2 Player Instruction, Entitlement
#define MSG_M_2PLAYTIM	19		// 2 Player Instruction, Time
#define MSG_M_FREEZER	20		// Default Time Freeze Unit
#define MSG_M_PLAYFRZ	21		// Play Freeze (%s = Time)
#define MSG_M_BALFRZ	22		// New Balance Freeze
#define MSG_M_TICKTIDF	23		// Ticketless TI Default Attract
#define MSG_M_TTIWIN	24		// Ticketless TI Winnings
#define MSG_M_TTIWCOLL	25		// Ticketless TI Winnings Collected
#define MSG_M_TKEATER	26		// Ticket Eater Default Attract
#define MSG_M_TKDONE	27		// Ticket Eater Finished
#define MSG_M_ACMIDLE	28		// ACM Device Idle
#define MSG_M_DUALPRC	29		// Default Dual Pricing
#define MSG_M_OPEN_30	30		// (unassigned)
#define MSG_M_OPEN_31	31		// (unassigned)
#define MSG_M_OPEN_32	32		// (unassigned)

#define MSG_R_OFFSET	1000	// Offest Value to Add for a Reason message
#define MSG_R_DISABLED	1001	// Account Disabled
#define MSG_R_LOWBAL	1002	// Insufficient Balance
#define MSG_R_NENTITLE	1003	// Not Entitled
#define MSG_R_NOTIME	1004	// Not Enough Time
#define MSG_R_CARDTYPE	1005	// Invalid Card Type
#define MSG_R_UNKNOWN	1006	// Unknown Card
#define MSG_R_TIMERNG	1007	// Invalid Time Use Range
#define MSG_R_DATERNG	1008	// Invalid Date Use Range
#define MSG_R_FUTURE	1009	// Invalid until future date
#define MSG_R_EXPIRED	1010	// Expired
#define MSG_R_LOCKED	1011	// Card Locked
#define MSG_R_RESTRCT	1012	// Restricted Game
#define MSG_R_NOWINS	1013	// No Winnings to Collect
#define MSG_R_TICKBAD	1014	// Ticket Invalid
#define MSG_R_TICKUNK	1015	// Ticket Unknown
#define MSG_R_NOTUSABLE	1016	// Balance not usable here.

typedef enum {
	ui_state_idle,
	ui_state_waitingfortimeout
} ui_state;

typedef struct {
	ui_state		state;
	unsigned long	timer;
	char			blinkstate;
	unsigned long	blinktimer;
	recd_MSSG_t		msg;
	char			renderedmsg[CMSG_TEXT_LEN];
	recd_MSSG_t		defaultmsg;
} ui_ctx;

extern ui_ctx g_UIctx;

#define UI_MSG_QUEUESIZE	10

extern long	g_UImsgqueue;
extern int	g_UImsgqueueLen;
extern int	g_UImsgqueueWrite;
extern int	g_UImsgqueueRead;
extern BOOL g_UIPushIndicator;

/*** EndHeader */

ui_ctx g_UIctx;
long g_UImsgqueue;
int	g_UImsgqueueLen;
int	g_UImsgqueueWrite;
int	g_UImsgqueueRead;
BOOL g_UIPushIndicator;

void UI_Init(void)
{
   #if 0 //rjt
   LCD_LM4052_Init();
	g_UIctx.state = ui_state_idle;
	g_UIctx.msg.mssgid = MSG_M_UNDEF;
	g_UIctx.msg.mssgtext[0] = '\0';
	g_UIctx.renderedmsg[0] = '\0';
	g_UIctx.blinkstate = 1;
	// Set the default message to blank.
	g_UIctx.defaultmsg.mssgid = MSG_M_UNDEF;
	g_UIctx.defaultmsg.mssgtext[0] = '\0';
	// Allocate the message queue.
	g_UImsgqueue = xalloc(UI_MSG_QUEUESIZE*sizeof(recd_MSSG_t));
	g_UImsgqueueLen = 0;
	g_UImsgqueueWrite = 0;
	g_UImsgqueueRead = 0;
	g_UIPushIndicator = FALSE;
   #endif
}

/*** BeginHeader UI_DoCMSG */
/*	--------------------------------------------------------------
	Function: UI_DoCMSG
	Params:
		messagenumber - CMSG ID to display.
		queue - If TRUE, the message is queued until the last
			message(s) is finished displaying. If FALSE, the
			display queue is cleared and the requested message
			is displayed.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Displays or queues the specified CMSG.
	------------------------------------------------------------ */
BOOL UI_DoCMSG(int messagenumber, BOOL queue);
/*** EndHeader */
BOOL UI_DoCMSG(int messagenumber, BOOL queue)
{
   #if 0 //rjt
   recd_MSSG_t	tmsg;
	if(!Messages_GetMessage(UI_SimilarMessageSelect(messagenumber),&tmsg))
		return FALSE;
	// If queue is requested, don't queue if nothing is displaying and nothing is on the queue.
	if(queue && ((g_UImsgqueueLen > 0) || (g_UIctx.state != ui_state_idle)) ) {
		if(g_UImsgqueueLen < UI_MSG_QUEUESIZE) {
			root2xmem(g_UImsgqueue+(g_UImsgqueueWrite*sizeof(recd_MSSG_t)),&tmsg,sizeof(tmsg));
			g_UImsgqueueLen++;
			g_UImsgqueueWrite++;
			if(g_UImsgqueueWrite >= UI_MSG_QUEUESIZE)
				g_UImsgqueueWrite = 0;
		}
	} else {
		// Wipe the queue
		g_UImsgqueueLen = 0;
		g_UImsgqueueWrite = 0;
		g_UImsgqueueRead = 0;
		g_UIctx.msg = tmsg;
		// Render immediately.
		UI_RenderContext();
	}
   #endif
	return TRUE;
}

/*** BeginHeader UI_SimilarMessageSelect */
/*	--------------------------------------------------------------
	Function: UI_SimilarMessageSelect
	Params:
		tMssg - requested CMSG ID.
	Returns:
		int - CMSG to display
	--------------------------------------------------------------
	Purpose:
		Helper function to query device and account state and
		possibly select alternative display messages if there is
		a more appropriate message than the one requested.
	------------------------------------------------------------ */
int UI_SimilarMessageSelect(int tMssg);
/*** EndHeader */
int UI_SimilarMessageSelect(int tMssg)
{
   #if 0 //rjt
   char *cpTks;
	cpTks = strchr(G_STOR_ACCT_MSG.baltick,':');
	// Process request
	switch (tMssg) {
	case MSG_M_PLAYBAL:
		// Check for Push Mode first
		if (CClient_IsPush())
			return MSG_M_PUSHPLAY;
		if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_TIMEFRZ)
			return MSG_M_PLAYFRZ;
		if (G_STOR_ACCT_MSG.approvetype) { // Using Alternate Payment (Time/Entitlement)?
			if (G_STOR_DEVC_MSG.gameacttype == 3) { // 2 Player Game?
				if (G_STOR_ACCT_MSG.approvetype==2) // Entitlement Balance?
					return MSG_M_2PLAYENT;	// 2 Player Game with Entitlement Balance.
				else
					return MSG_M_2PLAYTIM;	// 2 Player Game with Time Balance.
			} else {
				if (G_STOR_ACCT_MSG.approvetype==2) // Entitlement Balance?
					return MSG_M_PLAYENT;	// 1 Player Game with Entitlement Balance.
				else
					return MSG_M_PLAYTIM;	// 1 Player Game with Time Balance.
			}
		} else {
			if (G_STOR_DEVC_MSG.gameacttype == 3) { // 2 Player Game?
				if (cpTks) // Tickets Balance?
					return MSG_M_2PLAYBTK;	// 2 Player Game with Tickets Balance.
				else
					return MSG_M_2PLAY;		// 2 Player Game
			} else if (cpTks)
				return MSG_M_PLAYBTKS;		// 1 Player Game with Tickets Balance.
		}
		break;
	case MSG_M_NEWBAL:
		// Check for Push Mode or used alternate payment
		if ( CClient_IsPush() || G_STOR_ACCT_MSG.approvetype)
			return MSG_M_PUSHSTRT;
		if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_TIMEFRZ)
			return MSG_M_BALFRZ;
		break;
	case MSG_M_DEFAULT:
		// Check for Device Default Attract
		if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_CHANGER)
			return MSG_M_CHANGER;
		else if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_TIMEFRZ)
			return MSG_M_FREEZER;
		else if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_TIKEATER)
			return MSG_M_TKEATER;
		else if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_ACCESS)
			return MSG_M_ACMIDLE;
		else if (Storage_IsTicketlessTokenInteractive()) {
			if (Storage_GetWinnings())
				return MSG_M_TTIWIN;
			else
				return MSG_M_TICKTIDF;
		} else {
			if (G_STOR_CNFG_MSG.pricemode == 1) {
				return MSG_M_DUALPRC;
			}
		}
		break;
	}
	return tMssg;
   #endif
   return 0;

}


/*** BeginHeader UI_DoManualMsg */
/*	--------------------------------------------------------------
	Function: UI_DoManualMsg
	Params:
		msg - text of message to display
		time - time to display
		blink - should the message blink (TRUE=YES)
		queue - If TRUE, the message is queued until the last
			message(s) is finished displaying. If FALSE, the
			display queue is cleared and the requested message
			is displayed.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Displays or queues the specified string.
	------------------------------------------------------------ */
void UI_DoManualMsg(char* msg, int time, BOOL blink, BOOL queue);
/*** EndHeader */
void UI_DoManualMsg(char* msg, int time, BOOL blink, BOOL queue)
{
   //Reusing this for the SkoobeIII product
   unsigned char outBuffer[255];
   int length;

   memset(outBuffer,0,sizeof(outBuffer));

   strcpy(&outBuffer[0], msg);

   length = strlen( &outBuffer[0] );

   if(length >= sizeof(outBuffer) - 7)
   	return;

   outBuffer[length++] = '\r';
   outBuffer[length++] = '\n';
   outBuffer[length++] = '\0';

   //Send to the LCD display
   CommandToLCDScreen(outBuffer,length);

   #if 0 //rjt
   recd_MSSG_t	tmsg;
	tmsg.isblink = blink;
	tmsg.isscroll = 0;	// Scroll not currently implemented.
	tmsg.mssgid = MSG_M_UNDEF;
	tmsg.timedisp = time;
	strncpy(tmsg.mssgtext,msg,CMSG_TEXT_LEN);
	tmsg.mssgtext[CMSG_TEXT_LEN-1] = '\0';
	// If queue is requested, don't queue if nothing is displaying and nothing is on the queue.
	if(queue && ((g_UImsgqueueLen > 0) || (g_UIctx.state != ui_state_idle)) ) {
		if(g_UImsgqueueLen < UI_MSG_QUEUESIZE) {
			root2xmem(g_UImsgqueue+(g_UImsgqueueWrite*sizeof(recd_MSSG_t)),&tmsg,sizeof(tmsg));
			g_UImsgqueueLen++;
			g_UImsgqueueWrite++;
			if(g_UImsgqueueWrite >= UI_MSG_QUEUESIZE)
				g_UImsgqueueWrite = 0;
		}
	} else {
		// Wipe the queue
		g_UImsgqueueLen = 0;
		g_UImsgqueueWrite = 0;
		g_UImsgqueueRead = 0;
		g_UIctx.msg = tmsg;
		// Render immediately.
		UI_RenderContext();
	}
   #endif
}

/*** BeginHeader UI_MessageDone */
/*	--------------------------------------------------------------
	Function: UI_MessageDone
	Params:
		void
	Returns:
		BOOL - TRUE if the last message is finished, FALSE if it
			has not finished.
	--------------------------------------------------------------
	Purpose:
		Messages have an associated display time. This function
		determines if that time has elapsed.
	------------------------------------------------------------ */
BOOL UI_MessageDone(void);
/*** EndHeader */
BOOL UI_MessageDone(void)
{
   #if 0 //rjt
   if(g_UIctx.state == ui_state_idle)
		return TRUE;
	return FALSE;
   #endif

   return TRUE;
}

/*** BeginHeader UI_RenderContext */
/*	--------------------------------------------------------------
	Function: UI_RenderContext
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Renders the current context, filling in any variable
		parameters depending upon the message type.
	------------------------------------------------------------ */
void UI_RenderContext(void);
/*** EndHeader */
void UI_RenderContext(void)
{
   #if 0 //rjt
   char* param1;
	char* param2;
	char tstring[6];

	// Ensure the LCD is on.
	LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_DISPCTL|LCD_DISP_ON);

	param1 = "";
	param2 = "";
	switch(g_UIctx.msg.mssgid) {
	case MSG_M_FREEZER:
	case MSG_M_CHANGER:
	case MSG_M_TKEATER:
	case MSG_M_ACMIDLE:
	case MSG_M_DUALPRC:
	case MSG_M_DEFAULT:
		// Determine the price mode based upon configuration.
		param1 = G_STOR_CNFG_MSG.prices[G_STOR_GLOB_MSG.skoobe_pricestate];
		if (MSG_M_DUALPRC == g_UIctx.msg.mssgid)
			param2 = G_STOR_CNFG_MSG.pricealt;
		else
			param2 = G_STOR_CNFG_MSG.denomination;
		break;
	case MSG_M_PLAYBAL:
	case MSG_M_2PLAY:
	case MSG_R_LOWBAL:
	case MSG_R_NOTUSABLE:
		param1 = G_STOR_ACCT_MSG.baltotl;
		break;
	case MSG_M_PLAYBTKS:
	case MSG_M_2PLAYBTK:
		param1 = G_STOR_ACCT_MSG.baltotl;
		param2 = G_STOR_ACCT_MSG.baltick;
		break;
	case MSG_M_PLAYFRZ:
	case MSG_M_PLAYTIM:
	case MSG_M_2PLAYTIM:
		param1 = G_STOR_ACCT_MSG.baltime;
		break;
	case MSG_M_PLAYENT:
	case MSG_M_2PLAYENT:
		param1 = G_STOR_ACCT_MSG.balentt;
		break;
	case MSG_M_NEWBAL:
		param1 = G_STOR_ACCT_MSG.balplay;
		break;
	case MSG_M_TTIWIN:
	   	itoa(Storage_GetWinnings(),tstring);
	   	param1 = tstring;
		break;
	}
	// Skip Leading Spaces
	if (param1 != NULL) {
		while (isspace(*param1))
			param1++;
	}
	if (param2 != NULL) {
		while (isspace(*param2))
			param2++;
	}

	snprintf(g_UIctx.renderedmsg,CMSG_TEXT_LEN,g_UIctx.msg.mssgtext,param1,param2);
	g_UIctx.renderedmsg[CMSG_TEXT_LEN-1] = '\0';

	LCD_LM4052_ClrScr();
	LCD_LM4052_GotoXY(0,0);
	UI_FormattedWrite(g_UIctx.renderedmsg);
	g_UIctx.timer = SEC_TIMER+g_UIctx.msg.timedisp;
	g_UIctx.state = ui_state_waitingfortimeout;
	g_UIctx.blinkstate = 1;
	g_UIctx.blinktimer = MS_TIMER+SII_MSG_BLINKTIME;
   #endif

}

/*** BeginHeader UI_FormattedWrite */
/*	--------------------------------------------------------------
	Function: UI_FormattedWrite
	Params:
		str - Formatted string to write to LCD.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Writes a formatted string to the display. Formatters
		include:
			\n - Newline
			\1 - Newline
			\2 - Clear current line
		Handles drawing of the push indicator.
	------------------------------------------------------------ */
void UI_FormattedWrite(char* str);
/*** EndHeader */
void UI_FormattedWrite(char* str)
{
   #if 0 //rjt
   char* tmsgptr;
	int i, j;

	tmsgptr = str;
	i = 0; j = 0;
	while(*tmsgptr) {
		switch(*tmsgptr) {
		case '\n':
		case '\1':
			if(j < LCD_ROW_MAX) {
				j++; i = 0;
				LCD_LM4052_GotoXY(i,j);
			}
			break;
		case '\2':
			LCD_LM4052_ClrLine(j);
			i = 0;
			break;
		default:
			if(i <= LCD_COL_MAX) {
				LCD_LM4052_OutChar(*tmsgptr);
				i++;
			}
			break;
		}
		tmsgptr++;
	}
	if(g_UIPushIndicator) {
		LCD_LM4052_GotoXY(LCD_COL_MAX,0);
		LCD_LM4052_OutChar(SII_PUSHMODE_CHAR);
	}
   #endif
}

/*** BeginHeader UI_Handler */
/*	--------------------------------------------------------------
	Function: UI_Handler
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Called repeatedly to update blinking text, etc.
	------------------------------------------------------------ */
void UI_Handler(void);
/*** EndHeader */
void UI_Handler(void)
{
   #if 0 //rjt
   if(g_UIctx.state != ui_state_idle) {
		if (Util_CheckSTimeout(g_UIctx.timer)) {
			if(g_UImsgqueueLen > 0) {
				xmem2root(&g_UIctx.msg,g_UImsgqueue+(g_UImsgqueueRead*sizeof(recd_MSSG_t)),sizeof(recd_MSSG_t));
				g_UImsgqueueLen--;
				g_UImsgqueueRead++;
				if(g_UImsgqueueRead >= UI_MSG_QUEUESIZE)
					g_UImsgqueueRead = 0;
				UI_RenderContext();
			} else {
				g_UIctx.state = ui_state_idle;
			}
		}
	}
	// Message can continue to blink while state is idle.
	if (g_UIctx.msg.isblink) {
		if (Util_CheckmsTimeout(g_UIctx.blinktimer)) {
			g_UIctx.blinktimer = MS_TIMER+SII_MSG_BLINKTIME;
			if (g_UIctx.blinkstate == 1) {
				LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_DISPCTL|LCD_DISP_OFF);
				g_UIctx.blinkstate = 0;
			} else {
				LCD_LM4052_Write(LCD_REG_CMD, LCD_CMD_DISPCTL|LCD_DISP_ON);
				g_UIctx.blinkstate = 1;
			}
		}
	}
   #endif
}

/*** BeginHeader UI_PushIndicator */
/*	--------------------------------------------------------------
	Function: UI_PushIndicator
	Params:
		enable - TRUE to turn the push indicator on, FALSE to
			turn off.
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Enables or disables the push indicator.
	------------------------------------------------------------ */
void UI_PushIndicator(BOOL enable);
/*** EndHeader */
void UI_PushIndicator(BOOL enable)
{
	//CC_GenerateLcdCnfgMessage(1);

   #if 0 //rjt
   if(g_UIPushIndicator != enable) {
		g_UIPushIndicator = enable;
		// Redraw the last rendered context.
		LCD_LM4052_ClrScr();
		LCD_LM4052_GotoXY(0,0);
		UI_FormattedWrite(g_UIctx.renderedmsg);
	}
   #endif
}


/*** BeginHeader */
#endif // #ifndef _UI_C_
/*** EndHeader */

