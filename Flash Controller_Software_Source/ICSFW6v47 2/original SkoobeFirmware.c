/* --------------------------------------------------------------
	Copyright (C) 2006 Ideal Software Systems. All Rights Reserved.

	Filename:	SkoobeFirmware.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Main SkoobeII Firmware
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-20-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
#define DEBUG
#define DEBUG_PRINTS

// Version is specified as 00(major) 00(minor) 00(build)
// This is a hexidecimal number, but don't use A-F. For example, to specify "5.00.10"
// Use 0x050010 instead of 0x05000A.
#define SII_FIRMWAREVERSION		0x050021L
#define SII_HARDWAREVERSION		0x020000L

#class auto		// local variables are stack variables by default.
#memmap xmem  	// default to locating code in extended memory (rather than root)

// Define basic definitions and configuration parameters
#ifdef WIN32
#include "libs\DebugSK.h"
#include "libs\Definitions.h"
#include "SkoobeConfig.h"
#include "libs\DIOPDefaults.h"
#else
// Custom Libraries
#use "DebugSK.h"
#use "Definitions.h"
#use "SkoobeConfig.h"
#use "DIOPDefaults.h"
#endif

// The current debug trace level.
#define SKOOBE_DEBUGTRACELEVEL		STRACE_DBG

// Must be defined before including "CashlessClient.c"
#define ENABLE_EBOOTLOADER	// Define this to enable the Ethernet Bootloader.

#ifdef ENABLE_EBOOTLOADER
// Include the RAM loader in FLASH (DLM)
//#define UDPDL_LOADER "C:\\MINDTRIBE\\Projects\\Ideal\\Skoobe II (03-28-2005)\\Code\\SkoobeFirmware\\UDPDL\\Loaders\\protected\\PDL-rcm3700.bin"
#define UDPDL_LOADER "C:\\Dev\\Projects\\ICSFW5\\v5\\UDPDL\\Loaders\\protected\\PDL-rcm3700.bin"
#endif

#ifdef WIN32
// Doesn't actually compile under WIN32, but I like to use Visual Studio
// for editing. The #includes help Intellisense.
#include "libs\EventQ.c"
#include "libs\Utilities.c"
#include "libs\SafeReboot.c"
#include "libs\SkoobeStructures.h"
#include "libs\StorageSystem.c"
#include "libs\Cards.c"
#include "libs\Messages.c"
#include "libs\Barcode.c"
#include "libs\Button.c"
#include "libs\IOSystem.c"
#include "libs\ISRs.c"
#include "libs\Push.c"
#include "libs\CashlessClient.c"
#include "libs\UI.c"
#else
// Custom Libraries
#use "EventQ.c"
#use "Utilities.c"
#use "SafeReboot.c"
#use "SkoobeStructures.h"
#use "StorageSystem.c"
#use "Cards.c"
#use "Messages.c"
#use "Barcode.c"
#use "Button.c"
#use "IOSystem.c"
#use "ISRs.c"
// Dynamic C Libraries
#use "dcrtcp.lib"		// Standard DC TCP/IP lib
#ifdef ENABLE_EBOOTLOADER
#use "udpdownl.lib"		// Download Manager (SHDesigns)
#endif
#use "Push.c"
#use "CashlessClient.c"
#use "UI.c"
#endif

typedef enum {
	SKOOBE_STATE_INIT = 0,
	SKOOBE_STATE_GETSNBB,
	SKOOBE_STATE_BOOT_START,
	SKOOBE_STATE_BOOT_WAIT,
	SKOOBE_STATE_BOOT_DONE,
	SKOOBE_STATE_NOSERVER,
	SKOOBE_STATE_PREPARE_TO_RUN,	// After configuration is received, perform some setup tasks before entering run mode.
	SKOOBE_STATE_RUN,				// Run mode.
	SKOOBE_STATE_TRYENTERPUSHMODE,
	SKOOBE_STATE_NOSERVERNOPUSHMODE,
    SKOOBE_STATE_NOCONNECTIONNOTALLOWED
} skoobe_state;
skoobe_state	g_skoobe_state;		// Skoobe operational state

typedef enum {
	SKOOBE_RUNSTATE_DEFAULT_ENTER = 0,
	SKOOBE_RUNSTATE_DEFAULT,				// Wait around for a scan.
	SKOOBE_RUNSTATE_START,					// Wait for scan transaction to complete.
	SKOOBE_RUNSTATE_BADCARD_WAIT,			// Invalid card swipe. Wait for message to timeout & don't accept same card.
	SKOOBE_RUNSTATE_ENGAGE_ENTER,
	SKOOBE_RUNSTATE_ENGAGE,
	SKOOBE_RUNSTATE_ENGAGEWAITMESSAGE,      // A cyclewait delay was specified, so don't allow more engage until after message time+cyclewait
	SKOOBE_RUNSTATE_TICKETEATER_ENTER,
	SKOOBE_RUNSTATE_TICKETEATER_UI,
	SKOOBE_RUNSTATE_TICKETEATER,
	SKOOBE_RUNSTATE_ACCESS_ENTER,
	SKOOBE_RUNSTATE_ACCESS_UI,
	SKOOBE_RUNSTATE_ACCESS,
	SKOOBE_RUNSTATE_FREEPLAY_ENTER,
	SKOOBE_RUNSTATE_FREEPLAYWAITBTND,
	SKOOBE_RUNSTATE_FREEPLAYWAITBTNU,
	SKOOBE_RUNSTATE_FREEPLAYWAITMESSAGE,    // A cyclewait delay was specified, so don't allow more engage until after message time+cyclewait
	SKOOBE_RUNSTATE_PUSHXFER_ENTER,			// Show the push buffer transfer message
	SKOOBE_RUNSTATE_PUSHXFER,				// Transfer the push buffer.
	SKOOBE_RUNSTATE_STANDBY_ENTER,
	SKOOBE_RUNSTATE_STANDBY,
	SKOOBE_RUNSTATE_NOTREGISTERED_ENTER,
	SKOOBE_RUNSTATE_NOTREGISTERED,
	SKOOBE_RUNSTATE_DELAYONMSGREENTER,		// Delay until a message is done before the next state. Re-enter the next state.
	SKOOBE_RUNSTATE_DELAY					// Induce a delay before the next state.
} skoobe_runstate;
skoobe_state	g_skoobe_runstate;		// Skoobe operational state
skoobe_state	g_skoobe_runstatenext;	// Skoobe next operational state (used by several states to determine next state.)

char g_VersionString[16];				// The version in format "ICS vXX.XX.XX" is used frequently, so render it here.
char g_currentscan[BC_MAXBARCODELEN+1];
char g_currentscanextradata[BC_MAXEXTRADATALEN+1];
BOOL g_testmode;


void ProcessCommandCard(int cardID);
void HandleScan(void);
void DisplayVersionInfo(void);
void DisplayNetworkInfo(void);
void DefaultEventHandler(EQ_event e);
void ValidateConfiguration(void);

void main(void)
{
	// Real variables
	unsigned long	gp_timer;			// General purpose timer re-used throughout main loop.
	int				tint;
	EQ_event		tevent;
	BOOL			terror;

	DebugPrint(STRACE_NFO,("0x%lX, 0x%lX",_BOS_USERDATA,_TOS_USERDATA));

	// Render version string.
#ifdef DEBUG
	snprintf(g_VersionString,sizeof(g_VersionString),"ICS v%02X.%02X.%02XD",
#else
	snprintf(g_VersionString,sizeof(g_VersionString),"ICS v%02X.%02X.%02X",
#endif
		(unsigned char)(SII_FIRMWAREVERSION >> 16)&0xFF,
		(unsigned char)(SII_FIRMWAREVERSION >> 8)&0xFF,
		(unsigned char)(SII_FIRMWAREVERSION)&0xFF);
	g_VersionString[sizeof(g_VersionString)-1] = '\0';

	// Initialize volatile variables.
	g_currentscan[0] = '\0';
    g_currentscanextradata[0] = '\0';
	tevent.evt = EVT_NULL;

	// Initialize libraries.
	EventQ_Init();
	Util_Init();
    SafeReboot_Init();
	UI_Init();

	Button_Init();
    IOSys_Init();
    ISR_Init(FALSE);	// Initialize ISRs, but don't start I/O processing here.
	Barcode_Init();
	Button_StartFade(SII_BUTTONFADE_POWERON,buttonfade_start_in,FALSE);

	UI_DoManualMsg("Hello. I'm booting.\nPlease wait...",0,FALSE,TRUE);

	// Bring up the File System.
	if (!Storage_InitFS()) {
		UI_DoManualMsg("File System Failure.\nHALT",0,FALSE,FALSE);
		while (1) {};
	}
	// Allow two iterations of the following loop. The first iteration tries to
	// bring up any existing files. If it fails, the file system is formatted
	// and we try again. If the second iteration fails, we need to halt the
	// system as there is nothing more we can do.
	tint = 2;
	do {
		terror = FALSE;
		if (!Storage_Init())
			terror = TRUE;
		if (!Cards_Init())
			terror = TRUE;
		if (!Messages_Init())
			terror = TRUE;
		if (!Push_Init())
			terror = TRUE;
		if (terror) {
			DebugPrint(STRACE_NFO,("File system is not correct. Possibly first boot. Formatting..."));
			Cards_UnInit();
			Messages_UnInit();
			Push_UnInit();
			UI_DoManualMsg("Creating Files.\nPlease Wait...",0,FALSE,FALSE);
			if(!Storage_Format()) {
				UI_DoManualMsg("File System Failure.\nHALT",0,FALSE,FALSE);
				while (1) {};
			}
			UI_DoManualMsg("Still booting.\nPlease wait...",0,FALSE,TRUE);
		}
		tint--;
	} while(terror && tint);
	if(terror) {
		UI_DoManualMsg("File System Failure.\nHALT",0,FALSE,FALSE);
		while (1) {};
	}

	// Validate the configuration.
	ValidateConfiguration();
	// If there is a valid configuration, set up the I/O stuff immediately to
	// put the outputs in their proper states. Otherwise, this will just have to
	// wait until there is a valid config.
	if (Storage_HaveValidConfig()) {
		IOSys_SetupProcessingFromConfig();
		IOSys_OutputsOff();
	}

	DebugPrint(STRACE_NFO,("There are %u messages in the push buffer.",Push_GetCount()));

	gp_timer = MS_TIMER+SII_BOOTDELAY_MS;
	g_testmode = FALSE;

	DisplayVersionInfo();
	// Wait until the version info has been displayed.
	while (!UI_MessageDone()) {
		g_testmode = Button_isDown();
		UI_Handler();
	}
	if (g_testmode) {
		if (Button_isDown()) {
			UI_DoManualMsg("Please release\nbutton.",5,FALSE,FALSE);
			while (!UI_MessageDone()) {
				if(!Button_isDown())
					break;
				UI_Handler();
			}
			if(Button_isDown()) {
				UI_DoManualMsg("Oops. Button stuck.\nPlease check button.",0,FALSE,FALSE);
				while(1){}
			}
		}
// No test mode yet.
//		UI_DoManualMsg("Entering Test Mode",5,FALSE,FALSE);
	}

	Button_StartFade(SII_BUTTONFADE_DEFAULT,buttonfade_start_out,TRUE);

	CClient_Init();

	/* STATE MACHINE */
	// Set the super state to init.
	g_skoobe_state = SKOOBE_STATE_INIT;
	// Set the sub state to default. This is the first run state that will be entered
	// when the super state changes to "run".
	// Messages from the server may override this during the boot process.
	g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;

	while (1) {
		CClient_Handler();
		//TODO: Map states...
		CClient_HAPYHandler();
		UI_Handler();
		Button_Handler();
		switch (g_skoobe_state) {
		case SKOOBE_STATE_INIT:
			if (EventQ_Pop(&tevent)) {
				switch (tevent.evt) {
				case EVT_ENET_FAIL:

					UI_DoManualMsg("Network Failed Init.",10,FALSE,FALSE);
            	g_skoobe_state = SKOOBE_STATE_TRYENTERPUSHMODE;
					break;
				case EVT_ENET_UP:
					DisplayNetworkInfo();
					g_skoobe_state = SKOOBE_STATE_GETSNBB;
					break;
				default:
					DefaultEventHandler(tevent);
					break;
				}
			}
			break;
		case SKOOBE_STATE_GETSNBB:
			if (CClient_GetSNBB()) {
				g_skoobe_state = SKOOBE_STATE_BOOT_START;
				gp_timer = MS_TIMER+SII_SNBBTOBOOTDELAY_MS;
			} else {
				g_skoobe_state = SKOOBE_STATE_TRYENTERPUSHMODE;
			}
			break;
		case SKOOBE_STATE_BOOT_START:
			if(Util_CheckmsTimeout(gp_timer)) {
				if (!CClient_SendMessage(SN_DID_BOOT)) {
					DebugPrint(STRACE_ERR,("Could not send BOOT message."));
					UI_DoManualMsg("Server found.\nCould not send boot",3,FALSE,TRUE);
					while (!UI_MessageDone()) { UI_Handler(); }
					g_skoobe_state = SKOOBE_STATE_TRYENTERPUSHMODE;
				} else {
					UI_DoManualMsg("Server found.\nBoot Message Sent...",3,FALSE,TRUE);
					g_skoobe_state = SKOOBE_STATE_BOOT_WAIT;
				}
			}
			break;
		case SKOOBE_STATE_BOOT_WAIT:
			if(EventQ_Pop(&tevent)) {
				// Wait for transaction complete (success or failure)
				switch(tevent.evt) {
				case EVT_TRANSACTIONCOMPLETESUCCESS:
				case EVT_TRANSACTIONCOMPLETEFAIL:
					g_skoobe_state = SKOOBE_STATE_BOOT_DONE;
					break;
				default:
					DefaultEventHandler(tevent);
					break;
				}
			}
			break;
		case SKOOBE_STATE_BOOT_DONE:
			// Validate the configuration again now that the BOOT transaction is complete.
			ValidateConfiguration();
			if (Storage_HaveValidConfig()) {
				DebugPrint(STRACE_DBG,("Booted!"));
				g_skoobe_state = SKOOBE_STATE_PREPARE_TO_RUN;
			} else {
				UI_DoManualMsg("No valid config.\nAttempting Restart.",10,FALSE,TRUE);
				while (!UI_MessageDone()) { UI_Handler(); }
				forceSoftReset();
			}
			break;
		case SKOOBE_STATE_TRYENTERPUSHMODE:
			if (!Storage_HaveValidConfig()) {
				UI_DoManualMsg("No valid config.\nRestarting...",10,FALSE,TRUE);
				while (!UI_MessageDone()) { UI_Handler(); }
				forceSoftReset();
			} else {
				// Check if push mode is allowed.
				if(!G_STOR_CNFG_MSG.pushenabled) {
					g_skoobe_state = SKOOBE_STATE_NOSERVERNOPUSHMODE;
				} else {
					UI_PushIndicator(TRUE);
					g_skoobe_state = SKOOBE_STATE_PREPARE_TO_RUN;
				}
			}
			break;
		case SKOOBE_STATE_NOSERVERNOPUSHMODE:
			// If push mode not allowed, we just keep rebooting until we can contact the server.
			UI_DoManualMsg("No server, push not\nallowed. Restarting.",10,FALSE,TRUE);
			while (!UI_MessageDone()) { UI_Handler(); }
			//FUP: Wait for network to come back up.
			forceSoftReset();
			break;
		case SKOOBE_STATE_NOCONNECTIONNOTALLOWED:
			// If we lost our connection and push mode is not allowed when there's no connection,
            // keep rebooting until we come back up with a network connection.
			UI_DoManualMsg("Lost connection.\nRestarting.",10,FALSE,TRUE);
			while (!UI_MessageDone()) { UI_Handler(); }
			//FUP: Wait for network to come back up.
			forceSoftReset();
			break;
		case SKOOBE_STATE_PREPARE_TO_RUN:
			DebugPrint(STRACE_DBG,("Entering run mode."));
			// Now that a valid configuration has been received, this sets up
			// various parameters based upon the configuration.
			CClient_HAPYSetFreq(G_STOR_CNFG_MSG.hbfreq);
			DebugPrint(STRACE_DBG,("HAPY Time: %d",G_STOR_CNFG_MSG.hbfreq));
			// Enable the I/O processing ISR.
			ISR_EnableIO(TRUE);
			IOSys_SetupDispenser();
			IOSys_SetupProcessingFromConfig();
			IOSys_OutputsOff();
			// Configure the barcode hardware and start it.
			Barcode_Startup();
			g_skoobe_state = SKOOBE_STATE_RUN;
			break;
		case SKOOBE_STATE_RUN:
			if (Barcode_Handler()) {
				// If a play card was scanned, HandleScan() creates a EVT_GOTPLAYBARCODE event.
				// If a command card was scanned, HandleScan() creates a EVT_GOTCMDBARCODE event.
				HandleScan();
			}
			IOHandler();
			switch (g_skoobe_runstate) {
			case SKOOBE_RUNSTATE_DEFAULT_ENTER:
                CC_Lock(FALSE); /* Entering a play mode always takes us out of standby */
				CClient_SetSSRVState(SSRVSTATE_DEFAULT);
				Button_StartFade(SII_BUTTONFADE_DEFAULT,buttonfade_start_out,TRUE);
				// Reset timer for ticketless token interactive
				gp_timer = SEC_TIMER;
				// Queue the default message.
				UI_DoCMSG(MSG_M_DEFAULT,TRUE);
            CC_GenerateLcdCnfgMessage(); /* Communicate change to LCD */
				g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT;
				break;
			case SKOOBE_RUNSTATE_DEFAULT:
				if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// Ignore this barcode if we're dispensing.
						if (IOSys_IsDispensing()) {
							UI_DoManualMsg("Please wait until\nwinnings dispensed.",5,FALSE,FALSE);
							g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
						} else {
							EventQ_Flush();	// Clear out any old events & just look for new ones.
							UI_DoManualMsg("Processing.\nPlease wait...",0,FALSE,FALSE);
							// Wipe out the account structure. This invalidates any existing account.
							memset(&G_STOR_ACCT_MSG,0x00,sizeof(G_STOR_ACCT_MSG));
							CClient_SetLastScan(g_currentscan,g_currentscanextradata);
							CClient_SendMessage(SN_DID_SCAN,g_currentscan,g_currentscanextradata);
							g_skoobe_runstate = SKOOBE_RUNSTATE_START;
						}
						break;
					case EVT_TOTALWINNINGS:
						if(Storage_IsTicketlessTokenInteractive()) {
							if(Storage_GetWinnings() == 0)
								Button_StartFade(SII_BUTTONFADE_ATTENTION,buttonfade_start_out,TRUE);
							Storage_SetWinnings(tevent.data1);
							UI_DoCMSG(MSG_M_TTIWIN,FALSE);
						}
						break;
					case EVT_REENTER:
						g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				if (Storage_IsTicketlessTokenInteractive()) {
					if (Util_CheckSTimeout(gp_timer)) {
						if ((Storage_GetWinnings() != IOSys_GetDispenserWinnings()) && (IOSys_GetDispenserWinnings() > 0)) {
							if (Storage_GetWinnings() == 0)
								Button_StartFade(SII_BUTTONFADE_ATTENTION,buttonfade_start_out,TRUE);
							gp_timer = SEC_TIMER+1;
							Storage_SetWinnings(IOSys_GetDispenserWinnings());
							UI_DoCMSG(MSG_M_TTIWIN,FALSE);
						}
					}
				}
				break;
			case SKOOBE_RUNSTATE_START:
				// Wait for a valid account or termination of the transaction.
				if (EventQ_Pop(&tevent)) {
					if ((tevent.evt == EVT_GOTVALIDACCT)||(tevent.evt == EVT_TRANSACTIONCOMPLETESUCCESS)||(tevent.evt == EVT_TRANSACTIONCOMPLETEFAIL)) {
						if (CClient_IsPush() && G_STOR_CNFG_MSG.pushenabled) {
							if (CClient_IsValidCard(CClient_GetLastScan())) {
								// Now determine where to go based on device type.
								if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_TIKEATER) {
									g_skoobe_runstate = SKOOBE_RUNSTATE_TICKETEATER_ENTER;
								} else if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_ACCESS) {
									g_skoobe_runstate = SKOOBE_RUNSTATE_ACCESS_ENTER;
								} else {
									g_skoobe_runstate = SKOOBE_RUNSTATE_ENGAGE_ENTER;
								}
							} else {
								UI_DoCMSG(MSG_R_UNKNOWN,FALSE);
								// Wait for message to time out before switching back to Default Run State
								// During this wait, don't allow the same card to be swiped again.
								g_skoobe_runstate = SKOOBE_RUNSTATE_BADCARD_WAIT;
							}
						} else {
							if (!G_STOR_ACCT_MSG.acctvalid) {
								UI_DoManualMsg("Please Swipe\nAgain",5,FALSE,FALSE);
								// Wait for message to time out before switching back to Default Run State
								// During this wait, don't allow the same card to be swiped again.
								g_skoobe_runstate = SKOOBE_RUNSTATE_BADCARD_WAIT;
							} else {
								// If the server returned a non-zero reasoncode, there is a problem with the account.
								if (G_STOR_ACCT_MSG.reasoncode) {
									// Not Approved, Display Message
									UI_DoCMSG(MSG_R_OFFSET+G_STOR_ACCT_MSG.reasoncode,FALSE);
									// Wait for message to time out before switching back to Default Run State
									// During this wait, don't allow the same card to be swiped again.
									g_skoobe_runstate = SKOOBE_RUNSTATE_BADCARD_WAIT;
								} else {
									// Now that there's a valid ACCT, reconfigure the dispenser
									// and the I/O. If the card is ticketless, then the dispenser setup
									// changes.
									IOSys_SetupDispenser();
									IOSys_SetupProcessingFromConfig();
									// Now determine where to go based on device type.
									if (Storage_IsTicketlessTokenInteractive()) {
										// Show winnings.
										UI_DoCMSG(MSG_M_TTIWCOLL,FALSE);
										// Clear out the winnings.
										Storage_SetWinnings(0);
										g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
									} else if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_TIKEATER) {
										g_skoobe_runstate = SKOOBE_RUNSTATE_TICKETEATER_ENTER;
									} else if (G_STOR_DEVC_MSG.devicetype == DEVTYPE_ACCESS) {
										g_skoobe_runstate = SKOOBE_RUNSTATE_ACCESS_ENTER;
									} else {
										g_skoobe_runstate = SKOOBE_RUNSTATE_ENGAGE_ENTER;
									}
								}
							}
						}
					} else {
						DefaultEventHandler(tevent);
					}
				}
				break;
			case SKOOBE_RUNSTATE_ENGAGE_ENTER:
				Button_StartFade(SII_BUTTONFADE_ATTENTION,buttonfade_start_out,TRUE);
				UI_DoCMSG(MSG_M_PLAYBAL,TRUE);
				g_skoobe_runstate = SKOOBE_RUNSTATE_ENGAGE;
				break;
			case SKOOBE_RUNSTATE_ENGAGE:
				if (Button_isDown()) {
					IOSys_DeviceStart(1);
					CClient_SendMessage(SN_DID_BUTN);
					// Show the new balance.
					UI_DoCMSG(MSG_M_NEWBAL,FALSE);
                    // See if we should perform an extra delay before allowing more swipes.
                    if (G_STOR_CNFG_MSG.cyclewait > 0) {
        				Button_StartFade(SII_BUTTONFADE_DEFAULT,buttonfade_start_out,TRUE);
                        g_skoobe_runstate = SKOOBE_RUNSTATE_ENGAGEWAITMESSAGE;
                    } else {
                        g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
                    }
				} else if (UI_MessageDone()) {
					CClient_SendMessage(SN_DID_EOTX);
					// If the message timed out before button was pressed, just return to default state.
					// First delay a bit so the EOTX can be processed and the connection closed.
					gp_timer = MS_TIMER+SII_EOTX_INTERVAL_MS;
					g_skoobe_runstate = SKOOBE_RUNSTATE_DELAY;
					g_skoobe_runstatenext = SKOOBE_RUNSTATE_DEFAULT_ENTER;
				}
				/* TODO: It would be nice if the user could swipe another card here instead of waiting
					for this one to time out. However, the server seems to ignore everything between a
					SCAN and a BUTN.
				*/
				else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// While waiting for a button press, ignore repeated scans of the
						// same card.
						if (strncmp(CClient_GetLastScan(),g_currentscan,BC_MAXBARCODELEN)!=0) {
							CClient_SendMessage(SN_DID_EOTX);
							UI_DoManualMsg("Processing.\nPlease wait...",0,FALSE,FALSE);
							// First delay a bit so the EOTX can be processed and the connection closed.
							// Push the barcode event back on & let the "SKOOBE_RUNSTATE_DEFAULT" state handle it.
							EventQ_PushRec(&tevent);
							gp_timer = MS_TIMER+SII_EOTX_INTERVAL_MS;
							g_skoobe_runstate = SKOOBE_RUNSTATE_DELAY;
							g_skoobe_runstatenext = SKOOBE_RUNSTATE_DEFAULT;
						}
						break;
					case EVT_REENTER:
						g_skoobe_runstate = SKOOBE_RUNSTATE_ENGAGE_ENTER;
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				break;
            case SKOOBE_RUNSTATE_ENGAGEWAITMESSAGE:
				if (UI_MessageDone()) {
                    gp_timer = MS_TIMER+(G_STOR_CNFG_MSG.cyclewait*1000);
                    g_skoobe_runstate = SKOOBE_RUNSTATE_DELAY;
                    g_skoobe_runstatenext = SKOOBE_RUNSTATE_DEFAULT_ENTER;
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// Don't drop this event.
						EventQ_PushRec(&tevent);
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
                break;
			case SKOOBE_RUNSTATE_BADCARD_WAIT:
				if (UI_MessageDone()) {
					g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// While waiting for "bad card" message to time out, ignore repeated scans of the same card.
						if (strncmp(CClient_GetLastScan(),g_currentscan,BC_MAXBARCODELEN)!=0) {
							CClient_SendMessage(SN_DID_EOTX);
							UI_DoManualMsg("Processing.\nPlease wait...",0,FALSE,FALSE);
							// Push the barcode event back on & let the "SKOOBE_RUNSTATE_DEFAULT" state handle it.
							EventQ_PushRec(&tevent);
							g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT;
						}
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				break;
			case SKOOBE_RUNSTATE_TICKETEATER_ENTER:
				// Show the message that prompts to press button when done.
				IOSys_DeviceStart(1);
				g_skoobe_runstate = SKOOBE_RUNSTATE_TICKETEATER_UI;
				break;
			case SKOOBE_RUNSTATE_TICKETEATER_UI:
				UI_DoCMSG(MSG_M_TKDONE,FALSE);
				Button_StartFade(SII_BUTTONFADE_ATTENTION,buttonfade_start_out,TRUE);
				g_skoobe_runstate = SKOOBE_RUNSTATE_TICKETEATER;
				break;
			case SKOOBE_RUNSTATE_TICKETEATER:
				if(Button_isDown() || UI_MessageDone()) {
					// Cancel the ticket eater.
					IOSys_DeviceStart(99);
					UI_DoCMSG(MSG_M_TTIWCOLL,FALSE);
					g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_REENTER:
						g_skoobe_runstate = SKOOBE_RUNSTATE_TICKETEATER_UI;
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				break;
			case SKOOBE_RUNSTATE_ACCESS_ENTER:
				IOSys_DeviceStart(1);
				g_skoobe_runstate = SKOOBE_RUNSTATE_ACCESS_UI;
				break;
			case SKOOBE_RUNSTATE_ACCESS_UI:
				// Show the message that prompts to press button when done.
				UI_DoCMSG(MSG_M_PUSHSTRT,FALSE);
				Button_StartFade(SII_BUTTONFADE_ATTENTION,buttonfade_start_out,TRUE);
				g_skoobe_runstate = SKOOBE_RUNSTATE_ACCESS;
				break;
			case SKOOBE_RUNSTATE_ACCESS:
				if(UI_MessageDone()) {
					// Cancel the access.
					IOSys_DeviceStart(99);
					g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_REENTER:
						g_skoobe_runstate = SKOOBE_RUNSTATE_ACCESS_UI;
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				break;
			case SKOOBE_RUNSTATE_FREEPLAY_ENTER:
                CC_Lock(FALSE); /* Entering a play mode always takes us out of standby */
				CClient_SetSSRVState(SSRVSTATE_FREEPLAY);
				// Display the freeplay message immediately.
				UI_DoCMSG(MSG_M_FREEPLAY,FALSE);
            CC_GenerateLcdCnfgMessage(); /* Communicate change to LCD */
				// Set the button to attract attention state.
				Button_StartFade(SII_BUTTONFADE_ATTENTION,buttonfade_start_out,TRUE);
				g_skoobe_runstate = SKOOBE_RUNSTATE_FREEPLAYWAITBTND;
				break;
			case SKOOBE_RUNSTATE_FREEPLAYWAITBTND:
				if (Button_isDown()) {
					IOSys_DeviceStart(1);
					CClient_SendMessage(SN_DID_BUTN);
					gp_timer = MS_TIMER+2000;
					g_skoobe_runstate = SKOOBE_RUNSTATE_FREEPLAYWAITBTNU;
				} else if (EventQ_Pop(&tevent)) {
					DefaultEventHandler(tevent);
				}
				break;
			case SKOOBE_RUNSTATE_FREEPLAYWAITBTNU:
				if (!Button_isDown()) {
                    Button_Flash(50,50,3);
                    // See if we should perform an extra delay before allowing more button presses.
                    if (G_STOR_CNFG_MSG.cyclewait > 0) {
        				Button_StartFade(SII_BUTTONFADE_DEFAULT,buttonfade_start_out,TRUE);
                        g_skoobe_runstate = SKOOBE_RUNSTATE_FREEPLAYWAITMESSAGE;
                    } else {
                        g_skoobe_runstate = SKOOBE_RUNSTATE_FREEPLAY_ENTER;
                    }
				} else if(Util_CheckmsTimeout(gp_timer)) {
					UI_DoManualMsg("Please release\nbutton.",0,FALSE,FALSE);
					// We only need to display the message once but, to avoid another state,
					// we extend the timeout. This may cause the message to flicker every few
					// seconds, but we don't care.
					gp_timer = MS_TIMER+5000;
				} else if (EventQ_Pop(&tevent)) {
					DefaultEventHandler(tevent);
				}
				break;
            case SKOOBE_RUNSTATE_FREEPLAYWAITMESSAGE:
				if (UI_MessageDone()) {
                    gp_timer = MS_TIMER+(G_STOR_CNFG_MSG.cyclewait*1000);
                    g_skoobe_runstate = SKOOBE_RUNSTATE_DELAY;
                    g_skoobe_runstatenext = SKOOBE_RUNSTATE_FREEPLAY_ENTER;
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// Don't drop this event.
						EventQ_PushRec(&tevent);
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
                break;
			case SKOOBE_RUNSTATE_PUSHXFER_ENTER:
				CClient_SetSSRVState(SSRVSTATE_BUFFERTRANSFER);
				UI_DoManualMsg("Transferring...",0,FALSE,FALSE);
				if(!Push_SendNextRecord())
					CClient_SendMessage(SN_DID_EOTX);
				gp_timer = MS_TIMER+SII_PUSHTRANSFER_TIMEOUT_MS;
            CC_GenerateLcdCnfgMessage(); /* Communicate change to LCD */
				g_skoobe_runstate = SKOOBE_RUNSTATE_PUSHXFER;
				break;
			case SKOOBE_RUNSTATE_PUSHXFER:
				if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_PUSHACKXFER:
						if(!Push_AckPreviousSendNext()) {
							CClient_SendMessage(SN_DID_EOTX);
						}
						gp_timer = MS_TIMER+SII_PUSHTRANSFER_TIMEOUT_MS;
						break;
					case EVT_TRANSACTIONCOMPLETESUCCESS:
						DebugPrint(STRACE_NFO,("Push transfer transaction completed successfully. Returning to last mode."));
						g_skoobe_runstate = g_skoobe_runstatenext;
						EventQ_Push(EVT_REENTER,0,0);
						break;
					case EVT_PUSHXFERENTER:
						// Be sure to ignore this event, or it could trap us in the push buffer transfer state.
						break;
					default:
						// Ignore all other events while transferring the push buffer.
						break;
					}
				} else if(Util_CheckmsTimeout(gp_timer)) {
					DebugPrint(STRACE_ERR,("Push transfer transaction timed out. Returning to run mode."));
					CClient_SendMessage(SN_DID_EOTX);
					g_skoobe_runstate = g_skoobe_runstatenext;
					EventQ_Push(EVT_REENTER,0,0);
				}
				break;
			case SKOOBE_RUNSTATE_STANDBY_ENTER:
				CClient_SetSSRVState(SSRVSTATE_STANDBY);
				// Display the standby message immediately.
				UI_DoCMSG(MSG_M_NOSERVC,FALSE);
				// Shut off the button.
				Button_LEDState(buttonLED_off);
				g_skoobe_runstate = SKOOBE_RUNSTATE_STANDBY;
				break;
			case SKOOBE_RUNSTATE_STANDBY:
				if (EventQ_Pop(&tevent)) {
					if (tevent.evt == EVT_REENTER) {
						g_skoobe_runstate = SKOOBE_RUNSTATE_STANDBY_ENTER;
					} else {
						DefaultEventHandler(tevent);
					}
				}
				break;
			case SKOOBE_RUNSTATE_NOTREGISTERED_ENTER:
				CClient_SetSSRVState(SSRVSTATE_NEWCONTROLLER);
				// Display the standby message immediately.
				UI_DoCMSG(MSG_M_NOSERVC,FALSE);
				// Shut off the button.
				Button_LEDState(buttonLED_off);
            CC_GenerateLcdCnfgMessage(); /* Communicate change to LCD */ 
				g_skoobe_runstate = SKOOBE_RUNSTATE_NOTREGISTERED;
				break;
			case SKOOBE_RUNSTATE_NOTREGISTERED:
				if (EventQ_Pop(&tevent)) {
					if (tevent.evt == EVT_REENTER) {
						g_skoobe_runstate = SKOOBE_RUNSTATE_NOTREGISTERED_ENTER;
					} else {
						DefaultEventHandler(tevent);
					}
				}
				break;
			case SKOOBE_RUNSTATE_DELAYONMSGREENTER:
				if(UI_MessageDone()) {
					g_skoobe_runstate = g_skoobe_runstatenext;
					EventQ_Push(EVT_REENTER,0,0);
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// Don't drop this event.
						EventQ_PushRec(&tevent);
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				break;
			case SKOOBE_RUNSTATE_DELAY:
				if(Util_CheckmsTimeout(gp_timer)) {
					g_skoobe_runstate = g_skoobe_runstatenext;
				} else if (EventQ_Pop(&tevent)) {
					switch (tevent.evt) {
					case EVT_GOTPLAYBARCODE:
						// Don't drop this event.
						EventQ_PushRec(&tevent);
						break;
					default:
						DefaultEventHandler(tevent);
						break;
					}
				}
				break;
			default:
				g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
				break;
			}
			break;
		}
    }
}

void HandleScan(void)
{
	int		tBClen;
	int		tCMDID;
	BOOL	tForward;

	tBClen = Barcode_Get(1,g_currentscan,BC_MAXBARCODELEN+1,g_currentscanextradata,BC_MAXEXTRADATALEN+1);
	if(!tBClen)
		tBClen = Barcode_Get(2,g_currentscan,BC_MAXBARCODELEN+1,g_currentscanextradata,BC_MAXEXTRADATALEN+1);
	if(!tBClen)
		return;
    DebugPrint(STRACE_DBG,("Found Barcode (len %d): %s. Extra data: %s",tBClen,g_currentscan,g_currentscanextradata));
	Button_Flash(50,50,3);

	tCMDID = Cards_GetCommandCardID(g_currentscan,&tForward);
	if (tCMDID) {
		if(tForward) {
			EventQ_Push(EVT_GOTCMDFWDBARCODE,tCMDID,0);
		} else
			EventQ_Push(EVT_GOTCMDBARCODE,tCMDID,0);
	} else {
		EventQ_Push(EVT_GOTPLAYBARCODE,0,0);
	}
}

void DisplayVersionInfo(void)
{
	char	tbuff[80];

	// Queue the version messages.
	UI_DoManualMsg("Ideal Cashless Sys.\n(c) 2008, ISS, Inc",3,FALSE,TRUE);
	snprintf(tbuff,sizeof(tbuff),"Ideal Cashless Sys.\n%s", g_VersionString);
	tbuff[sizeof(tbuff)-1] = '\0';
	UI_DoManualMsg(tbuff,3,FALSE,TRUE);
}

void DisplayNetworkInfo(void)
{
	char	tbuff[80];
	char	tbuff2[16];

	// Queue the IP & MAC message.
	inet_ntoa(tbuff2, gethostid());
	snprintf(tbuff,sizeof(tbuff),"MA %s\nIP %s",g_ccMACAddress,tbuff2);
	tbuff[sizeof(tbuff)-1] = '\0';
	UI_DoManualMsg(tbuff,3,FALSE,TRUE);
}

/*	--------------------------------------------------------------
	Function: ProcessCommandCard
	Params:
		cardID - ID of the command card.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Executes a command based on a command card.
	------------------------------------------------------------ */
void ProcessCommandCard(int cardID) {
	char cMsg[20];

    DebugPrint(STRACE_DBG,("Processing Command Card ID: %d",cardID));
	switch (cardID) {
	case 1: // Reboot
		UI_DoManualMsg("Resetting. Be right\nback! :)",2,FALSE,FALSE);
		while (!UI_MessageDone()) {
			UI_Handler();
		}
		forceSoftReset();
		return;
	case 2: // Show Version & Network info.
		EventQ_Push(EVT_SHOWVERSIONINFO,0,0);
		return;
	case 3:		// Cycle Relays 1 x @ 500/500 (protected)
		if(!g_testmode) //TODO: Protect!
			return;
		DebugPrint(STRACE_DBG,("Cycle Relay 500/500"));
		IOSys_DeviceStart(-3);
		return;
	case 5:		// Cycle Relays 1 x @ 200/200 (protected)
		if(!g_testmode) //TODO: Protect!
			return;
	case 11:	// Cycle Relays 1 x @ 200/200 (unprotected)
		DebugPrint(STRACE_DBG,("Cycle Relay 200/200"));
		IOSys_DeviceStart(-5);
		return;
	case 4: // Blink LEDs for 8 seconds
		EventQ_Push(EVT_BLINKLEDS,0,0);
		return;
	case 6: // Select DL-1275 Dispenser
		EventQ_Push(EVT_SETDISPENSERTYPE,1,0);
		return;
	case 7: // Select DL-4 Dispenser
		EventQ_Push(EVT_SETDISPENSERTYPE,2,0);
		return;
	case 8: // Dispense 2 Tickets (protected)
		if(!g_testmode) //TODO: Protect!
			return;
	case 12: // Dispense 2 Tickets (unprotected)
		EventQ_Push(EVT_DISPENSE,2,0);
		return;
	case 9: // Select Robo Play mode (toggle)
		return;
	case 10: // Display State Diagnostic Info
		return;
	case 103: // Reboot all devices
	case 104: // Reboot this device
		SafeReboot_SoftReboot();
		return;
	case 105: // Standby all devices
	case 106: // Standby this device
		EventQ_Push(EVT_LOCK,0,0);
		return;
	case 111: // Cancel Discount on all devices
	case 112: // Cancel Discount on this device
		Storage_SetPriceState(PRICE_STATE_STANDARD);
		EventQ_Push(EVT_PRICECHANGED,0,0);
		return;
	case 113: // Activate Discount 1 on all devices
	case 114: // Activate Discount 1 on this device
		Storage_SetPriceState(PRICE_STATE_DISCNT1);
		EventQ_Push(EVT_PRICECHANGED,0,0);
		return;
	case 115: // Activate Discount 2 on all devices
	case 116: // Activate Discount 2 on this device
		Storage_SetPriceState(PRICE_STATE_DISCNT2);
		EventQ_Push(EVT_PRICECHANGED,0,0);
		return;
	case 117: // Activate Discount 3 on all devices
	case 118: // Activate Discount 3 on this device
		Storage_SetPriceState(PRICE_STATE_DISCNT3);
		EventQ_Push(EVT_PRICECHANGED,0,0);
		return;
	case 119: // Free Play on all devices
	case 120: // Free Play on this device
		EventQ_Push(EVT_FREEPLAY,0,0);
		return;
	case 121: // Time Play on all devices
	case 122: // Time Play on this device
		// We don't support time play.
		return;
	case 123: // Value Play on all devices
	case 124: // Value Play on this device
		Storage_SetPriceState(PRICE_STATE_STANDARD);
		EventQ_Push(EVT_VALUEPLAY,0,0);
		return;
	}
}

/*	--------------------------------------------------------------
	Function: ValidateConfiguration
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Validates the configuration. If all necessary configuration
		data has been recieved (or loaded from nonvolatile storage)
		& is valid this function sets a flag that can be retrieved
		with Storage_HaveValidConfig().
	------------------------------------------------------------ */
void ValidateConfiguration(void)
{
	BOOL tresult;

	tresult = TRUE;
	// Checks Non volatile storage structures and wipes them if they are invalid.
	if(!Storage_ValidateNonVolatileRAMConfig()) {
		DebugPrint(STRACE_ERR,("Storage_ValidateNonVolatileRAMConfig() failed."));
		tresult = FALSE;
	}
	// Checks the Cards file to see if it's valid.
	if(!Cards_ValidateFile()) {
		DebugPrint(STRACE_ERR,("Cards_ValidateFile() failed."));
		tresult = FALSE;
	}
	// Checks the Messages file to see if it's valid.
	if(!Messages_ValidateFile()) {
		DebugPrint(STRACE_ERR,("Messages_ValidateFile() failed."));
		tresult = FALSE;
	}
	Storage_SetValidConfig(tresult);
}

/*	--------------------------------------------------------------
	Function: DefaultEventHandler
	Params:
		e - event to handle.
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Performs default event handling if a particular state
		doesn't wish to process an event.
	------------------------------------------------------------ */
void DefaultEventHandler(EQ_event e)
{
	switch (e.evt) {
    case EVT_NETWORKCONN_FAIL:
        if(G_STOR_DEVC_MSG.MustHaveNetwork) {
            g_skoobe_state = SKOOBE_STATE_NOCONNECTIONNOTALLOWED;
        }
        break;
	case EVT_PUSHXFERENTER:
		if(g_skoobe_runstate != SKOOBE_RUNSTATE_PUSHXFER) {
			g_skoobe_runstatenext = g_skoobe_runstate;
			g_skoobe_runstate = SKOOBE_RUNSTATE_PUSHXFER_ENTER;
		}
		return;
	case EVT_PUSHMODEENTER:
        g_skoobe_state = SKOOBE_STATE_TRYENTERPUSHMODE;
		return;
	case EVT_PUSHMODEEXIT:
		UI_PushIndicator(FALSE);
		return;
	case EVT_DIOS:
		CClient_SendMessage(SN_DID_DIOS,e.data1,e.data2);
		DebugPrint(STRACE_DBG,("DIOP #%d, count %d\n",e.data1,e.data2));
		return;
	case EVT_FREEPLAY:
		g_skoobe_runstate = SKOOBE_RUNSTATE_FREEPLAY_ENTER;
		return;
	case EVT_VALUEPLAY:
		g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
		return;
	case EVT_LOCK:
		CC_Lock(TRUE);
		g_skoobe_runstate = SKOOBE_RUNSTATE_STANDBY_ENTER;
		return;
	case EVT_UNLOCK:
        /* Switching to the default state automatically unlocks the skoobe */
		g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
		return;
	case EVT_BLINKLEDS:
		Button_StartFade(100,buttonfade_start_out,TRUE);
		UI_DoManualMsg("Flashy Flashy\nBlinky Blinky",8,TRUE,FALSE);
		if(g_skoobe_runstate != SKOOBE_RUNSTATE_DELAYONMSGREENTER) {
			// Don't reenter the SKOOBE_RUNSTATE_DELAYONMSGREENTER state, or we'll be trapped there
			// because g_skoobe_runstatenext will be set to it.
			g_skoobe_runstatenext = g_skoobe_runstate;
			g_skoobe_runstate = SKOOBE_RUNSTATE_DELAYONMSGREENTER;
		}
		return;
	case EVT_SHOWVERSIONINFO:
		// Enqueue Version Info
		DisplayVersionInfo();
		// Enqueue Network Info
		DisplayNetworkInfo();
		if(g_skoobe_runstate != SKOOBE_RUNSTATE_DELAYONMSGREENTER) {
			// Don't reenter the SKOOBE_RUNSTATE_DELAYONMSGREENTER state, or we'll be trapped there
			// because g_skoobe_runstatenext will be set to it.
			g_skoobe_runstatenext = g_skoobe_runstate;
			g_skoobe_runstate = SKOOBE_RUNSTATE_DELAYONMSGREENTER;
		}
		return;
	case EVT_GOTCMDBARCODE:
		ProcessCommandCard(e.data1);
		return;
	case EVT_GOTCMDFWDBARCODE:
		// If it is a command card that needs to be forwarded send the scan to the server.
		ProcessCommandCard(e.data1);
		CClient_SendMessage(SN_DID_SCAN,g_currentscan,g_currentscanextradata);
		return;
	case EVT_SETDISPENSERTYPE:
		if (IOSys_IsDispensing()) {
			UI_DoManualMsg("Please wait until\nwinnings dispensed.",5,FALSE,FALSE);
		} else {
			if(e.data1 == 1) {
				UI_DoManualMsg("DL-1275 Selected.",5,FALSE,FALSE);
				G_STOR_DEVC_MSG.dispenstype = DISPENSETYPE_MOTOR;
			} else if(e.data1 == 2) {
				UI_DoManualMsg("DL-4 Selected.",5,FALSE,FALSE);
				G_STOR_DEVC_MSG.dispenstype = DISPENSETYPE_PULSE;
			} else
				return;
			G_STOR_DEVC.chksum =  Storage_Checksum(&G_STOR_DEVC_MSG, sizeof(G_STOR_DEVC_MSG));
			IOSys_SetupDispenser();
			IOSys_SetupProcessingFromConfig();
			if(g_skoobe_runstate != SKOOBE_RUNSTATE_DELAYONMSGREENTER) {
				// Don't reenter the SKOOBE_RUNSTATE_DELAYONMSGREENTER state, or we'll be trapped there
				// because g_skoobe_runstatenext will be set to it.
				g_skoobe_runstatenext = g_skoobe_runstate;
				g_skoobe_runstate = SKOOBE_RUNSTATE_DELAYONMSGREENTER;
			}
		}
		return;
	case EVT_DISPENSE:
		IOSys_Dispense(e.data1);
		return;
	case EVT_PRICECHANGED:
		// This causes the display to update with the latest price.
		g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
		return;
	case EVT_CHANGESTATE:
		switch ((SSRV_STATE)e.data1) {
		case SSRVSTATE_STANDBY:
			g_skoobe_runstate = SKOOBE_RUNSTATE_STANDBY_ENTER;
			return;
		case SSRVSTATE_DEFAULT:
			g_skoobe_runstate = SKOOBE_RUNSTATE_DEFAULT_ENTER;
			return;
		case SSRVSTATE_FREEPLAY:
			g_skoobe_runstate = SKOOBE_RUNSTATE_FREEPLAY_ENTER;
			return;
		case SSRVSTATE_NEWCONTROLLER:
			g_skoobe_runstate = SKOOBE_RUNSTATE_NOTREGISTERED_ENTER;
			return;
		case SSRVSTATE_BUFFERTRANSFER:
			EventQ_Push(EVT_PUSHXFERENTER,0,0);
			return;
		default:
			g_skoobe_runstate = SKOOBE_RUNSTATE_STANDBY_ENTER;
			return;
		}
		return;
	}
}

