/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	SkoobeConfig.h
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Skoobe II hard-coded firmware parameters.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _SKOOBECONFIG_H_
#define _SKOOBECONFIG_H_
/*** EndHeader */

/*** BeginHeader */

/* --------------------------------------------------------------
	Network Configuration for Dynamic C Network Libraries
   ------------------------------------------------------------- */
//
// Empty TCP configuration, everything is configured at runtime.
#define TCPCONFIG					6
//
// Size of reserved TCP buffers
#define TCP_BUF_SIZE				4096
//
// Size of reserved UDP buffers
#define UDP_BUF_SIZE				4096
//
// Maximum Transmission Unit for Ethernet (most routers can support an MTU of up to 1500; however, some VPN
// and NAT configurations are known to have issues with devices that actually send transmission units of
// a full 1500 bytes)
#define ETH_MTU						1000
//
// Number of allocated TCP socket buffers. (1 for Application)
// If DNS is enabled, it allocates its own buffer.
#define MAX_TCP_SOCKET_BUFFERS		2
//
// Number of allocated UDP socket buffers. (1 for Application, 1 for loader)
// DHCP allocates its own buffer.
#define MAX_UDP_SOCKET_BUFFERS		2
//
// Compile in DHCP capabilities.
#define USE_DHCP
//
// Report this as our class to the DHCP server.
#define DHCP_CLASS_ID				"ICS:IdealSS:ICSController:5.x"
//
// Report our MAC Address as our client ID to the DHCP server.
#define DHCP_CLIENT_ID_MAC
//
// Save memory by disabling DNS lookup. Skoobe doesn't require this right now
// as everything is referred to by IP only.
#define DISABLE_DNS

/* --------------------------------------------------------------
	Serial Port Configuration for Dynamic C Serial Libraries
   ------------------------------------------------------------- */
//
// Set the Port D & E buffer sizes
#define DINBUFSIZE		31
#define DOUTBUFSIZE		31

/* --------------------------------------------------------------
	Dynamic C File System Configuration
   ------------------------------------------------------------- */
//
// Skoobe only uses a few files.
//MSS#define FS_MAX_FILES 5
#define FS_MAX_FILES 6

//
// This is a seed value for all checksums performed on Skoobe storage
// structures. If the firmware changes & needs to invalidate old versions
// of the structures that may be in RAM, this can be incremented.
#define SII_STORAGE_SEED			0x0006

/* --------------------------------------------------------------
					Boot Parameters
   ------------------------------------------------------------- */
//
// The Skoobe shows the boot screen and waits this long before
// continuing to boot.
#define SII_BOOTDELAY_MS			5000	// Time in milliseconds
//
// The Skoobe waits this long to acquire a SNBB packet.
#define SII_SNBBWAIT_S				30	// Time in seconds
//
// The Skoobe listens on this port for a SNBB packet.
#define SII_SNBBLISTEN_PORT			2030	// Port number
//
// The Skoobe accepts.
#define SII_BROADCAST_IP			"255.255.255.255"	// Port number

/* --------------------------------------------------------------
					Network Parameters
   ------------------------------------------------------------- */
//
// DHCP Timeout. Wait this long before assuming DHCP has failed.
#define SII_DHCP_TIMEOUT_S			30	// Time in seconds
//
// Sometimes it seems to take a while after initialization before there is
// a link. If no link exists at startup, wait this long for a link before
// failing.
#define SII_NOINITIALLINKWAIT_S		5	// Time in seconds
//
// If the interface goes down, try to bring it back up, waiting this long between retries.
#define SII_IFACEDOWN_RETRY_S		5	// Time in seconds
//
// If we couldn't get DHCP, but we can run in push, check again for DHCP repeatedly,
// waiting this long between retries.
#define SII_NODHCP_RETRY_S			60	// Time in seconds
//
// This is the maximum network transmission buffer size.
#define SII_NET_TXBUF_LEN			255	// Maximum buffer size
//
// This is the maximum network reception buffer size.
#define SII_NET_RXBUF_LEN			255	// Maximum buffer size
//
// BOOT Connect timeout. Wait this long to establish connection to server.
#define SII_BOOTCONN_TIMEOUT_S		20	// Time in seconds
//
// BOOT config timeout. Wait this long to negotiate configuration with server.
#define SII_BOOTCFG_TIMEOUT_S		20	// Time in seconds
//
// Stale transaction timeout. If a transaction doesn't complete in this time, kill it.
#define SII_TRANS_STALE_TIMEOUT_S	15	// Time in seconds
//
// HAPY interval. During idle periods, a HAPY will be sent at this interval.
#define SII_HAPY_INTERVAL_S			5	// Time in seconds
//
// EOTX wait interval. After sending an EOTX and closing the connection, wait this long before
// trying another transaction. If the socket doesn't close properly in this time, it will be reset.
#define SII_EOTX_INTERVAL_MS		250	// Time in milliseconds
//
// PUSH Transfer wait interval. Wait this long after sending a push buffer record before assuming
// the server is not going to respond and closing the connection.
#define SII_PUSHTRANSFER_TIMEOUT_MS	10000 // Time in milliseconds
//
// PUSH Transfer wait interval. Wait this long after sending a push buffer record before assuming
// the server is not going to respond and closing the connection.
#define SII_SNBBTOBOOTDELAY_MS	500 // Time in milliseconds

/* --------------------------------------------------------------
					UI Parameters
   ------------------------------------------------------------- */
//
// Character shown in upper-right corner in push mode.
#define SII_PUSHMODE_CHAR '*'			// Single character
//
// Default button fade speed (0-100, 100=fastest). Button fades in/out at this speed
// when controller isn't doing anything in particular.
#define SII_BUTTONFADE_DEFAULT	20		// Fade speed 0-100
//
// Button "attention" fade speed (0-100, 100=fastest). Button fades in/out at this speed
// when controller wants user to notice button.
#define SII_BUTTONFADE_ATTENTION 100	// Fade speed 0-100
//
// Power on button fade speed (0-100, 100=fastest). Button fades in at this speed
// when controller is turned on.
#define SII_BUTTONFADE_POWERON	50		// Fade speed 0-100

/* --------------------------------------------------------------
					Other Parameters
   ------------------------------------------------------------- */
#define SII_TRANSCARD_MAXLENGTH		32	// Maximum length of a transaction card's ID.
#define SII_COMMANDCARD_CMDLENGTH	18	// Length of a command card.

//MSS#define SII_NUMFILES			5		// Number of files Skoobe uses (must be <= FS_MAX_FILES, above)
#define SII_NUMFILES			6		// Number of files Skoobe uses (must be <= FS_MAX_FILES, above)

#define SII_CHECKSUMFILE		1		// File containing checksums of other files.
#define SII_CRDSFILE			2		// File containing Command Cards
#define SII_CMSGFILE			3		// File containing Messages
#define SII_PUSHPTRFILE			4		// File containing Push Buffer Pointers
#define SII_PUSHFILE			5		// File containing Push Buffer

#define SII_MSG_BLINKTIME		200		// Blink time in ms.

#define SII_MSG_RSNOFFSET		1000	// Offset of "reason" message IDs.

//
// This is the maximum number of push buffer records. This will allocate
// at least SII_MAX_PUSHBUFFER*sizeof(recd_TXNS_t) bytes in file system
// RAM. There will also be some file system overhead, but it is not
// possible to calculate how much based upon the Dynamic C documentation.
// Care should be taken when increasing this number, as there needs to be
// sufficient room left in the file system for the other configuration data.
//TODO: Figure out how big this can be.
#define SII_MAX_PUSHBUFFER      1800


/* --------------------------------------------------------------
	Built-in Command Cards
   ------------------------------------------------------------- */
#define SII_NUM_BUILTINCARDS	12
const char* sII_BuiltInCmdCards[SII_NUM_BUILTINCARDS] = {
	"111111111111111120",		// ID 01 - Reboot
	"111111111111111121",		// ID 02 - Show Version
	"111111111111111122",		// ID 03 - Cycle Relays 1 x @ 500/500 (protected)
	"111111111111111123",		// ID 04 - Blink LEDs for 8 seconds
	"111111111111111124",		// ID 05 - Cycle Relays 1 x @ 200/200 (protected)
	"111111111111111101",		// ID 06 - Select DL-1275 Dispenser
	"111111111111111102",		// ID 07 - Select DL-4 Dispenser
	"111111111111111103",		// ID 08 - Dispense 2 Tickets (protected)
	"111111111111111104",		// ID 09 - Select Robo Play mode (toggle)
	"111111111111111105",		// ID 10 - Display State Diagnostic Info
	"790915540307042000",		// ID 11 - Cycle Relays 1 x @ 200/200 (unprotected)
	"790915540307042001"		// ID 12 - Dispense 2 Tickets (unprotected)
};
/*** EndHeader */

/*** BeginHeader */
#endif // #ifndef _SKOOBECONFIG_H_
/*** EndHeader */