/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	ISRs.c
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Interrupt Service Routines
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _ISRS_C_
#define _ISRS_C_
/*** EndHeader */

/*** BeginHeader ISR_Init */
/*--------------------------------------------------------------
  Function: ISR_Init
  --------------------------------------------------------------
  Purpose:	Initializes the ISRs library.
  ------------------------------------------------------------*/
void ISR_Init(BOOL startIO);

#define TIMERB_RUNFADE_BIT		0
#define TIMERB_RUNIO_BIT		1
extern char			g_TimerBFlags;			// Timer B flags

extern int			g_TmrB_inc;	  			// Timer B increment value
extern int			g_TmrB_match;			// Calculated Timer B match value
extern unsigned int	g_TimerB_IOCounter;		// Timer B Counter value for IO polling
extern unsigned int	g_TimerB_IOScalar;		// Timer B Divider value for IO polling
extern unsigned int	g_TimerB_FadeCounter;	// Timer B Counter value for button fading
extern unsigned int	g_TimerB_FadeScalar;	// Timer B Divider value for button fading
interrupt ISR_Timer(void);
/*** EndHeader */
char			g_TimerBFlags;			// Timer B flags
int				g_TmrB_inc;	  			// Timer B increment value
int				g_TmrB_match;			// Calculated Timer B match value
unsigned int	g_TimerB_IOCounter;		// Timer B Counter value for IO polling
unsigned int	g_TimerB_IOScalar;		// Timer B Divider value for IO polling
unsigned int	g_TimerB_FadeCounter;	// Timer B Counter value for button fading
unsigned int	g_TimerB_FadeScalar;	// Timer B Divider value for button fading

void ISR_Init(BOOL startIO)
{
	auto long divisor;
	auto unsigned long base_frequency;
    int intlevel;

	if (startIO) {
		g_TimerBFlags = (0x01 << TIMERB_RUNIO_BIT);
	} else {
		g_TimerBFlags = 0;
	}

	#if __SEPARATE_INST_DATA__
		interrupt_vector timerb_intvec ISR_Timer;
	#else
		SetVectIntern(0x0B, ISR_Timer);	// set up ISR
	#endif

	// Set up PWM for button fading.
	base_frequency = (long)freq_divider * (long)300;	// PWM_FREQ_FACTOR = 300
	//rjt divisor = base_frequency/BUTTON_PWM_BASE - 1;
   divisor = base_frequency/500 - 1; //rjt just to get a compile
	if(divisor < 0)
		divisor = 0;
	else if(divisor > 255)
		divisor = 255;
	WrPortI(TAT9R, &TAT9RShadow, (char)divisor);

	// Set up Timer B to fire every 1ms.
	// Start Timer B Interrupt.
	intlevel = CalcButtonTimerValues(1000);

	// Init the IO scalar to 2 (I/O polling will happen every 2ms)
	g_TimerB_IOCounter		= 2;
	g_TimerB_IOScalar		= 2;

	// Init the fade scalar (may be changed by the button fading start routine)
	g_TimerB_FadeCounter	= 20;	// Medium fade speed of about every 20ms
	g_TimerB_FadeScalar		= 20;

	ipset(3);
#asm
	ioi ld	a,(TBCLR)
	ld		l,a
	ioi ld	a,(TBCMR)
	or		0x3f
	ld		h,a
	ld		de,(g_TmrB_inc)
	add	hl,de
	ld		(g_TmrB_match),hl
	ld 		a,h
	ioi ld (TBM1R),a
	ld		a,l
	ioi ld (TBL1R),a
#endasm
	ipres();
	WrPortI(TBCR, &TBCRShadow, intlevel);
	WrPortI(TBCSR, &TBCSRShadow, 0x03);
}

/*** BeginHeader ISR_EnableIO */
/*--------------------------------------------------------------
  Function: ISR_EnableIO
	Params:
		enable - if TRUE, IO processing is serviced. If FALSE,
			IO processing is not serviced.
	Returns:
		void
  --------------------------------------------------------------
  Purpose:	Enables or disables the IO processing portion of the
	timer ISR.
  ------------------------------------------------------------*/
void ISR_EnableIO(BOOL enable);
/*** EndHeader */
void ISR_EnableIO(BOOL enable)
{
	if(enable) {
		ipset(3);
		g_TimerBFlags |= (0x01<<TIMERB_RUNIO_BIT);
		ipres();
	} else {
		ipset(3);
		g_TimerBFlags &= ~(0x01<<TIMERB_RUNIO_BIT);
		ipres();
	}
}


/*** BeginHeader ISR_EnableFade */
/*--------------------------------------------------------------
  Function: ISR_EnableFade
	Params:
		enable - if TRUE, fade routine is serviced. If FALSE,
			fade routine is not serviced.
	Returns:
		void
  --------------------------------------------------------------
  Purpose:	Enables or disables the fading portion of the timer
	ISR.
  ------------------------------------------------------------*/
void ISR_EnableFade(BOOL enable);
/*** EndHeader */
void ISR_EnableFade(BOOL enable)
{
	if(enable) {
		ipset(3);
		g_TimerBFlags |= (0x01<<TIMERB_RUNFADE_BIT);
		ipres();
	} else {
		ipset(3);
		g_TimerBFlags &= ~(0x01<<TIMERB_RUNFADE_BIT);
		ipres();
	}
}

/*** BeginHeader ISR_SetFadeSpeed */
/*--------------------------------------------------------------
  Function: ISR_SetFadeSpeed
	Params:
		fspeed - 0-65535. Speed corresponds to (fspeed+1)ms
	Returns:
		void
  --------------------------------------------------------------
  Purpose:	Sets the fade speed
  ------------------------------------------------------------*/
void ISR_SetFadeSpeed(int fspeed);
/*** EndHeader */
void ISR_SetFadeSpeed(int fspeed)
{
	ipset(3);
	g_TimerB_FadeCounter = fspeed;
	g_TimerB_FadeScalar = fspeed;
	ipres();
}

/*** BeginHeader CalcButtonTimerValues */
/*--------------------------------------------------------------
  Function: CalcButtonTimerValues
	Params:
		usec - timer period in us.
	Returns:
		void
  --------------------------------------------------------------
  Purpose:	Calculates timer B values for the current processor
	speed to achieve the period specified in "usec".
  ------------------------------------------------------------*/
char CalcButtonTimerValues(unsigned long usec);
/*** EndHeader */
char CalcButtonTimerValues(unsigned long usec)
{
	int	timeradiv;
	char intlevel;
	unsigned long divisor;
	unsigned long intscale;
	long txtalfreq;
	#GLOBAL_INIT {
		// init Timer A for 1/256 divide.
		WrPortI(TAT1R,&TAT1RShadow,255);
	}

	txtalfreq = ((long)freq_divider * 307200l);
	intlevel = 2;	// Use interrupt priority 2 for timer.

	g_TimerB_FadeScalar = 0;	// no scale be default
	timeradiv=TAT1RShadow+1;	// if serial ports are usung timer A1 we can't modify it.
	intscale=(unsigned long)((float)usec*(float)txtalfreq/1.024e9);	// divisor is #of 1024 count timerb rollovers
	if (intscale < timeradiv)	{	// max for using both timer a and b
		divisor=(unsigned long)(((float)usec*(float)txtalfreq)/1.0e6+0.5);
		if (intscale == 0) {				// No need for scaling.
			intlevel &= 3;					// Clock Timer B by perclk/2
		} else if (intscale < 8) {			// timer b clk/16
			intlevel = (0x08|(intlevel&0x03));		// mask valid set pclk/2
			divisor=(unsigned long)(((float)usec*(float)txtalfreq)/8.0e6+0.5);
		} else {
			intlevel = 0x4|(intlevel&3); // use Timer A
			// try to calc best value
		 	divisor=(divisor+(timeradiv>>1))/timeradiv; // nearest multiple of timeradiv
		}
	} else {
		// period is too slow for Timer B, even when clocked by Timer A.
		// We would have to use another scalar in the ISR, which we don't want to do.
		DebugPrint(STRACE_ERR,("Unable to properly configure Timer B!"));
	}
	if (divisor>1023)
		divisor = 1023;
	g_TmrB_inc=(int)((divisor&0xff)|((divisor<<6)&0xc000));	// val for isr
	return intlevel;
}

/*** BeginHeader ISR_Timer */
/*--------------------------------------------------------------
  Function: ISR_Timer
  --------------------------------------------------------------
  Purpose:	Interrupt routine that scales timer B and calls
			fading routine appropriately.
  ------------------------------------------------------------*/
interrupt ISR_Timer(void);
/*** EndHeader */
nodebug
interrupt ISR_Timer(void)
{
#asm nodebug
	ld		hl,(g_TmrB_match)
	ld		a,h
	or		0x3F	// will cause carry to bit 8 to propigate to bit 14
	ld		h,a
	ld		de,(g_TmrB_inc)
	add		hl,de
	ld		(g_TmrB_match),hl

	ioi	ld	a, (TBCSR)
	ld		a,h
	ioi	ld	(TBM1R), a
	ld		a,l
	ioi	ld	(TBL1R), a

	ld		a, (g_TimerBFlags)
	bit		TIMERB_RUNIO_BIT,a
	jr		z, TimerB_skip_IO

	ld		hl, (g_TimerB_IOScalar)
	ld		ix, hl
	bool	hl
	jr		z, TimerB_noscale_IO

	ld		hl, (g_TimerB_IOCounter)
	dec		hl
	ld		(g_TimerB_IOCounter), hl
	bool	hl
	jr		nz, TimerB_skip_IO
	ld		(g_TimerB_IOCounter), ix
TimerB_noscale_IO:
	call	IORoutine
TimerB_skip_IO:

	ld		a, (g_TimerBFlags)
	bit		TIMERB_RUNFADE_BIT,a
	jr		z, TimerB_skip_Fade

	ld		hl, (g_TimerB_FadeScalar)
	ld		ix, hl
	bool	hl
	jr		z, TimerB_noscale_fade

	ld		hl, (g_TimerB_FadeCounter)
	dec		hl
	ld		(g_TimerB_FadeCounter), hl
	bool	hl
	jr		nz, TimerB_skip_Fade
	ld		(g_TimerB_FadeCounter), ix
TimerB_noscale_fade:
	;rjt call	FadeRoutine
TimerB_skip_Fade:
#endasm
}

/*** BeginHeader */
#endif // #ifndef _ISRS_C_
/*** EndHeader */

