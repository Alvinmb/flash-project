/* --------------------------------------------------------------
	Copyright (C) 2011 Ideal Software Systems. All Rights Reserved.

	Filename:	LightString.c
	By:			Rob Toth
	Purpose:	   Run the daisy chained LED light string using the WS2801.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	08-31-2011	v1.0
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _LIGHT_STRING_C_
#define _LIGHT_STRING_C_

/*** EndHeader */

/*** BeginHeader LightString_Init */
/*	--------------------------------------------------------------
	Function: LightString_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the Light String library.
	------------------------------------------------------------ */
void LightString_Init(void);

#define LS_PORT_DR			PADR
#define LS_PORT_SHADOW		PADRShadow
#define LS_SET_BIT_HIGH		1
#define LS_SET_BIT_LOW  	0
#define LS_CLOCK_LINE  		0    //Port A pin 0
#define LS_DATA_LINE   		1    //Port A pin 1

#define LS_CLOCKS_PER_DEVICE 		24
#define LS_NUMBER_WS2801_DEVICES 12
#define LS_REFRESH_PERIOD 			50 //50 msec
#define LS_TWO_SECOND_TICKS      40
#define LS_BIT24						0x00800000
#define LS_RED_STEP			20		//Used for push mode


extern unsigned short g_tickCount;

extern unsigned long g_colorOneStep;
extern unsigned long g_colorTwoStep;
extern unsigned long g_currentColor;
extern unsigned long	g_lightStringTimer;
extern unsigned long g_colorOneStepNormal;
extern unsigned long g_colorTwoStepNormal;
extern unsigned long g_scanFlashTimer;
extern unsigned long g_colorRedStep;

/*** EndHeader */

unsigned short g_tickCount;

unsigned long g_colorOneStep;
unsigned long g_colorTwoStep;
unsigned long g_currentColor;
unsigned long g_lightStringTimer;
unsigned long g_colorOneStepNormal;
unsigned long g_colorTwoStepNormal;
unsigned long g_scanFlashTimer;		//Used to set the 2 second scanner read status
unsigned long g_colorRedStep;

void LightString_Init(void)
{
   // PA0 is output.
   // PA1 is output.
   WrPortI(SPCR, &SPCRShadow, 0x84); //Make PortA a byte wide output
   BitWrPortI(LS_PORT_DR, &LS_PORT_SHADOW, LS_SET_BIT_LOW, LS_DATA_LINE);
   BitWrPortI(LS_PORT_DR, &LS_PORT_SHADOW, LS_SET_BIT_LOW, LS_CLOCK_LINE);

   g_tickCount = 0;

   g_colorOneStep = 0;
   g_colorTwoStep = 0;

   g_currentColor = 0;
   g_scanFlashTimer = 0;

   g_colorRedStep = 0x00140000; //step size of 20

   g_lightStringTimer = (MS_TIMER + LS_REFRESH_PERIOD);

   DebugPrint(STRACE_NFO,("LED light string configured on PortA"));
}

/*** BeginHeader HandleLightString */
/*	--------------------------------------------------------------
	Function: HandleLightString
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Control the daisy chained LEDs

	------------------------------------------------------------ */
void HandleLightString(void);
/*** EndHeader */
void HandleLightString(void)
{
	unsigned short clockCount;
   unsigned long mask;

   mask = LS_BIT24;

   if( Util_CheckmsTimeout(g_lightStringTimer) )
   {
   	g_lightStringTimer = (MS_TIMER + LS_REFRESH_PERIOD); //Reload for next time

      //Check and see if we are doing a scanner status flash
      if(g_scanFlashTimer > 0)
      {
         g_scanFlashTimer--;
         if(g_scanFlashTimer == 0)
         {
         	//Time to put everything back to normal
            g_colorOneStep = g_colorOneStepNormal;
            g_colorTwoStep = g_colorTwoStepNormal;
            g_tickCount = 40;     //Put this at a low point
         }
      }
      else  //Handle the dimming/brightening of the LEDs
      {
 			#if 1 //new way
         g_tickCount++;

         if(CClient_IsPush())
         {
         	//Do special push mode sequence
            g_tickCount++;
      		if(g_tickCount < 10)
      		{
         		g_currentColor += g_colorOneStep;
      		}
            else if(g_tickCount >= 10 && g_tickCount < 30)
           	{
          		//No change, leave the LEDs at this brightness for a bit
           	}
            else if(g_tickCount >= 30 && g_tickCount < 40)
      	  	{
      			g_currentColor -= g_colorOneStep;
      		}
      		else if(g_tickCount == 40)
      	   {
           		g_currentColor = g_colorRedStep;
      	  	}
      	  	else if(g_tickCount > 40 && g_tickCount < 50)
	      	{
	           	g_currentColor += g_colorRedStep;
	      	}
	      	else if(g_tickCount >= 30 && g_tickCount < 60)
	      	{
	         	g_currentColor -= g_colorRedStep;
	      	}
            else if(g_tickCount == 60)
      		{
         		g_currentColor = g_colorRedStep;
      		}
            else if(g_tickCount > 60 && g_tickCount < 70)
	      	{
	         	g_currentColor += g_colorRedStep;
	      	}
	      	else if(g_tickCount >= 70 && g_tickCount < 80)
	      	{
	         	g_currentColor -= g_colorRedStep;
	      	}
            else if(g_tickCount == 80)
      		{
					g_currentColor = g_colorTwoStep;
      		}
      		else if(g_tickCount > 80 && g_tickCount < 90)
	      	{
	         	g_currentColor += g_colorTwoStep;
	      	}
         	else if(g_tickCount >= 90 && g_tickCount < 110)
         	{
          		//No change, leave the LEDs at this brightness for a bit
         	}
         	else if(g_tickCount >= 110 && g_tickCount < 120)
	      	{
	         	g_currentColor -= g_colorTwoStep;
	      	}
	      	else
	      	{
	         	g_currentColor = g_colorOneStep;
	         	g_tickCount = 0;
	      	}
         }
         else
         {
            //Not Push mode
            if(g_tickCount < 10)
      		{
         		g_currentColor += g_colorOneStep;
      		}
         	else if(g_tickCount >= 10 && g_tickCount < 30)
         	{
          		//No change, leave the LEDs at this brightness for a bit*
               g_currentColor += g_colorOneStep;
         	}
            else if(g_tickCount >= 30 && g_tickCount < 40)
      		{
      		  	g_currentColor -= g_colorOneStep;
      		}
      	  else if(g_tickCount == 40)
      	  	{
         	 	g_currentColor = g_colorTwoStep;
      	  }
      	  else if(g_tickCount > 40 && g_tickCount < 50)
	        {
	        	g_currentColor += g_colorTwoStep;
	        }
           	else if(g_tickCount >= 50 && g_tickCount < 60)
         	{
          		//No change, leave the LEDs at this brightness for a bit
         	}
         	else if(g_tickCount >= 70 && g_tickCount < 80)
	      	{
	         	g_currentColor -= g_colorTwoStep;
	      	}
	      	else
	      	{
	         	g_currentColor = g_colorOneStep;
	         	g_tickCount = 0;
	      	}
         }

         #else //The original way
      	g_tickCount++;
      	if(g_tickCount < 10)
      	{
         	g_currentColor += g_colorOneStep;
      	}
      	else if(g_tickCount >= 10 && g_tickCount < 20)
      	{
      		g_currentColor -= g_colorOneStep;
      	}
      	else if(g_tickCount == 20)
      	{
         	g_currentColor = g_colorTwoStep;
      	}
      	else if(g_tickCount > 20 && g_tickCount < 30)
	      {
	         g_currentColor += g_colorTwoStep;
	      }
	      else if(g_tickCount >= 30 && g_tickCount < 40)
	      {
	         g_currentColor -= g_colorTwoStep;
	      }
	      else
	      {
	         g_currentColor = g_colorOneStep;
	         g_tickCount = 0;
	      }
         #endif
      }

      clockCount = 0;

   	while( clockCount++ < (LS_CLOCKS_PER_DEVICE * LS_NUMBER_WS2801_DEVICES) )
   	{
      	//Bring clock line low
      	BitWrPortI(PADR, &PADRShadow, LS_SET_BIT_LOW, LS_CLOCK_LINE);


      	//Get next bit to send out
      	if( g_currentColor & mask )
         {
         	BitWrPortI(LS_PORT_DR, &LS_PORT_SHADOW, LS_SET_BIT_HIGH, LS_DATA_LINE);

         }
         else
         {
           	BitWrPortI(LS_PORT_DR, &LS_PORT_SHADOW, LS_SET_BIT_LOW, LS_DATA_LINE);

         }

      	//Adjust mask for next time through
      	mask = mask >> 1;
      	if(mask == 0)
         	mask = LS_BIT24;

      	//Bring clock line high
      	BitWrPortI(LS_PORT_DR, &LS_PORT_SHADOW, LS_SET_BIT_HIGH, LS_CLOCK_LINE);

   	}
      //Clock line low to start latch data time out
   	BitWrPortI(LS_PORT_DR, &LS_PORT_SHADOW, LS_SET_BIT_LOW, LS_CLOCK_LINE);
   }

}

/*** BeginHeader SetLightStringColor */
/*	--------------------------------------------------------------
	Function: SetLightStringColor
	Params:
   		whichOne - 1 or 2
         red		- The intensity of red range 0 to 255
         green		- The intensity of green range 0 to 255
         blue		- The intensity of blue range 0 to 255
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Set the color for the light string

	------------------------------------------------------------ */
void SetLightStringColor(unsigned char whichOne, unsigned char red, unsigned char green, unsigned char blue);
/*** EndHeader */
void SetLightStringColor(unsigned char whichOne, unsigned char red, unsigned char green, unsigned char blue)
{
   unsigned char color[3], remainder, i;
   unsigned long tempStep;

   color[0] = green;
   color[1] = red;
   color[2] = blue;

   /*Make sure that the incoming color values get broken up into equal steps to
   be used while fading the color up/down.
   */

	//Let's set some range limits
  // for(i=0; i<3; i++)
   //{
   //change value from 250 to 10
   	//if(color[i] > 0)
      //	color[i] = 255;
     // else if(color[i] > 0 && color[i] < 10)
      //change value from 10 to 255
    //  	color[1] = 10;
    //  else
    //  {
         //Round to the nearest 10
    //     remainder = color[i] % 10;
    //     if( remainder < 5 )
     //    	color[i] -= remainder;
     //    else
     //       color[i] += remainder;
     // }
  // }

   //Now build the step interval
   tempStep = (color[0] / 10); //green
   tempStep <<= 8;

   tempStep |= (color[1] / 10); //red
   tempStep <<= 8;

   tempStep |= (color[2] / 10); //blue

   if(whichOne == 1)
   {
   	g_colorOneStep = tempStep;
      g_colorOneStepNormal = tempStep;
      g_scanFlashTimer = 0;
   }
   else if(whichOne == 2)
   {
      g_colorTwoStep = tempStep;
      g_colorTwoStepNormal = tempStep;
      g_scanFlashTimer = 0;
   }
   else if(whichOne == 3)
   {
      g_currentColor = 0x0000fb00;   //Green
      g_scanFlashTimer = LS_TWO_SECOND_TICKS;
   }
   else
   {
      g_currentColor = 0x00fb0000;   //Red
      g_scanFlashTimer = LS_TWO_SECOND_TICKS;
   }

   g_tickCount = 0;

}


/*** BeginHeader */
#endif // #ifndef _LIGHT_STRING_C_
/*** EndHeader */

