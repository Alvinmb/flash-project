/* --------------------------------------------------------------
	Copyright (C) 2006 Ideal Software Systems. All Rights Reserved.

	Filename:	RFID.c
	By:			Scott Nortman, MindTribe Product Engineering. Modified for
				integration with Skoobe Firmware by Jerry Ryle, MindTribe
				Product Engineering
	Purpose:	Library for communicating with the S4100 Multi-Protocol
				RFID serial reader module.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	02-03-2006	v1.0	<jerry@mindtribe.com>
		Reworked for integration with Skoobe II firmware.
	Update:	01-23-2006	v1.0	<scott@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */

/*** BeginHeader */
#ifndef _RFID_C_
#define _RFID_C_

#ifdef WIN32
#include "RFID.h"
#else
#use "RFID.h"
#endif
/*** EndHeader */

/*** BeginHeader */
/*	-----------------------------------------------------------------------------
	Macro:	RFID_SERIAL_BUFFER_SIZE
	Desc:	This macro determines the size of the input and output serial
			buffers used for the RFID communication.
	Notes:	The max size of 128 bytes is given in the Base Application Protocol
			Reference Guide, Reader Series 4000, available from www.ti-rfid.com.
			However, we currently do not need the max. size. Rabbit code requires
			that the buffer be 2^n -1, so changed to 63.
	---------------------------------------------------------------------------- */
#define RFID_SERIAL_BUFFER_SIZE					( 63 )

/*	-----------------------------------------------------------------------------
	Macros:	RFID_seropen, RFID_serclose, RFID_serread, RFID_serwrite,
			RFID_serrdFlush, RFID_serwrFlush, RFID_serrdUsed, RFID_serwrUsed
	Desc:	These macros map the Dynamic C serial port functions that should be
			used for communicating with the reader.
	Note:	These should be changed if a different serial port is used.
	---------------------------------------------------------------------------- */
#define RFID_seropen	serEopen
#define RFID_serclose	serEclose
#define RFID_serread	serEread
#define RFID_serwrite	serEwrite
#define RFID_serrdFlush	serErdFlush
#define RFID_serwrFlush	serEwrFlush
#define RFID_serrdUsed	serErdUsed
#define RFID_serwrUsed	serEwrUsed

/*	-----------------------------------------------------------------------------
	Macro:	EINBUFSIZE, EOUTBUFSIZE
	Desc:	These macros are used by the included Rabbit / Dynamic C serial port
			libraries.  They determine the size of the serial input/output buffers.
	Note:	These must be defined before serEOpen is called.
	Note:	These should be changed if a different serial port is used.
	---------------------------------------------------------------------------- */
#define EINBUFSIZE		RFID_SERIAL_BUFFER_SIZE
#define EOUTBUFSIZE		RFID_SERIAL_BUFFER_SIZE

/*	--------------------------------------------------------------
	Macro:	SERIAL_RFID_DEFAULT_BAUD_RATE
	Desc:	The macro determines the default baud rate used for
			communicating with the reader.
	------------------------------------------------------------- */
#define RFID_SERIAL_DEFAULT_BAUD_RATE			( 9600 )
/*** EndHeader */

/*** BeginHeader RFID_Startup */
/*	--------------------------------------------------------------
	Function: RFID_Startup
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the RFID reader library and configures the RFID
		reader.
	------------------------------------------------------------ */
extern unsigned char g_RFID_LastScan[RFID_ISO15693_ID_SIZE];
extern unsigned char g_RFID_LastScanDataBlock[RFID_ISO15693_DATABLOCK_SIZE];
extern char g_RFID_State;
extern unsigned long g_RFID_timer;

typedef enum {
	RFS_SENDFINDTOKENREQUEST = 0,
	RFS_WAITFORFINDTOKENPROCESSING,
	RFS_SENDREADSINGLEBLOCKREQUEST,
	RFS_WAITFORREADSINGLEBLOCKPROCESSING,
    RFS_WAITNEXTREQUEST,
	RFS_GOTTOKEN
} rfid_state;

void RFID_Startup(void);
/*** EndHeader */
unsigned char g_RFID_LastScan[RFID_ISO15693_ID_SIZE];
unsigned char g_RFID_LastScanDataBlock[RFID_ISO15693_DATABLOCK_SIZE];
char g_RFID_State;
unsigned long g_RFID_timer;

void RFID_Startup(void)
{
	unsigned char tchar;

	memset(g_RFID_LastScan,0,sizeof(g_RFID_LastScan));
	memset(g_RFID_LastScanDataBlock,0,sizeof(g_RFID_LastScanDataBlock));


	RFID_seropen(RFID_SERIAL_DEFAULT_BAUD_RATE);

	// Set the ISO15693 token to the highest priority
	tchar = RFID_TOKEN_TYPE_15693;
	_RFID_ExecuteComm(RFID_CMD1_APPLICATION_LAYER, RFID_CMD2_TOKEN_PRIORITY, &tchar, 1);

	// Turn on the transmitter for the ISO15693 Layer
	_RFID_ExecuteComm(RFID_CMD1_ISO15693_LAYER, RFID_CMD2_TRANSMITTER_ON, NULL, 0);

	g_RFID_State = RFS_SENDFINDTOKENREQUEST;
}

/*** BeginHeader RFID_Shutdown */
/*	--------------------------------------------------------------
	Function: RFID_Shutdown
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Un-configures the RFID reader and shuts down the RFID library.
		After this function is called, RFID_Startup() may be called
		again.
	------------------------------------------------------------ */
int RFID_Shutdown(void);
/*** EndHeader */
int RFID_Shutdown(void)
{
	RFID_serclose();
}

/*** BeginHeader RFID_Get */
/*	--------------------------------------------------------------
	Function: RFID_Get
	Params:
		readernumber - 1 or 2, which reader to get a barcode from.
		barcode - a character pointer of at least BC_MAXBARCODELEN
			characters long to hold the decoded barcode. This
			string will be null terminated.
		buflen - the length of the barcode buffer, including room
			for a NULL terminator.
		extradata - a character pointer of at least BC_MAXEXTRADATALEN
			characters long to hold the extra barcode data. This
			string will be null terminated.
		edbuflen - the length of the extra data buffer, including room
			for a NULL terminator.
	Returns:
		int - number of barcode characters copied into the
			provided barcode buffer, not including the NULL
            terminator.
	--------------------------------------------------------------
	Purpose:
		Fetches a decoded "barcode" from a RFID reader.
	Notes:
        The RFID library does return "extra data" from the RFID tag.
		Currently only RFID reader 2 is supported, so calling this
		with a "readernumber" of 1 will always fail.
	------------------------------------------------------------ */
int RFID_Get(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen);
/*** EndHeader */
int RFID_Get(int readernumber, unsigned char* barcode, int buflen, unsigned char* extradata, int edbuflen)
{
	int i, j;
	unsigned char tuschar;

	if(readernumber == 2) {
		if(g_RFID_State == RFS_GOTTOKEN) {

            j = 0;
            if(edbuflen > 0) {
                // Copy the Extra Data
                for(i = 0; (i < RFID_ISO15693_DATABLOCK_SIZE) && (j < edbuflen-1); i++) {
                    tuschar = (unsigned char)g_RFID_LastScanDataBlock[i];
                    sprintf(extradata+j,"%02X",tuschar);
                    j+=2;
                }
			    extradata[j] = 0;
            }

            j = 0;
            if(buflen > 0) {
                // Copy the Barcode_Get
			    for(i = 0; (i < RFID_ISO15693_ID_SIZE) && (j < buflen-1); i++) {
                    // The token is stored LSB first, so flip it here.
				    tuschar = (unsigned char)g_RFID_LastScan[(RFID_ISO15693_ID_SIZE-1)-i];
				    sprintf(barcode+j,"%02X",tuschar);
				    j+=2;
			    }
			    barcode[j] = 0;
            }

            // At this point, RFID tag scanning has stopped. Start scanning for RFID tags again.
			g_RFID_State = RFS_SENDFINDTOKENREQUEST;

			return j;
		}
	}
	if(buflen > 0)
		barcode[0] = '\0';
	if(edbuflen > 0)
		extradata[0] = '\0';
	return 0;
}

/*** BeginHeader RFID_Handler */
/*	--------------------------------------------------------------
	Function: RFID_Handler
	Params:
		readernumber - 1 or 2, which reader this should handle.
	Returns:
		BOOL - TRUE when a barcode is sucessfully decoded, FALSE
			otherwise.
	--------------------------------------------------------------
	Purpose:
		Called periodically to run the barcode decoding mechanism
		for RFID readers. Once the return value is TRUE, a
		barcode can be fetched with Barcode_Get().
	Notes:
		Currently only reader 2 is supported, so calling this
		with a "readernumber" of 1 won't do anything.
	------------------------------------------------------------ */
BOOL RFID_Handler(int readernumber);
/*** EndHeader */
BOOL RFID_Handler(int readernumber)
{
	unsigned char tRFID_TxBuffer[RFID_SERIAL_BUFFER_SIZE];
	unsigned char tRFID_RxBuffer[RFID_SERIAL_BUFFER_SIZE];
	unsigned char tTokenData[RFID_ISO15693_ID_SIZE];
    BOOL tsecured;
    char trequestdatabuffer[16];
	unsigned char tdatasize;
	unsigned char tsize;
    int i;

	if(readernumber == 2) {
		switch(g_RFID_State) {
		case RFS_SENDFINDTOKENREQUEST:
			memset(tRFID_TxBuffer,0,RFID_SERIAL_BUFFER_SIZE);
			trequestdatabuffer[0] = 1;
			_RFID_AddTxDataLayer(tRFID_TxBuffer, RFID_CMD1_APPLICATION_LAYER, RFID_CMD2_FIND_TOKEN, trequestdatabuffer, 1 ,&tdatasize);
			_RFID_AddTxLinkLayer(tRFID_TxBuffer, tdatasize, &tsize);
			RFID_serwrFlush();
			RFID_serrdFlush();
			RFID_serwrite(tRFID_TxBuffer, tsize);
			g_RFID_timer = MS_TIMER+RFID_SERIAL_MS_DELAY;
			g_RFID_State = RFS_WAITFORFINDTOKENPROCESSING;
			break;
		case RFS_WAITFORFINDTOKENPROCESSING:
			if(Util_CheckmsTimeout(g_RFID_timer)) {
				// Read the response packet (if any)
				memset(tRFID_RxBuffer, 0, RFID_SERIAL_BUFFER_SIZE);
				RFID_serread(tRFID_RxBuffer, RFID_serrdUsed(), RFID_SERIAL_BYTE_TIMEOUT);
				// Check if packet is okay
				if (RFID_ERROR_NONE == _RFID_ParseRxLinkLayer(tRFID_RxBuffer)) {
					// Check if an error was returned from the reader.
					if(RFID_ERROR_NONE == tRFID_RxBuffer[RFID_RX_PACKET_STATUS_OFFSET]) {
						// If a token was found, get the ID.
						if(RFID_ERROR_NONE == _RFID_GetISO15693TokenID(tRFID_RxBuffer,tTokenData)) {
							// If this wasn't the last token seen, then capture it as a scan.
							if (memcmp(g_RFID_LastScan,tTokenData,RFID_ISO15693_ID_SIZE) != 0) {
								memcpy(g_RFID_LastScan,tTokenData,RFID_ISO15693_ID_SIZE);
                                // Now prepare to get the token's data.
								g_RFID_State = RFS_SENDREADSINGLEBLOCKREQUEST;
                            } else {
                                // Already found this token, start over
    			                g_RFID_timer = MS_TIMER+RFID_SERIAL_MS_RETRY;
                                g_RFID_State = RFS_WAITNEXTREQUEST;
                            }
                            // We found a token, so return.
                            break;
						}
                    }
                    // Token not present. Clear out the last scan.
					memset(g_RFID_LastScan,0,sizeof(g_RFID_LastScan));
				}
    			g_RFID_timer = MS_TIMER+RFID_SERIAL_MS_RETRY;
                g_RFID_State = RFS_WAITNEXTREQUEST;
			}
			break;
        case RFS_SENDREADSINGLEBLOCKREQUEST:
			memset(tRFID_TxBuffer,0,RFID_SERIAL_BUFFER_SIZE);
            trequestdatabuffer[0] = 0; // Not a select message
            trequestdatabuffer[1] = 1; // Return block security status
            trequestdatabuffer[2] = 19; // Block number 19 (20th block)
            memcpy(&trequestdatabuffer[3],g_RFID_LastScan,RFID_ISO15693_ID_SIZE); // the ID of the token to read from.
			_RFID_AddTxDataLayer(tRFID_TxBuffer, RFID_CMD1_ISO15693_LAYER, RFID_CMD2_READ_SINGLE_BLOCK, trequestdatabuffer, 11, &tdatasize);
			_RFID_AddTxLinkLayer(tRFID_TxBuffer, tdatasize, &tsize);
			RFID_serwrFlush();
			RFID_serrdFlush();
			RFID_serwrite(tRFID_TxBuffer, tsize);
			g_RFID_timer = MS_TIMER+RFID_SERIAL_MS_DELAY;
			g_RFID_State = RFS_WAITFORREADSINGLEBLOCKPROCESSING;
            break;
        case RFS_WAITFORREADSINGLEBLOCKPROCESSING:
			if(Util_CheckmsTimeout(g_RFID_timer)) {
				// Read the response packet (if any)
				memset(tRFID_RxBuffer, 0, RFID_SERIAL_BUFFER_SIZE);
				RFID_serread(tRFID_RxBuffer, RFID_serrdUsed(), RFID_SERIAL_BYTE_TIMEOUT);
				// Check if packet is okay.
				if (RFID_ERROR_NONE == _RFID_ParseRxLinkLayer(tRFID_RxBuffer)) {
					// Check if an error was returned from the reader.
					if(RFID_ERROR_NONE == tRFID_RxBuffer[RFID_RX_PACKET_STATUS_OFFSET]) {
						// If data was found, get the data block
						if(RFID_ERROR_NONE == _RFID_GetISO15693DataBlock(tRFID_RxBuffer,&tsecured,tTokenData)) {
							// Capture the data block
							memcpy(g_RFID_LastScanDataBlock,tTokenData,RFID_ISO15693_DATABLOCK_SIZE);
		                    g_RFID_State = RFS_GOTTOKEN;
                            break;
						}
                    }
                    // Token not present or read error. Clear out the last scan to try again from the start.
					memset(g_RFID_LastScan,0,sizeof(g_RFID_LastScan));
				}
    			g_RFID_timer = MS_TIMER+RFID_SERIAL_MS_RETRY;
                g_RFID_State = RFS_WAITNEXTREQUEST;
			}
            break;
        case RFS_WAITNEXTREQUEST:
			if(Util_CheckmsTimeout(g_RFID_timer)) {
                g_RFID_State = RFS_SENDFINDTOKENREQUEST;
            }
            break;
		case RFS_GOTTOKEN:
			return TRUE;
		}
	}
	return FALSE;
}


/*** BeginHeader _RFID_ParseRxLinkLayer */
/*	-----------------------------------------------------------------------------
	Function: _RFID_ParseRxLinkLayer
	Params:
		buffer - pointer to packet to parse.
	Returns:
		unsigned char - an error code indicating that the packet was parsed without
						errors.  RFID_ERROR_NONE is returned for success, and
						RFID_ERROR_UNDEFINED is returned upon failure.  Note that
						the returned value has nothing to do with the STATUS_BYTE
						field in the received data packet.
	-----------------------------------------------------------------------------
	Purpose:
		This function is called to verify the link layer for received data in the
		buffer array.  First, the 0th byte must be a start-of-frame (0x01)
		character, and the next two bytes are the length of the entire packet
		(LSB then MSB).  Then the BCC bytes are calculated on the data and
		compared to the received BCC bytes.  If they match, the received data is
		assumed to be valid.
	---------------------------------------------------------------------------- */
unsigned char _RFID_ParseRxLinkLayer(char* buffer);
/*** EndHeader */
unsigned char _RFID_ParseRxLinkLayer(char* buffer)
{
	unsigned int	Length, index;
	unsigned char	LRC;

	Length 	= 0;
	LRC     = 0;

	if (RFID_PACKET_SOF == buffer[RFID_PACKET_SOF_OFFSET]) {
		// We have a valid SOF, continue to get length of packet
		Length = ((buffer[RFID_PACKET_LENGTH_MSB_OFFSET]<<8) | buffer[RFID_PACKET_LENGTH_LSB_OFFSET] );
		if(Length > RFID_SERIAL_BUFFER_SIZE)
			return RFID_ERROR_UNDEFINED;

		// Calculate BCC values; start w/ LRC (don't include BCC bytes in length)
        for (index = 0; index < Length - 2; index++) {
			LRC ^= buffer[index];
        }

		if( buffer[Length - 2] == LRC )
			if( buffer[Length - 1] == ~LRC )
				return RFID_ERROR_NONE;
	}
	// We don't have valid data, return an error.
	return RFID_ERROR_UNDEFINED;
}

/*** BeginHeader _RFID_AddTxDataLayer */
/*	-----------------------------------------------------------------------------
	Function: _RFID_AddTxDataLayer
	Params:
		buffer   - pointer to packet buffer to modify
		Command1 - value of the CMD1 location in the Tx packet
		Command2 - value of the CMD2 location in the Tx packet
		Data     - pointer to the data that's copied to the packet
		Length   - number of data bytes that are copied
        PacketLength - returns the total size of the packet.
	Returns:
		unsigned char - RFID_ERROR_NONE on success, RFID_ERROR_UNDEFINED on
						failure
	-----------------------------------------------------------------------------
	Purpose:
		This function is called to set up the Data Layer of the Tx packet which
		may be sent to the RFID module. The RFID_Init function must be called
		prior to calling this function.
	----------------------------------------------------------------------------*/
unsigned char _RFID_AddTxDataLayer(unsigned char* buffer, unsigned char Command1, unsigned char Command2, unsigned char *Data, unsigned char Length,  unsigned char *PacketLength);
/*** EndHeader */
unsigned char _RFID_AddTxDataLayer(unsigned char* buffer, unsigned char Command1, unsigned char Command2, unsigned char *Data, unsigned char Length,  unsigned char *PacketLength)
{
	memset(buffer, 0, RFID_SERIAL_BUFFER_SIZE );

	buffer[RFID_PACKET_CMD1_OFFSET] = Command1;
	buffer[RFID_PACKET_CMD2_OFFSET] = Command2;

	if (Length <= RFID_DATA_MAX_BYTES) {
		memcpy(&buffer[RFID_TX_PACKET_DATA_OFFSET], Data, Length);
        *PacketLength = 2+Length;
		return RFID_ERROR_NONE;
	}
	return RFID_ERROR_UNDEFINED;
}

/*** BeginHeader _RFID_AddTxLinkLayer */
/*	-----------------------------------------------------------------------------
	Function: _RFID_AddTxLinkLayer
	Params:
		buffer - pointer to packet buffer to modify.
		DataLayerLength	- number of valid bytes in Data Layer of the the RFID
			packet including the CMD1 field up to (but not including) the BCC
			field.
		PacketLength	- a pointer which gets the total size of the packet
			(includes all of the link layer data).
	Returns:
		unsigned char - indicating if the RFID Link Layer was successfully added
			to the buffer. RFID_ERROR_NONE is returned on success,
			RFID_ERROR_UNDEFINED is returned on failure.
	----------------------------------------------------------------------------
	Purpose:
		This function adds the appropriate link layer values to the packet
		buffer. This should be called after RFID_AddTxDataLayer and before
		sending the Tx packet to the RFID module.
	---------------------------------------------------------------------------- */
unsigned char _RFID_AddTxLinkLayer(unsigned char* buffer, unsigned char DataLayerLength, unsigned char *PacketLength);
/*** EndHeader */
unsigned char _RFID_AddTxLinkLayer(unsigned char* buffer, unsigned char DataLayerLength, unsigned char *PacketLength)
{
	unsigned char index;
	unsigned char LRC;

	LRC = 0;

	//Set constant values in tx buffer
	buffer[RFID_PACKET_SOF_OFFSET]			= RFID_PACKET_SOF;			//SOF
	buffer[RFID_PACKET_DEVICE_ID_OFFSET]	= RFID_PACKET_DEVICE_ID;	//ID

	//Set length values
	buffer[RFID_PACKET_LENGTH_LSB_OFFSET] = ((DataLayerLength+6)&0x00FF);
	buffer[RFID_PACKET_LENGTH_MSB_OFFSET] = ((DataLayerLength+6)>>8);

	//Loop through packet to calculate LRC sum from data
	for( index = 0; index < DataLayerLength + 4; index++ ){
		LRC ^= buffer[index];
	}
	buffer[DataLayerLength+4] = LRC;
	buffer[DataLayerLength+5] = ~LRC;

	//See if we need to set length
	if( PacketLength )
		*PacketLength = DataLayerLength+6;
	return RFID_ERROR_NONE;
}

/*** BeginHeader _RFID_ExecuteComm */
/*	-----------------------------------------------------------------------------
	Function: _RFID_ExecuteComm
	Params:
		Cmd1 - CMD1 value for the TxBuffer serial packet
		Cmd2 - CMD2 value for the TxBuffer serial packet
		Data - pointer to the data needed for the commands.
		DataSize - number of data bytes to copy.
	Returns:
		unsigned char - RFID_ERROR_UNDEFINED upon failure, otherwise a valid
			error code from the received data link layer.
	-----------------------------------------------------------------------------
	Purpose:
		This function is called to assemble and transmit a valid serial packet to
		the RFID module. The response packet is received, and the link layer is
		verified. If the link layer is OK, the error code from the data layer is
		returned.
	---------------------------------------------------------------------------- */
unsigned char _RFID_ExecuteComm(unsigned char Cmd1, unsigned char Cmd2, unsigned char *Data, unsigned char DataSize);
/*** EndHeader */
unsigned char _RFID_ExecuteComm(unsigned char Cmd1, unsigned char Cmd2, unsigned char *Data, unsigned char DataSize)
{
	unsigned char tdatasize;
	unsigned char tsize;
	char tRFID_TxBuffer[RFID_SERIAL_BUFFER_SIZE];
	char tRFID_RxBuffer[RFID_SERIAL_BUFFER_SIZE];

	// Add data layer to TxBuffer
	_RFID_AddTxDataLayer(tRFID_TxBuffer, Cmd1, Cmd2, Data, DataSize, &tdatasize);

	// Add TxLinkLayer
	_RFID_AddTxLinkLayer(tRFID_TxBuffer, tdatasize, &tsize );

	// Flush the serial port E buffers
	RFID_serwrFlush();
	RFID_serrdFlush();

	// Send the packet
	RFID_serwrite(tRFID_TxBuffer, tsize);

	// Delay while RFID parses packet
	Util_Delay(RFID_SERIAL_MS_DELAY);

	// Grab response packet (if any) from serial port
	memset(tRFID_RxBuffer, 0, RFID_SERIAL_BUFFER_SIZE);
	RFID_serread(tRFID_RxBuffer, RFID_serrdUsed(), RFID_SERIAL_BYTE_TIMEOUT);

	//Verify data link layer
	if (_RFID_ParseRxLinkLayer(tRFID_RxBuffer) == RFID_ERROR_NONE) {
		//Link layer is OK, return data layer error command from RFID unit
		return tRFID_RxBuffer[RFID_RX_PACKET_STATUS_OFFSET];
	}
	return( RFID_ERROR_UNDEFINED );
}

/*** BeginHeader _RFID_GetISO15693TokenID */
/*	-----------------------------------------------------------------------------
	Function: _RFID_GetISO15693TokenID
	Params:
		buffer - pointer to a packet buffer to process.
		tokendata - pointer to a data buffer which will hold unique ID data from the token.
	Returns:
		unsigned char - RFID_ERROR_NONE for success, or RFID_ERROR_TOKEN_NOT_PRESENT
			or RFID_ERROR_UNDEFINED upon failure.
	----------------------------------------------------------------------------
	Purpose:
		The RFID_RX_PACKET_STATUS_OFFSET status byte is checked for the presence
		of a valid token.  If the status byte is RFID_ERROR_NONE, the unique ID
		of the token is assumed to be in buffer locations 10 through 17, and the
		contents are copied to the location pointed to by Buffer. If the status
		byte is RFID_ERROR_TOKEN_NOT_PRESENT, then that error code is returned
		and nothing is copied.  Otherwise, RFID_ERROR_UNDEFINED is returned.
	Note:
		tokendata must be RFID_ISO15693_ID_SIZE bytes long.
	---------------------------------------------------------------------------- */
unsigned char _RFID_GetISO15693TokenID(unsigned char *buffer, unsigned char *tokendata);
/*** EndHeader */
unsigned char _RFID_GetISO15693TokenID(unsigned char *buffer, unsigned char *tokendata)
{
	int i;

	if ( RFID_ERROR_NONE == buffer[RFID_RX_PACKET_STATUS_OFFSET]) {
		// Copy the token ID
		for(i = 0; i < RFID_ISO15693_ID_SIZE; i++)
			tokendata[i] = buffer[RFID_RX_PACKET_ISO15693_ID_OFFSET+i];
		return RFID_ERROR_NONE;

	} else if( RFID_ERROR_TOKEN_NOT_PRESENT == buffer[RFID_RX_PACKET_STATUS_OFFSET] )
		return RFID_ERROR_TOKEN_NOT_PRESENT;

	return RFID_ERROR_UNDEFINED;
}

/*** BeginHeader _RFID_GetISO15693DataBlock */
/*	-----------------------------------------------------------------------------
	Function: _RFID_GetISO15693DataBlock
	Params:
		buffer - pointer to a packet buffer to process.
        secured - pointer to bool buffer to hold whether or not block is secured.
		tokendata - pointer to a data buffer which will hold data block from the token.
	Returns:
		unsigned char - RFID_ERROR_NONE for success, or RFID_ERROR_TOKEN_NOT_PRESENT
			or RFID_ERROR_UNDEFINED upon failure.
	----------------------------------------------------------------------------
	Purpose:
		The RFID_RX_PACKET_STATUS_OFFSET status byte is checked for the presence
		of a valid token.  If the status byte is RFID_ERROR_NONE, the unique ID
		of the token is assumed to be in buffer locations 9 through 12, and the
		contents are copied to the location pointed to by Buffer. If the status
		byte is RFID_ERROR_TOKEN_NOT_PRESENT, then that error code is returned
		and nothing is copied.  Otherwise, RFID_ERROR_UNDEFINED is returned.
	Note:
		tokendata must be RFID_ISO15693_DATABLOCK_SIZE bytes long.
	---------------------------------------------------------------------------- */
unsigned char _RFID_GetISO15693DataBlock(unsigned char *buffer, BOOL* secured, unsigned char *tokendata);
/*** EndHeader */
unsigned char _RFID_GetISO15693DataBlock(unsigned char *buffer, BOOL* secured,  unsigned char *tokendata)
{
	int i;

	if ( RFID_ERROR_NONE == buffer[RFID_RX_PACKET_STATUS_OFFSET]) {
		// Copy the token data block
        *secured = (buffer[RFID_RX_PACKET_ISO15693_DATABLOCK_OFFSET-1] != 0);
        for(i = 0; i < RFID_ISO15693_DATABLOCK_SIZE; i++) {
			tokendata[i] = buffer[RFID_RX_PACKET_ISO15693_DATABLOCK_OFFSET+i];
        }
		return RFID_ERROR_NONE;

	} else if( RFID_ERROR_TOKEN_NOT_PRESENT == buffer[RFID_RX_PACKET_STATUS_OFFSET] )
		return RFID_ERROR_TOKEN_NOT_PRESENT;

	return RFID_ERROR_UNDEFINED;
}

/*** BeginHeader */
#endif //#ifndef _RFID_C_
/*** EndHeader */