/* --------------------------------------------------------------
	Copyright (C) 2011 Ideal Software Systems. All Rights Reserved.

	Filename:	VideoScreen.c
	By:			Rob Toth
	Purpose:	   Run serially connected LCD video screen.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	09-12-2011	v1.0
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _VIDEO_SCREEN_C_
#define _VIDEO_SCREEN_C_

/*** EndHeader */

/*** BeginHeader VideoScreen_Init */
/*	--------------------------------------------------------------
	Function: VideoScreen_Init
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose:
		Initializes the Video Screen library.
	------------------------------------------------------------ */
void VideoScreen_Init(void);


#define VS_SET_BIT_HIGH		1
#define VS_SET_BIT_LOW  	0


extern unsigned short g_videoCount;

/*** EndHeader */

unsigned short g_videoCount;


void VideoScreen_Init(void)
{
   serEopen(115200);
	serEdatabits(PARAM_8BIT);

   DebugPrint(STRACE_NFO,("Video Screen Test harness on connector P4"));
   DebugPrint(STRACE_NFO,("Video Screen - RS232 Started."));
   
}

/*** BeginHeader HandleVideoScreen */
/*	--------------------------------------------------------------
	Function: HandleVideoScreen
	Params:
		void
	Returns:
		void
	--------------------------------------------------------------
	Purpose: Control the Video Screen

	------------------------------------------------------------ */
void HandleVideoScreen(void);
/*** EndHeader */
void HandleVideoScreen(void)
{


}




/*** BeginHeader */
#endif // #ifndef _VIDEO_SCREEN_C_
/*** EndHeader */

