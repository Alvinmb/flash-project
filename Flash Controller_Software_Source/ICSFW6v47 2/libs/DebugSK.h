/* --------------------------------------------------------------
	Copyright (C) 2005 Ideal Software Systems. All Rights Reserved.

	Filename:	DebugSK.h
	By:			Jerry Ryle, MindTribe Product Engineering
	Purpose:	Skoobe Debugging configuration and functions.
	Target(s):  Rabbit 3000

	Revisions
	----------
	Update:	05-25-2005	v1.0	<jerry@mindtribe.com>
		Initial Revision.

   ------------------------------------------------------------- */
/*** BeginHeader */
#ifndef _DEBUGSK_H_
#define _DEBUGSK_H_
/*** EndHeader */

/*** BeginHeader */
// Debug tracing levels. Used in DebugPrint() calls to indicate severity of event.
#define STRACE_DBG	3	// Trivial or verbose informational messages.
#define STRACE_NFO	2	// Important informational messages
#define STRACE_ERR	1	// Critical Error messsages
#define STRACE_NONE 0	// No Debug Tracing. (Don't use this in a DebugPrint() call)

#ifndef DEBUG
#nodebug		// Don't generate debug instructions.
#endif

#ifndef DEBUG_PRINTS

#define DebugPrint(_t_, _x_)	{}

#else //#ifndef DEBUG_PRINTS

// This sets debug output to go to the serial port.
#if (_TARGETLESS_COMPILE_ == 1)
#define	STDIO_DEBUG_SERIAL	SADR
#define	STDIO_DEBUG_BAUD	115200
#define	STDIO_DEBUG_FORCEDSERIAL
#define	STDIO_DEBUG_ADDCR
#endif //#if _TARGETLESS_COMPILE_ == 1

#define DebugPrint(_t_, _x_) \
	{if (SKOOBE_DEBUGTRACELEVEL >= (_t_)) {  \
		if((_t_) == STRACE_DBG) printf("DBG: "); else if((_t_) == STRACE_NFO) printf("NFO: "); else printf("ERR: "); \
		printf _x_ ; \
		printf("\n"); \
	}}

#endif //#ifndef DEBUG_PRINTS

/*** EndHeader */


/*** BeginHeader */
#endif // #ifndef _DEBUGSK_H_
/*** EndHeader */